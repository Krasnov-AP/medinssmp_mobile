﻿unit Controller.AnyData;

interface

 uses
   System.Classes, System.SysUtils, System.Generics.Collections,
   Core.Common, Controller.Http, Model.AnyData
   {$ifdef CACHE_SUPPORT}
     ,FireDac.Comp.Client,
     Data.Main
   {$endif};

 type

  TEntityType = (etEntity, etEntityList);
  TRequestMethod = (rmGET, rmPOST);

  TRequestParams = class
    strict private
     FFilter:TStrings;
     FCached: boolean;
     FEntityType: TEntityType;
     FEntityId:Integer;
    public
     property Filter:TStrings read FFilter write FFilter;
     property Cached:boolean read FCached write FCached;
     property EntityType:TEntityType read FEntityType write FEntityType;
     property EntityId:integer read FEntityId write FEntityId;
     constructor Create;
     destructor Destroy; override;
  end;

  TControllerAnyData = class (TBaseController)
   strict private
    FHttpRequest:THttpRequest;
    FData:TModelAnyData;
    FEndpoint:string;
    FKind:string;
    FUrl:string;
    FHost:string;
    FPort:Word;
    FEntityName:string;
    FEntityListName:string;
    FOnRequestSuccess: TControllerEvent;
    FOnRequestFail: TControllerEvent;
    FOnRequestSuccess2: TControllerEvent2;
    FOnRequestFail2: TControllerEvent2;
    FStatusCode: integer;
    {$ifdef CACHE_SUPPORT}
      FDbConnection: TFDConnection;
    {$endif}
   strict private
    procedure RequestCompleted;

   public
    property HttpRequest:THttpRequest read FHttpRequest write FHttpRequest;
    property Data:TModelAnyData read FData write FData;
    property Url:string read FUrl write FUrl;
    property Host:string read FHost write FHost;
    property Port:Word read FPort write FPort default 54002;
    property StatusCode:integer read FStatusCode;
    property OnRequestSuccess:TControllerEvent read FOnRequestSuccess write FOnRequestSuccess;
    property OnRequestFail:TControllerEvent read FOnRequestFail write FOnRequestFail;
    property OnRequestSuccess2:TControllerEvent2 read FOnRequestSuccess2 write FOnRequestSuccess2;
    property OnRequestFail2:TControllerEvent2 read FOnRequestFail2 write FOnRequestFail2;

     {$ifdef CACHE_SUPPORT}
      property DbConnection: TFDConnection read FDBConnection;
     {$endif}

    function MakeRequestBody:TStringStream; virtual;

    procedure InsertData(AFilter:TStrings = nil); virtual;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); virtual;

    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TControllerAnyData }

constructor TControllerAnyData.Create;
begin
  //todo
  inherited Create;
  self.FHttpRequest := THttpRequest.Create;
  self.FData := TModelAnyData.Create;

  {$ifdef CACHE_SUPPORT}
    if DataMain.FDConnectionMain.Connected then
     try
      self.FDbConnection := TFDConnection.Create(nil);
      self.FDbConnection.Params.Values['Database'] := DataMain.FDConnectionMain.Params.Values['Database'];
      self.FDbConnection.DriverName := 'SQLite';
      self.FDbConnection.Connected := true;
     except on E:Exception do
      TLogger.Send('[TControllerAnyData.Create] error: '+e.Message, TLogLevel.Error);
     end;
   {$endif}

end;

destructor TControllerAnyData.Destroy;
begin
 SafeFree(self.FHttpRequest);
 //SafeFree(self.FData);
 //self.FHttpRequest.Free;
 SafeFree(self.FData);
 inherited;
end;

procedure TControllerAnyData.InsertData(AFilter:TStrings = nil);
var
 LBody:TStringStream;

 {$ifdef CACHE_SUPPORT}
  LFDQuery:TFDQuery;
  LSQL: string;
 {$endif}

begin
 LBody := self.MakeRequestBody;

 self.IsBusy := true;
 if not Assigned(self.FHttpRequest) then self.FHttpRequest := THttpRequest.Create;

 self.FHttpRequest.OnRequestCompleted := self.RequestCompleted;
 self.IsSuccess := false;

 {$ifdef CACHE_SUPPORT}
   if Assigned(self.FDbConnection) then
    if self.FDbConnection.Connected then
     begin
      LFDQuery := TFDQuery.Create(nil);
       try
          LFDQuery.Connection := self.FDbConnection;

          LSql := 'insert into entities (entity_content, status, entity_name, endpoint, method, kind, sync_date)' +
                                      ' values ' +
                                       '(:entity_content, :status, :entity_name, :endpoint, :method, :kind, current_timestamp)';


          LFDQuery.SQL.Text := LSQL;

          LFDQuery.ParamByName('entity_content').AsString := self.FData.AsJSON;
          LFDQuery.ParamByName('status').AsInteger := 2;
          LFDQuery.ParamByName('entity_name').AsString := self.ClassName;
          LFDQuery.ParamByName('endpoint').AsString := self.FUrl;
          LFDQuery.ParamByName('method').AsString := 'POST';
          LFDQuery.ParamByName('kind').AsString := '';

          LFDQuery.Connection.StartTransaction;

            try
              LFDQuery.ExecSQL;
              LFDQuery.Connection.Commit;
            except on E:Exception do
             begin
              LFDQuery.Connection.Rollback;
              TLogger.Send('[TControllerAnyData.InsertData] insert into db error: '+e.Message, TLogLevel.Error);
             end;
            end;

           LFDQuery.Close;

      finally
       LFDQuery.Free;
      end;
     end;
  {$endif}

  self.FHttpRequest.PostAsync(self.Url, LBody, true, true, 'application/json');

end;

function TControllerAnyData.MakeRequestBody: TStringStream;
var
 ts:TStrings;
begin
 result := TStringStream.Create;
 ts := TStringList.Create;

 try
  ts.Text := self.Data.GetDataString;

  result := TStringStream.Create('', TEncoding.UTF8);
  result.WriteString(ts.Text);
  result.Seek(0, TSeekOrigin.soBeginning);
 finally
  ts.Free;
 end;

end;

procedure TControllerAnyData.RequestCompleted;
begin
 //todo
 self.IsBusy := false;
 self.FStatusCode := self.FHttpRequest.StatusCode;
 if (self.FHttpRequest.StatusCode<>200) and (self.FHttpRequest.StatusCode<>201) then
  begin
    self.IsSuccess := false;
    if Assigned(self.FOnRequestFail) then self.FOnRequestFail
     else
      if Assigned(self.FOnRequestFail2) then self.FOnRequestFail2(self);
  end
   else
  begin
    self.IsSuccess := true;
    self.FData.SetDataString(self.FHttpRequest.Response);
    if Assigned(self.FOnRequestSuccess) then self.FOnRequestSuccess
     else
      if Assigned(self.FOnRequestSuccess2) then
       begin
        //TLogger.Save('FOnRequestSuccess2 start!');
        self.FOnRequestSuccess2(self);
       end;
  end;
end;

procedure TControllerAnyData.RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false);
var
 i:integer;
 {$ifdef CACHE_SUPPORT}
   LSQL: string;
   LFDQuery: TFDQuery;
 {$endif}
 LBody:TStringStream;
begin
   self.IsBusy := true;
   if not Assigned(self.FHttpRequest) then self.FHttpRequest := THttpRequest.Create;

   self.FHttpRequest.OnRequestCompleted := self.RequestCompleted;
   self.IsSuccess := false;

   case AMethod  of
     rmGET: self.FHttpRequest.GetAsync(self.Url, true, true, '');
     rmPOST:
      begin
       LBody := self.MakeRequestBody;
       self.FHttpRequest.PostAsync(self.Url, LBody, true, true, '');
      end;
   end;

 {$ifdef CACHE_SUPPORT}
 if ACached then
  begin
   if Assigned(self.FDbConnection) then
    if self.FDbConnection.Connected then
     begin
      LFDQuery := TFDQuery.Create(nil);
       try
          LFDQuery.Connection := self.FDbConnection;

          LSql := 'select * from entities where entity_name = "'+self.ClassName+'" order by id';

          LFDQuery.SQL.Text := LSQL;

          LFDQuery.Connection.StartTransaction;
          i:=0;

            try
              LFDQuery.Open;
                while not LFDQuery.Eof do
                 begin

                  self.Data.FromJson(LFDQuery.FieldByName('entity_content').AsString);
                  inc(i);

                  LFDQuery.Next;

                 end;

              LFDQuery.Connection.Commit;
            except on E:Exception do
             begin
              LFDQuery.Connection.Rollback;
              TLogger.Send('[TControllerAnyData.RetrieveData] Retrieve from db error: '+e.Message, TLogLevel.Error);
             end;
            end;

           LFDQuery.Close;

      finally
       LFDQuery.Free;
      end;
    end;

 end; //ACached = true
 {$endif}

end;

{ TRequestParams }

constructor TRequestParams.Create;
begin
 self.FEntityType := TEntityType.etEntity;
 self.FFilter := TStringList.Create;
 self.FEntityId := -1;
end;

destructor TRequestParams.Destroy;
begin
 SafeFree(self.FFilter);
 inherited;
end;

end.
