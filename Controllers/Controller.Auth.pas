﻿unit Controller.Auth;

interface

 uses
  System.Classes, System.SysUtils, System.Generics.Collections,
   Core.Common, Controller.Http, Model.AnyData, Controller.AnyData, Model.Global,
   Model.User, System.Hash;

  type
   TControllerAuth = class(TControllerAnyData)
   strict private
     const
      URL_POST = 'http://#host#:#port#/?command=userLogin&connectionUserId=0&login=#login#&pwrd=#password#';
      URL_GET = 'http://#host#:#port#/?command=userLogin&connectionUserId=0&login=#login#&pwrd=#password#';
   strict private
     FUser:TUser;
   public
     property User:TUser read FUser write FUser;
     procedure InsertData(AFilter:TStrings = nil); override;
     procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
     constructor Create;
     destructor Destroy; override;
   end;

implementation

constructor TControllerAuth.Create;
begin
  inherited;
  self.FUser := TUser.Create;
end;

destructor TControllerAuth.Destroy;
begin
  SafeFree(self.FUser);
  inherited;
end;

procedure TControllerAuth.InsertData;
begin
   try
    self.Data.SetDataString(FUser.ConvertToJson);
    self.url := URL_POST;
    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerAuth.InsertData] err: '+e.Message, TLogLevel.Error);
   end;
end;

procedure TControllerAuth.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
begin
   try
    if self.Port = 0 then self.Port := 54002;

    self.url := self.URL_GET.Replace('#host#', self.Host)
                .Replace('#port#', self.Port.ToString)
                .Replace('#login#', self.User.Name)
                .Replace('#password#', UpperCase(System.Hash.THashMD5.GetHashString(self.User.Password)));
    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerAuth.InsertData] err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
