﻿unit Controller.Cars;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.SMPService, Model.Global,
 Model.Cars, Core.Funcs;

type

 TControllerCars = class(TControllerAnyData)
   strict private
     const
      URL_GET = 'http://#host#:#port#/?' +
      'command=getTableDataWithCondition&tableName=cars&condition="IsDeleted"=0#filter#&DBType=0&userLogin=#userLogin#';  //filter =  and "Id" in (411,1209)
      URL_UPDATE = 'http://#host#:#port#/?' +
      'command=#command#&connectionUserId=#connectionUserId#&userLogin=#userLogin#&carId=#carId#&orderId=#orderId#';
   strict private
    FCars: TCars;
    FCurrentOrderId: integer;
   public
    property Cars: TCars read FCars write FCars;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;

implementation

{ TControllerOrders }

constructor TControllerCars.Create;
begin
 inherited;
 self.FCars := TCars.Create;
end;

destructor TControllerCars.Destroy;
begin
  SafeFree(self.FCars);
  inherited;
end;

procedure TControllerCars.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
var
 LFilterStr: string;
 i: integer;
 LKeyValue: TKeyValue;
 LIsUpdate: boolean;
begin
  try
    if self.Port = 0 then self.Port := 54002;

    LIsUpdate := not (AFilter = nil);
    if LIsUpdate then
      LIsUpdate := AFilter.IndexOfName('command') > -1;

    if not LIsUpdate then
     begin
      self.url := self.URL_GET.Replace('#host#', self.Host)
                  .Replace('#port#', self.Port.ToString)
                  .Replace('#userLogin#', Model.Global.ModelDataGlobal.CurrentUser.Name);
     end
      else
     begin
      self.Url := self.URL_UPDATE;
      self.url := self.url.Replace('#host#', self.Host)
                  .Replace('#port#', self.Port.ToString);

      for i:=0 to AFilter.Count - 1 do
        begin
         LKeyValue := ExtractKeyValue(AFilter[i]);
         self.url := self.Url.Replace('#'+LKeyValue.Key+'#', LKeyValue.Value);
        end;
     end;

    if AFilter <> nil then
     begin
      LFilterStr := AFilter.Text;
      self.url := self.Url.Replace('#filter#', LFilterStr.Replace(sLineBreak, ''));
     end
      else
       self.url := self.Url.Replace('#filter#', '');

    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerCars.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
