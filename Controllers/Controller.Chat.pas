﻿unit Controller.Chat;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.SMPService, Model.Global,
 Model.Chat, Core.Funcs;

 type

  TControllerChat = class(TControllerAnyData)
   strict private
     const
      URL_INSERT = 'http://#host#:#port#' +
      '/?command=insertData&tableName=chat&#filter#';
      URL_GET = 'http://#host#:#port#' +
      '/?command=getTableDataWithCondition&tableName=get_chat&condition="Id"+=+#chat_id#';
   strict private
    FChat: TChat;
   public
    property Chat: TChat read FChat write FChat;
    constructor Create;
    destructor Destroy; override;
    procedure InsertData(AFilter:TStrings = nil); override;
    procedure SendChatMessage(ASkCode:string = '');
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;

 TControllerChats = class(TControllerAnyData)
   strict private
     const
      URL_GET = 'http://#host#:#port#' +
      '/?command=getTableDataWithCondition&tableName=chat&condition='+
      '("IsDeleted"=0) and ("DtCreate">=(CURRENT_DATE-7))&'+
      'DBType=0&userLogin=#userLogin#&UseZip=0';
   strict private
    FChats: TChats;
    FCurrentOrderId: integer;
   public
    property Chats: TChats read FChats write FChats;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;

implementation

{ TControllerOrder }

constructor TControllerChat.Create;
begin
  inherited;
  self.FChat := TChat.Create;

end;

destructor TControllerChat.Destroy;
begin

  inherited;
end;

procedure TControllerChat.InsertData(AFilter: TStrings);
begin
   try
    if self.Port = 0 then self.Port := 54002;
    self.Data.SetDataString(FChat.ConvertToJson);
    TLogger.Save('[TControllerChat.InsertData] after convert to json ');

    if (AFilter = nil) or (not Assigned(AFilter)) then
      begin
       self.url := self.URL_INSERT.Replace('#host#', self.Host)
                             .Replace('#port#', self.Port.ToString)
                             .Replace('#filter#', '');
      end
       else
      begin
       AFilter.Delimiter := '&';
       self.url := self.URL_INSERT.Replace('#host#', self.Host)
                             .Replace('#port#', self.Port.ToString)
                             .Replace('#filter#', AFilter.DelimitedText);

      end;

    TLogger.Save('[TControllerChat.InsertData] before inherited '+self.Url);
    inherited;
   except on E:Exception do
    begin
     if Assigned(self.OnRequestFail) then self.OnRequestFail;
     TLogger.Save('[TControllerChat.InsertData] err: '+e.Message, TLogLevel.Error);
    end;
   end;

end;

procedure TControllerChat.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
var
 LFilterStr: string;
 i: integer;
 LKeyValue: TKeyValue;
 LIsUpdate: boolean;
begin
  try
    if self.Port = 0 then self.Port := 54002;

    self.url := self.URL_GET.Replace('#host#', self.Host)
                  .Replace('#port#', self.Port.ToString);

    if AFilter <> nil then
     self.Url := self.Url.Replace('#chat_id#', AFilter.Values['chat_id']);

    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerCars.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;


procedure TControllerChat.SendChatMessage(ASkCode:string = '');
var
 LFilter: TStrings;
begin
 try

   LFilter := TStringList.Create;

   if ASkCode.Trim <> '' then
    begin
     LFilter.Add('SKCode='+ASkCode);
    end;

   self.InsertData(LFilter);
 finally
  SafeFree(LFilter);
 end;
end;

{ TControllerChats }

constructor TControllerChats.Create;
begin
 inherited;
 self.Chats := TChats.Create;
end;

destructor TControllerChats.Destroy;
begin
  SafeFree(self.Chats.Items);
  inherited;
end;

procedure TControllerChats.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
var
 LFilterStr: string;
begin
  try
    if self.Port = 0 then self.Port := 54002;

    self.url := self.URL_GET.Replace('#host#', self.Host)
                .Replace('#port#', self.Port.ToString)
                .Replace('#userLogin#', Model.Global.ModelDataGlobal.CurrentUser.Name);

    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerChats.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
