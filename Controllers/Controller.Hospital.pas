﻿unit Controller.Hospital;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.Global,
 Core.Funcs, Model.Hospital;

type

 TControllerHospitals = class(TControllerAnyData)
   strict private
     const
      URL_GET = 'http://#host#:#port#/?' +
      'command=getTableDataWithCondition&tableName=handbook_hospitals&condition="IsDeleted"=0&DBType=0&userLogin=#userLogin#';  //filter =  and "Id" in (411,1209)
   strict private
    FHospitals: THospitals;
    FCurrentOrderId: integer;
   public
    property Hospitals: THospitals read FHospitals write FHospitals;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;


implementation

{ TControllerHospitals }

constructor TControllerHospitals.Create;
begin
  inherited;
  self.FHospitals := THospitals.Create;
end;

destructor TControllerHospitals.Destroy;
begin

  inherited;
end;

procedure TControllerHospitals.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
var
 LFilterStr: string;
 i: integer;
 LKeyValue: TKeyValue;
 LIsUpdate: boolean;
begin
  try
    if self.Port = 0 then self.Port := 54002;

    self.url := self.URL_GET.Replace('#host#', self.Host)
                  .Replace('#port#', self.Port.ToString)
                  .Replace('#userLogin#', Model.Global.ModelDataGlobal.CurrentUser.Name);

    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerHospitals.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
