﻿/// <summary>
///   Модуль обмена данными с HTTP(S)-сервером
/// </summary>
/// <remarks>
///   <list type="table">
///     <listheader>
///       <term>Параметр</term>
///       <description>Значение</description>
///     </listheader>
///     <item>
///       <term>Версия приложения</term>
///       <description>0.1</description>
///     </item>
///     <item>
///       <term>Дата последнего изменения</term>
///       <description>10.Oct.2021</description>
///     </item>
///     <item>
///       <term>Поддерживаемые платформы разработки</term>
///       <description>Embarcadero Delphi 10.4 Sydney Update 2 и выше</description>
///     </item>
///     <item>
///       <term>Поддерживаемые ОС</term>
///       <description>
///         <p>Windows 7 (SP1+)/8.1/10 (x32/x64)</p>
///         <p>Windows Server 2012 R2/2016/2019 (x32/x64)</p>
///         <p>macOS 10.13/10.14/10.15/11.1 (x64)</p>
///       </description>
///     </item>
///   </list>
/// </remarks>
unit Controller.Http;

interface

uses
  System.Types, System.Classes, System.SysUtils, System.SyncObjs, System.StrUtils, System.Threading,
  System.Net.URLClient, System.Net.HttpClient, System.Net.HttpClientComponent, System.Net.Mime, System.NetEncoding,
  FMX.Types,
  {$IFDEF MSWINDOWS}
   Winapi.Windows,
  {$ENDIF}
  Core.Common
   {$ifdef INDY_USAGE}
   , IdCoderMIME, IdGlobal, IdCoder,
   IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
   IdHTTP
   {$endif}
  ;

const

 CONNECTION_TIMEOUT = 5000;
 RESPONSE_TIMEOUT = 5000;

type

THttpRequestEvent = procedure of object;
TControllerEvent = procedure of object;
TControllerEvent2 = procedure (Sender: TObject) of object;

THttpRequest = class
  strict private
    /// <summary>
    ///   Json-дерево ответа наш запрос (или вообще какая-то строка, не обязательно json)
    /// </summary>
   [volatile] FResponse: string;
   FSession: TNetHTTPClient;
   {$ifdef INDY_USAGE}
    FIndySession: TIdHTTP;
   {$endif}
   FOnRequestCompleted: THttpRequestEvent;
   [volatile] FStatusCode:integer;
   FCritical: System.SyncObjs.TCriticalSection;

   property Session: TNetHTTPClient read FSession write FSession;
   {$ifdef INDY_USAGE}
    property IndySession: TIdHTTP read FIndySession write FIndySession;
   {$endif}

   procedure MakeSessionRequestHeaders(const AIsWithAuth: Boolean);
   procedure ApplySessionHeaders(const ASource: IHTTPResponse);

   procedure SetResponse(AValue:string);
   function GetResponse:string;

   procedure SetStatusCode(AValue:integer);
   function GetStatusCode:integer;


   public
    property OnRequestCompleted:THttpRequestEvent read FOnRequestCompleted write FOnRequestCompleted;
    /// <summary>
    ///   Сессия соединения с HTTP(S)-сервером
    /// </summary>

    property Response: string read GetResponse; // write SetResponse;
    property StatusCode: integer read GetStatusCode; // write SetStatusCode;

    function Get(const AUrl: string; const AIsWithAuth, AIsURLEncode: Boolean; const AContentType: string;
                              out AStatusCode: Integer): string;

    procedure GetAsync(const AUrl: string; const AIsWithAuth, AIsURLEncode: Boolean; const AContentType: string);

    function Post(const AUrl: string; const ASource: TStream; const AIsWithAuth, AIsURLEncode: Boolean;
                               const AContentType: string; out AStatusCode: Integer): string;

    procedure PostAsync(const AUrl: string; const ASource: TStream; const AIsWithAuth, AIsURLEncode: Boolean;
                        const AContentType: string);

    function Put(const AUrl: string; const ASource: TStream; const AIsWithAuth, AIsURLEncode: Boolean;
                const AContentType: string; out AStatusCode: Integer): string;

    procedure PutAsync(const AUrl: string; const ASource: TStream;
                const AIsWithAuth, AIsURLEncode: Boolean; const AContentType: string);


    constructor Create;
    destructor Destroy; override;
end;

TBaseController = class
  strict private
   [volatile] FIsSuccess:boolean;
   [volatile] FIsBusy:boolean;
   FCritical: System.SyncObjs.TCriticalSection;

   procedure SetIsBusy(AValue:boolean);
   function GetIsBusy:boolean;
   procedure SetIsSuccess(AValue:boolean);
   function GetIsSuccess:boolean;
  public
   property IsSuccess:boolean read GetIsSuccess write SetIsSuccess;
   property IsBusy:boolean read GetIsBusy write SetIsBusy;
   constructor Create;
   destructor Destroy; virtual;
   procedure Lock;
   procedure Unlock;
end;

implementation

procedure AsyncAwait(async, await: TProc);
begin

  TTask.Run(
    procedure
    begin

      async();

      TThread.Queue(nil,
        procedure
        begin

          await();

        end
      ); // TThread.Queue
    end
  ); //TTask.Run

end;

{ THttpRequest }
procedure THttpRequest.ApplySessionHeaders(const ASource: IHTTPResponse);
begin
  {if Assigned(ASource) then
   begin
      AuthDataGlobal.SessionAccessToken := ASource.HeaderValue[HEADER_ACCESS_TOKEN];
      AuthDataGlobal.SessionClient := ASource.HeaderValue[HEADER_CLIENT];
      AuthDataGlobal.SessionUid := ASource.HeaderValue[HEADER_UID];
   end;}
end;

constructor THttpRequest.Create;
begin
 self.FSession := TNetHttpClient.Create(nil);
  //work only on Win platforms?
 self.FSession.ConnectionTimeout := CONNECTION_TIMEOUT;
 self.FSession.ResponseTimeout := RESPONSE_TIMEOUT;
 self.FCritical := TCriticalSection.Create;

  {$ifdef INDY_USAGE}
   self.FIndySession := TIdHTTP.Create(nil);
   self.FIndySession.ConnectTimeout := CONNECTION_TIMEOUT;
   self.FIndySession.ReadTimeout := RESPONSE_TIMEOUT;
  {$endif}

 inherited;
end;

destructor THttpRequest.Destroy;
begin

 try
  self.FSession.Free;
  self.FCritical.Free;
  inherited;
 except on E:Exception do

 end;

end;

function THttpRequest.Get(const AUrl: string; const AIsWithAuth, AIsURLEncode: Boolean; const AContentType: string;
  out AStatusCode: Integer): string;
var
  LRetryCount: Integer;
  LResult: TStringStream;
  LResponse: IHTTPResponse;
{$IFDEF TASK_LOG}
  LStatusText: string;
{$ENDIF}
begin
  AStatusCode := 0;
{$IFDEF TASK_LOG}
  LStatusText := '';
{$ENDIF}

  LResult := TStringStream.Create('', TEncoding.UTF8);
  try

    try

      Session.ContentType := AContentType;
      MakeSessionRequestHeaders(AIsWithAuth);
    finally

    end;

    LRetryCount := 3;
    repeat


     try
      if AIsURLEncode then
        LResponse := Session.Get(TNetEncoding.URL.Encode(AUrl, [], []), LResult)
      else
        LResponse := Session.Get(AUrl, LResult);
     except on E:Exception do
       begin
        TLogger.Save('[THttpRequest.Get] url='+AUrl+' err: '+e.Message, '', TLogLevel.Error);
        if e.Message.Contains('timed out')  then AStatusCode := -1;
        exit;
       end;
      end;


      if AIsWithAuth then
        ApplySessionHeaders(LResponse);

      Result := LResult.DataString;
      self.FResponse := Result;

      if Assigned(LResponse) then
      begin
        AStatusCode := LResponse.StatusCode;
      {$IFDEF TASK_LOG}
        LStatusText := LResponse.StatusText;
      {$ENDIF}
      end;

      self.FStatusCode := AStatusCode;
      sleep(10);

      if AStatusCode = 0 then
      begin
        Sleep(10);
        Dec(LRetryCount);
      end;
    until (AStatusCode > 0) or (LRetryCount < 1);
  finally
    SafeFree(LResult);
  end;

{$IFDEF TASK_LOG}
  TLogger.Save(
    '[THttpRequest.Get] url: "' + AUrl + '", '
    + sLineBreak + ' server answer code: ' + AStatusCode.ToString
    + IfThen(LStatusText.IsEmpty, '', ' (' + LStatusText + '),  server answer: "' + Result + '"'),
    TLogLevel.Info
    );
{$ENDIF}
end;

procedure THttpRequest.GetAsync(const AUrl: string; const AIsWithAuth,
  AIsURLEncode: Boolean; const AContentType: string);
begin

   TLogger.Save('[GetAsync] start url='+AUrl);

   AsyncAwait(
    procedure
    begin
      self.Get(Aurl, AIsWithAuth, AIsURLEncode, AContentType, self.FStatusCode);
    end,
    procedure
    begin
      if Assigned(self.FOnRequestCompleted) then self.FOnRequestCompleted;
    end);

end;

function THttpRequest.GetResponse: string;
begin
     Self.FCritical.Acquire;
      try
       result := FResponse;
      finally
       self.FCritical.Release;
      end;
end;

function THttpRequest.GetStatusCode: integer;
begin
   Self.FCritical.Acquire;
    try
     result := FStatusCode;
    finally
     self.FCritical.Release;
    end;
end;

procedure THttpRequest.SetResponse(AValue: string);
begin
   try
    self.FCritical.Acquire;
     try
      self.FResponse := AValue;
     finally
      self.FCritical.Release;
     end;
  Except on E:Exception do
   {$IFDEF TASK_LOG}
     TLogger.Save('[THttpRequest.SetResponse] error: '+e.Message, TLogLevel.Error);
   {$ENDIF}
   end;
end;

procedure THttpRequest.SetStatusCode(AValue: integer);
begin
  try
    self.FCritical.Acquire;
     try
      self.FStatusCode := AValue;
     finally
      self.FCritical.Release;
     end;
  Except on E:Exception do
    {$IFDEF TASK_LOG}
     TLogger.Save('[THttpRequest.SetStatusCode] error: '+e.Message, TLogLevel.Error);
    {$ENDIF}
   end;
end;

function THttpRequest.Post(const AUrl: string; const ASource: TStream; const AIsWithAuth, AIsURLEncode: Boolean;
  const AContentType: string; out AStatusCode: Integer): string;
var
  LResult: TStringStream;
  LResponse: IHTTPResponse;
 {$ifdef INDY_USAGE}
   ts: TStrings;
   LBody: TStringStream;
 {$endif}
{$IFDEF TASK_LOG}
  LParamsInfo: string;
  LParams: TStringStream;
  LStatusText: string;
 {$ENDIF}
  LThreadId: string;
begin
  AStatusCode := 0;

  {$IFDEF TASK_LOG}
    LStatusText := '';
  {$ENDIF}


     {$IF defined(MSWINDOWS) }
      LThreadId := GetCurrentThreadId.ToString;
     {$ELSE}
      LThreadId := 'unknown';
     {$ENDIF}

  LResult := TStringStream.Create('', TEncoding.UTF8);
  try
    try
      Session.ContentType := AContentType;
      MakeSessionRequestHeaders(AIsWithAuth);
    finally
    end;

    try
     {$ifdef INDY_USAGE}
       ts := TStringList.Create;
       try
         LBody := TStringStream.Create;
         ASource.Seek(0, TSeekOrigin.soBeginning);
         LBody.CopyFrom(ASource, ASource.Size);
         LBody.Seek(0, TSeekOrigin.soBeginning);

         ts.Text := LBody.DataString;
         if AIsURLEncode then
           result := IndySession.Post(TNetEncoding.URL.Encode(AUrl, [], []), ts)
            else
           result := IndySession.Post(AUrl, ts);

         AStatusCode := 200;

         self.FResponse := Result;

         TLogger.Save(
                '[THttpRequest.Post.Indy] ThreadId='+LThreadId+' url: "' + AUrl + '", params: ' + ts.Text + ',' + sLineBreak
                + 'server answer code: ' + AStatusCode.ToString
                + ' server answer: "' + Result + '"',
                TLogLevel.Info
               );

       finally
        LBody.DisposeOf;
        ts.DisposeOf;
       end;
     {$else}
       if AIsURLEncode then
         LResponse := Session.Post(TNetEncoding.URL.Encode(AUrl, [], []), ASource, LResult)
          else
         LResponse := Session.Post(AUrl, ASource, LResult);
      {$endif}

    except on E:Exception do
     begin
      TLogger.Save('[THttpRequest.Post] url='+AUrl+' err: '+e.Message, '', TLogLevel.Error);
      if e.Message.Contains('timed out')  then AStatusCode := -1;
      exit;
     end;
    end;

    {$ifndef INDY_USAGE}
      if AIsWithAuth then
        ApplySessionHeaders(LResponse);

      Result := LResult.DataString;
      self.FResponse := Result;
       if Assigned(LResponse) then
        begin
          AStatusCode := LResponse.StatusCode;
        {$IFDEF TASK_LOG}
          LStatusText := LResponse.StatusText;
        {$ENDIF}
        end;
     {$endif}

  finally
    SafeFree(LResult);
  end;

{$IFDEF TASK_LOG}

  LParams := TStringStream.Create;
  try
    if Assigned(ASource) then
    begin
      if ASource.Size > 2048*2048 then
        LParamsInfo := 'hidden (size ' + ASource.Size.ToString + ' bytes)'
      else
      begin
        ASource.Seek(0, TSeekOrigin.soBeginning);
        LParams.CopyFrom(ASource, ASource.Size);
        LParams.Seek(0, TSeekOrigin.soBeginning);
        LParamsInfo := '"' + LParams.DataString + '"';
      end;
    end;

     TLogger.Save(
      '[THttpRequest.Post.Stream] ThreadId='+LThreadId+' url: "' + AUrl + '", params: ' + LParamsInfo + ',' + sLineBreak
      + 'server answer code: ' + AStatusCode.ToString + IfThen(LStatusText.IsEmpty, '', ' (' + LStatusText + '), '
      + ' server answer: "' + Result + '"'),
      TLogLevel.Info
      );
  finally
    SafeFree(LParams);
  end;

{$ENDIF}
end;

procedure THttpRequest.PostAsync(const AUrl: string; const ASource: TStream;
  const AIsWithAuth, AIsURLEncode: Boolean; const AContentType: string);
begin
  AsyncAwait(
    procedure
    begin
      self.Post(Aurl, ASource, AIsWithAuth, AIsURLEncode, AContentType, self.FStatusCode);
    end,
    procedure
    begin
      if Assigned(self.FOnRequestCompleted) then self.FOnRequestCompleted;
    end);
end;

function THttpRequest.Put(const AUrl: string; const ASource: TStream; const AIsWithAuth, AIsURLEncode: Boolean;
                               const AContentType: string; out AStatusCode: Integer): string;
var
  LResult: TStringStream;
  LResponse: IHTTPResponse;
{$IFDEF TASK_LOG}
  LParamsInfo: string;
  LParams: TStringStream;
  LStatusText: string;
  LThreadId: string;
{$ENDIF}
begin
  AStatusCode := 0;
{$IFDEF TASK_LOG}
  LStatusText := '';
{$ENDIF}

  LResult := TStringStream.Create('', TEncoding.UTF8);
  try
    try
      Session.ContentType := AContentType;
      MakeSessionRequestHeaders(AIsWithAuth);
    finally
    end;

    if AIsURLEncode then
      LResponse := Session.Put(TNetEncoding.URL.Encode(AUrl, [], []), ASource, LResult)
    else
      LResponse := Session.Put(AUrl, ASource, LResult);

    if AIsWithAuth then
      ApplySessionHeaders(LResponse);

    Result := LResult.DataString;
    self.FResponse := Result;
    if Assigned(LResponse) then
    begin
      AStatusCode := LResponse.StatusCode;
    {$IFDEF TASK_LOG}
      LStatusText := LResponse.StatusText;
    {$ENDIF}
    end;
  finally
    SafeFree(LResult);
  end;

{$IFDEF TASK_LOG}
  LParams := TStringStream.Create;
  try
    if Assigned(ASource) then
    begin
      if ASource.Size > 2048 then
        LParamsInfo := 'hidden (size ' + ASource.Size.ToString + ' bytes)'
      else
      begin
        ASource.Seek(0, TSeekOrigin.soBeginning);
        LParams.CopyFrom(ASource, ASource.Size);
        LParams.Seek(0, TSeekOrigin.soBeginning);
        LParamsInfo := '"' + LParams.DataString + '"';
      end;
    end;

     {$IF DEFINED(MSWINDOWS)}
      LThreadId := GetCurrentThreadId.ToString;
     {$ELSE}
      LThreadId := 'unknown';
     {$ENDIF}

    TLogger.Save(
      '[THttpRequest.Put.Stream] ThreadId='+LThreadId+' url: "' + AUrl + '", params: ' + LParamsInfo + ',' + sLineBreak
      + 'server answer code: ' + AStatusCode.ToString + IfThen(LStatusText.IsEmpty, '', ' (' + LStatusText + '), '
      + ' server answer: "' + Result + '"'),
      TLogLevel.Info
      );
  finally
    SafeFree(LParams);
  end;
{$ENDIF}
end;

procedure THttpRequest.PutAsync(const AUrl: string; const ASource: TStream;
  const AIsWithAuth, AIsURLEncode: Boolean; const AContentType: string);
{$IFDEF TASK_LOG}
var
  LThreadId:string;
 {$ENDIF}
begin
  AsyncAwait(
    procedure
    begin
      self.Put(Aurl, ASource, AIsWithAuth, AIsURLEncode, AContentType, self.FStatusCode);
    end,
    procedure
    begin

      {$IFDEF TASK_LOG}

          {$IFDEF MSWINDOWS}
            LThreadId := GetCurrentThreadId.ToString;
          {$ELSE}
            LThreadId := 'unknown';
          {$ENDIF}

      TLogger.Save(
        '[THttpRequest.PutAsync.OnRequestCompleted] ThreadId='+LThreadId,
        TLogLevel.Info
        );

      {$ENDIF}
      if Assigned(self.FOnRequestCompleted) then self.FOnRequestCompleted;

    end);
end;

procedure THttpRequest.MakeSessionRequestHeaders(const AIsWithAuth: Boolean);
var
  LContentType: string;
begin

  {try
    Session.CustomHeaders[HEADER_TOKEN_TYPE] :=
      IfThen(AIsWithAuth and (not Controller.Broker.AuthDataGlobal.SessionAccessToken.IsEmpty), 'Bearer', '');
    Session.CustomHeaders[HEADER_ACCESS_TOKEN] := IfThen(AIsWithAuth, Controller.Broker.AuthDataGlobal.SessionAccessToken, '');
    Session.CustomHeaders[HEADER_CLIENT] := IfThen(AIsWithAuth, Controller.Broker.AuthDataGlobal.SessionClient, '');
    Session.CustomHeaders[HEADER_UID] := IfThen(AIsWithAuth, Controller.Broker.AuthDataGlobal.SessionUid, '');

    // примечание: манипуляция с Content-Type предназначена, чтобы сервер корректно видел этот самый Content-Type
    // также важно, что при пустом Content-Type данный заголовок отсутствовал, иначе сервер выдает 500 ошибку
    LContentType := Session.ContentType;
    Session.ContentType := '';
    if LContentType.IsEmpty then
    begin
      try
        Session.CustHeaders.Delete(HEADER_CONTENT_TYPE);
      except
        on E: Exception do
          if not (E is EArgumentOutOfRangeException) then
            raise;
      end;
    end
    else
      Session.CustomHeaders[HEADER_CONTENT_TYPE] := LContentType;
  finally

  end;}
end;

{ TBaseController }

constructor TBaseController.Create;
begin
 inherited;
 self.FCritical := System.SyncObjs.TCriticalSection.Create;
 self.IsBusy := false;
end;

destructor TBaseController.Destroy;
begin
 self.FCritical.Free;
 //SafeFree(self);
end;

function TBaseController.GetIsBusy: boolean;
begin
 self.FCritical.Acquire;
 try
  result := self.FIsBusy;
 finally
  self.FCritical.Release;
 end;
end;

function TBaseController.GetIsSuccess: boolean;
begin
 self.FCritical.Acquire;
 try
  result := self.FIsSuccess;
 finally
  self.FCritical.Release;
 end;
end;

procedure TBaseController.Lock;
begin
 if Assigned(Self.FCritical) then
  self.FCritical.Acquire;
end;

procedure TBaseController.SetIsBusy(AValue: boolean);
begin
 self.FCritical.Acquire;
 try
  self.FIsBusy := AValue;
 finally
  self.FCritical.Release;
 end;
end;

procedure TBaseController.SetIsSuccess(AValue: boolean);
begin
 self.FCritical.Acquire;
 try
  self.FIsSuccess := AValue;
 finally
  self.FCritical.Release;
 end;
end;

procedure TBaseController.Unlock;
begin
 if Assigned(Self.FCritical) then
  self.FCritical.Release;
end;

end.
