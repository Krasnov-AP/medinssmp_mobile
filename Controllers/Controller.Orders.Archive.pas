﻿unit Controller.Orders.Archive;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.SMPService, Model.Global,
 Model.Orders;

 type

  TControllerOrdersArchive = class(TControllerAnyData)
   strict private
     const
//      URL_GET = 'http://#host#:#port#' +
//      '/?command=getTableDataWithCondition&tableName=orders&condition='+
//      '("IsDeleted"=0) and (("TimeCall">=(CURRENT_DATE-14)) or ("IsPlan"=1 and "State"=0))&'+
//      'DBType=0&userLogin=#userLogin#&UseZip=0';
      URL_GET = 'http://#host#:#port#' +
      '/?command=getTableDataWithCondition&tableName=orders&condition='+
      '#condition#&'+
      'DBType=0&userLogin=#userLogin#&UseZip=0';
   strict private
    FOrders: TOrders;
    FCurrentOrderId: integer;
   public
    property Orders: TOrders read FOrders write FOrders;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;

implementation

{ TControllerCalls }

constructor TControllerOrdersArchive.Create;
begin
  inherited;
  self.Orders := TOrders.Create;
end;

destructor TControllerOrdersArchive.Destroy;
begin
  SafeFree(self.FOrders.Items);
  inherited;
end;

procedure TControllerOrdersArchive.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
var
 LFilterStr: string;
begin
  try
    if self.Port = 0 then self.Port := 54002;

    self.url := self.URL_GET.Replace('#host#', self.Host)
                .Replace('#port#', self.Port.ToString)
                .Replace('#userLogin#', Model.Global.ModelDataGlobal.CurrentUser.Name);

    if AFilter <> nil then
     begin
      LFilterStr := AFilter.Values['condition'];
      self.url := self.Url.Replace('#condition#', LFilterStr.Replace(sLineBreak, ''));
     end
      else
       self.url := self.Url.Replace('#condition#', '("IsDeleted"=0) and ("TimeCall">=(CURRENT_DATE-1))');

    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerOrders.RetrieveData] url='+self.url+' err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
