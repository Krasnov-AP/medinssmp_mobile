﻿unit Controller.Orders;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.SMPService, Model.Global,
 Model.Orders;

 type

  TControllerOrder = class(TControllerAnyData)
   strict private
     const
//      URL_GET = 'http://#host#:#port#' +
//      '/?command=getTableDataWithCondition&tableName=orders&condition='+
//      '("IsDeleted"=0) and (("TimeCall">=(CURRENT_DATE-14)) or ("IsPlan"=1 and "State"=0))&'+
//      'DBType=0&userLogin=#userLogin#&UseZip=0';
      URL_INSERT = 'http://#host#:#port#' +
      '/?command=insertData&tableName=orders';
      URL_UPDATE = 'http://#host#:#port#' +
      '/?command=updateData&tableName=orders&recordId=#orderId#';
   strict private
    FOrder: TOrder;
   public
    property Order: TOrder read FOrder write FOrder;
    constructor Create;
    destructor Destroy; override;
    procedure InsertData(AFilter:TStrings = nil); override;
    procedure SaveOrder(AOrderId: integer);
    procedure UpdateOrder(AOrderId: integer);
 end;

  TControllerOrders = class(TControllerAnyData)
   strict private
     const
//      URL_GET = 'http://#host#:#port#' +
//      '/?command=getTableDataWithCondition&tableName=orders&condition='+
//      '("IsDeleted"=0) and (("TimeCall">=(CURRENT_DATE-14)) or ("IsPlan"=1 and "State"=0))&'+
//      'DBType=0&userLogin=#userLogin#&UseZip=0';
      URL_GET = 'http://#host#:#port#' +
      '/?command=getTableDataWithCondition&tableName=orders&condition='+
      '("IsDeleted"=0) and ("TimeCall">=(CURRENT_DATE-7)) #filter#&'+
      'DBType=0&userLogin=#userLogin#&UseZip=0';
   strict private
    FOrders: TOrders;
    FCurrentOrderId: integer;
   public
    property Orders: TOrders read FOrders write FOrders;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;

implementation

{ TControllerCalls }

constructor TControllerOrders.Create;
begin
  inherited;
  self.Orders := TOrders.Create;
end;

destructor TControllerOrders.Destroy;
begin
  SafeFree(self.FOrders.Items);
  inherited;
end;

procedure TControllerOrders.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
var
 LFilterStr: string;
begin
  try
    if self.Port = 0 then self.Port := 54002;

    self.url := self.URL_GET.Replace('#host#', self.Host)
                .Replace('#port#', self.Port.ToString)
                .Replace('#userLogin#', Model.Global.ModelDataGlobal.CurrentUser.Name);

    if AFilter <> nil then
     begin
      LFilterStr := AFilter.Text;
      self.url := self.Url.Replace('#filter#', LFilterStr.Replace(sLineBreak, ''));
     end
      else
       self.url := self.Url.Replace('#filter#', '');

    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerOrders.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

{ TControllerOrder }

constructor TControllerOrder.Create;
begin
  inherited;
  self.FOrder := TOrder.Create;
end;

destructor TControllerOrder.Destroy;
begin

  inherited;
end;

procedure TControllerOrder.InsertData(AFilter: TStrings);
begin
   try
    if self.Port = 0 then self.Port := 54002;
    self.Data.SetDataString(FOrder.ConvertToJson);

    if AFilter = nil then
      begin
       self.url := self.URL_INSERT.Replace('#host#', self.Host)
                             .Replace('#port#', self.Port.ToString);
      end
       else
      begin
       self.url := self.URL_UPDATE.Replace('#host#', self.Host)
                             .Replace('#port#', self.Port.ToString)
                             .Replace('#orderId#', AFilter.Text);

      end;
    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerOrder.InsertData] err: '+e.Message, TLogLevel.Error);
   end;

end;

procedure TControllerOrder.SaveOrder(AOrderId: integer);
begin
 self.Order.Id := AOrderId.ToString;
 self.InsertData(nil);
end;

procedure TControllerOrder.UpdateOrder(AOrderId: integer);
var
 ts: TStrings;
begin
 self.Order.Id := AOrderId.ToString;
 ts:= TStringList.Create;
  try
    ts.Text := AOrderId.ToString;
    self.InsertData(ts);
  finally
    ts.Free;
  end;
end;

end.
