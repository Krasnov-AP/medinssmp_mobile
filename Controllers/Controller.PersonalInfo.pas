﻿unit Controller.PersonalInfo;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.SMPService, Model.Global,
 Model.PersonalInfo;

 type
  TControllerPersonalInfo = class(TControllerAnyData)
   strict private
     const
      URL_GET = 'http://#host#:#port#' +
      '/?command=getTableDataWithCondition&tableName=users&condition="IsDeleted"=0#filter#'+ //+and+"Login"=''1''
      '&DBType=0&userLogin=#userLogin#&UseZip=0';
   strict private
    FPersonalInfo: TPersonalInfo;
   public
    property PersonalInfo: TPersonalInfo read FPersonalInfo write FPersonalInfo;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;

implementation

{ TControllerPersonalInfo }

constructor TControllerPersonalInfo.Create;
begin
 inherited;
 self.PersonalInfo := TPersonalInfo.Create;
end;

destructor TControllerPersonalInfo.Destroy;
begin
  SafeFree(self.FPersonalInfo);
  inherited;
end;

procedure TControllerPersonalInfo.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
var
 i: integer;
 LFilter: string;
begin
   try
    if self.Port = 0 then self.Port := 54002;

    self.url := self.URL_GET.Replace('#host#', self.Host)
                            .Replace('#port#', self.Port.ToString)
                            .Replace('#userLogin#', ModelDataGlobal.CurrentUser.Name);

    if AFilter <> nil then
     begin
      if AFilter.IndexOfName('Login')>-1 then
        begin
          LFilter := '+and+"Login"='''+AFilter.Values['Login']+'''';
        end;
     end
      else
       LFilter := '';

    self.Url := self.url.Replace('#filter#', LFilter);

    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerCalls.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
