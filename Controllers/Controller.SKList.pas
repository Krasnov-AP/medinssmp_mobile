﻿unit Controller.SKList;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.Global,
 Core.Funcs, Model.SkList;

type

 TControllerSKList = class(TControllerAnyData)
   strict private
     const
      URL_GET = 'http://#host#:#port#/?' +
      'command=getTableDataWithCondition&tableName=sk_list&condition="IsDeleted"=0&DBType=0&userLogin=#userLogin#';  //filter =  and "Id" in (411,1209)
   strict private
    FSKList: TSKList;
    FCurrentOrderId: integer;
   public
    property SKList: TSKList read FSKList write FSKList;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;


implementation

{ TControllerSKList }

constructor TControllerSKList.Create;
begin
 inherited;
 self.FSKList := TSKList.Create;
end;

destructor TControllerSKList.Destroy;
begin
  SafeFree(self.FSKList);
  inherited;
end;

procedure TControllerSKList.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
var
 LFilterStr: string;
 i: integer;
 LKeyValue: TKeyValue;
 LIsUpdate: boolean;
begin
  try
    if self.Port = 0 then self.Port := 54002;

    self.url := self.URL_GET.Replace('#host#', self.Host)
                  .Replace('#port#', self.Port.ToString)
                  .Replace('#userLogin#', Model.Global.ModelDataGlobal.CurrentUser.Name);

//   if AFilter <> nil then
//    for i:=0 to AFilter.Count - 1 do
//      begin
//        LKeyValue := ExtractKeyValue(AFilter[i]);
//        self.url := self.Url.Replace('#'+LKeyValue.Key+'#', LKeyValue.Value);
//      end;
//
//
//    if AFilter <> nil then
//     begin
//      LFilterStr := AFilter.Text;
//      self.url := self.Url.Replace('#filter#', LFilterStr.Replace(sLineBreak, ''));
//     end
//      else
//       self.url := self.Url.Replace('#filter#', '');

    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerSKList.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
