﻿unit Controller.SMPService;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.SMPService, Model.Global;

type

 TControllerSMPService = class(TControllerAnyData)
   strict private
     const
      URL_GET = HOST_HTTP + ':' + PORT_HTTP + '/?command=getCompanyList';
   strict private
    FSMPServices: TSMPServices;
   public
    property SMPServices: TSMPServices read FSMPServices write FSMPServices;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;

implementation

{ TControllerSMPService }

constructor TControllerSMPService.Create;
begin
 inherited;
 self.FSMPServices := TSMPServices.Create;
end;

destructor TControllerSMPService.Destroy;
begin
  SafeFree(self.FSMPServices);
  inherited;
end;

procedure TControllerSMPService.RetrieveData(AMethod:TRequestMethod = rmGET; AFilter: TStrings= nil;
  ACached: boolean = false);
begin
  try
    self.url := URL_GET;
    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerAuth.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
