﻿unit Controller.Survey;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.SMPService, Model.Global,
 Model.PersonalInfo;

 type
  TControllerSurvey = class(TControllerAnyData)
   strict private
     const
      URL_GET = 'http://#host#:#port#' +
      '/?command=setSurveyState&connectionUserId=#connectionUserId#&userLogin="userLogin"'+
      '&orderId=#orderId#&state=#state#&value=#value#';
   public
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;

implementation

{ TControllerPersonalInfo }

constructor TControllerSurvey.Create;
begin
 inherited;
end;

destructor TControllerSurvey.Destroy;
begin
  inherited;
end;

procedure TControllerSurvey.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
var
 i: integer;
 LFilter: string;
begin
   try
    if self.Port = 0 then self.Port := 54002;

    if ( (AFilter = nil) or (not Assigned(AFilter)) ) then
     begin
       TLogger.Save('[TControllerSurvey.RetrieveData] Filter not set');
       raise Exception.Create('Не указано значение фильтра');
     end;

    self.url := self.URL_GET.Replace('#host#', self.Host)
                            .Replace('#port#', self.Port.ToString)
                            .Replace('#userLogin#', ModelDataGlobal.CurrentUser.Name)
                            .Replace('#connectionUserId#', ModelDataGlobal.CurrentUser.Id.ToString)
                            .Replace('#orderId#', AFilter.Values['orderId'])
                            .Replace('#state#', AFilter.Values['state'])
                            .Replace('#value#', AFilter.Values['value']);

    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerSurvey.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
