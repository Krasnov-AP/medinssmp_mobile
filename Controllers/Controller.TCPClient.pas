﻿unit Controller.TCPClient;

interface

 uses
  System.Messaging, IdBaseComponent, System.SysUtils,
  IdComponent, IdTCPConnection, IdTCPClient, IdThreadComponent, Model.Global;

 type
  TTcpClient = class
    const
     HOST = '176.122.25.189';
    strict private
      idTCPClient         : TIdTCPClient;
      idThreadComponent   : TIdThreadComponent;
      FConnected: boolean;
    strict private
      procedure SetConnected(AValue: boolean);
      function GetConnected: boolean;
    private
     procedure IdTCPClientConnected(Sender: TObject);
     procedure IdTCPClientDisconnected(Sender: TObject);

     procedure IdThreadComponentRun(Sender: TIdThreadComponent);
     procedure SendMessage(Sender: TObject; AMsg: string);

    public
     property Connected: boolean read GetConnected write SetConnected;
    public
     constructor Create;
     procedure Destroy;
  end;

implementation

{ TTcpClient }

constructor TTcpClient.Create;
begin
    idTCPClient                 := TIdTCPClient.Create();

    idTCPClient.Host            := self.HOST;
    idTCPClient.Port            := 20010;

    idTCPClient.OnConnected     := IdTCPClientConnected;
    idTCPClient.OnDisconnected  := IdTCPClientDisconnected;
    idThreadComponent           := TIdThreadComponent.Create();

    idThreadComponent.OnRun     := IdThreadComponentRun;

    self.FConnected := false;
end;

procedure TTcpClient.Destroy;
begin
    if idThreadComponent.active then begin
       idThreadComponent.active := False;
    end;

    IdTCPClient.DisposeOf;
    IdThreadComponent.DisposeOf;
end;

function TTcpClient.GetConnected: boolean;
begin

  {result := not Assigned(self.idTCPClient.IOHandler);
  self.FConnected := result;
  exit;}

  if not Assigned(IdTCPClient.IOHandler) then exit(false);   //необходимо!! для Андроида
  
  try
   IdTCPClient.IOHandler.WriteLn('test');
   //IdThreadComponent.Active  := True;
   result := true;
  except on E:Exception do
   begin
     self.SendMessage(self, 'tcp_client_disconnected');
     self.FConnected := false;
     result := false;
     exit;
   end;

  end;
end;

procedure TTcpClient.IdTCPClientConnected(Sender: TObject);
begin

  try
   IdTCPClient.IOHandler.WriteLn('mq_server='+ModelDataGlobal.AppSettings.SMPService.IP);
   IdThreadComponent.Active  := True;
  except on E:Exception do
   begin
     self.SendMessage(self, 'tcp_client_disconnected');
     self.FConnected := false;
     exit;
   end;

  end;

  self.SendMessage(self, 'tcp_client_connected');
  self.FConnected := true
end;

procedure TTcpClient.IdTCPClientDisconnected(Sender: TObject);
begin
  self.SendMessage(self, 'tcp_client_disconnected');
  self.FConnected := false;
end;

procedure TTcpClient.IdThreadComponentRun(Sender: TIdThreadComponent);
var
 LMsg: string;
begin
 LMsg := IdTCPClient.IOHandler.ReadLn();
 self.SendMessage(self, 'message: '+LMsg)
end;

procedure TTcpClient.SendMessage(Sender: TObject; AMsg: string);
var
 Message: TMessage;
begin
 Message := TMessage<String>.Create(AMsg);
 TMessageManager.DefaultManager.SendMessage(Sender, Message, True);
end;

procedure TTcpClient.SetConnected(AValue: boolean);
begin

  if AValue = self.FConnected then exit;

  self.FConnected := AValue;

  if AValue then
   begin

    //if self.idTCPClient.Connected then exit;

     try
      self.idTCPClient.Connect;
     except on E:Exception do
      begin
       self.FConnected := false;
       self.SendMessage(self, 'error='+e.Message);
       raise Exception.Create(e.Message);
       exit;
      end;
     end;

   end
    else
   begin

    //if not self.idTCPClient.Connected then exit;

     try
      self.idTCPClient.DisConnect;
     except on E:Exception do
      begin
       self.FConnected := true;
       self.SendMessage(self, 'error='+e.Message);
       raise Exception.Create(e.Message);
       exit;
      end;
     end;

   end;

end;


end.
