﻿unit Controller.Yandex.Geocode;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Controller.AnyData, Model.Yandex.Geocode, Model.Global,
 Model.Orders;

 type

  TControllerYandexGeocode = class(TControllerAnyData)
   strict private
     const
      URL_GET = 'https://suggest-maps.yandex.ru/suggest-geo?add_chains_loc=0&add_rubrics_loc=0&'+
      'bases=geo&client_reqid=1638607140912_226306&fullpath=0&lang=ru_RU&ll=37.622504,55.753215'+
      '&origin=maps-search-form&outformat=json&part=#address#&pos=5&spn=1.4447021484375,0.6250096969194132'+
      '&v=9&yu=8695085751613425624';
   strict private
    FYandexGeoData: TYandexGeoData;
   public
    property YandexGeoData: TYandexGeoData read FYandexGeoData write FYandexGeoData;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;

  TControllerYandexCoord = class(TControllerAnyData)
   strict private
     const
      URL_GET = 'https://geocode-maps.yandex.ru/1.x/?geocode=#address#&format=json&apikey=#apikey#';
   strict private
    FYandexCoordData: TYandexCoordData;
   public
    property YandexCoordData: TYandexCoordData read FYandexCoordData write FYandexCoordData;
    constructor Create;
    destructor Destroy; override;
    procedure RetrieveData(AMethod:TRequestMethod = rmGET; AFilter:TStrings = nil; ACached: boolean = false); override;
 end;


implementation

{ TControllerYandexGeocode }

constructor TControllerYandexGeocode.Create;
begin
 inherited;
 self.FYandexGeoData := TYandexGeodata.Create;
end;

destructor TControllerYandexGeocode.Destroy;
begin
  SafeFree(self.FYandexGeoData);
  inherited;
end;

procedure TControllerYandexGeocode.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
  var
   s: string;
   i: integer;
begin
  try
    self.Url := self.URL_GET;
    if AFilter<>nil then
     begin
      for I := 0 to AFilter.Count - 1 do
       self.Url := self.Url.Replace('#'+AFilter.Names[i]+'#', AFilter.Values[AFilter.Names[i]]);
     end;
    inherited;
   except on E:Exception do
    TLogger.Save('[TControllerYandexGeocode.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

{ TControllerYandexCoord }

constructor TControllerYandexCoord.Create;
begin
 inherited;
 self.FYandexCoordData := TYandexCoordData.Create;
end;

destructor TControllerYandexCoord.Destroy;
begin
  SafeFree(self.FYandexCoordData);
  inherited;
end;

procedure TControllerYandexCoord.RetrieveData(AMethod: TRequestMethod;
  AFilter: TStrings; ACached: boolean);
  var
   s: string;
   i: integer;
begin
  try

    self.Url := self.URL_GET;

    if AFilter<>nil then
     begin
      for I := 0 to AFilter.Count - 1 do
       self.Url := self.Url.Replace('#'+AFilter.Names[i]+'#', AFilter.Values[AFilter.Names[i]]);
     end;
     inherited;
   except on E:Exception do
    TLogger.Save('[TControllerYandexGeocode.RetrieveData] err: '+e.Message, TLogLevel.Error);
   end;
end;

end.
