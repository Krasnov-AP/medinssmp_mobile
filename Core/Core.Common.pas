unit Core.Common;

interface

uses

 {$IFDEF MSWINDOWS}
  Winapi.Windows, Winapi.Messages,
  {$ENDIF}
  System.Classes, System.Types, System.SysUtils,
  System.StrUtils, System.IOUtils, System.SyncObjs, System.Generics.Collections,
  System.Messaging, System.Math, FMX.Dialogs, FMX.Platform, FMX.VirtualKeyboard,
  FMX.Controls, FMX.Types
  {$ifdef ANDROID}
    ,
    FMX.Platform.Android,
    Androidapi.Helpers,
    Androidapi.JNI.Os,
    Androidapi.JNI.GraphicsContentViewText,
    Androidapi.JNI.JavaTypes,
    Androidapi.JNI.Webkit,
    Androidapi.JNI.Net,
    Androidapi.JNIBridge,
    Androidapi.JNI.App,
    Androidapi.JNI.Support,
    Androidapi.JNI.Provider,
    Androidapi.JNI.Widget,
    Androidapi.JNI.Embarcadero
  {$endif}

  {$IFDEF IOS}
    ,FMX.Forms
  {$ENDIF}
  {$IFDEF Android}
    ,FMX.Helpers.Android
  {$ENDIF}
  ;

 const
  APP_NAME = 'medins_smp';

 type

 {$REGION '��������-����� TLogger ����������'}

  {$REGION 'TLogLevel'}
  TLogLevel = (
    Info = 0,
    Warning = 1,
    Error = 2
    );

  TLogLevelHelper = record helper for TLogLevel
  strict private
    const
      LEVEL_CODES: array[TLogLevel] of string = (
        'info', 'warning', 'error'
        );

  public
    function ToInteger: Integer;
    function ToString: string;
  end;
  {$ENDREGION'}

  TLogger = class sealed
   strict private
    const
     DEFAULT_FILE_MASK = '%0:s-%1:s%2:s%3:s.log';

   strict private
    class var

      FCritical: System.SyncObjs.TCriticalSection;
      FLogStrings: TStrings;
      FInstance: TLogger;
      FFilePath, FFileName: string;

   strict private

     class function GetInstance: TLogger; static;
     class procedure SetInstance(const AValue: TLogger); static;
     class function WordToStrAligned(const AValue: Word; const ADigits: Byte = 2): string;

   private

     constructor InternalCreate(const ADummy: Integer = 0);

   public

     class property FileName: string read FFilePath;
     class property FilePath: string read FFilePath write FFilePath;
     class property Instance: TLogger read GetInstance write SetInstance;
     class property LogStrings: TStrings read FLogStrings write FLogStrings;

     constructor Create;
     destructor Destroy; override;

     class procedure Save(const AMessage, AValue: string; const ALogLevel: TLogLevel); overload;
     class procedure Save(const AMessage: string; const ALogLevel: TLogLevel = Info); overload;

     class procedure SendMessage(AMsg: String);

  end;

  TLangValue = record
    EN, RU:string;
  end;

  TLangValues = TDictionary<string, TLangValue>;

  TLangProvider = class
   strict private
    FLangValues:TLangValues;
   public
    property LangValues:TLangValues read FLangValues write FLangValues;
   public
    procedure LoadData; virtual;
    constructor Create;
  end;

  TLangManager = class
    strict private
     class var FLocale:string;
     class var FLangValues:TLangValues;
    public
     class property Locale:string read FLocale write FLocale;
     class property LangValues:TLangValues read FLangValues write FLangValues;
     class function Translate(AMsg:string):string;
  end;

  TToast = class
   public
    class procedure Show(AMsg:string; ALong: boolean = false);
  end;

  TAppKeyboard = class
    private
     class var FService: IFMXVirtualKeyboardService;
     class var FIsReady: boolean;
     class var FIsVisible: boolean;
    public
     class property IsVisible: boolean read FIsVisible write FIsVisible;
    public
     class function Visible:boolean;
     class procedure Show(AOwner: TFmxObject);
     class procedure Hide;
  end;

 {$ENDREGION}

 {$REGION ������������������� ��������� ��� ������������ ������}
  procedure SafeFree(AObject: TObject); overload;
  procedure SafeFree(AObject: TList<TObject>); overload;
 {$ENDREGION}

implementation

{$REGION '��������-����� TLogger ����������'}

class procedure TLogger.SendMessage(AMsg: String);
var
 Message: TMessage;
begin
 Message := TMessage<String>.Create(AMsg);
 TMessageManager.DefaultManager.SendMessage(self.Instance, Message, True);
end;

constructor TLogger.Create;
  begin
     raise EInvalidOperation.Create('�������� ����� ��� ���������');
  end;

destructor TLogger.Destroy;
  begin
   try
      if Assigned(FCritical) then
      begin
        try
          try
            FLogStrings.DisposeOf;
            FCritical.DisposeOf;
          finally
            FCritical := nil;
          end;
        except
          // ���������� ����� ���������� ��� �������������� �������� ��������� ������
        end;
      end;
    finally
      inherited Destroy;
    end;
  end;

class function TLogger.GetInstance: TLogger;
  begin
    if Assigned(FCritical) then FCritical.Acquire;
    try
      Result := FInstance;
    finally
     if Assigned(FCritical) then FCritical.Release;
    end;
  end;

constructor TLogger.InternalCreate(const ADummy: Integer);
 begin
  inherited Create;

  FCritical := System.SyncObjs.TCriticalSection.Create;
  FLogStrings := TStringList.Create;

  if Assigned(FCritical) then FCritical.Acquire;
  try
    FInstance := nil;
    FFilePath :=
    {$IF DEFINED(IOS)}
      System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetDocumentsPath, 'log')
    {$ELSEIF DEFINED(ANDROID)}
      System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetSharedDocumentsPath, 'log')
    {$ELSEIF DEFINED(LINUX)}
      '/var/log'     // do not localize
    {$ELSEIF DEFINED(MACOS)}
      System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetDocumentsPath, 'log')
    {$ELSE}
      System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetHomePath, 'Log')
    {$ENDIF}
      ;
  finally
    if Assigned(FCritical) then FCritical.Release;
  end;
end;

class procedure TLogger.Save(const AMessage, AValue: string; const ALogLevel: TLogLevel);
  var
  LText: string;
  LDateTime: TDateTime;
  LYear: Word;
  LMonth: Word;
  LDay: Word;
  LHour: Word;
  LMin: Word;
  LSec: Word;
  LMSec: Word;
  LFilePath: string;
  LFileName: string;
  LAppName: string;
{$IFDEF MSWINDOWS}
  LFile: THandle;
  LCount: DWORD;
{$ELSE}
  LStream: TFileStream;
  LBuffer: TBytes;
{$ENDIF}
begin
 try

  //self.SendMessage(AMessage);
  //self.FLogStrings.Add(AMessage);

  {$ifdef ANDROID}
    exit;
  {$ENDIF}

  try
    // ��������� ������ � ����
    LDateTime := System.SysUtils.Now;
    DecodeDate(LDateTime, LYear, LMonth, LDay);
    DecodeTime(LDateTime, LHour, LMin, LSec, LMSec);
    LText := FormatDateTime('yyyy-mm-dd hh:nn:ss', LDateTime) + #9 + '[' + ALogLevel.ToString + ']' + #9 + AMessage
      + #9 + AValue + System.sLineBreak;
    if Assigned(FCritical) then FCritical.Acquire;
    try
      {$ifdef MSWINDOWS}
       LAppName := ExtractFileName(ParamStr(0)).Replace('.exe', '').Replace('.app', '');
      {$else}
       LAppName := APP_NAME;
      {$endif}
      //self.SendMessage('AppName='+LAppName);
      LFilePath := System.IOUtils.TPath.Combine( FFilePath, LAppName );
      //self.SendMessage('FilePath='+LFilePath);
      LFileName :=
        System.IOUtils.TPath.Combine(
          {$IFDEF MSWINDOWS}'\\?\' + {$ENDIF}LFilePath,
          Format(DEFAULT_FILE_MASK, [LAppName, WordToStrAligned(LYear, 4), WordToStrAligned(LMonth), WordToStrAligned(LDay),
            WordToStrAligned(LHour), WordToStrAligned(LMin), WordToStrAligned(LSec)])
          );
      //self.SendMessage('LFileName='+LFileName);
    finally
      if Assigned(FCritical) then FCritical.Release;
    end;

    // �������� ������������� �������� �����
    {$IFNDEF LINUX}
    try
      //self.SendMessage('try to force dirs='+LFilePath);
      if not System.IOUtils.TDirectory.Exists(LFilePath) then
        System.SysUtils.ForceDirectories(LFilePath);
    except on E:Exception do
      // ���������� ����� ���������� ��� �������������� �������� ��������� ������
      //self.SendMessage(e.Message);
    end;
    {$ENDIF}

    self.FFileName := LFileName;
    //if not System.IOUtils.TDirectory.Exists(LFilePath) then
    // self.SendMessage('force dirs fails=');

    // ���������� � ����� �����
    if Assigned(FCritical) then FCritical.Acquire;
    try
    {$IFDEF MSWINDOWS}
      LFile := CreateFile(PWideChar(LFileName), GENERIC_READ or GENERIC_WRITE, FILE_SHARE_READ or FILE_SHARE_WRITE, nil,
        OPEN_ALWAYS, FILE_ATTRIBUTE_ARCHIVE, 0);
      if LFile <> INVALID_HANDLE_VALUE then
      begin
        try
          SetFilePointer(LFile, 0, nil, 2);
          WriteFile(LFile, Pointer(LText)^, Length(LText) * SizeOf(Char), LCount, nil);
        finally
          CloseHandle(LFile);
        end;
      end;
    {$ELSE}
      if System.IOUtils.TFile.Exists(LFileName) then
        LStream := TFileStream.Create(LFileName, fmOpenReadWrite)
      else
      begin
        //self.SendMessage('try to force dirs2='+ExtractFilePath(LFileName));
        ForceDirectories(ExtractFilePath(LFileName));
        //if Not DirectoryExists(ExtractFilePath(LFileName)) then
        // self.SendMessage('try to force dirs2 fails');
        LStream := TFileStream.Create(LFileName, fmCreate);
      end;
      try
        LStream.Seek(0, TSeekOrigin.soEnd);
        try
          LBuffer := TEncoding.UTF8.GetBytes(LText);
          LStream.WriteBuffer(LBuffer, Length(LBuffer));
        finally
          SetLength(LBuffer, 0);
        end;
      finally
        LStream.DisposeOf;
      end;

    {$ENDIF}

    finally
      if Assigned(FCritical) then FCritical.Release;
    end;

  except
    // ���������� ����� ���������� ��� �������������� �������� ��������� ������
  end;
 finally
  //self.SendMessage(AMessage);
 end;
end;

class procedure TLogger.Save(const AMessage: string;
  const ALogLevel: TLogLevel = Info);
begin
 self.Save(AMessage, '', ALogLevel);
end;

class procedure TLogger.SetInstance(const AValue: TLogger);
  begin
    if Assigned(FCritical) then FCritical.Acquire;
    try
      FInstance := AValue;
    finally
      if Assigned(FCritical) then FCritical.Release;
    end;
  end;

 {$ENDREGION}
 {$REGION 'TLogLevel'}

 class function TLogger.WordToStrAligned(const AValue: Word; const ADigits: Byte): string;
  begin
    Result := IntToStr(AValue);
     while Length(Result) < ADigits do
      Result := '0' + Result;
  end;

 function TLogLevelHelper.ToInteger: Integer;
  begin
    Result := Integer(Self);
  end;

function TLogLevelHelper.ToString: string;
  begin
    Result := LEVEL_CODES[Self];
  end;

{$ENDREGION}

 procedure InternalSafeFree(AObject: TObject); overload;
  begin
    try
      if Assigned(AObject) then
        AObject.DisposeOf;
    except
      on E: Exception do
        TLogger.Save('[InternalSafeFree(TObject)] exception: ' + E.ClassName + ', detail: ' + E.Message, '', TLogLevel.Error);
    end;
  end;


procedure InternalSafeFree(AObject: TList<TObject>); overload;
var
  I: Integer;
  LFirst: Integer;
  LLast: Integer;
  LItem: TObject;
begin
  try
    if Assigned(AObject) then
    begin
      try
        if AObject.Count > 0 then
        begin
          LFirst := 0;
          LLast := AObject.Count - 1;
          for I := LLast downto LFirst do
          begin
            try
              try
                LItem := AObject[I];
                SafeFree(LItem);
              finally
                try
                  AObject[I] := nil;
                finally
                  AObject.Delete(I);
                end;
              end;
            except
              // ����������, � ��������� ������ ����������� ������ ��������� � ������ �� ���������
              on E: Exception do
                TLogger.Save('[InternalSafeFree(TList<TObject>)] index: ' + IntToStr(I) + ', exception: ' + E.ClassName
                  + ', detail: ' + E.Message, '', TLogLevel.Error);
            end;
          end;
        end;
      finally
        AObject.DisposeOf;
      end;
    end;
  except
    on E: Exception do
      TLogger.Save('[InternalSafeFree(TList<TObject>)] exception: ' + E.ClassName + ', detail: ' + E.Message, '',
        TLogLevel.Error);
  end;
end;

 procedure SafeFree(AObject: TObject); overload;
   begin
    try
      InternalSafeFree(AObject);
    except
      on E: Exception do
        TLogger.Save('[SafeFree(TObject)] exception: ' + E.ClassName + ', detail: ' + E.Message, '', TLogLevel.Error);
    end;
  end;

  procedure SafeFree(AObject: TList<TObject>); overload;
  begin
     try
      InternalSafeFree(AObject);
    except
      on E: Exception do
        TLogger.Save('[SafeFree(TList<TObject>)] exception: ' + E.ClassName + ', detail: ' + E.Message, '', TLogLevel.Error);
    end;
  end;

{ TLangManager }

class function TLangManager.Translate(AMsg: string): string;
var
 LLangVal:TLangValue;
 b:boolean;
begin
 //todo ����������� ����������� ����� ��������� ���������������

 b := Assigned(self.LangValues);
 if b then
  b := self.LangValues.TryGetValue(AMsg.ToUpper, LLangVal);

 if b then
  begin
   if self.Locale = 'RU' then
    result := LLangVal.RU
     else
   if self.Locale = 'EN' then
    result := LLangVal.EN
     else
    result := AMsg;
  end
   else
  begin
   if AMsg = 'smp_services_load_error' then
    result := '������ ��� �������� ������ �����'
     else
   if AMsg = 'choose_company' then
    result := '�������� ��������'
     else
   if AMsg = 'loading' then
    result := '�������� ������...'
     else
    result := AMsg;
  end;

end;

{ TToast }

class procedure TToast.Show(AMsg: string; ALong: boolean = false);
var
 LMsg:string;
 LMessage: TMessage;
begin

  LMsg := TLangManager.Translate(AMsg);

  {$ifdef ANDROID}
   if not ALong then
     TJToast.JavaClass.makeText(SharedActivityContext,
                                StrToJCharSequence(LMsg),TJToast.JavaClass.LENGTH_SHORT).show
      else
     TJToast.JavaClass.makeText(SharedActivityContext,
                                StrToJCharSequence(LMsg),TJToast.JavaClass.LENGTH_LONG).show;
  {$else}
    TThread.Synchronize(nil, procedure begin
    //���� ����� �������������� ����� ShowMessage ����� ������ (��� ���-�� ���), �� �����
    //1)� ������� � Conditional Defines ���������� ���� IN_APP_SHOW_MESSAGE
    //2)� ������ ��. ����� ����������� �� ������� �� ������ TToast
    //��������:
    // TMessageManager.DefaultManager.SubscribeToMessage(TMessage<String>, self.ProcessMessages);
    //3) � ������ ��. ����� ProcessMessages ���������� ����� ����� � ����������.
    //��������:
    //procedure TfrmMain.ProcessMessages(const Sender: TObject; const M: TMessage);
    //var
    // LMsg:string;
    //begin
    //
    // LMsg := (M as TMessage<String>).Value;
    //
    // if (Sender is TToast) then
    //  begin
    //   frmMessage.Position := TFormPosition.poScreenCenter;
    //   frmMessage.ShowMessage(LMsg); //ShowMessage - ����� ����� frmMessage, ������� ���������� Label-� ����� Lmsg � ���������� ����
    //  end;
    //
    //end;


      LMessage := TMessage<String>.Create(AMsg);
      TMessageManager.DefaultManager.SendMessage(self.NewInstance, LMessage, True);

    {$IFDEF IN_APP_SHOW_MESSAGE}
    {$ELSE}
      ShowMessage(LMsg);
    {$ENDIF}
    end);

  {$endif}

end;

{ TLangProvider }

constructor TLangProvider.Create;
begin
 self.FLangValues := TLangValues.Create;
end;

procedure TLangProvider.LoadData;
begin
 //todo
end;

{ TAppKeyboard }

class procedure TAppKeyboard.Hide;
begin
  if self.FService = nil then
    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(self.FService));

    self.FIsReady := self.FService <> nil;

  if self.FIsReady then
   if self.Visible then
    begin
     self.FIsVisible := false;
     self.FService.HideVirtualKeyboard;
    end;
end;

//class function TAppKeyboard.Visible: boolean;
//begin
//
//  if self.FService = nil then
//    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(self.FService));
//
//    self.FIsReady := self.FService <> nil;
//
//  result := false;
//   if self.FIsReady then
//    result := self.FService.VirtualKeyboardState in [TVirtualKeyboardState.Visible, TVirtualKeyboardState.Transient];
//
//end;

class procedure TAppKeyboard.Show(AOwner: TFmxObject);
//{$IFDEF IOS}
//begin
//  try
//    Screen.ActiveForm.Focused := nil;
//  except
//  end;
//end;
//{$ENDIF}
//{$IFDEF Android}
//var
//  TextView: JFMXEditText;
//    begin
//      try
//        begin
//          TextView := MainActivity.getEditText;
//          TextView.setFocusable(false);
//          TextView.setFocusableInTouchMode(false);
//          TextView.sho .showSoftInput(true);
//          TextView.clearFocus;
//          TextView.setFocusable(true);
//          TextView.setFocusableInTouchMode(true);
//        end
//      except
//      end;
//    end;
//{$ENDIF}
begin
 if self.FService = nil then
    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(self.FService));

    self.FIsReady := self.FService <> nil;

  if self.FIsReady then
   if not self.Visible then
    begin
     self.FIsVisible := true;
     self.FService.ShowVirtualKeyboard(AOwner);
    end;
end;

class function TAppKeyboard.Visible: boolean;
begin

  exit(self.FIsVisible);

  if self.FService = nil then
    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(self.FService));

    self.FIsReady := self.FService <> nil;

  result := false;
  if self.FIsReady then
   begin
     result := (TVirtualKeyboardState.Visible in self.FService.VirtualKeyboardState) or
               (TVirtualKeyboardState.Transient in self.FService.VirtualKeyboardState);
   end;

end;

initialization
  try
    TLogger.Instance := TLogger.InternalCreate;
  except
    TLogger.Instance := nil;
  end;

finalization
  try
    if Assigned(TLogger.Instance) then
    begin
      try
        TLogger.Instance.DisposeOf;
      finally
        TLogger.Instance := nil;
      end;
    end;
  except
    // ���������� ����� ���������� ��� �������������� �������� ��������� ������
  end;

end.
