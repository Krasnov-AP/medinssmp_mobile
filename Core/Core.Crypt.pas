﻿unit Core.Crypt;

interface

uses


 {$IFDEF MSWINDOWS}
  Winapi.Windows, Winapi.Messages,
  {$ENDIF}
  System.Classes, System.Types, System.SysUtils,
  System.StrUtils, System.IOUtils, System.SyncObjs, System.Generics.Collections,
  System.Messaging, ElAES, System.Math;

const

  DECRYPT_KEY = 'medinssofterium';

type

   ArrayOfByte = array of byte;

   TCrypter = class
   strict private
    class function StringToHex(S: AnsiString): AnsiString;
    class function HexToString(S: AnsiString): AnsiString;
    class function HexToString2(S: String): AnsiString;
    class function HexToByte(S: String): ArrayOfByte;
   public
    class function Decrypt(AInputStr:AnsiString; DoHexToString:boolean = true):AnsiString;
    class function Decrypt2(AInputStr:String):String; //работает только под винду
    class function Encrypt(AInputStr:AnsiString; WithStrToHex: Boolean = true):AnsiString;
  end;

  var
   DECRYPT_KEY_BYTE : TAESKey128 = (109, 101, 100, 105, 110, 115, 115, 111, 102, 116, 101, 114, 105, 117, 109, 0); //medinssofterium


implementation

class function TCrypter.Decrypt(AInputStr: AnsiString; DoHexToString:boolean = true): Ansistring;
var
  Source: TStringStream;
  Dest: TStringStream;
  Start, Stop: cardinal;
  Size: integer;
  Key: TAESKey128;
  str: AnsiString;
  buf: AnsiString;
begin
  // Convert hexadecimal to a strings before decrypting...
  Source := TStringStream.Create( );
  Dest   := TStringStream.Create( );

  try
    if DoHexToString then
     begin
      buf := AnsiReplaceStr(AInputStr, '00000000', '');
      str := HexToString2( buf );
     end
      else
       str := AInputStr;
    Source.Write(PAnsiChar(str)^, length(str));
    Source.Position := 0;
    // Start decryption...
    Size := Source.Size;
    Source.ReadBuffer(Size, SizeOf(Size));

    // Prepare key...
    FillChar(Key, SizeOf(Key), 0);
    Move(PAnsiChar(AnsiString(DECRYPT_KEY))^, Key, Min(SizeOf(Key), Length(DECRYPT_KEY)));
    key := DECRYPT_KEY_BYTE;

    // Decrypt now...
    DecryptAESStreamECB(Source, Source.Size - Source.Position, Key, Dest);

    //SetString(Result, PChar(Dest.Memory), Dest.Size div SizeOf(Char));

    Dest.position := 0;
    SetLength(str, dest.Size);
    Dest.Read(PAnsiChar(str)^, dest.Size);
    result := str;

  finally
    Source.DisposeOf;
    Dest.DisposeOf;
  end;
end;

class function TCrypter.Decrypt2(AInputStr: String): string;
var
  Source: TMemoryStream;
  Dest: TMemoryStream;
  Start, Stop: cardinal;
  Size: integer;
  Key: TAESKey128;
  str: arrayOfByte;

    function Hex2Str(aBuf: arrayOfByte): string;
    var
      i: integer;
      bufLength: integer;
    begin
      bufLength := Length(aBuf);
      Result := '';
      for i := 0 to BufLength - 1 do
      begin
        Result := Result + Chr(aBuf[i]);
      end;
    end;


begin
  Source := TMemoryStream.Create( );
  Dest   := TMemoryStream.Create( );

  try
    str := HexToByte(AInputStr);
    Source.Write(str[0], length(str));
    Source.Position := 0;
    Size := Source.Size;
    Source.ReadBuffer(Size, SizeOf(Size));

    FillChar(Key, SizeOf(Key), 0);
    key := DECRYPT_KEY_BYTE;
    DecryptAESStreamECB(Source, Source.Size - Source.Position, Key, Dest);

    Dest.position := 0;
    SetLength(str, dest.Size);
    Dest.Read(str[0], dest.Size);
    result := Hex2Str(str);

  finally
    Source.DisposeOf;
    Dest.DisposeOf;
  end;
end;

class function TCrypter.Encrypt(AInputStr: Ansistring; WithStrToHex: Boolean = true): Ansistring;
var
  SS, DS: TStringStream;
  Size: Int64;
  AESKey128: TAESKey128;
  key: TAESKey128;
  str: AnsiString;
begin
  Result := '';
  SS := TStringStream.Create(AInputStr);
  DS := TStringStream.Create('');
  try
    Size := SS.Size;
    DS.WriteBuffer(Size, SizeOf(Size));

    key := DECRYPT_KEY_BYTE;

      FillChar(AESKey128, SizeOf(AESKey128), 0 );
      Move(PAnsiChar(AnsiString(DECRYPT_KEY))^, Key, Min(SizeOf(Key), Length(DECRYPT_KEY)));
      key := DECRYPT_KEY_BYTE;
      EncryptAESStreamECB(SS, 0, Key, DS);

    DS.position := 0;
    SetLength(str, DS.Size);
    DS.Read(PAnsiChar(str)^, DS.Size);
    result := str;

    if WithStrToHex then
      Result := StringToHex(result)
    else
      Result := result;
  finally
    SS.Free;
    DS.Free;
  end;
end;

class function TCrypter.HexToByte(S: String): ArrayOfByte;
var
  i: Integer;
  sHexLength: Integer;
  sHexWithoutSpaces: string;
begin
  sHexWithoutSpaces := '';
  for i := 1 to Length(S) do
   if S[i] <>  ' ' then
     sHexWithoutSpaces := sHexWithoutSpaces + S[i];

  sHexLength := Length(sHexWithoutSpaces);

  if (sHexLength > 0) and (sHexLength mod 2 = 0) then
  begin
    SetLength(Result, sHexLength div 2);
    for i := 0 to High(Result) do
      Result[i] := StrToInt('0x' + Copy(sHexWithoutSpaces, i * 2 + 1, 2));
  end;
end;

class function TCrypter.HexToString(S: AnsiString): AnsiString;
var
	i: integer;

begin
  Result := '';

  // Go throught every single hexadecimal characters, and convert
  // them to ASCII characters...
  for i := 1 to Length( S ) do
  begin
    // Only process chunk of 2 digit Hexadecimal...
  	if ((i mod 2) = 1) then
	  	Result := Result + AnsiChar( StrToInt( '0x' + Copy( S, i, 2 )));
  end;
end;


class function TCrypter.HexToString2(S: String): AnsiString;
var
	i: integer;

begin
  Result := '';

  SetLength(Result, Length(s) div 2);
  HexToBin(PChar(s), PAnsiChar(Result), Length(Result));
end;

class function TCrypter.StringToHex(S: AnsiString): AnsiString;
var
	i: integer;

begin
  Result := '';

  // Go throught every single characters, and convert them
  // to hexadecimal...
	for i := 1 to Length( S )  do
  	Result := Result + IntToHex( ord( S[i] ), 2 );
end;


end.
