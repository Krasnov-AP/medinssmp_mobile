﻿/// <summary>
///   <para>Модуль поддержки протоколирования необработанных исключений в приложении.</para>
///   <para>Дополнительно для Android и iOS протоколируется стек вызовов в момент возникновения исключения.</para>
/// </summary>
/// <remarks>
///   <list type="table">
///     <listheader>
///       <term>Параметр</term>
///       <description>Значение</description>
///     </listheader>
///     <item>
///       <term>Версия модуля</term>
///       <description>1.0</description>
///     </item>
///     <item>
///       <term>Дата последнего изменения</term>
///       <description>16.Mar.2021</description>
///     </item>
///     <item>
///       <term>Поддерживаемые платформы разработки</term>
///       <description>Embarcadero Delphi 10.4 Sydney Update 2 и выше</description>
///     </item>
///     <item>
///       <term>Поддерживаемые ОС</term>
///       <description>
///         <p>Windows 7 (SP1+)/8.1/10 (x32/x64)</p>
///         <p>Windows Server 2012 R2/2016/2019 (x32/x64)</p>
///         <p>macOS 10.13/10.14/10.15/11.1 (x64)</p>
///         <p>Android 6/7/8/9/10</p>
///         <p>iOS 11/12/13/14</p>
///       </description>
///     </item>
///   </list>
/// </remarks>
unit Core.ErrorReporting;

{$REGION 'Инструкция по установке и использованию модуля'}

///
///  Инструкция по установке и использованию модуля
///  ----------------------------------------------
///    1. Для Android:
///       1.1. Выбрать "Project | Options..." -> "All configurations - Android platform"
///       1.2. Выбрать закладку "Delphi Compiler | Linking"
///       1.3. В поле "Options passed to the LD linker" установить:
///            --version-script=Core.ErrorReporting.vsr
///            Примечание: возможно потребуется установить полный или относительный путь Core.ErrorReporting.Sydney.vsr
///    2. Установить обработчик протоколирования:
///         Application.OnException := TErExceptionReporter.ExceptionHandler;
///    3. Реализовать у формы обработчик события
///         procedure RecieveExceptionReport(const Sender: TObject; const M: TMessage);
///    4. Подписаться в конструкторе формы на сообщения типа TErExceptionReportMessage:
///         TMessageManager.DefaultManager.SubscribeToMessage(TErExceptionReportMessage, RecieveExceptionReport);
///    5. Отписаться в деструкторе формы от сообщений типа TErExceptionReportMessage:
///         TMessageManager.DefaultManager.Unsubscribe(TErExceptionReportMessage, RecieveExceptionReport, True);
///    6. Важно: модуль System.Messaging должен быть объявлен ПОСЛЕ модуля Winapi.Messages (если последний используется)
///
///  Пример использования
///  --------------------
///    procedure TMainForm.FormCreate(Sender: TObject);
///    begin
///      Application.OnException := TErExceptionReporter.ExceptionHandler;
///      TMessageManager.DefaultManager.SubscribeToMessage(TErExceptionReportMessage, RecieveExceptionReport);
///    end;
///
///    procedure TMainForm.FormDestroy(Sender: TObject);
///    begin
///      TMessageManager.DefaultManager.Unsubscribe(TErExceptionReportMessage, RecieveExceptionReport, True);
///    end;
///
///    procedure TMainForm.RecieveExceptionReport(const Sender: TObject; const M: TMessage);
///    var
///      LReport: IErExceptionReport;
///    begin
///      if M is TErExceptionReportMessage then
///      begin
///        LReport := TErExceptionReportMessage(M).Report;
///        { Далее добавить необходимый код обработки. Например можно:
///          - отправить отчет в утилиту удаленного логгирования, вариант:
///              TLogger.Send(LReport.Report, TLogLevel.Error);
///          - записать отчет в локальный файл, вариант:
///              TFile.WriteAllText(
///              {$IFDEF ANDROID}
///                TPath.Combine(TPath.GetSharedDocumentsPath, 'ErrorReport.txt'),
///              {$ELSE}
///                TPath.Combine(TPath.GetDocumentsPath, 'ErrorReport.txt'),
///              {$ENDIF}
///                LReport.Report
///                );
///          - отправить e-mail с отчетом;
///          - отправить отчет в облачный сервер;
///          - сообщить о проблеме пользователю. При этом необходимо помнить, что сообщение может придти с потока,
///            не являющимся основным UI потоком. Поэтому необходимо соблюдать правила работы с визуальными
///            компонентами FMX из других потоков;
///          - и т.д.
///        Также необходимо помнить, что приложение может быть в нестабильном состоянии (т.к. сработал данный обработчик
///        необработанного исключения в самом коде приложения). Поэтому одним из вариантов является сохранение отчета
///        в локальный файл и завершение приложения (вызовом Halt), а после нового запуска приложения проверить данный
///        локальный файл и тогда уже обработать его (см. варианты выше).
///        }
///      end;
///    end;
///

{$ENDREGION}

interface

{$REGION 'модули'}

uses
  {$REGION 'стандартные модули для Android и iOS'}
  {$IF DEFINED(ANDROID) OR DEFINED(IOS)}
  Posix.Dlfcn, Posix.Stdlib,
  {$ENDIF}
  {$ENDREGION}
  {$REGION 'мульти-платформенные стандартные модули'}
  System.Classes, System.SysUtils, System.Messaging, System.Generics.Collections,
  {$ENDREGION}
  {$REGION 'модули приложения'}
  Core.Common;
  {$ENDREGION}

{$ENDREGION}

{$REGION 'типы данных'}

type
  /// <summary>
  ///   Событие TApplication.OnException
  /// </summary>
  TErExceptionEvent = procedure(Sender: TObject; E: Exception) of object;

  /// <summary>
  ///   Элемент стека вызовов
  /// </summary>
  TErCallStackEntry = record
  public
    {$REGION 'общедоступные свойства'}

    /// <summary>
    ///   Адрес кода в стеке
    /// </summary>
    CodeAddress: UIntPtr;

    /// <summary>
    ///   Адрес начала метода, содержащего код, в стеке
    /// </summary>
    /// <remarks>
    ///   Значение "CodeAddress - RoutineAddress" представляет собой смещение кода в методе (в стеке вызовов)
    /// </remarks>
    RoutineAddress: UIntPtr;

    /// <summary>
    ///   Базовый адрес модуля, в котором найден адрес кода в стеке
    /// </summary>
    ModuleAddress: UIntPtr;

    /// <summary>
    ///   Имя метода по адресу кода в стеке, если доступно
    /// </summary>
    RoutineName: string;

    /// <summary>
    ///   Имя модуля, в котором расположен адрес кода в стеке. Если модуль неопределен, то содержится имя исполняемого
    ///   модуля (в Android имя .so-файла)
    /// </summary>
    ModuleName: string;

    {$ENDREGION}
    {$REGION 'общедоступные методы'}

    /// <summary>
    ///   Очистка внутренних данных
    /// </summary>
    procedure Clear;

    {$ENDREGION}
  end;

  /// <summary>
  ///   Стек вызовов
  /// </summary>
  TErCallStack = TArray<TErCallStackEntry>;

{$ENDREGION}

{$REGION 'интерфейсы'}

type
  /// <summary>
  ///   Отчет об исключении
  /// </summary>
  /// <remarks>
  ///   При обнаружении необработанного исключения создается отчет о нем и производится рассылка TErExceptionReportMessage
  /// </remarks>
  IErExceptionReport = interface
  ['{A949A858-3B30-4C39-9A18-AD2A86B4BD9F}']
    {$REGION 'методы общедоступных свойств'}

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|IErExceptionReport.ExceptionMessage">ExceptionMessage</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    function _GetExceptionMessage: string;

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|IErExceptionReport.ExceptionLocation">ExceptionLocation</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    function _GetExceptionLocation: TErCallStackEntry;

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|IErExceptionReport.CallStack">CallStack</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    function _GetCallStack: TErCallStack;

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|IErExceptionReport.Report">Report</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    function _GetReport: string;

    {$ENDREGION}
    {$REGION 'общедоступные свойства'}

    /// <summary>
    ///   Текст сообщения исключения
    /// </summary>
    /// <remarks>
    ///   Содержит значение свойства Exception.Message
    /// </remarks>
    property ExceptionMessage: string read _GetExceptionMessage;

    /// <summary>
    ///   Информация о возникновении исключения
    /// </summary>
    property ExceptionLocation: TErCallStackEntry read _GetExceptionLocation;

    /// <summary>
    ///   Стек вызовов в момент возникновения исключения
    /// </summary>
    /// <remarks>
    ///   Также включаются вызовы в сам обработчик исключений
    /// </remarks>
    property CallStack: TErCallStack read _GetCallStack;

    /// <summary>
    ///   Текстовая версия исключения
    /// </summary>
    /// <remarks>
    ///   Для Android и iOS ткже включается информация о стеке вызовов (тоже в текстовом виде)
    /// </remarks>
    property Report: string read _GetReport;

    {$ENDREGION}
  end;

{$ENDREGION}

{$REGION 'классы'}

type
  /// <summary>
  ///   Широковещательное сообщение об отчетах необработанных исключений
  /// </summary>
  /// <remarks>
  ///   <para>Для получения данного типа сообщений необходимо подписаться на него
  ///    (с помощью TMessageManager.DefaultManager.SubscribeToMessage)</para>
  ///   <para>ВАЖНО: сообщение может придти с потока, не являющимся основным UI потоком. Поэтому необходимо соблюдать
  ///     правила работы с визуальными компонентами FMX из других потоков</para>
  /// </remarks>
  TErExceptionReportMessage = class(TMessage)
  strict private
    {$REGION 'внутренние данные'}

    /// <summary>
    ///   Внутренние данные свойства <see cref="Core.ErrorReporting|TErExceptionReportMessage.Report">Report</see>
    /// </summary>
    FReport: IErExceptionReport;

    {$ENDREGION}
  public
    {$REGION 'общедоступные свойства'}

    /// <summary>
    ///   Отчет об исключении
    /// </summary>
    property Report: IErExceptionReport read FReport;

    {$ENDREGION}
    {$REGION 'конструктор'}

    /// <summary>
    ///   Конструктор
    /// </summary>
    /// <param name="AReport">
    ///   Отчет об исключении
    /// </param>
    /// <remarks>
    ///   Используется для внутреннего создания сообщения
    /// </remarks>
    constructor Create(const AReport: IErExceptionReport);

    {$ENDREGION}
  end;

  /// <summary>
  ///   Генератор отчетов и уведомлений подписчикам о возникающих необработанных исключениях
  /// </summary>
  TErExceptionReporter = class sealed
  strict private
    {$REGION 'внутренние данные'}

    class var
      /// <summary>
      ///   Внутренние данные свойства <see cref="Core.ErrorReporting|TErExceptionReporter.Instance">Instance</see>
      /// </summary>
      FInstance: TErExceptionReporter;

    {$ENDREGION}
  strict private
    {$REGION 'внутренние данные'}

    /// <summary>
    ///   Внутренние данные свойства
    ///   <see cref="Core.ErrorReporting|TErExceptionReporter.MaxCallStackDepth">MaxCallStackDepth</see>
    /// </summary>
    FMaxCallStackDepth: Integer;

    /// <summary>
    ///   Внутренние данные свойства
    ///   <see cref="Core.ErrorReporting|TErExceptionReporter.ModuleAddress">ModuleAddress</see>
    /// </summary>
    FModuleAddress: UIntPtr;

    /// <summary>
    ///   Внутренние данные свойства
    ///   <see cref="Core.ErrorReporting|TErExceptionReporter.ReportingException">ReportingException</see>
    /// </summary>
    FReportingException: Boolean;

    {$ENDREGION}
    {$REGION 'методы общедоступных свойств'}

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|TErExceptionReporter.Instance">Instance</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    class function GetInstance: TErExceptionReporter; static;

    /// <summary>
    ///   Запись свойства <see cref="Core.ErrorReporting|TErExceptionReporter.Instance">Instance</see>
    /// </summary>
    /// <param name="AValue">
    ///   Записываемое значение свойства
    /// </param>
    class procedure SetInstance(const AValue: TErExceptionReporter); static;

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|TErExceptionReporter.ExceptionHandler">ExceptionHandler</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    class function GetExceptionHandler: TErExceptionEvent; static;

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|TErExceptionReporter.MaxCallStackDepth">MaxCallStackDepth</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    class function GetMaxCallStackDepth: Integer; static;

    /// <summary>
    ///   Запись свойства <see cref="Core.ErrorReporting|TErExceptionReporter.MaxCallStackDepth">MaxCallStackDepth</see>
    /// </summary>
    /// <param name="AValue">
    ///   Записываемое значение свойства
    /// </param>
    class procedure SetMaxCallStackDepth(const AValue: Integer); static;

    {$ENDREGION}
    {$REGION 'внутренние методы'}

    /// <summary>
    ///   Создание отчета об исключении
    /// </summary>
    /// <param name="AExceptionObject">
    ///   Объект исключения
    /// </param>
    /// <param name="AExceptionAddress">
    ///   Адрес возникновения исключения
    /// </param>
    procedure ReportException(const AExceptionObject: TObject; const AExceptionAddress: Pointer);

    /// <summary>
    ///   Конвертация данных стека TCallStack в TErCallStack
    /// </summary>
    /// <param name="AStackInfo">
    ///   Указатель на TCallStack
    /// </param>
    /// <returns>
    ///   Данные стека в TErCallStack
    /// </returns>
    class function GetCallStack(const AStackInfo: Pointer): TErCallStack; static;

    { TODO : вставить описание }
    class function GetCallStackEntry(var AEntry: TErCallStackEntry): Boolean; static;

    {$ENDREGION}
    {$REGION 'обработчики глобальных событий работы с исключениями'}

    { TODO : вставить описание }
    procedure GlobalHandleException(Sender: TObject; E: Exception);

    { TODO : вставить описание }
    class procedure GlobalExceptionAcquiredHandler(AObj: Pointer); static;

    { TODO : вставить описание }
    class procedure GlobalExceptHandler(ExceptObject: TObject; ExceptAddr: Pointer); static;

    { TODO : вставить описание }
    class function GlobalGetExceptionStackInfo(P: PExceptionRecord): Pointer; static;

    /// <summary>
    ///   Освобождение памяти, выделенной GlobalGetExceptionStackInfo
    /// </summary>
    /// <param name="AInfo">
    ///   Освобождаемая память
    /// </param>
    class procedure GlobalCleanUpStackInfo(AInfo: Pointer); static;

    {$ENDREGION}
  private
    {$REGION 'внутренний конструктор'}

    /// <summary>
    ///   Конструктор
    /// </summary>
    /// <param name="ADummy">
    ///   Параметр для исключения ложного предупреждения компилятора
    /// </param>
    constructor InternalCreate(const ADummy: Integer = 0);

    {$ENDREGION}
  public
    {$REGION 'общедоступные свойства'}

    /// <summary>
    ///   Экземпляр генератора
    /// </summary>
    class property Instance: TErExceptionReporter read GetInstance write SetInstance;

    /// <summary>
    ///   Обработчик необработанных исключений в главном потоке (UI) приложения
    /// </summary>
    /// <remarks>
    ///   <para>Обработчик вызывается только для необработанных исключений, возникших в главном потоке приложения</para>
    ///   <para>В главном окне приложения выполнить: Application.OnException := TErExceptionReporter.ExceptionHandler;</para>
    /// </remarks>
    class property ExceptionHandler: TErExceptionEvent read GetExceptionHandler;

    /// <summary>
    ///   Максимальная глубина стека вызовов
    /// </summary>
    /// <remarks>
    ///   <para>При каждом возникновении исключения обрабатывается стек вызовов. Необходимо помнить, что формирование
    ///   стека весьма долгая операция, поэтому чем больше глубина, тем дольше стек формируется</para>
    ///   <para>Значение 0 отключает формирование стека</para>
    /// </remarks>
    class property MaxCallStackDepth: Integer read GetMaxCallStackDepth write SetMaxCallStackDepth;

    {$ENDREGION'}
    {$REGION 'конструктор'}

    /// <summary>
    ///   Констуктор
    /// </summary>
    /// <remarks>
    ///   Вызов конструктора вручную запрещен, т.к. это singleton
    /// </remarks>
    constructor Create;

    {$ENDREGION'}
  end;

{$ENDREGION}

{$REGION 'глобальные процедуры и функции'}

/// <summary>
///   Конвертация представления имен C++ в имена Pascal
/// </summary>
/// <param name="ASymbol">
///   Имя в представлении C++ для конвертации
/// </param>
/// <returns>
///   Транслированное имя в Pascal
/// </returns>
/// <remarks>
///   Данная функция осуществляет трасляцию имен C++ в имена Pascal только приблизительно, т.к. обрабатываются не все
///   возможные конструкции C++. Поэтому в некоторых случаях может вернуться нетранслируемое значение.
///   Тем не менее, результат позволит посмотреть соответствующий код в исходных кодах приложения.
/// </remarks>
function ErCppSymbolToPascal(const ASymbol: string): string;

{$ENDREGION}

implementation

{$REGION 'классы'}

type
  /// <summary>
  ///   Отчет об исключении
  /// </summary>
  TErExceptionReport = class(TInterfacedObject, IErExceptionReport)
  strict private
    {$REGION 'внутренние данные'}

    /// <summary>
    ///   Стек вызовов
    /// </summary>
    FCallStack: TErCallStack;

    /// <summary>
    ///   Информация об исключении
    /// </summary>
    FExceptionLocation: TErCallStackEntry;

    /// <summary>
    ///   Текстовое сообщение исключения
    /// </summary>
    FExceptionMessage: string;

    /// <summary>
    ///   Отчет об исключении
    /// </summary>
    FReport: string;

    {$ENDREGION}
    {$REGION 'внутренние методы'}

    /// <summary>
    ///   Конвертация адреса в строку
    /// </summary>
    /// <param name="AAddress">
    ///   Адрес
    /// </param>
    /// <returns>
    ///   Адрес в текстовом виде
    /// </returns>
    class function AddressToString(const AAddress: UIntPtr): string; static;

    /// <summary>
    ///   Создание отчета об исключении
    /// </summary>
    /// <returns>
    ///   Отчет об исключении в текстовом виде
    /// </returns>
    function BuildReport: string;

    {$ENDREGION}
  protected
    {$REGION 'реализация методов интерфейса IErExceptionReport'}

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|IErExceptionReport.ExceptionMessage">ExceptionMessage</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    function _GetExceptionMessage: string;

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|IErExceptionReport.ExceptionLocation">ExceptionLocation</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    function _GetExceptionLocation: TErCallStackEntry;

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|IErExceptionReport.CallStack">CallStack</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    function _GetCallStack: TErCallStack;

    /// <summary>
    ///   Чтение свойства <see cref="Core.ErrorReporting|IErExceptionReport.Report">Report</see>
    /// </summary>
    /// <returns>
    ///   Значение свойства
    /// </returns>
    function _GetReport: string;

    {$ENDREGION}
  public
    {$REGION 'конструктор'}

    /// <summary>
    ///   Конструктор
    /// </summary>
    /// <param name="AExceptionMessage">
    ///   Текстовое сообщение исключения
    /// </param>
    /// <param name="AExceptionLocation">
    ///   Информация об исключении
    /// </param>
    /// <param name="ACallStack">
    ///   Стек вызовов
    /// </param>
    constructor Create(const AExceptionMessage: string; const AExceptionLocation: TErCallStackEntry;
      const ACallStack: TErCallStack);

    {$ENDREGION}
  end;

  /// <summary>
  ///   Конвертор представления имен C++ в имена Pascal
  /// </summary>
  TErSymbolConverter = class
  strict private
    {$REGION 'внутренние данные'}

    class var
      /// <summary>
      ///   Список типов данных
      /// </summary>
      FTypeMap: TDictionary<string, string>;

      /// <summary>
      ///   Список операторов
      /// </summary>
      FOperatorMap: TDictionary<string, string>;

    {$ENDREGION}
    {$REGION 'внутренние методы'}

    class function ConvertCtorDtor(const AStr: string): string; static;
    class function ConvertOperatorName(const AName: string): string; static;
    class function ConvertSet(const AStr: string): string; static;
    class function ConvertSpecialFunctionName(const AStr: string): string; static;
    class function ConvertStaticArray(const AStr: string): string; static;
    class function ParseFunctionHeading(var AStr: PChar): string; static;
    class function ParseGenericArg(var AStr: PChar): string; static;
    class function ParseGenericArgs(var AStr: PChar): string; static;
    class function ParseName(var AStr: PChar): string; static;
    class function ParseParam(var AStr: PChar): string; static;
    class function ParseParams(var AStr: PChar): string; static;
    class function ParseQualifiedName(var AStr: PChar): string; overload; static; inline;
    class function ParseQualifiedName(var AStr: PChar; out AInSystem: Boolean): string; overload; static;

    class function ParseRemainder(var AStr: PChar): string; static;
    class procedure SkipWhitespace(var AStr: PChar); static; inline;

    {$ENDREGION}
  public
    {$REGION 'конструктор/деструктор'}

    /// <summary>
    ///   Конструктор
    /// </summary>
    class constructor Create;

    /// <summary>
    ///   Деструктор
    /// </summary>
    class destructor Destroy;

    {$ENDREGION}
    {$REGION 'общедоступные методы'}

    /// <summary>
    ///   <para>Конвертация представления имен C++ в имена Pascal:</para>
    ///   <para>ASymbol -> FunctionHeading ('::' FunctionHeading)*</para>
    /// </summary>
    /// <param name="ASymbol">
    ///   Имя в представлении C++ для конвертации
    /// </param>
    /// <returns>
    ///   Транслированное имя в Pascal
    /// </returns>
    /// <remarks>
    ///   <para>Также разрешены вложенные методы. Например:</para>
    ///   <para>  Foo(int)::Bar(float)</para>
    ///   <para>транслируется в</para>
    ///   <para>  Foo(Integer).Bar(Single)</para>
    ///   <para>где Bar это вложенный метод внутри метода Foo.</para>
    /// </remarks>
    class function CppToPascal(const ASymbol: string): string; static;

    {$ENDREGION}
  end;

{$ENDREGION}

{$REGION 'типы данных'}

{$REGION 'TErCallStackEntry'}

procedure TErCallStackEntry.Clear;
begin
  CodeAddress := 0;
  RoutineAddress := 0;
  ModuleAddress := 0;
  RoutineName := '';
  ModuleName := '';
end;

{$ENDREGION}

{$ENDREGION}

{$REGION 'классы'}

{$REGION 'TErExceptionReport'}

{$REGION 'TErExceptionReport - внутренние методы'}

class function TErExceptionReport.AddressToString(const AAddress: UIntPtr): string;
begin
  Result := '$' + IntToHex(AAddress, {$IFDEF CPU64BITS} 16 {$ELSE} 8 {$ENDIF});
end;

function TErExceptionReport.BuildReport: string;
var
  LSB: TStringBuilder;
  LEntry: TErCallStackEntry;
begin
  LSB := TStringBuilder.Create;
  try
    with LSB do
    begin
      AppendLine(FExceptionMessage);
      Append('At address: ');
      Append(AddressToString(FExceptionLocation.CodeAddress));
      if not FExceptionLocation.RoutineName.IsEmpty then
      begin
        Append(' (');
        Append(FExceptionLocation.RoutineName);
        Append(' + ');
        Append(FExceptionLocation.CodeAddress - FExceptionLocation.RoutineAddress);
        AppendLine(')');
      end
      else
        AppendLine;

      AppendLine;
      if FCallStack <> nil then
      begin
        AppendLine('Call stack:');
        for LEntry in FCallStack do
        begin
          AppendFormat('%-25s %s', [ExtractFilename(LEntry.ModuleName), AddressToString(LEntry.CodeAddress)]);
          if not LEntry.RoutineName.IsEmpty then
          begin
            Append(' ');
            Append(LEntry.RoutineName);
            Append(' + ');
            Append(LEntry.CodeAddress - LEntry.RoutineAddress);
          end;
          AppendLine;
        end;
      end;
    end;
    Result := LSB.ToString;
  finally
    SafeFree(LSB);
  end;
end;

{$ENDREGION}

{$REGION 'TErExceptionReport - реализация методов интерфейса IErExceptionReport'}

function TErExceptionReport._GetExceptionMessage: string;
begin
  Result := FExceptionMessage;
end;

function TErExceptionReport._GetExceptionLocation: TErCallStackEntry;
begin
  Result := FExceptionLocation;
end;

function TErExceptionReport._GetCallStack: TErCallStack;
begin
  Result := FCallStack;
end;

function TErExceptionReport._GetReport: string;
begin
  if FReport.IsEmpty then
    FReport := BuildReport;
  Result := FReport;
end;

{$ENDREGION}

{$REGION 'TErExceptionReport - конструктор'}

constructor TErExceptionReport.Create(const AExceptionMessage: string; const AExceptionLocation: TErCallStackEntry;
  const ACallStack: TErCallStack);
begin
  inherited Create;

  FExceptionMessage := AExceptionMessage;
  FExceptionLocation := AExceptionLocation;
  FCallStack := ACallStack;
end;

{$ENDREGION}

{$ENDREGION}

{$REGION 'TErSymbolConverter'}

{$REGION 'TErSymbolConverter - внутренние методы'}

class function TErSymbolConverter.ConvertCtorDtor(const AStr: string): string;
{ Convert constructor and destructor.
  * Foo.TBar.TBar -> Foo.TBar.Create
  * Foo.TBar.~TBar -> Foo.TBar.Destroy }
var
  I: Integer;
  TypeName: string;
begin
  I := AStr.LastIndexOf('.');
  if (I < 0) then
    Exit(AStr);

  if (I < (AStr.Length - 1)) and (AStr.Chars[I + 1] = '~') then
    Result := AStr.Substring(0, I) + '.Destroy'
  else
  begin
    TypeName := AStr.Substring(I);
    Result := AStr.Substring(0, I);
    if (Result.EndsWith(TypeName)) then
      Result := Result + '.Create'
    else
      Result := AStr;
  end;
end;

class function TErSymbolConverter.ConvertOperatorName(const AName: string): string;
begin
  if (not FOperatorMap.TryGetValue(AName, Result)) then
    Result := AName;
end;

class function TErSymbolConverter.ConvertSet(const AStr: string): string;
{ Set<XYZ, ...> -> set of XYZ }
var
  I: Integer;
begin
  I := AStr.IndexOf(',');
  if (I < 0) then
    Result := AStr
  else
    Result := 'set of ' + AStr.Substring(4, I - 4);
end;

class function TErSymbolConverter.ConvertSpecialFunctionName(const AStr: string): string;
{ Convert constructor and destructor.
  * Foo.Finalization() -> Foo.finalization }
begin
  if (AStr.EndsWith('initialization()')) then
    Result := AStr.Substring(0, AStr.Length - 16) + 'initialization'
  else if (AStr.EndsWith('Finalization()')) then
    Result := AStr.Substring(0, AStr.Length - 14) + 'finalization'
  else
    Result := AStr;
end;

class function TErSymbolConverter.ConvertStaticArray(const AStr: string): string;
{ StaticArray<XYZ, N> -> array [0..N-1] of XYZ }
var
  CommaPos, N: Integer;
begin
  CommaPos := AStr.IndexOf(',');
  if (CommaPos < 0) then
    Exit(AStr);

  N := StrToIntDef(AStr.Substring(CommaPos + 1, AStr.Length - CommaPos - 2), 0);
  if (N = 0) then
    Exit(AStr);

  Result := 'array [0..' + IntToStr(N - 1) + '] of ' + AStr.Substring(12, CommaPos - 12);
end;

class function TErSymbolConverter.ParseFunctionHeading(var AStr: PChar): string;
{ FunctionHeading -> QualifiedName '(' Params ')' }
begin
  Result := ParseQualifiedName(AStr);
  Result := ConvertCtorDtor(Result);

  if (AStr^ <> '(') then
    Exit(Result + ParseRemainder(AStr));

  Inc(AStr);
  Result := Result + '(' + ParseParams(AStr);

  if (AStr^ <> ')') then
    Result := Result + ParseRemainder(AStr)
  else
  begin
    Inc(AStr);
    Result := Result + ')';
  end;

  Result := ConvertSpecialFunctionName(Result);
end;

class function TErSymbolConverter.ParseGenericArg(var AStr: PChar): string;
{ GenericArg -> ['(' QualifiedName ')'] Param
  The QualifiedName can be used in typecasts inside generic arguments, as in:
    (Foo::Bar)0 }
begin
  if (AStr^ = '(') then
  begin
    Inc(AStr);
    Result := ParseQualifiedName(AStr); { Ignore the typecast }
    if (AStr^ = ')') then
      Inc(AStr)
    else
      Exit(Result + ParseRemainder(AStr));
  end;
  Result := ParseParam(AStr);
end;

class function TErSymbolConverter.ParseGenericArgs(var AStr: PChar): string;
{ GenericArgs -> GenericArg (',' GenericArg)* }
begin
  Result := ParseGenericArg(AStr);
  while (AStr^ = ',') do
  begin
    Inc(AStr);
    SkipWhitespace(AStr);
    Result := Result + ', ' + ParseGenericArg(AStr);
  end;
end;

class function TErSymbolConverter.ParseName(var AStr: PChar): string;
{ Name -> NameChar+ ['<' GenericArgs '>']
  NameChar -> <any character except some delimiters> }
var
  I, J: Integer;
  P: PChar;
begin
  P := AStr;
  while True do
  begin
    case P^ of
      #0..#32,
      '<', '>',
      '(', ')',
      ',', ':':
        Break;
    else
      Inc(P);
    end;
  end;

  SetString(Result, AStr, P - AStr);

  { Rename class operators }
  if (AStr[0] = '_') and (AStr[1] = 'o') and (AStr[2] = 'p') and (AStr[3] = '_') then
    Result := ConvertOperatorName(Result);

  AStr := P;
  if (AStr^ = '<') then
  begin
    { When using generic arguments, the name may end with a discriminator,
      like "__1". Strip this. }
    I := Result.IndexOf('__');
    if (I > 0) then
    begin
      J := I + 2;
      while (J < Result.Length) and (Result.Chars[J] >= '0') and (Result.Chars[J] <= '9') do
        Inc(J);
      if (J = Result.Length) then
        Result := Result.Substring(0, I);
    end;

    Inc(AStr);
    Result := Result + '<' + ParseGenericArgs(AStr);
    SkipWhitespace(AStr);
    if (AStr^ <> '>') then
      Result := Result + ParseRemainder(AStr)
    else
    begin
      Inc(AStr);
      Result := Result + '>';
    end;
  end;
end;

class function TErSymbolConverter.ParseParam(var AStr: PChar): string;
{ Param -> QualifiedName+
  A parameter can take multiple names in these cases:
  * The C++ type takes multiple names, like 'signed char' or 'unsigned long long'.
  * The parameter has qualifiers, like 'const&' }
var
  CppName, Name, Suffix: string;
  InSystem, IsReference: Boolean;
  I: Integer;
begin
  CppName := ParseQualifiedName(AStr, InSystem);

  while (AStr^ <> #0) and (AStr^ <= ' ') do
  begin
    SkipWhitespace(AStr);
    Name := ParseQualifiedName(AStr);
    if (not Name.IsEmpty) and (Name <> 'const&') then
      CppName := CppName + ' ' + Name;
  end;

  { Check for references (int&) and pointers (int*, int** etc).
    Make exception for 'void*' which translates to Pointer. }
  IsReference := CppName.EndsWith('&');
  if IsReference then
    CppName := CppName.Substring(0, CppName.Length - 1);

  I := CppName.IndexOf('*');
  if (I < 0) then
    Suffix := ''
  else if (CppName.StartsWith('void*')) then
  begin
    Suffix := CppName.Substring(5);
    CppName := 'Pointer';
  end
  else
  begin
    Suffix := CppName.Substring(I);
    CppName := CppName.Substring(0, I);
  end;

  { Convert (C++) type to Delphi type }
  if (not FTypeMap.TryGetValue(CppName, Result)) then
    Result := CppName;

  { Special cases:
    * For names in the System namespace:
      * DynamicArray<XYZ> -> array of XYZ
      * StaticArray<XYZ, N> -> array [0..N-1] of XYZ
      * DelphiInterface<XYZ> -> XYZ
      * Set<XYZ, ...> -> set of XYZ
    * XYZ& -> var XYZ }

  if InSystem then
  begin
    if (Result.StartsWith('DynamicArray<')) then
      Result := 'array of ' + Result.Substring(13, Result.Length - 14)
    else if (Result.StartsWith('StaticArray<')) then
      Result := ConvertStaticArray(Result)
    else if (Result.StartsWith('DelphiInterface<')) then
      Result := Result.Substring(16, Result.Length - 17)
    else if (Result.StartsWith('Set<')) then
      Result := ConvertSet(Result);
  end;

  if (IsReference) then
    Result := 'var ' + Result;

  Result := Result + Suffix;
end;

class function TErSymbolConverter.ParseParams(var AStr: PChar): string;
{ Params -> Param (',' Param)*
  Convert special case "TVarRec*, Integer" to "array of const" }
var
  PrevIsVarRec: Boolean;
  Param: string;
begin
  Result := ParseParam(AStr);
  PrevIsVarRec := (Result = 'TVarRec*');
  while (AStr^ = ',') do
  begin
    Inc(AStr);
    SkipWhitespace(AStr);
    Param := ParseParam(AStr);
    if (PrevIsVarRec) then
    begin
      if (Param = 'Integer') then
      begin
        Result := Result.Substring(0, Result.Length - 8);
        Result := Result + 'array of const';
      end
      else
        Result := Result + ', ' + Param;
      PrevIsVarRec := False;
    end
    else
    begin
      Result := Result + ', ' + Param;
      PrevIsVarRec := (Param = 'TVarRec*');
    end;
  end;
end;

class function TErSymbolConverter.ParseQualifiedName(var AStr: PChar): string;
var
  LDummy: Boolean;
begin
  Result := ParseQualifiedName(AStr, LDummy);
end;

class function TErSymbolConverter.ParseQualifiedName(var AStr: PChar; out AInSystem: Boolean): string;
{ QualifiedName -> Name ('::' Name)*
  For brevity, we ignore the System namespace in names that start with it. }
begin
  Result := ParseName(AStr);
  AInSystem := (Result = 'System') and (AStr[0] = ':') and (AStr[1] = ':');
  if AInSystem then
  begin
    Inc(AStr, 2);
    Result := ParseName(AStr);
  end;

  while (AStr[0] = ':') and (AStr[1] = ':') do
  begin
    Inc(AStr, 2);
    Result := Result + '.' + ParseName(AStr);
  end;
end;

class function TErSymbolConverter.ParseRemainder(var AStr: PChar): string;
{ Is called when string cannot be parsed.
  Just returns the remainder of the string as-is. }
var
  P: PChar;
begin
  P := AStr;
  while (P^ <> #0) do
    Inc(P);
  SetString(Result, AStr, P - AStr);
  AStr := P;
end;

class procedure TErSymbolConverter.SkipWhitespace(var AStr: PChar);
begin
  while (AStr^ <> #0) and (AStr^ <= ' ') do
    Inc(AStr);
end;

{$ENDREGION}

{$REGION 'TErSymbolConverter - конструктор/деструктор'}

class constructor TErSymbolConverter.Create;
begin
  FTypeMap := TDictionary<string, string>.Create;
  FOperatorMap := TDictionary<String, string>.Create;

  // типы C++
  FTypeMap.Add('bool', 'Boolean');
  FTypeMap.Add('signed char', 'ShortInt');
  FTypeMap.Add('unsigned char', 'Byte');
  FTypeMap.Add('char', 'Byte');
  FTypeMap.Add('wchar_t', 'Char');
  FTypeMap.Add('char16_t', 'Char');
  FTypeMap.Add('char32_t', 'UCS4Char');
  FTypeMap.Add('short', 'SmallInt');
  FTypeMap.Add('short int', 'SmallInt');
  FTypeMap.Add('signed short', 'SmallInt');
  FTypeMap.Add('signed short int', 'SmallInt');
  FTypeMap.Add('unsigned short', 'Word');
  FTypeMap.Add('unsigned short int', 'Word');
  FTypeMap.Add('int', 'Integer');
  FTypeMap.Add('signed', 'Integer');
  FTypeMap.Add('signed int', 'Integer');
  FTypeMap.Add('unsigned', 'Cardinal');
  FTypeMap.Add('unsigned int', 'Cardinal');
  FTypeMap.Add('long', 'LongInt');
  FTypeMap.Add('long int', 'LongInt');
  FTypeMap.Add('signed long', 'LongInt');
  FTypeMap.Add('signed long int', 'LongInt');
  FTypeMap.Add('unsigned long', 'LongWord');
  FTypeMap.Add('unsigned long int', 'LongWord');
  FTypeMap.Add('long long', 'Int64');
  FTypeMap.Add('long long int', 'Int64');
  FTypeMap.Add('signed long long', 'Int64');
  FTypeMap.Add('signed long long int', 'Int64');
  FTypeMap.Add('unsigned long long', 'UInt64');
  FTypeMap.Add('unsigned long long int', 'UInt64');
  FTypeMap.Add('float', 'Single');
  FTypeMap.Add('double', 'Double');
  FTypeMap.Add('long double', 'Extended');

  // типы Delphi
  FTypeMap.Add('UnicodeString', 'String');

  FOperatorMap.Add('_op_Implicit', 'operator_Implicit');
  FOperatorMap.Add('_op_Explicit', 'operator_Explicit');
  FOperatorMap.Add('_op_UnaryNegation', 'operator_Negative');
  FOperatorMap.Add('_op_UnaryPlus', 'operator_Positive');
  FOperatorMap.Add('_op_Increment', 'operator_Inc');
  FOperatorMap.Add('_op_Decrement', 'operator_Dec');
  FOperatorMap.Add('_op_LogicalNot', 'operator_LogicalNot');
  FOperatorMap.Add('_op_Trunc', 'operator_Trunc');
  FOperatorMap.Add('_op_Round', 'operator_Round');
  FOperatorMap.Add('_op_In', 'operator_In');
  FOperatorMap.Add('_op_Equality', 'operator_Equal');
  FOperatorMap.Add('_op_Inequality', 'operator_NotEqual');
  FOperatorMap.Add('_op_GreaterThan', 'operator_GreaterThan');
  FOperatorMap.Add('_op_GreaterThanOrEqual', 'operator_GreaterThanOrEqual');
  FOperatorMap.Add('_op_LessThan', 'operator_LessThan');
  FOperatorMap.Add('_op_LessThanOrEqual', 'operator_LessThanOrEqual');
  FOperatorMap.Add('_op_Addition', 'operator_Add');
  FOperatorMap.Add('_op_Subtraction', 'operator_Subtract');
  FOperatorMap.Add('_op_Multiply', 'operator_Multiply');
  FOperatorMap.Add('_op_Division', 'operator_Divide');
  FOperatorMap.Add('_op_IntDivide', 'operator_IntDivide');
  FOperatorMap.Add('_op_Modulus', 'operator_Modulus');
  FOperatorMap.Add('_op_LeftShift', 'operator_LeftShift');
  FOperatorMap.Add('_op_RightShift', 'operator_RightShift');
  FOperatorMap.Add('_op_LogicalAnd', 'operator_LogicalAnd');
  FOperatorMap.Add('_op_LogicalOr', 'operator_LogicalOr');
  FOperatorMap.Add('_op_ExclusiveOr', 'operator_LogicalXor');
  FOperatorMap.Add('_op_BitwiseAnd', 'operator_BitwiseAnd');
  FOperatorMap.Add('_op_BitwiseOr', 'operator_BitwiseOr');
  FOperatorMap.Add('_op_BitwiseXOR', 'operator_BitwiseXor');

  FTypeMap.TrimExcess;
  FOperatorMap.TrimExcess;
end;

class destructor TErSymbolConverter.Destroy;
begin
  try
    SafeFree(FTypeMap);
    SafeFree(FOperatorMap);
  finally
    FTypeMap := nil;
    FOperatorMap := nil;
  end;
end;

{$ENDREGION}

{$REGION 'TErSymbolConverter - общедоступные методы'}

class function TErSymbolConverter.CppToPascal(const ASymbol: string): string;
var
  P: PChar;
begin
  P := PChar(ASymbol);
  if (P^ = #0) then
    Exit('');

  Result := ParseFunctionHeading(P);
  while (P[0] = ':') and (P[1] = ':') do
  begin
    Inc(P, 2);
    Result := Result + '.' + ParseFunctionHeading(P);
  end;
end;

{$ENDREGION}

{$ENDREGION}

{$REGION 'TErExceptionReportMessage'}

{$REGION 'TErExceptionReportMessage - конструктор'}

constructor TErExceptionReportMessage.Create(const AReport: IErExceptionReport);
begin
  inherited Create;

  FReport := AReport;
end;

{$ENDREGION}

{$ENDREGION}

{$REGION 'TErExceptionReporter'}

{$REGION 'TErExceptionReporter - методы общедоступных свойств'}

class function TErExceptionReporter.GetInstance: TErExceptionReporter;
begin
  Result := FInstance;
end;

class procedure TErExceptionReporter.SetInstance(const AValue: TErExceptionReporter);
begin
  FInstance := AValue;
end;

class function TErExceptionReporter.GetExceptionHandler: TErExceptionEvent;
begin
  if Assigned(FInstance) then
    Result := FInstance.GlobalHandleException
  else
    Result := nil;
end;

class function TErExceptionReporter.GetMaxCallStackDepth: Integer;
begin
  if Assigned(FInstance) then
    Result := FInstance.FMaxCallStackDepth
  else
    Result := 20;
end;

class procedure TErExceptionReporter.SetMaxCallStackDepth(const AValue: Integer);
begin
  if Assigned(FInstance) then
    FInstance.FMaxCallStackDepth := AValue;
end;

{$ENDREGION}

{$REGION 'TErExceptionReporter - внутренние методы'}

procedure TErExceptionReporter.ReportException(const AExceptionObject: TObject; const AExceptionAddress: Pointer);
var
  I: Integer;
  E: Exception;
  LExceptionMessage: string;
  LCallStack: TErCallStack;
  LExceptionLocation: TErCallStackEntry;
  LReport: IErExceptionReport;
begin
  // если в текущий момент уже происходит обработка другого исключения, то текущее исключение не обрабатываем
  if (FReportingException) then
    Exit;

  FReportingException := True;
  try
    LCallStack := nil;
    if (AExceptionObject = nil) then
      LExceptionMessage := 'Unknown Error'
    else if (AExceptionObject is EAbort) then
      Exit
    else if (AExceptionObject is Exception) then
    begin
      E := Exception(AExceptionObject);
      LExceptionMessage := E.Message;
      if (E.StackInfo <> nil) then
      begin
        LCallStack := GetCallStack(E.StackInfo);
        for I := 0 to Length(LCallStack) - 1 do
        begin
          // для записи в стеке, находящейся в текущем модуле, пытаемся получить транслированные данные в Pascal
          if LCallStack[I].ModuleAddress = FModuleAddress then
            LCallStack[I].RoutineName := ErCppSymbolToPascal(LCallStack[I].RoutineName);
        end;
      end;
    end
    else
      LExceptionMessage := 'Unknown Error (' + AExceptionObject.ClassName + ')';

    LExceptionLocation.Clear;
    LExceptionLocation.CodeAddress := UIntPtr(AExceptionAddress);
    GetCallStackEntry(LExceptionLocation);
    if (LExceptionLocation.ModuleAddress = FModuleAddress) then
      LExceptionLocation.RoutineName := ErCppSymbolToPascal(LExceptionLocation.RoutineName);

    LReport := TErExceptionReport.Create(LExceptionMessage, LExceptionLocation, LCallStack);
    try
      TMessageManager.DefaultManager.SendMessage(Self, TErExceptionReportMessage.Create(LReport));
    except
      // игнорируем любые исключения для исключения рекурсивной обработки
    end;
  finally
    FReportingException := False;
  end;
end;

{$REGION 'реализация для iOS'}

{$IFDEF IOS}

const
  libSystem = '/usr/lib/libSystem.dylib';

function backtrace(buffer: PPointer; size: Integer): Integer; external libSystem name 'backtrace';

function cxa_demangle(const mangled_name: MarshaledAString; output_buffer: MarshaledAString; length: NativeInt;
  out status: Integer): MarshaledAString; cdecl; external libSystem name '__cxa_demangle';

type
  /// <summary>
  ///   Стек вызовов
  /// </summary>
  TCallStack = record
    /// <summary>
    ///   Количество записей в стеке
    /// </summary>
    Count: Integer;

    /// <summary>
    ///   Записи в стеке
    /// </summary>
    Stack: array [0..0] of UIntPtr;
  end;

  /// <summary>
  ///   Указатель на стек вызовов
  /// </summary>
  PCallStack = ^TCallStack;

class function TErExceptionReporter.GlobalGetExceptionStackInfo(P: PExceptionRecord): Pointer;
var
  LCallStack: PCallStack;
begin
  /// ВАЖНО:
  ///   Don't call into FInstance here. That would only add another entry to the call stack.
  ///   Instead, retrieve the entire call stack from within this method.
  ///   Just return nil if we are already reporting an exception, or call stacks are disabled.
  if (FInstance = nil) or (FInstance.FReportingException) or (FInstance.FMaxCallStackDepth <= 0) then
    Exit(nil);

  GetMem(LCallStack, SizeOf(Integer) + FInstance.FMaxCallStackDepth * SizeOf(Pointer));

  CallStack.Count := backtrace(@LCallStack.Stack, FInstance.FMaxCallStackDepth);
  Result := LCallStack;
end;

class function TErExceptionReporter.GetCallStack(const AStackInfo: Pointer): TErCallStack;
var
  I: Integer;
  LCallStack: PCallStack;
begin
  LCallStack := AStackInfo;
  SetLength(Result, LCallStack.Count);
  for I := 0 to LCallStack.Count - 1 do
  begin
    Result[I].CodeAddress := LCallStack.Stack[I];
    GetCallStackEntry(Result[I]);
  end;
end;

{$ENDIF}

{$ENDREGION}

{$REGION 'реализация для Android x64'}

{$IFDEF ANDROID64}

function cxa_demangle(const mangled_name: MarshaledAString; output_buffer: MarshaledAString; length: NativeInt;
  out status: Integer): MarshaledAString; cdecl; external 'libc++abi.a' name '__cxa_demangle';

type
  _PUnwind_Context = Pointer;
  _Unwind_Ptr = UIntPtr;

type
  _Unwind_Reason_code = Integer;

const
  _URC_NO_REASON = 0;
  _URC_FOREIGN_EXCEPTION_CAUGHT = 1;
  _URC_FATAL_PHASE2_ERROR = 2;
  _URC_FATAL_PHASE1_ERROR = 3;
  _URC_NORMAL_STOP = 4;
  _URC_END_OF_STACK = 5;
  _URC_HANDLER_FOUND = 6;
  _URC_INSTALL_CONTEXT = 7;
  _URC_CONTINUE_UNWIND = 8;

type
  _Unwind_Trace_Fn = function(context: _PUnwind_Context; userdata: Pointer): _Unwind_Reason_code; cdecl;

const
  LIB_UNWIND = 'libunwind.a';

procedure _Unwind_Backtrace(fn: _Unwind_Trace_Fn; userdata: Pointer); cdecl; external LIB_UNWIND;
function _Unwind_GetIP(context: _PUnwind_Context): _Unwind_Ptr; cdecl; external LIB_UNWIND;

type
  /// <summary>
  ///   Стек вызовов
  /// </summary>
  TCallStack = record
    /// <summary>
    ///   Количество записей в стеке
    /// </summary>
    Count: Integer;

    /// <summary>
    ///   Записи в стеке
    /// </summary>
    Stack: array [0..0] of UIntPtr;
  end;

  /// <summary>
  ///   Указатель на стек вызовов
  /// </summary>
  PCallStack = ^TCallStack;

function UnwindCallback(AContext: _PUnwind_Context; AUserData: Pointer): _Unwind_Reason_code; cdecl;
var
  Callstack: PCallstack;
begin
  Callstack := AUserData;
  if (TErExceptionReporter.Instance = nil) or (Callstack.Count >= TErExceptionReporter.Instance.MaxCallStackDepth) then
    Exit(_URC_END_OF_STACK);

  Callstack.Stack[Callstack.Count] := _Unwind_GetIP(AContext);
  Inc(Callstack.Count);
  Result := _URC_NO_REASON;
end;

class function TErExceptionReporter.GlobalGetExceptionStackInfo(P: PExceptionRecord): Pointer;
var
  LCallStack: PCallStack;
begin
  /// ВАЖНО:
  ///   Don't call into FInstance here. That would only add another entry to the call stack.
  ///   Instead, retrieve the entire call stack from within this method.
  ///   Just return nil if we are already reporting an exception, or call stacks are disabled.
  if (FInstance = nil) or (FInstance.FReportingException) or (FInstance.FMaxCallStackDepth <= 0) then
    Exit(nil);

  GetMem(LCallStack, SizeOf(Integer) + FInstance.FMaxCallStackDepth * SizeOf(Pointer));

  LCallStack.Count := 0;
  _Unwind_Backtrace(UnwindCallback, LCallStack);
  Result := LCallStack;
end;

class function TErExceptionReporter.GetCallStack(const AStackInfo: Pointer): TErCallStack;
var
  I: Integer;
  LCallStack: PCallStack;
begin
  LCallStack := AStackInfo;
  SetLength(Result, LCallStack.Count);
  for I := 0 to LCallStack.Count - 1 do
  begin
    Result[I].CodeAddress := LCallStack.Stack[I];
    GetCallStackEntry(Result[I]);
  end;
end;

{$ENDIF}

{$ENDREGION}

{$REGION 'реализация для Android x32'}

{$IFDEF ANDROID32}

type
  TGetFramePointer = function: NativeUInt; cdecl;

const
  { We need a function to get the frame pointer. The address of this frame
    pointer is stored in register R7.
    In assembly code, the function would look like this:
      ldr R0, [R7]       // Retrieve frame pointer
      bx  LR             // Return to caller
    The R0 register is used to store the function result.
    The "bx LR" line means "return to the address stored in the LR register".
    The LR (Link Return) register is set by the calling routine to the address
    to return to.
    We could create a text file with this code, assemble it to a static library,
    and link that library into this unit. However, since the routine is so
    small, it assembles to just 8 bytes, which we store in an array here. }
  GET_FRAME_POINTER_CODE: array [0..7] of Byte = (
    $00, $00, $97, $E5,  // ldr R0, [R7]
    $1E, $FF, $2F, $E1); // bx  LR

var
  { Now define a variable of a procedural type, that is assigned to the
    assembled code above }
  GetFramePointer: TGetFramePointer = @GET_FRAME_POINTER_CODE;

function cxa_demangle(const mangled_name: MarshaledAString;
  output_buffer: MarshaledAString; length: NativeInt; out status: Integer): MarshaledAString; cdecl;
  external {$IF (RTLVersion < 34)}'libgnustl_static.a'{$ELSE}'libc++abi.a'{$ENDIF} name '__cxa_demangle';

type
  { For each entry in the call stack, we save 7 values for inspection later.
    See GlobalGetExceptionStackInfo for explaination. }
  TStackValues = array [0..6] of UIntPtr;

type
  TCallStack = record
    { Number of entries in the call stack }
    Count: Integer;

    { The entries in the call stack }
    Stack: array [0..0] of TStackValues;
  end;
  PCallStack = ^TCallStack;

class function TErExceptionReporter.GlobalGetExceptionStackInfo(
  P: PExceptionRecord): Pointer;
const
  { On most Android systems, each thread has a stack of 1MB }
  MAX_STACK_SIZE = 1024 * 1024;
var
  MaxCallStackDepth, Count: Integer;
  FramePointer, MinStack, MaxStack: UIntPtr;
  Address: Pointer;
  CallStack: PCallStack;
begin
  { Don't call into FInstance here. That would only add another entry to the
    call stack. Instead, retrieve the entire call stack from within this method.
    Just return nil if we are already reporting an exception, or call stacks
    are disabled. }
  if (FInstance = nil) or (FInstance.FReportingException) or (FInstance.FMaxCallStackDepth <= 0) then
    Exit(nil);

  MaxCallStackDepth := FInstance.FMaxCallStackDepth;

  { Allocate a PCallStack record large enough to hold just MaxCallStackDepth
    entries }
  GetMem(CallStack, SizeOf(Integer{TCallStack.Count}) +
    MaxCallStackDepth * SizeOf(TStackValues));

  (*We manually walk the stack to create a stack trace. This is possible since
    Delphi creates a stack frame for each routine, by starting each routine with
    a prolog. This prolog is similar to the one used by the iOS ABI (Application
    Binary Interface, see
    https://developer.apple.com/library/content/documentation/Xcode/Conceptual/iPhoneOSABIReference/Articles/ARMv7FunctionCallingConventions.html
    The prolog looks like this:
    * Push all registers that need saving. Always push the R7 and LR registers.
    * Set R7 (frame pointer) to the location in the stack where the previous
      value of R7 was just pushed.
    A minimal prolog (used in many small functions) looks like this:
      push {R7, LR}
      mov  R7, SP
    We are interested in the value of the LR (Link Return) register. This
    register contains the address to return to after the routine finishes. We
    can use this address to look up the symbol (routine name). Using the
    example prolog above, we can get to the LR value and walk the stack like
    this:
    1. Set FramePointer to the value of the R7 register.
    2. At this location in the stack, you will find the previous value of the
       R7 register. Lets call this PreviousFramePointer.
    3. At the next location in the stack, we will find the LR register. Add its
       value to our stack trace so we can use it later to look up the routine
       name at this address.
    4. Set FramePointer to PreviousFramePointer and go back to step 2, until
       FramePointer is 0 or falls outside of the stack.
    Unfortunately, Delphi doesn't follow the iOS ABI exactly, and it may push
    other registers between R7 and LR. For example:
      push {R4, R5, R6, R7, R8, R9, LR}
      add  R7, SP, #12
    Here, it pushed 3 registers (R4-R6) before R7, so in the second line it sets
    R7 to point 12 bytes into the stack (so it still points to the previous R7,
    as required). However, it also pushed registers R8 and R9, before it pushes
    the LR register. This means we cannot assume that the LR register will be
    directly located after the R7 register in the stack. There may be (up to 6)
    registers in between. We don't know which one represents LR, so we just
    store all 7 values after R7, and later try to figure out which one
    represents LR (in the GetCallStack method). *)
  FramePointer := GetFramePointer;

  { The stack grows downwards, so all entries in the call stack leading to this
    call have addresses greater than FramePointer. We don't know what the start
    and end address of the stack is for this thread, but we do know that the
    stack is at most 1MB in size, so we only investigate entries from
    FramePointer to FramePointer + 1MB. }
  MinStack := FramePointer;
  MaxStack := MinStack + MAX_STACK_SIZE;

  { Now we can walk the stack using the algorithm described above. }
  Count := 0;
  while (Count < MaxCallStackDepth) and (FramePointer <> 0)
    and (FramePointer >= MinStack) and (FramePointer < MaxStack) do
  begin
    { The first value at FramePointer contains the previous value of R7.
      Store the 7 values after that. }
    Address := Pointer(FramePointer + SizeOf(UIntPtr));
    Move(Address^, CallStack.Stack[Count], SizeOf(TStackValues));
    Inc(Count);

    { The FramePointer points to the previous value of R7, which contains the
      previous FramePointer. }
    FramePointer := PNativeUInt(FramePointer)^;
  end;

  CallStack.Count := Count;
  Result := CallStack;
end;

class function TErExceptionReporter.GetCallStack(const AStackInfo: Pointer): TErCallStack;
var
  I, J: Integer;
  CallStack: PCallStack;
  FoundLR: Boolean;
begin
  CallStack := AStackInfo;
  SetLength(Result, CallStack.Count);
  for I := 0 to CallStack.Count - 1 do
  begin
    { For each entry in the call stack, we have up to 7 values now that may
      represent the LR register. Most of the time, it will be the first value.
      We try to find the correct LR value by passing up to all 7 addresses to
      the dladdr API (by calling GetCallStackEntry). If the API call succeeds,
      we assume we found the value of LR. However, this is not fool proof
      because an address value we pass to dladdr may be a valid code address,
      but not the LR value we are looking for.
      Also, the LR value contains the address of the next instruction after the
      call instruction. Delphi usually uses the BL or BLX instruction to call
      another routine. These instructions takes 4 bytes, so LR will be set to 4
      bytes after the BL(X) instruction (the return address). However, we want
      to know at what address the call was made, so we need to subtract 4
      bytes.
      There is one final complication here: the lowest bit of the LR register
      indicates the mode the CPU operates in (ARM or Thumb). We need to clear
      this bit to get to the actual address, by AND'ing it with "not 1". }
    FoundLR := False;
    for J := 0 to Length(CallStack.Stack[I]) - 1 do
    begin
      Result[I].CodeAddress := (CallStack.Stack[I, J] and not 1) - 4;
      if GetCallStackEntry(Result[I]) then
      begin
        { Assume we found LR }
        FoundLR := True;
        Break;
      end;
    end;

    if (not FoundLR) then
      { None of the 7 values were valid.
        Set CodeAddress to 0 to signal we couldn't find LR. }
      Result[I].CodeAddress := 0;
  end;
end;

{$ENDIF}

{$ENDREGION}

{$REGION 'реализация для Windows'}

{$IFDEF MSWINDOWS}

class function TErExceptionReporter.GlobalGetExceptionStackInfo(P: PExceptionRecord): Pointer;
begin
  Result := nil;
end;

class function TErExceptionReporter.GetCallStack(const AStackInfo: Pointer): TErCallStack;
begin
  Result := nil;
end;

class function TErExceptionReporter.GetCallStackEntry(var AEntry: TErCallStackEntry): Boolean;
begin
  Result := False;
end;

{$ENDIF}

{$ENDREGION}

{$REGION 'реализация для macOS'}

{$IFDEF OSX}

class function TErExceptionReporter.GlobalGetExceptionStackInfo(P: PExceptionRecord): Pointer;
begin
  Result := nil;
end;

class function TErExceptionReporter.GetCallStack(const AStackInfo: Pointer): TErCallStack;
begin
  Result := nil;
end;

class function TErExceptionReporter.GetCallStackEntry(var AEntry: TErCallStackEntry): Boolean;
begin
  Result := False;
end;

{$ENDIF}

{$ENDREGION}

{$IF DEFINED(IOS) or DEFINED(ANDROID)}

class function TErExceptionReporter.GetCallStackEntry(var AEntry: TErCallStackEntry): Boolean;
var
  LInfo: dl_info;
  LStatus: Integer;
  LDemangled: MarshaledAString;
begin
  Result := (dladdr(AEntry.CodeAddress, LInfo) <> 0) and (LInfo.dli_saddr <> nil);
  if (Result) then
  begin
    AEntry.RoutineAddress := UIntPtr(LInfo.dli_saddr);
    AEntry.ModuleAddress := UIntPtr(LInfo.dli_fbase);

    LDemangled := cxa_demangle(LInfo.dli_sname, nil, 0, LStatus);
    if (LDemangled = nil) then
      AEntry.RoutineName := string(LInfo.dli_sname)
    else
    begin
      AEntry.RoutineName := string(LDemangled);
      Posix.Stdlib.free(LDemangled);
    end;

    AEntry.ModuleName := string(LInfo.dli_fname);
  end;
end;

{$ENDIF}

{$ENDREGION}

{$REGION 'TErExceptionReporter - обработчики глобальных событий работы с исключениями'}

procedure TErExceptionReporter.GlobalHandleException(Sender: TObject; E: Exception);
begin
  ReportException(E, ExceptAddr);
end;

class procedure TErExceptionReporter.GlobalExceptionAcquiredHandler(AObj: Pointer);
begin
  if Assigned(FInstance) then
    FInstance.ReportException(AObj, ExceptAddr);
end;

class procedure TErExceptionReporter.GlobalExceptHandler(ExceptObject: TObject; ExceptAddr: Pointer);
begin
  if Assigned(FInstance) then
    FInstance.ReportException(ExceptObject, ExceptAddr);
end;

class procedure TErExceptionReporter.GlobalCleanUpStackInfo(AInfo: Pointer);
begin
  if AInfo <> nil then
    FreeMem(AInfo);
end;

{$ENDREGION}

{$REGION 'TErExceptionReporter - внутренний конструктор'}

constructor TErExceptionReporter.InternalCreate(const ADummy: Integer);
{$IF DEFINED(IOS) or DEFINED(ANDROID)}
var
  LInfo: dl_info;
{$ENDIF}
begin
  inherited Create;

  FMaxCallStackDepth := 20;
  ExceptionAcquired := @GlobalExceptionAcquiredHandler;
  ExceptProc := @GlobalExceptHandler;
  Exception.GetExceptionStackInfoProc := GlobalGetExceptionStackInfo;
  Exception.CleanUpStackInfoProc := GlobalCleanUpStackInfo;

{$IF Defined(IOS) or Defined(ANDROID)}
  if (dladdr(UIntPtr(@TErExceptionReporter.InternalCreate), LInfo) <> 0) then
    FModuleAddress := UIntPtr(LInfo.dli_fbase);
{$ENDIF}
end;

{$ENDREGION}

{$REGION 'TErExceptionReporter - конструктор'}

constructor TErExceptionReporter.Create;
begin
  raise EInvalidOperation.Create('Invalid singleton constructor call');
end;

{$ENDREGION}

{$ENDREGION}

{$ENDREGION}

{$REGION 'глобальные процедуры и функции'}

function ErCppSymbolToPascal(const ASymbol: string): string;
begin
  Result := TErSymbolConverter.CppToPascal(ASymbol);
end;

{$ENDREGION}

initialization
  try
    TErExceptionReporter.Instance := TErExceptionReporter.InternalCreate;
  except
    TErExceptionReporter.Instance := nil;
  end;

finalization
  try
    if Assigned(TErExceptionReporter.Instance) then
    begin
      try
        SafeFree(TErExceptionReporter.Instance);
      finally
        TErExceptionReporter.Instance := nil;
      end;
    end;
  except
    // игнорируем любые исключения
  end;

end.
