﻿unit Core.Funcs;

interface

uses

 System.Types, System.SysUtils, XSuperObject, System.Classes, Core.Crypt, EncdDecd,
 IdCoderMIME, IdGlobal, FMX.TextLayout, System.Math, FMX.Graphics, FMX.Types,
 FMX.StdCtrls;

type
 TKeyValue = record
   Key, Value: string;
 end;

function Base64DecodeAnsi(AValue:string):AnsiString;
function Base64Encode(AValue:string):String;

function ExtractKeyValue(AStr: string):TKeyValue;

function IsNumeric(AValue: string): boolean;

function CalcTextSize(Text: string; Font: TFont; Size: Single = 0): TSizeF;
function RetrieveTextHeight(const ALabel: TLabel):double; //в строках!!

implementation

function RetrieveTextHeight(const ALabel: TLabel):double;
var
  LLayout: TTextLayout;
  LHeight, LWidth: double;
begin
  LLayout := TTextLayoutManager.DefaultTextLayout.Create;
  try
    LLayout.BeginUpdate;
    try
      // примечание: используется коррекция возможных ошибок
      if Assigned(ALabel.TextSettings) then
      begin
        LLayout.Font.Assign(ALabel.TextSettings.Font);
        LLayout.Font.Size := ALabel.TextSettings.Font.Size + 0.1;
        LLayout.VerticalAlign := ALabel.TextSettings.VertAlign;
        LLayout.HorizontalAlign := ALabel.TextSettings.HorzAlign;
        //LLayout.WordWrap := ALabel.TextSettings.WordWrap;
        LLayout.Trimming := ALabel.TextSettings.Trimming;
      end;

    finally
      LLayout.EndUpdate;
    end;

    LWidth := LLayout.Width;

    result := trunc(LWidth/ALabel.Width) + 1;

  finally
    LLayout.DisposeOf;
  end;
end;


function CalcTextSize(Text: string; Font: TFont; Size: Single = 0): TSizeF;
var
  TextLayout: TTextLayout;
begin
  TextLayout := TTextLayoutManager.DefaultTextLayout.Create;
  try
    TextLayout.BeginUpdate;
    try
      TextLayout.Text := Text;
      TextLayout.MaxSize := TPointF.Create(9999, 9999);
      TextLayout.Font.Assign(Font);
      if not SameValue(0, Size) then
      begin
        TextLayout.Font.Size := Size;
      end;
      TextLayout.WordWrap := true;
      TextLayout.Trimming := TTextTrimming.None;
      TextLayout.HorizontalAlign := TTextAlign.Leading;
      TextLayout.VerticalAlign := TTextAlign.Leading;
    finally
      TextLayout.EndUpdate;
    end;

    Result.Width := TextLayout.Width;
    Result.Height := TextLayout.Height;
  finally
    TextLayout.Free;
  end;
end;

function IsNumeric(AValue: string): boolean;
var
 i: integer;
 begin
  try
    i:= StrToInt(AValue);
    exit(true);
  except on E:Exception do
    exit(false);
  end;
 end;

function ExtractKeyValue(AStr: string):TKeyValue;
var
 a: TArray<string>;
 k: integer;
 begin

  result.Key := Astr;
  result.Value := '';

  if not AStr.Contains('=') then exit;

  k := pos('=', AStr);
  if k>0 then
   begin
    result.Key := AStr.Substring(0, k-1);
    result.Value := AStr.Substring(k);
   end;

  //нам нужно даже из строки вида key=value=somevalue вычленить ключ "key"
  //и значение "value=somevalue" (со знаком равно), так что нижеследующий код не прокатит
  exit;

  a := AStr.Split(['=']);
  result.Key := a[0];
  result.Value := a[1];

 end;

//работает и под win, и под мобилку. Стандартный TNetEncoding под мобилку не работает, все время будет вываливаться
//No mapping for the Unicode character exists in the target multi-byte code page
function Base64Encode(AValue:string):String;
 begin
  result := TIdEncoderMIME.EncodeString(AValue, IndyTextEncoding_UTF8);
 end;

function Base64DecodeAnsi(AValue:string):AnsiString;
var
 LBS: TBytesStream;
 len:integer;
 str: ArrayOfByte;

   function Hex2Str(aBuf: ArrayOfByte): AnsiString;
    var
      i: integer;
      bufLength: integer;
    begin
      bufLength := Length(aBuf);
      Result := '';
      for i := 0 to BufLength - 1 do
      begin
        Result := Result + AnsiChar(aBuf[i]);
      end;
    end;


 begin

  if AValue.Trim = '' then Exit('');
  
  LBS := TBytesStream.Create;
   try
    LBS := Base64Decode(AValue);
    LBS.Position := 0;
    SetLength(str, LBS.Size);
    LBS.Read(str[0], LBS.Size);
    len := LBS.Size - LBS.Position;
    result := Hex2Str(str);

   finally
    LBS.DisposeOf;
   end;

 end;

end.
