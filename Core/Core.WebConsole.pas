﻿//позволяет перехватывать сообщения ИЗ браузера, независимо от системы, для стандартного TWebBrowser.
// Для этого нужно:
// 1) определить схему (идентификатор перед ":"). Например, "Delphi"
// 2) Определить в классе подпрограмму-перехватчик. Например, IntercepterFun(AParams:string) begin ShowMessage(AParams); end;
// 3) где-то на странице, которую нужно перехватить, по какому-либо событию перенаправить
//    window.location.href = 'Delphi:IntercepterFun("hellow, delphi!")';
//    или создать ссылку с тем же адресом
// Установку и пример см:
//3dparty/Scriptgate


unit Core.WebConsole;

interface

uses
 Core.Common, FMX.WebBrowser,  System.SysUtils, System.Types, System.UITypes,
 System.Classes, System.Variants,
 FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
 FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, SG.ScriptGate, System.Messaging;

type

 TWebConsole = class
   strict private
    FWebBrowser: TWebBrowser;
    FScriptGate: TScriptGate;
    FScriptResult: string;
   strict private
    procedure SendMessage(AMsg: string);
   public
    constructor Create(AWebBrowser: TWebBrowser);
    procedure WebConsole(AMsg: string);
    procedure SetJsParam(const aParam, aValue: string); overload;
    procedure SetJsParam(const aParam: string; aValue: boolean); overload;
    procedure ExecJs(AScript: string);
 end;

implementation

{ TWebConsole }

constructor TWebConsole.Create(AWebBrowser: TWebBrowser);
begin
 self.FWebBrowser := AWebBrowser;
 self.FScriptGate := TScriptGate.Create(self, self.FWebBrowser, 'delphi');
end;

procedure TWebConsole.WebConsole(AMsg: string);
begin
 self.SendMessage(AMsg);
end;

procedure TWebConsole.ExecJs(AScript: string);
begin
 if (FWebBrowser <> nil) then
  if Assigned(Self.FScriptGate) then
   begin
    try
     TLogger.Save('[TWebConsole.ExecJs] '+AScript);

     {TThread.CreateAnonymousThread(
            procedure()
              begin
                Sleep(10);
                TThread.Synchronize(TThread.CurrentThread,
                procedure
                  begin
                    self.FScriptGate.CallScript(AScript, nil);
                  end);
              end).Start;}

     self.FScriptGate.CallScript(AScript, nil);
    except on E:Exception do
     TToast.Show(e.Message);
    end;
   end;
end;

procedure TWebConsole.SendMessage(AMsg: string);
var
 Message: TMessage;
begin
 Message := TMessage<String>.Create(AMsg);
 TMessageManager.DefaultManager.SendMessage(self, Message, True);
end;

procedure TWebConsole.SetJsParam(const aParam: string; aValue: boolean);
var
 s: string;
begin
 if (FWebBrowser <> nil) then
  if Assigned(Self.FScriptGate) then
   begin
    try
     s := format('set%s(%s)', [aParam, AnsiLowerCase(BoolToStr(aValue, true))]);
     TLogger.Save('[TWebConsole.SetJsParam] '+s);
     self.FScriptGate.CallScript(s, nil);
    except on E:Exception do
     TToast.Show(e.Message);
    end;
   end;
   // FWebBrowser.EvaluateJavaScript(format('set%s(%s)', [aParam, AnsiLowerCase(BoolToStr(aValue, true))]));
end;

procedure TWebConsole.SetJsParam(const aParam, aValue: string);
var
 s: string;
begin
 if (FWebBrowser <> nil) then
  if Assigned(Self.FScriptGate) then
   begin
    try
     s := format('set%s(''%s'')', [aParam, AnsiLowerCase(aValue)]);
     TLogger.Save('[TWebConsole.SetJsParam] s='+s);
     self.FScriptGate.CallScript(s, nil);
    except on E:Exception do
     TToast.Show(e.Message);
    end;
   end;
end;

end.
