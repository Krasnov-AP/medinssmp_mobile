﻿unit data.main;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.FMXUI.Wait,
  Data.DB, FireDAC.Comp.Client, System.IOUtils, Core.Common, Core.Crypt,
  FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteWrapper.Stat, FireDAC.Comp.UI, FireDac.DApt, Model.Global;

type

  {TLangProviderSQLite = class(TLangProvider)
    public
     procedure LoadData; override;
  end;}

  TDataMain = class(TDataModule)
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDConnectionMain: TFDConnection;
    lnkDB: TFDPhysSQLiteDriverLink;
    procedure DataModuleCreate(Sender: TObject);
  private

    { Private declarations }
  public
    function CreateDatabase:boolean;
    procedure PatchDB;
    procedure ExecQuery(ASql: string; ADoStartTransaction:boolean = true; ARaiseIfError: boolean = false);
    function OpenQuery(ASql: string; ADoStartTransaction:boolean = true; ADoRaiseIfError: boolean = false):boolean;
    function LoadLangData:TLangValues;
    function GetPath:string;
    function Test:string;
    procedure SaveSettings;
    function LoadSettings: boolean;
    procedure SaveUser;
    function LoadUser: boolean;
  end;

var
  DataMain: TDataMain;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

{ TDataMain }

function TDataMain.CreateDatabase:boolean;
var
 LDir:string;
begin

  result := false;

  LDir := '';

   {$ifdef MSWINDOWS}
    LDir := ExtractFilePath(ParamStr(0))+'data';
   {$endif}

   {$IF DEFINED(iOS) or DEFINED(ANDROID)}
    LDir := System.IOUtils.TPath.GetDocumentsPath;
   {$endif}

   if LDir = '' then
    begin
      raise Exception.Create('unknown_db_path'); //будет поймано обработчиком необработанных исключений
      Exit(false);
    end;

     if not Assigned(FDConnectionMain) then
       FDConnectionMain := TFDConnection.Create(nil)
        else
       FDConnectionMain.Connected := false;

       //если базы нет, то создатся автоматом (надо проверить под Мак)
       try

         {$ifdef MSWINDOWS}
          FDConnectionMain.Params.Values['Database'] := ExtractFilePath(ParamStr(0))+'data\data.db';
         {$endif}
         {$ifdef ANDROID}
          FDConnectionMain.Params.Values['Database'] := System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetDocumentsPath, 'data.db');
         {$endif}

        FDConnectionMain.DriverName := 'SQLite';
        TLogger.Save('[TDataMain.CreateDatabase] db_path='+FDConnectionMain.Params.Values['Database']);
        FDConnectionMain.Connected := true;
       except on E:Exception do
        begin
         TLogger.Save('[TDataMain.CreateDatabase] error: '+e.Message, TLogLevel.Error);
         exit(false);
        end;
       end;

    result := true;
end;

function TDataMain.OpenQuery(ASql: string; ADoStartTransaction:boolean = true; ADoRaiseIfError: boolean = false):boolean;
var
 LFDQuery: TFDQuery;
begin

  result := false;
  LFDQuery := TFDQuery.Create(nil);
  try

   LFDQuery.Connection := self.FDConnectionMain;

   LFDQuery.SQL.Text := ASql;
   if ADoStartTransaction then
    LFDQuery.Connection.StartTransaction;

    try
      LFDQuery.Open;
      if LFDQuery.RecordCount>0 then result := true;
    except on E:Exception do
     begin
      if ADoStartTransaction then LFDQuery.Connection.Rollback;
      TLogger.Save('[TDataMain.OpenQuery] error: '+e.Message, TLogLevel.Error);
      if ADoRaiseIfError then
       raise Exception.Create(e.Message);
      exit(false);
     end;
    end;

   if ADoStartTransaction then LFDQuery.Connection.Commit;
   LFDQuery.Close;

  finally
   LFDQuery.Free;
  end;

end;

procedure TDataMain.DataModuleCreate(Sender: TObject);
begin

 try
  TLogger.Save('[TDataMain.DataModuleCreate] start');
  if self.CreateDatabase then self.PatchDB;

  TLogger.Save('loading langs before');
  TLangManager.Locale := 'RU';
   try
    TLangManager.LangValues := DataMain.LoadLangData;
   except on E:Exception do
    TLogger.Save('[TLangManager.LangValues := DataMain.LoadLangData;] err: '+e.Message);
   end;

  TLogger.Save('[ModelDataGlobal := TModelDataGlobal.Create;] before');
  ModelDataGlobal := TModelDataGlobal.Create;

 except on E:Exception do
  begin
    TLogger.Save('[TDataMain.DataModuleCreate] error: '+e.Message, TLogLevel.Error);
  end;
 end;

end;

procedure TDataMain.ExecQuery(ASql: string; ADoStartTransaction:boolean = true; ARaiseIfError: boolean = false);
var
 LFDQuery: TFDQuery;
begin

  LFDQuery := TFDQuery.Create(nil);

  try

   LFDQuery.Connection := self.FDConnectionMain;

   LFDQuery.SQL.Text := ASql;
   if ADoStartTransaction then
    LFDQuery.Connection.StartTransaction;

    try
      LFDQuery.ExecSQL;
      if ADoStartTransaction then LFDQuery.Connection.Commit;
    except on E:Exception do
     begin
      if ADoStartTransaction then LFDQuery.Connection.Rollback;
      TLogger.Save('[TDataMain.ExecQuery] error: '+e.Message, TLogLevel.Error);
       if ARaiseIfError then
        raise Exception.Create(e.Message);
     end;
    end;

   LFDQuery.Close;

  finally
   LFDQuery.Free;
  end;

end;

function TDataMain.GetPath: string;
begin
 result := self.FDConnectionMain.Params.Values['database'];
end;

function TDataMain.LoadLangData: TLangValues;
var
 LFDQuery: TFDQuery;
 LSql:string;
 LLangVal:TLangValue;
begin

  TLogger.Save('[LoadLangData] start');
  result := TLangValues.Create;
  LFDQuery := TFDQuery.Create(nil);
  LSQL := 'select * from translation2';
  TLogger.Save(LSQL);
  try

   LFDQuery.Connection := self.FDConnectionMain;

   LFDQuery.SQL.Text := LSql;
   LFDQuery.Connection.StartTransaction;

    try
      LFDQuery.Open;

      TLogger.Save('query cnt='+LFDQuery.RecordCount.ToString);
      while not LFDQuery.Eof do
       begin
        LLangVal.EN := LFDQuery.FieldByName('trans_en').AsString;
        LLangVal.RU := LFDQuery.FieldByName('trans_ru').AsString;
        result.Add(LFDQuery.FieldByName('ident').AsString.ToUpper, LLangVal);
        LFDQuery.Next;
       end;

    except on E:Exception do
     begin
      LFDQuery.Connection.Rollback;
      TLogger.Save('[TDataMain.OpenQuery] error: '+e.Message, TLogLevel.Error);
      exit;
     end;
    end;

   LFDQuery.Connection.Commit;
   LFDQuery.Close;

  finally
   LFDQuery.Free;
  end;

end;

function TDataMain.LoadSettings: boolean;
var
 LFDQuery: TFDQuery;
begin

  result := false;

  try
    LFDQuery := TFDQuery.Create(nil);
    try

     LFDQuery.Connection := self.FDConnectionMain;

     LFDQuery.SQL.Text := 'select * from app_settings where num=1';
     LFDQuery.Connection.StartTransaction;

      try
        LFDQuery.Open;

        if LFDQuery.RecordCount > 0 then
         begin
           result := true;
           ModelDataGlobal.AppSettings.ConvertFromJson(LFDQuery.FieldByName('settings').AsString.Trim);
         end;

      except on E:Exception do
       begin
        LFDQuery.Connection.Rollback;
        TLogger.Save('[TDataMain.LoadSettings] Open error: '+e.Message, TLogLevel.Error);
        exit(false);
       end;
      end;

     LFDQuery.Connection.Commit;
     LFDQuery.Close;

    finally
     LFDQuery.Free;
    end;
  except on E:Exception do
   TLogger.Save('[TDataMain.LoadSettings] err: '+e.Message);
  end;

end;

function TDataMain.LoadUser: boolean;
var
 LFDQuery: TFDQuery;
begin

  result := false;
  LFDQuery := TFDQuery.Create(nil);
  try

   LFDQuery.Connection := self.FDConnectionMain;

   LFDQuery.SQL.Text := 'select * from user where num=1';
   LFDQuery.Connection.StartTransaction;

    try
      LFDQuery.Open;

      if LFDQuery.RecordCount > 0 then
       begin
         result := true;
         ModelDataGlobal.CurrentUser.Name := LFDQuery.FieldByName('username').AsString;
         ModelDataGlobal.CurrentUser.Password := Trim(TCrypter.Decrypt(LFDQuery.FieldByName('password').AsString));
       end;

    except on E:Exception do
     begin
      LFDQuery.Connection.Rollback;
      TLogger.Save('[TDataMain.OpenQuery] error: '+e.Message, TLogLevel.Error);
      exit(false);
     end;
    end;

   LFDQuery.Connection.Commit;
   LFDQuery.Close;

  finally
   LFDQuery.Free;
  end;

end;

procedure TDataMain.PatchDB;
var
 LSql:string;
 LFDQuery: TFDQuery;
begin
 //наверняка в процессе будет что-то добавляться/убираться в метаданные
 //здесь будем описывать структуру

  TLogger.Save('[TDataMain.PatchDB] start');

  try
   self.OpenQuery('select * from app_settings where num=1', true, true);
  except on E: Exception do
   begin
    TLogger.Save('[TDataMain.PatchDB] looks like table app_settings does not exists');
    if e.Message.Contains('no such table') then
     begin
      LSQL := 'CREATE TABLE app_settings ('+
              'id       INTEGER PRIMARY KEY AUTOINCREMENT,'+
              'settings VARCHAR,'+
              'num      INTEGER'+
              ');';
      try
       begin
        self.ExecQuery(LSQL);
        TLogger.Save('[TDataMain.PatchDB] table app_settings created');
       end;
      except on E:Exception do
       TToast.Show('[TDataMain.PatchDB] err: '+e.Message);
      end;
     end;
   end;
  end;

  try
   self.OpenQuery('select * from user where num=1', true, true);
  except on E: Exception do
   begin
    TLogger.Save('[TDataMain.PatchDB] looks like table user does not exists');
    if e.Message.Contains('no such table') then
     begin
      LSQL := 'CREATE TABLE user ('+
              'id       INTEGER PRIMARY KEY AUTOINCREMENT,'+
              'num      INTEGER,'+
              'username VARCHAR,'+
              'password VARCHAR'+
              ');';

      try
       begin
        self.ExecQuery(LSQL);
        TLogger.Save('[TDataMain.PatchDB] table user created');
       end;
      except on E:Exception do
       TToast.Show('[TDataMain.PatchDB] err: '+e.Message);
      end;
     end;
   end;
  end;

end;


procedure TDataMain.SaveSettings;
var
 LFDQuery: TFDQuery;
 LSQL: string;
 LSettings: string;
begin

  if not ModelDataGlobal.AppSettings.DoSaveSettings then exit;

   try
    LFDQuery := TFDQuery.Create(nil);
    try

     LFDQuery.Connection := self.FDConnectionMain;

     if self.OpenQuery('select * from app_settings where num=1') then
      LSQL := 'update app_settings set settings=:settings where num=1'
       else
      LSQL := 'insert into app_settings (settings, num) values (:settings, 1)';

     LFDQuery.SQL.Text := LSQL;
     LSettings := ModelDataGlobal.AppSettings.ConvertToJson;

     LFDQuery.ParamByName('settings').AsString := LSettings;

     LFDQuery.Connection.StartTransaction;

      try
        LFDQuery.ExecSQL;
      except on E:Exception do
       begin
        LFDQuery.Connection.Rollback;
        TLogger.Save('[TDataMain.SaveSettings] ExecSQL error: '+e.Message, TLogLevel.Error);
        exit;
       end;
      end;

     LFDQuery.Connection.Commit;
     LFDQuery.Close;

    finally
     LFDQuery.Free;
    end;
   except on E:Exception do
     begin
       TLogger.Save('[TDataMain.SaveSettings] main err: '+e.Message);
     end;
   end;

end;

procedure TDataMain.SaveUser;
var
 LFDQuery: TFDQuery;
 LSQL: string;
begin
  try
    LFDQuery := TFDQuery.Create(nil);
    try

     LFDQuery.Connection := self.FDConnectionMain;

     if self.OpenQuery('select * from user where num=1') then
      LSQL := 'update user set username=:username, password=:password where num=1'
       else
      LSQL := 'insert into user (username, password, num) values (:username, :password, 1)';

      LFDQuery.SQL.Text := LSQL;

      LFDQuery.ParamByName('username').AsString := ModelDataGlobal.CurrentUser.Name;
      LFDQuery.ParamByName('password').AsString := TCrypter.Encrypt(ModelDataGlobal.CurrentUser.Password);

      LFDQuery.Connection.StartTransaction;

      try
        LFDQuery.ExecSQL;
      except on E:Exception do
       begin
        LFDQuery.Connection.Rollback;
        TLogger.Save('[TDataMain.SaveUser] ExecSQL error: '+e.Message, TLogLevel.Error);
        exit;
       end;
      end;

     LFDQuery.Connection.Commit;
     LFDQuery.Close;

    finally
     LFDQuery.Free;
    end;
  except on E:Exception do
   TLogger.Save('[TDataMain.SaveUser] error: '+e.Message, TLogLevel.Error);
  end;

end;

function TDataMain.Test: string;
var
 LFDQuery: TFDQuery;
begin

  result := '';
  LFDQuery := TFDQuery.Create(nil);
  try

   LFDQuery.Connection := self.FDConnectionMain;

   LFDQuery.SQL.Text := 'select * from translation2';
   LFDQuery.Connection.StartTransaction;

    try
      LFDQuery.Open;
      while not LFDQuery.Eof do
       begin
         result := result + LFDQuery.FieldByName('ident').AsString + ' ,';
         LFDQuery.Next;
       end;
    except on E:Exception do
     begin
      LFDQuery.Connection.Rollback;
      TLogger.Save('[TDataMain.OpenQuery] error: '+e.Message, TLogLevel.Error);
      exit('');
     end;
    end;

   LFDQuery.Connection.Commit;
   LFDQuery.Close;

  finally
   LFDQuery.Free;
  end;

end;

end.
