﻿unit Form.Base;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  IPPeerClient, REST.Client, Data.Bind.Components, Data.Bind.ObjectScope,
  REST.Types, DBXJSON, System.JSON, FMX.Forms, FMX.StdCtrls, FMX.Dialogs,
  FMX.Layouts, FMX.ListBox, FMX.Types, Data.DbxSqlite, Data.DB, Data.SqlExpr,
  System.StrUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Comp.Client, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  System.IOUtils, FMX.Memo, IDUri, FMX.objects, FMX.Controls, FMX.Edit,
  FMX.VirtualKeyboard, FMX.Ani, FMX.MultiView, FMX.ListView,
  FMX.ListView.Appearances, FMX.Platform, FMX.WebBrowser, FMX.Maps,
  System.Notification, FMX.Graphics, FMX.ListView.Types, Model.Global,
  System.Messaging, Model.AppSettings, View.Base, Model.ViewParams,
  FMX.Controls.Presentation, Core.Common, Model.LangConsts, Helper.View.Input
   {$IFDEF ANDROID}
    , FMX.Platform.Android, Androidapi.JNI.GraphicsContentViewText,
    Androidapi.Helpers, Androidapi.JNI.Telephony, Androidapi.JNI.JavaTypes,
    Androidapi.JNIBridge, Androidapi.JNI.Os, FMX.Helpers.Android,
    Androidapi.JNI.Provider
  {$ENDIF}
  {$IFDEF MSWINDOWS}
    , Winapi.Windows, System.ImageList
  {$ENDIF}
 ,FMX.InertialMovement, FMX.MultiView.Types;

type

  //как будет выглядеть форма
  //flSimple - просто форма беза меню и ToolBar
  //flToolBarMenu - с гл. меню и тулбаром
  TFormLook = (flSimple, flToolBar, flToolBarMenu);

  TFormBase = class(TForm)
    pnl1: TPanel;
    ToolBar1: TToolBar;
    ToolBarLabel: TLabel;
    btn2: TSpeedButton;
    ltFormContentWrapper: TLayout;
    ltMainMenu: TLayout;
    rectMainMenuWrapper: TRectangle;
    ltMainMenuTitle: TLayout;
    rectMainMenuTitle: TRectangle;
    lbMainMenuTitle: TLabel;
    btnBack: TSpeedButton;
    btnOK: TSpeedButton;
    btnCancel: TSpeedButton;
    imgDone: TImage;
    btnAdd: TSpeedButton;
    imgAdd: TImage;
    imgBack: TImage;
    rectShadow: TRectangle;
    btnRefresh: TSpeedButton;
    imgRefresh: TImage;
    btnFilter: TSpeedButton;
    imgFilter: TImage;
    ltFormDescr: TLayout;
    aniFormLoader: TAniIndicator;
    lbFormDescr: TLabel;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnBackClick(Sender: TObject);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  strict private    { Private declarations }
    CurMenuItem: byte;
    FFormLook: TFormLook;
    FLocale: string;
    FTitle: string;
    FViewCurrent, FViewFilterCurrent: ClassViewBase;
    FActionButtons: TActionButtons;
    FViewParams: TViewParams;
    procedure SetFormLook(AValue: TFormLook);
    procedure DisableMenu;
    procedure EnableMenu;
    procedure SetLocale(AValue: string);
    procedure SetTitle(AValue: string);
    procedure SetActionButtons(AButtons: TActionButtons);
    procedure ProcessMsgDlg(const AResult: TModalResult);
  strict protected
    FControlledScrollBox: TCustomScrollBox;
    FDoCloseOnOKClicked: boolean;
    function GetActiveScrollBox: TCustomScrollBox; virtual;
    procedure SendMessage(AMsg: string); overload;
    procedure SendMessage(AMsg: string; Sender: TObject); overload;
    procedure ShowFilterInternal(AView: ClassViewBase = nil);
  public
    multiMenu: TMultiView;
    multiFilter: TMultiView;
    lstMenu: TListView;
    img1: TImage;
    sender: TObject; //откуда вызываем форму, кнопка, комбо или прочая шняга...
  public
    property FormLook: TFormLook read FFormLook write SetFormLook;
    property Locale: string read FLocale write SetLocale;
    property Title: string read FTitle write SetTitle;
    property ViewCurrent: ClassViewBase read FViewCurrent write FViewCurrent;
    property ViewFilterCurrent: ClassViewBase read FViewFilterCurrent write FViewFilterCurrent;
    property ActionButtons: TActionButtons read FActionButtons write SetActionButtons;
  public
    constructor Create(AOwner: TComponent); override;
    procedure onItemClick(const Sender: TObject; const AItem: TListViewItem);
    procedure onActivateProc(Sender: TObject);
    procedure onMenuHidden(Sender: TObject);
    procedure onMenuShown(Sender: TObject);
    procedure onFilterHidden(Sender: TObject);
    procedure onFilterShown(Sender: TObject);
    procedure onKeyUpProc(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
    procedure MakeMainMenu;
    procedure MakeMainFilter;
    procedure Translate; virtual;
    procedure ShowView(AViewParams: TViewParams); overload;
    procedure ShowView(AView: ClassViewBase; AFormLook: TFormLook = flToolBarMenu); overload;
    procedure CloseModal;
    procedure ShowShadow;
    procedure HideShadow;
    procedure ShowLoader(ADescr: string = '');
    procedure HideLoader;
    function ObjectAtPoint(AScreenPoint: TPointF): IControl; override;
    procedure ProcessMessages(const Sender: TObject; const M: TMessage);
    //procedure ShowFilter(AView: ClassViewBase = nil); virtual;  //будем переопределять в потомках в зависимости от текущей вьюхи
  end;

implementation

{$R *.fmx}
{$R *.iPhone47in.fmx IOS}
{$R *.LgXhdpiPh.fmx ANDROID}

type
  TAniCalculationsAccess = class(TAniCalculations);

function ControlMoved(ScrollBox: TCustomScrollBox): Boolean;
begin
  Result:=False;
  if Assigned(ScrollBox) then
  if ScrollBox is TCustomListBox then
    Result:=ScrollBox.AniCalculations.Moved
  else
    Result:=ScrollBox.AniCalculations.Moved and TAniCalculationsAccess(ScrollBox.AniCalculations).Enabled and
      (ScrollBox.AniCalculations.LastTimeCalc>0);
end;

{ TFormBase }

procedure TFormBase.btnAddClick(Sender: TObject);
begin
 self.SendMessage('btn_add_clicked');
end;

procedure TFormBase.btnBackClick(Sender: TObject);
begin
 self.CloseModal;
end;

procedure TFormBase.btnCancelClick(Sender: TObject);
begin
  self.CloseModal;
end;

procedure TFormBase.btnFilterClick(Sender: TObject);
begin
  self.ShowFilterInternal(nil);
end;

procedure TFormBase.btnOKClick(Sender: TObject);
begin
 self.SendMessage('btn_ok_clicked');
 if self.FDoCloseOnOKClicked then
  self.CloseModal;
end;

procedure TFormBase.btnRefreshClick(Sender: TObject);
begin
 self.SendMessage('refresh_data');
end;

procedure TFormBase.CloseModal;
begin
  self.SendMessage('close_modal');
end;

constructor TFormBase.Create(AOwner: TComponent);
var
  pnl1: TPanel;
  btnMultiMenu: TSpeedButton;
  lbItem: TListViewItem;
  i, c: Byte;

  function calcRealMenuCount: integer;
  var
    i: byte;
  begin
    result := 0;
    for i := 0 to Model.AppSettings.MAIN_MENU_COUNT - 1 do
      if Model.Global.ModelDataGlobal.AppSettings.MainMenuList[i].Visible then
        inc(result);
  end;

begin

  inherited;

  self.onActivate := self.onActivateProc;
  Self.OnKeyUp := self.onKeyUpProc;

  self.FLocale := Model.Global.ModelDataGlobal.AppSettings.Locale;

  self.ActionButtons := [abNone];

  self.MakeMainMenu;
end;

procedure TFormBase.DisableMenu;
begin
  //self.btnMultiMenu.Visible := false;
  self.lstMenu.Visible := false;
end;

procedure TFormBase.EnableMenu;
begin
  //self.btnMultiMenu.Visible := true;
  self.lstMenu.Visible := true;
end;

procedure TFormBase.FormCreate(Sender: TObject);
begin
 self.FDoCloseOnOKClicked := true;
end;

procedure TFormBase.FormVirtualKeyboardHidden(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
 if Assigned(KeyboardHelper) then
  begin
   KeyboardHelper.KeyboardBounds := Bounds;
   self.SendMessage('keyboard_hidden', KeyboardHelper);
   TAppKeyboard.IsVisible := false;
  end;
end;

procedure TFormBase.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
 if Assigned(KeyboardHelper) then
  begin
   KeyboardHelper.KeyboardBounds := Bounds;
   self.SendMessage('keyboard_shown', KeyboardHelper);
   TAppKeyboard.IsVisible := true;
  end;
end;

function TFormBase.GetActiveScrollBox: TCustomScrollBox;
begin
  Result := FControlledScrollBox;
end;

procedure TFormBase.HideLoader;
begin
 self.lbFormDescr.Text := '';
 self.aniFormLoader.Enabled := false;
 self.HideShadow;
end;

procedure TFormBase.HideShadow;
begin
 //todo
 self.rectShadow.Visible := false;
end;

procedure TFormBase.MakeMainFilter;
var
  pnl1: TPanel;
  btnMultiMenu: TSpeedButton;
  lbItem: TListViewItem;
  i, c: Byte;
  LEdit: TEdit;

begin

  pnl1 := self.FindComponent('pnl1') as TPanel;

  if (pnl1 = nil) then
    Exit;

  if not Assigned(multiFilter) then
  begin

    self.multiFilter := TMultiView.Create(self);
    self.multiFilter.Parent := self;

    Self.multiFilter.Mode := TMultiViewMode.Drawer;
    Self.multiFilter.DrawerOptions.Placement := TPanelPlacement.Right;
    self.multiFilter.TargetControl := pnl1;

    self.multiFilter.OnStartShowing := self.onFilterShown;
    self.multiFilter.OnHidden := self.onFilterHidden;

    self.multiFilter.Width := self.Width * 0.8;

  end;

end;




procedure TFormBase.MakeMainMenu;
var
  pnl1: TPanel;
  btnMultiMenu: TSpeedButton;
  lbItem: TListViewItem;
  i, c: Byte;
  LEdit: TEdit;

  function calcRealMenuCount: integer;
  var
    i: byte;
  begin
    result := 0;
    for i := 0 to Model.AppSettings.MAIN_MENU_COUNT - 1 do
      if Model.Global.ModelDataGlobal.AppSettings.MainMenuList[i].Visible then
        inc(result);
  end;

begin

  pnl1 := self.FindComponent('pnl1') as TPanel;

  if (pnl1 = nil) then
    Exit;

  if not Assigned(multiMenu) then
  begin
    self.multiMenu := TMultiView.Create(self);
    self.multiMenu.Parent := self;
    //self.multiMenu.

    Self.multiMenu.Mode := TMultiViewMode.Drawer;
    self.multiMenu.TargetControl := pnl1;

    self.multiMenu.OnStartShowing := self.onMenuShown;
    self.multiMenu.OnHidden := self.onMenuHidden;

    btnMultiMenu := self.FindComponent('btnMultiMenu') as TSpeedButton;
    if (btnMultiMenu <> nil) then
      Self.multiMenu.MasterButton := btnMultiMenu;

    self.multiMenu.Width := self.Width * 0.8;

    //лого
    img1 := TImage.Create(self.multiMenu);
    img1.Parent := self.multiMenu;
    img1.Align := TAlignLayout.MostTop;

    img1.Width := 280 * 0.8;
    img1.Height := 100 * 0.8;

    img1.Margins.Bottom := 15;
    img1.Margins.Top := 10;

    img1.Bitmap.LoadFromStream(Model.Global.ModelDataGlobal.AppSettings.imgLogo);

    self.lstMenu := TListView.Create(self.multiMenu);
    self.lstMenu.Parent := Self.multiMenu;
    self.lstMenu.Align := TAlignLayout.Top;
    c := calcRealMenuCount;
    self.lstMenu.Height := 60 * c + 2 * c;
    self.lstMenu.ItemAppearanceName := 'ImageListItemBottomDetail';
    self.lstMenu.ItemAppearanceObjects.ItemObjects.Text.Font.Size := 18;

    self.lstMenu.ItemAppearanceObjects.ItemObjects.Text.Font.Style := [TFontStyle.fsBold];
    self.lstMenu.ItemAppearanceObjects.ItemObjects.Text.TextColor := TAlphaColorRec.White;
    self.lstMenu.ItemAppearanceObjects.ItemObjects.Detail.TextColor := TAlphaColorRec.Darkgray;
    self.lstMenu.ItemAppearanceObjects.ItemObjects.Detail.Visible := true;
    self.lstMenu.ItemAppearanceObjects.ItemObjects.Image.Visible := True;
    self.lstMenu.ItemAppearanceObjects.ItemObjects.Text.WordWrap := False;

    //self.lstMenu.ItemAppearanceObjects.ItemObjects.Detail.Font.Size := 18;
    //self.lstMenu.ItemAppearanceObjects.ItemObjects.Detail.Font.Style := [TFontStyle.fsBold];

    self.lstMenu.OnItemClick := self.onItemClick;
  end
  else
  begin
    self.lstMenu.Items.Clear;
  end;

  for i := 0 to Model.AppSettings.MAIN_MENU_COUNT - 1 do
  begin

    if not Model.Global.ModelDataGlobal.AppSettings.MainMenuList[i].Visible then
      continue;

    lbItem := self.lstMenu.Items.Add;



    lbItem.Accessory := TAccessoryType.More;

    lbItem.Height := 60;

    lbItem.Text := Model.Global.ModelDataGlobal.AppSettings.MainMenuList[i].Title;
    lbItem.Detail := Model.Global.ModelDataGlobal.AppSettings.MainMenuList[i].Descr;

    lbItem.Tag := Model.Global.ModelDataGlobal.AppSettings.MainMenuList[i].Tag;

    try
      if FindResource(0, PChar('menu_' + lbItem.Tag.ToString), PChar(RT_RCDATA)) <> 0 then
      begin
        lbItem.Bitmap := FMX.Graphics.TBitmap.Create;
        lbItem.Bitmap.LoadFromStream(TResourceStream.Create(0, 'menu_' + lbItem.Tag.ToString, PChar(RT_RCDATA)));
      end;
    except
      on E: Exception do
        TLogger.Save('[TFormBase.MakeMainMenu] while load img from resource err: ', TLogLevel.Error);
    end;

      //временно отключаем менюшку
      //if i=5 then lbItem.Height:=0;

  end;

end;

function TFormBase.ObjectAtPoint(AScreenPoint: TPointF): IControl;
var ScrollBox: TCustomScrollBox;
begin

  var P:=ScreenToClient(AScreenPoint);

  ScrollBox:=GetActiveScrollBox;

  if ControlMoved(ScrollBox) and ScrollBox.PointInObject(P.X,P.Y) then
  begin
    Result:=ScrollBox;
    if Assigned(Captured) and (Captured<>Result) then
    begin
      //Captured.MouseUp(TMouseButton.mbLeft,[],-1,-1);
      SetCaptured(nil);
    end;
  end else
    Result:=inherited;

end;

procedure TFormBase.onActivateProc(Sender: TObject);
begin
 //todo
end;

procedure TFormBase.onFilterHidden(Sender: TObject);
begin
 self.SendMessage('main_filter_hide');
end;

procedure TFormBase.onFilterShown(Sender: TObject);
begin
 self.SendMessage('main_filter_shown');
end;

procedure TFormBase.onItemClick(const Sender: TObject; const AItem: TListViewItem);
var
  c: TComponent;
  LViewParams: TViewParams;

begin

  if AItem.Tag <> 7 then
    multiMenu.HideMaster;
  Application.ProcessMessages;
  {if self.CurMenuItem = AItem.Tag then
    Exit;}
  self.CurMenuItem := AItem.Tag;

  try
    LViewParams := TViewParams.Create;
    case AItem.Tag of
      1:
        begin
          //todo
          self.SendMessage('show_archive');
        end;
      2:
        begin
          LViewParams.Title := TLangManager.Translate(Model.LangConsts.LANG_SETTINGS);
          LViewParams.ViewClass := 'TViewSettings';
          LViewParams.IsModal := true;
          LViewParams.ActionButtons := [abOK, abCancel];
          self.ShowView(LViewParams);
        end;
      3:
        begin
         self.SendMessage('do_logoff');
         exit;
         {LViewParams.Title := TLangManager.Translate(Model.LangConsts.LANG_LOG_IN);
         LViewParams.ViewClass := 'TViewAuth';
         LViewParams.IsModal := true;
         LViewParams.ActionButtons := abNone;
         LViewParams.MainMenuVisible := false;
         self.FViewParams := LViewParams;
         MessageDlg(TLangManager.Translate(Model.LangConsts.LANG_DO_LOGOFF_APP),
                    TMsgDlgType.mtInformation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0,
                    self.ProcessMsgDlg
         );} //так не работает под мобилки, под десктопы - ок. Видимо, из-за того что вызывается через MessageDlg, неохота разбираться.
        end;
    end;
  finally
    LViewParams.DisposeOf;
  end;

end;

procedure TFormBase.onKeyUpProc(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin

end;

procedure TFormBase.onMenuHidden(Sender: TObject);
var
  c: TComponent;
begin

  self.SendMessage('main_menu_hide');

  c := self.FindComponent('tbFooter');
  if c <> nil then
  begin
    if c.Tag = 10 then
    begin
      (c as TToolBar).Visible := true;
      c.Tag := 1;
    end;
  end;

end;

procedure TFormBase.onMenuShown(Sender: TObject);
var
  c: TComponent;
begin

  self.SendMessage('main_menu_shown');

  c := self.FindComponent('tbFooter');
  if c <> nil then
  begin
    (c as TToolBar).Visible := false;
    c.Tag := 10;
  end;

end;

procedure TFormBase.ProcessMessages(const Sender: TObject; const M: TMessage);
var
 LMsg:string;
 LViewClass: ClassViewBase;
begin

  LMsg := (M as TMessage<String>).Value;

  if LMsg = 'patch_scrollbox' then
   begin
     if Sender.InheritsFrom(TCustomScrollBox) then
      self.FControlledScrollBox := (Sender as TCustomScrollbox);
   end
    else
  if LMsg = 'show_filter_menu' then
    begin

    end;
end;

procedure TFormBase.ProcessMsgDlg(const AResult: TModalResult);
begin
   case AResult of
      mrYes:
       begin
         self.SendMessage('do_logoff');
         exit;
         FViewParams.Title := TLangManager.Translate(Model.LangConsts.LANG_LOG_IN);
         FViewParams.ViewClass := 'TViewAuth';
         FViewParams.IsModal := true;
         FViewParams.ActionButtons := [abNone];
         FViewParams.MainMenuVisible := false;
         self.ShowView(FViewParams);
       end;
      mrNo:
       begin
       end;
   end;
end;

procedure TFormBase.SendMessage(AMsg: string);
var
  Message: TMessage;
begin
  Message := TMessage<string>.Create(AMsg);
  TMessageManager.DefaultManager.SendMessage(self, Message, True);
end;

procedure TFormBase.SendMessage(AMsg: string; Sender: TObject);
var
  Message: TMessage;
begin
  Message := TMessage<string>.Create(AMsg);
  TMessageManager.DefaultManager.SendMessage(Sender, Message, True);
end;

procedure TFormBase.SetActionButtons(AButtons: TActionButtons);
var
 buf:Integer;
begin

 self.btnBack.Visible := false;
 self.btnOK.Visible := false;
 self.btnCancel.Visible := false;
 self.btnAdd.Visible := false;
 self.btnRefresh.Visible := false;
 self.btnFilter.Visible := false;

 if (abBack in AButtons) then self.btnBack.Visible := true;
 if (abOK in AButtons) then self.btnOK.Visible := true;
 if (abCancel in AButtons) then self.btnBack.Visible := true;
 if (abAdd in AButtons) then self.btnAdd.Visible := true;
 if (abRefresh in AButtons) then self.btnRefresh.Visible := true;
 if (abFilter in AButtons) then self.btnFilter.Visible := true;

end;

procedure TFormBase.SetFormLook(AValue: TFormLook);
begin
  self.FFormLook := AValue;
  case self.FFormLook of
    flSimple:
      begin
        self.ToolBar1.Visible := false;
        self.DisableMenu;
      end;
    flToolBar:
      begin
        self.ToolBar1.Visible := true;
        self.DisableMenu;
      end;
    flToolBarMenu:
      begin
        self.ToolBar1.Visible := true;
        self.EnableMenu;
      end;
  end;
end;

procedure TFormBase.SetLocale(AValue: string);
begin
  if self.FLocale <> AValue then
  begin
    self.FLocale := AValue;
    self.MakeMainMenu;
    self.Translate;
  end;
end;

procedure TFormBase.SetTitle(AValue: string);
begin
  self.ToolBarLabel.Text := AValue;
end;

//procedure TFormBase.ShowFilter(AView: ClassViewBase = nil);
//begin
//  self.ShowFilterInternal(AView);
//end;

procedure TFormBase.ShowFilterInternal(AView: ClassViewBase);
begin
  if AView = nil then
   self.SendMessage('show_filter_menu='+self.FViewCurrent.ClassName.Replace('View', 'ViewFilter'))
    else
   self.SendMessage('show_filter_menu='+AView.ClassName);
end;

procedure TFormBase.ShowLoader(ADescr: string);
begin

 if ADescr = '' then
  begin
    self.aniFormLoader.Enabled := false;
    self.lbFormDescr.Text := '';
    self.ltFormDescr.Visible := false;
  end
   else
  begin
    self.aniFormLoader.Enabled := true;
    self.ltFormDescr.Visible := true;
    self.lbFormDescr.Text := ADescr;
  end;

  self.ShowShadow;

end;

procedure TFormBase.ShowShadow;
begin
 //todo
 self.rectShadow.Visible := true;
end;

procedure TFormBase.ShowView(AView: ClassViewBase; AFormLook: TFormLook);
begin
 //todo
end;

procedure TFormBase.ShowView(AViewParams: TViewParams);
begin
 //todo
  self.SendMessage(AViewParams.ConvertToJson);
end;

procedure TFormBase.Translate;
begin
 //todo
end;

end.

