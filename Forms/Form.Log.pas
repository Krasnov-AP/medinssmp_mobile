﻿unit Form.Log;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Memo.Types,
  FMX.ScrollBox, FMX.Memo, FMX.Controls.Presentation, FMX.StdCtrls, Core.Common;

type
  TfrmLog = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLog: TfrmLog;

implementation

{$R *.fmx}

procedure TfrmLog.Button1Click(Sender: TObject);
begin
 self.Memo1.SelStart := 0;
 self.Memo1.SelectAll;
end;

procedure TfrmLog.Button3Click(Sender: TObject);
begin
  self.Memo1.Lines.Clear;
  self.Memo1.Lines.AddStrings(TLogger.LogStrings);
end;

procedure TfrmLog.FormShow(Sender: TObject);
begin
  self.Button3Click(self);
end;

end.
