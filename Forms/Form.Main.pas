﻿unit Form.Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  System.Rtti, FMX.Grid.Style, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, FMX.Controls.Presentation,
  FMX.ScrollBox, FMX.Grid, FMX.WebBrowser, FMX.StdCtrls, FMX.Objects, System.Messaging,
  System.Generics.Collections,
  Core.Common, View.Base, Form.Modal, View.Auth, Model.Global, FMX.Memo.Types,
  FMX.Memo, System.Sensors, System.Sensors.Components, System.Permissions, Core.ErrorReporting,
  Form.ShowMessage, Form.Base, View.Map, data.main, Model.viewParams, View.Settings,
  Model.LangConsts, View.Orders, View.NewOrder, Helper.View.Input, Controller.TCPClient,
  Core.WebConsole, View.Cars, Core.Funcs, View.OrderDetail, View.Filter.Cars, Model.Cars,
  View.Orders.Archive, View.Filter.Archive, View.Order.Archive.Detail, View.Chat,
  View.NewOrder2, Form.NewOrder, View.SKList, View.ChatRecpt, System.Notification,
  View.Hospitals, FMX.Media, System.IOUtils, View.OrderLog, View.Survey
  {$ifdef ANDROID} ,FMX.Media.Android {$endif}
  {$ifdef MSWINDOWS} ,Registry {$endif}
  ;

const
 SOUND_CHAT = 1;
 SOUND_CLIENT = 2;
 SOUND_CLIENT_FROM_SK = 3;

type

  TViews = TDictionary<ClassViewBase, TViewBase>;
  TViewNames = TDictionary<string, ClassViewBase>;

  TfrmMain = class(TFormBase)
    rectOverShadow: TRectangle;
    StyleBook1: TStyleBook;
    lsGeoData: TLocationSensor;
    Timer1: TTimer;
    pnlBottom: TPanel;
    pnlltBottom: TGridPanelLayout;
    btnMap: TSpeedButton;
    Image7: TImage;
    btnArchive: TSpeedButton;
    Image1: TImage;
    btnChat: TSpeedButton;
    Image2: TImage;
    btnMenu: TSpeedButton;
    Image3: TImage;
    lbMap: TLabel;
    lbArchive: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    timCheckTCPClient: TTimer;
    circleNewChatMessages: TCircle;
    lbNewChatMessages: TLabel;
    mpSoundSKClient: TMediaPlayer;
    timSoundSKClient: TTimer;
    circleOrderChat: TCircle;
    Image4: TImage;
    circleSKOrder: TCircle;
    timBlinkSKClient: TTimer;
    mpSoundChat: TMediaPlayer;
    lbSKOrder: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    function GetView(const AViewClass:ClassViewBase):TViewBase;
    procedure FormDestroy(Sender: TObject);
    procedure lsGeoDataLocationChanged(Sender: TObject; const OldLocation,
      NewLocation: TLocationCoord2D);
    procedure Button2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ltMapClick(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Image2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure btnArchiveClick(Sender: TObject);
    procedure btnMapClick(Sender: TObject);
    procedure btnMenuClick(Sender: TObject);
    procedure btnChatClick(Sender: TObject);
    procedure timCheckTCPClientTimer(Sender: TObject);
    procedure timSoundSKClientTimer(Sender: TObject);
    procedure timBlinkSKClientTimer(Sender: TObject);
  private
   procedure ShowViewAuth;
   procedure ShowViewOrders(ACaller: integer = View.Base.VIEW_CALLER_DEFAULT);
   procedure ShowViewMap(ADisableAfterShow: boolean = false);
   procedure ShowNewOrder;
   procedure ShowNewOrder2;
   procedure ShowCars;
   procedure ShowArchive;
   procedure ShowChat(ACaller: integer = View.Base.VIEW_CALLER_DEFAULT);
   procedure ShowSKList(ACaller: integer = 1);
   procedure ShowChatRcpt;
   procedure ShowHospitals(ACaller: integer = 1);
  private
   procedure ShowCarsFilter;
   procedure NotificationClicked(Sender: TObject; ANotification: TNotification);
   procedure NotificationOrdersClicked(Sender: TObject; ANotification: TNotification);
  private
    { Private declarations }
    FViews:TViews;
    FViewNames:TViewNames;
    FIsFirstRun:boolean;
    FPermissionsGranted:boolean;
    FTcpClient: TTcpClient;
    FNotificationCenter: TNotificationCenter;
    procedure ProcessMessages(const Sender: TObject; const M: TMessage);
    procedure ProcessTCPMessages(AMsg: string);
    procedure ProcessWebConsoleMessages(AMsg: string);
    procedure ShowViewModal(Sender:TObject; AView: ClassViewBase; APosition: TModalPosition = mpCenter; AFormLook: TFormLook = flToolBarMenu);
    procedure ShowView(AView: ClassViewBase; AFormLook:TFormLook = flToolBarMenu); overload;
    procedure ShowView(AViewParams: TViewParams); overload;
    procedure ShowFilter(AView: ClassViewBase; AIsModal: boolean = false);
    property Views[const AViewClass: ClassViewBase]: TViewBase read GetView;

    procedure RequestPermissions;
    procedure PatchBrowser;
    procedure PrepareViewNames;

    procedure ApplicationEventMessageHandler(const Sender: TObject; const M: TMessage);
    procedure ShowNewMessageNotification;
    procedure ShowNewMessageForOrderNotification;
  public
    { Public declarations }
    procedure ShowShadow;
    procedure HideShadow;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: integer); override;
    procedure PlaySound(AWav: integer);
    procedure StopSound(AWav: integer);
    procedure NewSKOrderStartBlinking;
    procedure NewSKOrderStopBlinking;
  end;

var
  frmMain: TfrmMain;

implementation

uses
 FMX.Platform
 {$ifdef ANDROID}
  , DW.Android.Helpers
 {$endif}
 ;


{$R *.fmx}
{$R *.iPhone55in.fmx IOS}

{$R *.LgXhdpiPh.fmx ANDROID}

procedure TfrmMain.btnChatClick(Sender: TObject);
var
 LCar: TCar;
begin

  self.ShowChat;
  exit;

  TToast.Show('Пока не реализовано');
  exit;
  //self.ShowCars;

  ShowMessage(FormatDateTime('mm/dd/yyyy hh:mm:ss', now));

  self.ShowCarsFilter;

  //ModelDataGlobal.Cars.GetById('411').VisibleOnMap := true;
  //ModelDataGlobal.Cars.GetById('411').VisibleOnMap := false;

  //ShowMessage(ModelDataGlobal.CurrentUser.Id.ToString);
end;

procedure TfrmMain.btnMapClick(Sender: TObject);
begin
  inherited;
  self.ShowViewMap;
end;

procedure TfrmMain.btnMenuClick(Sender: TObject);
begin
  inherited;
  self.multiMenu.ShowMaster;
end;

procedure TfrmMain.Button1Click(Sender: TObject);
begin
  //Model.Global.ModelDataGlobal.AppSettings.Locale := 'EN';
  //self.Locale := Model.Global.ModelDataGlobal.AppSettings.Locale;
  self.ShowView(TViewMap);
end;

procedure TfrmMain.Button2Click(Sender: TObject);
begin
 {$ifdef ANDROID}
   FormModal.FormStyle := TFormStyle.Normal;
 {$ENDIF}
 self.ShowViewModal(self, TViewAuth);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin

 Application.CreateForm(TfrmMessage, frmMessage);

 {$ifdef MSWINDOWS}
  self.PatchBrowser;
 {$endif}

 Application.OnException := TErExceptionReporter.ExceptionHandler;
 TMessageManager.DefaultManager.SubscribeToMessage(TErExceptionReportMessage, self.ProcessMessages);
 TMessageManager.DefaultManager.SubscribeToMessage(TMessage<String>, self.ProcessMessages);

 {$ifdef ANDROID}
   TAndroidHelperEx.RestartIfNotIgnoringBatteryOptimizations;
   TMessageManager.DefaultManager.SubscribeToMessage(TApplicationEventMessage, ApplicationEventMessageHandler);
 {$endif}

 self.FViews := TViews.Create;
 self.PrepareViewNames;

 TLogger.Save('[app started]', '', TLogLevel.Info);
 FIsFirstRun := true;
 self.FPermissionsGranted := false;

 self.Timer1.Enabled := true;

 self.FTcpClient := TTCPClient.Create;

 self.FNotificationCenter := TNotificationCenter.Create(nil);

 self.circleNewChatMessages.Visible := false;
 self.circleOrderChat.Visible := false;
 self.circleSKOrder.Visible := false;

 //self.timCheckTCPClient.Enabled := true;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
 TMessageManager.DefaultManager.Unsubscribe(TErExceptionReportMessage, self.ProcessMessages, True);
 TMessageManager.DefaultManager.Unsubscribe(TApplicationEventMessage, ApplicationEventMessageHandler);
 SafeFree(FViews);
end;

function TfrmMain.GetView(const AViewClass: ClassViewBase): TViewBase;
begin
  if Assigned(AViewClass) then
  begin
    if not FViews.TryGetValue(AViewClass, Result) then
      Result := nil;
    if not Assigned(Result) then
    begin
      Result := AViewClass.Create(nil);
      Result.Align := TAlignLayout.Client;
      Result.ViewParams := TViewParams.Create;
      FViews.Add(AViewClass, Result);
    end;
  end
  else
    Result := nil;
end;

procedure TfrmMain.HideShadow;
begin
 self.rectOverShadow.Visible := false;
end;

procedure TfrmMain.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  self.ShowView(TViewMap);
end;

procedure TfrmMain.Image2Click(Sender: TObject);
begin
 self.ShowView(TViewAuth);
end;

procedure TfrmMain.Image2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  self.ShowView(TViewAuth);
end;

procedure TfrmMain.lsGeoDataLocationChanged(Sender: TObject; const OldLocation,
  NewLocation: TLocationCoord2D);
var
 LLon, LLat:string;
begin
 //todo
 //здесь будем отправлять геоданные на сервер
 {$ifdef ANDROID}
   LLon :=  NewLocation.Longitude.ToString;
   LLat :=  NewLocation.Latitude.ToString;
 {$endif}
end;

procedure TfrmMain.ltMapClick(Sender: TObject);
begin
 self.ShowView(TViewMap);
end;

procedure TfrmMain.NewSKOrderStartBlinking;
begin
 self.timBlinkSKClient.Enabled := true;
end;

procedure TfrmMain.NewSKOrderStopBlinking;
begin
 self.timBlinkSKClient.Enabled := false;
 self.timBlinkSKClient.Tag := -1;
 self.circleSKOrder.Visible := false;
 self.StopSound(Form.Main.SOUND_CLIENT_FROM_SK);
end;

procedure TfrmMain.NotificationClicked(Sender: TObject;
  ANotification: TNotification);
begin
 self.ShowChat;
end;

procedure TfrmMain.NotificationOrdersClicked(Sender: TObject;
  ANotification: TNotification);
begin
 self.ShowViewOrders;
end;

procedure TfrmMain.PatchBrowser;
const
  cHomePath = 'SOFTWARE';
  cFeatureBrowserEmulation =
    'Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION\';
  cIE11 = 11001;
var
 {$ifdef MSWINDOWS}
  Reg: TRegIniFile;
 {$endif}
  sKey: string;
begin
  /// <summary>
  /// Поскольку движок стандартного WebBrowser совсем уж древний, то немного его патчим, чтобы эмулировать IE11
  ///  как описано здесь:
  /// https://docwiki.embarcadero.com/Libraries/Sydney/en/FMX.WebBrowser.TWebBrowser
  /// </summary>
  {$ifdef MSWINDOWS}
  sKey := ExtractFileName(ParamStr(0));
  Reg := TRegIniFile.Create(cHomePath);
  try
    if Reg.OpenKey(cFeatureBrowserEmulation, True) and
      not(TRegistry(Reg).KeyExists(sKey) and (TRegistry(Reg).ReadInteger(sKey)
      = cIE11)) then
      TRegistry(Reg).WriteInteger(sKey, cIE11);
  finally
    Reg.Free;
  end;
 {$endif}
end;

procedure TfrmMain.PlaySound(AWav: integer);
var
 LFileName: string;
begin
 case AWav of
   Form.Main.SOUND_CLIENT_FROM_SK:
    begin
     self.mpSoundSKClient.Tag := 1;
     self.timSoundSKClient.Enabled := true;
    end;
   Form.Main.SOUND_CHAT:
    begin

       try

        {$ifdef MSWINDOWS}
         LFileName := ExtractFilePath(ParamStr(0))+'sound\Chat.wav';
        {$endif}
        {$ifdef ANDROID}
         LFileName := System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetDocumentsPath, 'Chat.wav');
        {$endif}

        if not (FileExists(LFileName)) then
         begin
          TLogger.Save('[TfrmMain.PlaySound] LFileName not found '+LFileName);
          exit;
         end;

        {$ifdef ANDROID}
         if self.mpSoundChat.FileName.Trim = '' then
        {$endif}
        self.mpSoundChat.FileName := LFileName;
        self.mpSoundChat.Play;

       finally

       end;
      end;

   end;
end;

procedure TfrmMain.PrepareViewNames;
begin
  if not Assigned(self.FViewNames) then self.FViewNames := TViewNames.Create;

  FViewNames.Add(TViewMap.ClassName, TViewMap);
  FViewNames.Add(TViewAuth.ClassName, TViewAuth);
  FViewNames.Add(TViewSettings.ClassName, TViewSettings);
  FViewNames.Add(TViewOrders.ClassName, TViewOrders);
  FViewNames.Add(TViewNewOrder.ClassName, TViewNewOrder);
  FViewNames.Add(TViewNewOrder2.ClassName, TViewNewOrder2);
  FViewNames.Add(TViewCars.ClassName, TViewCars);
  FViewNames.Add(TViewOrderDetail.ClassName, TViewOrderDetail);
  FViewNames.Add(TViewFilterCars.ClassName, TViewFilterCars);
  FViewNames.Add(TViewOrdersArchive.ClassName, TViewOrdersArchive);
  FViewNames.Add(TViewOrderArchiveDetail.ClassName, TViewOrderArchiveDetail);
  FViewNames.Add(TViewFilterOrdersArchive.ClassName, TViewFilterOrdersArchive);
  FViewNames.Add(TViewChat.ClassName, TViewChat);
  FViewNames.Add(TViewSKList.ClassName, TViewSKList);
  FViewNames.Add(TViewChatRecpt.ClassName, TViewChatRecpt);
  FViewNames.Add(TViewHospitals.ClassName, TViewHospitals);
  FViewNames.Add(TViewOrderLog.ClassName, TViewOrderLog);
  FViewNames.Add(TViewSurvey.ClassName, TViewSurvey);

end;

procedure TfrmMain.ProcessMessages(const Sender: TObject; const M: TMessage);
var
 LMsg:string;
 LReport: IErExceptionReport;
 LViewParams: TViewParams;
 LViewClass: ClassViewBase;
 ts: TStrings;
 LViewClassName: string;
 LKeyValue: TKeyValue;
begin

  inherited;

  if M is TErExceptionReportMessage then
   begin
    LReport := TErExceptionReportMessage(M).Report;
    TLogger.Save(LReport.Report, TLogLevel.Error);
    TToast.Show(LReport.ExceptionMessage);
    exit;
   end;

  LMsg := (M as TMessage<String>).Value;

  if Sender is TTCPClient then
   begin
     TLogger.Save('[TCPClient message] : '+LMsg, TLogLevel.Info);
     self.ProcessTCPMessages(LMsg);
     exit;
   end;

 if (Sender is TToast) then
  begin
   frmMessage.Position := TFormPosition.poScreenCenter;
   frmMessage.ShowMessage(LMsg);
  end;

 if (Sender is TLogger) then
  begin
   TThread.Synchronize(nil, procedure begin
     //self.moLog.Lines.Insert(0, LMsg);
   end);
   exit;
  end;

 if (Sender is TWebConsole) then
  begin
   TThread.Synchronize(nil, procedure begin
    //TToast.Show(LMsg);
    self.ProcessWebConsoleMessages(LMsg);
   end);
   exit;
  end;

  if LMsg.Contains('show_filter_menu') then
   begin
    LViewClassName := Core.Funcs.ExtractKeyValue(LMsg).Value;
    if LViewClassName <> '' then
     begin
      if self.FViewNames.TryGetValue(LViewClassName, LViewClass) then
       begin
        self.ShowFilter(LViewClass, FormModal.Visible)
       end;
     end
   end
   else
  if LMsg.Contains('hide_filter') then
   begin
    if (not FormModal.Visible) then
     self.multiFilter.HideMaster
      else
     FormModal.multiFilter.HideMaster;
   end
    else
  if LMsg = 'show_archive' then
   begin
    self.ShowArchive;
   end
    else
  if LMsg = 'save_settings' then
   begin
    DataMain.SaveSettings;
   end
    else
  if LMsg = 'confirm_car' then
   begin
     if Assigned(self.Views[TViewOrderDetail]) then
      TViewOrderDetail(self.Views[TViewOrderDetail]).ConfirmCar;
   end
    else
  if LMsg = 'refresh_data' then
   begin
    if (not FormModal.Visible) then
      TViewBase(self.Views[self.ViewCurrent]).RefreshData
       else
      TViewBase(self.Views[FormModal.ViewCurrent]).RefreshData;
   end
   else
 if LMsg = 'refresh_orders' then
   begin
     TViewOrders(self.Views[TViewOrders]).RefreshOrders(true);
   end
    else
 if LMsg = 'map_loaded' then
   begin
     //TToast.Show('map_loaded');
     self.timCheckTCPClient.Interval := 10000;
     self.timCheckTCPClientTimer(self);
   end
    else
 if LMsg = 'show_shadow' then
  begin
   {if FormModal.Visible then
     self.Views[FormModal.ViewCurrent].ShowShadow
      else
     self.Views[self.ViewCurrent].ShowShadow; }
   if FormModal.Visible then
     FormModal.ShowShadow
      else
     self.ShowShadow;
  end
   else
 if LMsg = 'hide_shadow' then
  begin
   {if FormModal.Visible then
     self.Views[FormModal.ViewCurrent].HideShadow
      else
     self.Views[self.ViewCurrent].HideShadow;}
    if FormModal.Visible then
     FormModal.HideShadow
      else
     self.HideShadow;
  end
   else
 if LMsg = 'btn_ok_clicked' then
  begin
   if FormNewOrder.Visible then
    begin
     FormNewOrder.SaveOrder;
    end
     else
   if FormModal.Visible then
    begin
      if FormModal.ViewCurrent = TViewNewOrder then
       begin
         TViewNewOrder(self.FViews[FormModal.ViewCurrent]).SaveOrder;
       end;
    end;
  end
   else
 if LMsg = 'btn_add_clicked' then
  begin
   self.ShowNewOrder;
  end
   else
 if (LMsg = 'main_menu_shown') or (LMsg = 'main_filter_shown') then
  begin
    self.pnlBottom.Visible := false;
    if self.ViewCurrent = TViewMap then
     begin
      self.Views[self.ViewCurrent].Visible:= false;
     end;
  end
  else
 if (LMsg = 'main_menu_hide') or (LMsg = 'main_filter_hide') then
  begin
    self.pnlBottom.Visible := true;
    if self.ViewCurrent = TViewMap then
     self.Views[self.ViewCurrent].Visible := true;
  end
   else
 if LMsg = 'close_modal' then
  begin
   self.HideShadow;
   //self.wbMap.Visible := true;
   self.Views[FormModal.ViewCurrent].BeforeClose;
   FormModal.Close;

   if not ( ( (FormModal.ViewCurrent = TViewSKList) and (self.Views[FormModal.ViewCurrent].Caller = View.Base.SKLIST_CALLER_NEW_ORDER) ) or
    ( (FormModal.ViewCurrent = TViewHospitals) and (self.Views[FormModal.ViewCurrent].Caller = View.Base.HOSPITALS_CALLER_NEW_ORDER) ) ) then
    if Assigned(FormNewOrder) then FormNewOrder.Close;

   if Assigned(ModelDataGlobal.CurrentUser) then
    self.Caption := 'Здравствуйте, '+ModelDataGlobal.CurrentUser.Name+'!';
    if self.FIsFirstRun then
     begin
      self.FIsFirstRun := false;
      self.Title := 'Карта';
      self.pnl1.SendToBack;
      //self.tbFooter.Parent := self.pnl1;
     {$ifdef ANDROID}
       self.RequestPermissions;
     {$endif}
     end;
    end
   else
 if LMsg = 'do_logoff' then
  begin
   DataMain.SaveSettings;
   TThread.Synchronize(nil, procedure begin
     self.ShowViewAuth;
   end);
  end
   else
 if (LMsg = 'smp_load_finished') or (LMsg='smp_changed') then
  begin
   {self.timCheckTCPClient.Enabled := false;
   self.FTcpClient.Connected := false;
   self.timCheckTCPClient.Enabled := true;}
  end
   else
 if LMsg = 'close_app' then
  begin
   try
    SafeFree(self.Views[FormModal.ViewCurrent]);
    self.Close;
   except on E:Exception do
    TLogger.Save('[TfrmMain.ProcessMessages] LMsg='+LMsg+' err: '+e.Message, TLogLevel.Error);
   end;
  end
   else
 if LMsg.Contains('JSON') then
  begin
   LViewParams := TViewParams.Create;
    try
     LViewParams.ConvertFromJson(LMsg);

    if not self.FViewNames.TryGetValue(LViewParams.ViewClass, LViewClass) then
      begin
        TToast.Show(LViewParams.ViewClass+' not found!');
        TLogger.Save(LViewParams.ViewClass+' not found!');
        exit;
      end;

    self.Views[LViewClass].Caller := LViewParams.Caller;
    self.Views[LViewClass].DisableAfterShow := LViewParams.DisableAfterShow;
    self.Views[LViewClass].ViewParams.Assign(LViewParams);

    if true {(LViewParams.Parent = '')} then
      begin
        if LViewParams.IsModal then
          begin
           FormModal.Title := LViewParams.Title;
           FormModal.ActionButtons := LViewParams.ActionButtons;
           if LViewParams.MainMenuVisible then
            self.ShowViewModal(self, LViewClass)
             else
            self.ShowViewModal(self, LViewClass, mpCenter, flToolbar);
          end
           else
          begin
           self.Title := LViewParams.Title;
           self.ActionButtons := LViewParams.ActionButtons;
           self.ShowView(LViewClass);
          end;
      end
       else
      begin
       self.Views[LViewClass].Align := TAlignLayout.Client;
       self.Views[LViewClass].Visible:= true;

       self.Views[LViewClass].AfterShow;
      end;
    except on E:Exception do
     TLogger.Save('[TfrmMain.ProcessMessages] JSON analize err: '+e.Message);
    end;

  end
   else
  if LMsg.Contains('set_car_visible') or LMsg.Contains('set_car_invisible') then
   begin
    LKeyValue := ExtractKeyValue(LMsg);
    if LKeyValue.Key = 'set_car_visible'  then
     (self.Views[TViewMap] as TViewMap).SetCarVisibleOnMap(LKeyValue.Value, true)
       else
     (self.Views[TViewMap] as TViewMap).SetCarVisibleOnMap(LKeyValue.Value, false);

    TLogger.Save('[TfrmMain.ProcessMessages] LMsg: '+LMsg+' Sender: '+Sender.ClassName);
   end
    else
   if LMsg = 'show_chat_recpt' then
    begin
     self.ShowChatRcpt;
    end
    else
   if LMsg = 'chat_first_load' then
    begin
     try
      self.ShowChat(View.Base.VIEW_CALLER_SILENT);
      self.ShowViewOrders(View.Base.VIEW_CALLER_SILENT);
      //self.ShowChat;

      self.ShowViewMap(true);
     except on E:Exception do
      TToast.Show(e.message);
     end;
     {self.Views[TViewChat].Parent := FormModal;
     self.Views[TViewChat].Visible := true;
     self.Views[TViewChat].RefreshData;
     TLogger.Save('self.Views[TViewChat].timCommands.Enabled := true;');}
    end
     else
   if LMsg = 'new_chat_message' then
    begin
     self.ShowNewMessageNotification;
    end
     else
   if LMsg = 'new_chat_message_for_order' then
    begin
     self.ShowNewMessageForOrderNotification;
    end
     else
   if LMsg = 'show_customer' then
    begin
      TToast.Show('show_customer');
      self.ShowSKList;
    end
     else
    if LMsg = 'show_sklist_new_order' then
     begin
      self.ShowSKList(SKLIST_CALLER_NEW_ORDER);
     end
     else
    if LMsg = 'show_hospitals_new_order' then
     begin
      self.ShowHospitals(HOSPITALS_CALLER_NEW_ORDER);
     end
     else
    if LMsg = 'confirm_skunit_for_new_order' then
     begin
      if Assigned(FormNewOrder) then
       begin
        FormNewOrder.ApplySKUnit;
       end;
     end
      else
    if LMsg = 'confirm_hospital_for_new_order' then
     begin
      if Assigned(FormNewOrder) then
       begin
        FormNewOrder.ApplyHospital;
       end;
     end
      else
    if LMsg = 'play_client_from_sk' then
     self.PlaySound(Form.Main.SOUND_CLIENT_FROM_SK)
      else
    if LMsg = 'stop_playing_client_from_sk' then
     self.StopSound(Form.Main.SOUND_CLIENT_FROM_SK)
      else
    if LMsg = 'start_blinking_client_from_sk' then
      self.NewSKOrderStartBlinking
      else
    if LMsg = 'stop_blinking_client_from_sk' then
     begin
      self.NewSKOrderStopBlinking;
     end;

end;

procedure TfrmMain.ProcessTCPMessages(AMsg: string);
var
 ts: TStrings;
 i: integer;
 LCommand, LCommandBuf: string;
 LOrderId, LCarId, LChatId: integer;
begin
  if AMsg.Contains('|') then
   begin
    ts := TStringList.Create;
      try
        ts.Delimiter := '|';
        ts.DelimitedText := AMsg;
        i := ts.IndexOfName('command');
        TLogger.Save('[TfrmMain.ProcessTCPMessages] msg='+AMsg);
         if i >=0  then
           begin
            LCommand := ts.Values['command'];
             if (LCommand.ToUpper = 'UPDATEORDER') or (LCommand.ToUpper = 'SETORDERSTATE') or
                (LCommand.ToUpper = 'UPDATEORDERPART1') or (LCommand.ToUpper = 'UPDATEORDERPART2') or
                (LCommand.ToUpper = 'DELETESURVEY') or (LCommand.ToUpper = 'INSERTORDER') or
                (LCommand.ToUpper = 'RUNSURVEY') or (LCommand.ToUpper = 'SETSURVEYSTATE') then
              begin
                //if (self.ViewCurrent = TViewOrders) then
                 begin
                  LOrderId := -1;
                   try
                    if ts.Values['orderId']<>'' then
                     LOrderId := ts.Values['orderId'].ToInteger;
                   except on E:Exception do
                     begin
                       TLogger.Save('[TfrmMain.ProcessTCPMessages] UPDATEORDER or SETORDERSTATE err: '+e.Message+' AMsg='+AMsg, TLogLevel.Error);
                       LOrderId := -1;
                     end;
                   end;
                   if (LCommand.ToUpper <> 'DELETESURVEY') then
                     ModelDataGlobal.AddCommand('UPDATEORDER' {LCommand.ToUpper} , LOrderId.ToString)
                      else
                     ModelDataGlobal.AddCommand('DELETESURVEY' {LCommand.ToUpper} , LOrderId.ToString);
                   //TViewOrders(Self.Views[self.ViewCurrent]).UpdateOrder(LOrderId);
                 end;
              end
               else
             if LCommand.ToUpper = 'SENDLOCATION' then
              begin
                   try
                    if ts.Values['carId']<>'' then
                     LCarId := ts.Values['carId'].ToInteger;
                   except on E:Exception do
                     begin
                       TLogger.Save('[TfrmMain.ProcessTCPMessages] SENDLOCATION err: '+e.Message+' AMsg='+AMsg, TLogLevel.Error);
                       LCarId := -1;
                     end;
                   end;
                   LCommandBuf := LCarId.ToString+','+ts.Values['lat']+','+ts.Values['lng'];
                   ModelDataGlobal.AddCommand(LCommand.ToUpper, LCommandBuf);
              end
               else
             if LCommand.ToUpper = 'SETSTATE' then
              begin
                   try
                    if ts.Values['carId']<>'' then
                     LCarId := ts.Values['carId'].ToInteger;
                   except on E:Exception do
                     begin
                       TLogger.Save('[TfrmMain.ProcessTCPMessages] SETSTATE err: '+e.Message+' AMsg='+AMsg, TLogLevel.Error);
                       LCarId := -1;
                     end;
                   end;
                   LCommandBuf := LCarId.ToString;
                   ModelDataGlobal.AddCommand(LCommand.ToUpper, LCommandBuf);
              end
               else
             if LCommand.ToUpper = 'UPDATECHAT' then
              begin

                 try
                  if ts.Values['id']<>'' then
                   begin
                    LChatId := ts.Values['id'].ToInteger;
                    LCommandBuf := LChatId.ToString;
                    ModelDataGlobal.AddCommand(LCommand.ToUpper, LCommandBuf);
                   end;
                   except on E:Exception do
                     begin
                       TLogger.Save('[TfrmMain.ProcessTCPMessages] UPDATECHAT err: '+e.Message+' AMsg='+AMsg, TLogLevel.Error);
                       LCarId := -1;
                     end;
                 end;

              end;

           end;
      finally
        ts.Free;
      end;
   end;

end;

procedure TfrmMain.ProcessWebConsoleMessages(AMsg: string);
var
 ts: TStrings;
 i: integer;
 LCommand: string;
 LOrderId: integer;
 LKeyVal: TKeyValue;
begin
  if AMsg.Contains('command=') then
   begin
     LKeyVal := Core.Funcs.ExtractKeyValue(AMsg);
     ModelDataGlobal.AddCommand('WEBCONSOLE', LKeyVal.Value.ToUpper);
   end;

end;

procedure TfrmMain.RequestPermissions;
const
  PermissionAccessFineLocation = 'android.permission.ACCESS_FINE_LOCATION';
  PermissionAccessCoarseLocation = 'android.permission.ACCESS_COARSE_LOCATION';
  PermissionAccessNetworkState = 'android.permission.ACCESS_NETWORK_STATE';
begin
 exit;
 {$ifdef ANDROID}
   PermissionsService.RequestPermissions([PermissionAccessFineLocation, PermissionAccessCoarseLocation, PermissionAccessNetworkState],
      procedure(const APermissions: TArray<string>; const AGrantResults: TArray<TPermissionStatus>)
      begin
        if (Length(AGrantResults) = 3) and (AGrantResults[0] = TPermissionStatus.Granted) and (AGrantResults[1] = TPermissionStatus.Granted) and (AGrantResults[2] = TPermissionStatus.Granted) then
          self.FPermissionsGranted := true
     end);
 {$endif}
end;

procedure TfrmMain.ShowArchive;
var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Архив');
  LViewParams.ViewClass := 'TViewOrdersArchive';
  LViewParams.IsModal := false;
  LViewParams.ActionButtons := [abFilter, abRefresh];
  LViewParams.MainMenuVisible := true;
  self.ShowView(LViewParams);
end;

procedure TfrmMain.ShowCars;
var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Бригады');
  LViewParams.ViewClass := 'TViewCars';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abCancel, abOK];
  LViewParams.MainMenuVisible := true;
  self.ShowView(LViewParams);
end;

procedure TfrmMain.ShowCarsFilter;
begin
 self.ShowFilter(TViewFilterCars);
end;

procedure TfrmMain.ShowChat;
var
 LViewParams: TViewParams;
begin
  inherited;

  self.circleNewChatMessages.Visible := false;
  self.lbNewChatMessages.Tag := 0;

  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Чат');
  LViewParams.ViewClass := 'TViewChat';
  LViewParams.IsModal := false;
  LViewParams.ActionButtons := [abRefresh];
  LViewParams.MainMenuVisible := true;
  LViewParams.Caller := ACaller;

  self.ShowView(LViewParams);
end;

procedure TfrmMain.ShowChatRcpt;
var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Выберите получателя');
  LViewParams.ViewClass := 'TViewChatRecpt';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abBack, abOK];
  LViewParams.MainMenuVisible := true;
  self.ShowView(LViewParams);
end;

procedure TfrmMain.ShowFilter(AView: ClassViewBase; AIsModal: boolean = false);
var
 LForm: TFormBase;
begin

 TLogger.Save('[TfrmMain.ShowFilter] AView='+AView.ClassName);

 if not AIsModal then
  LForm := self else LForm := FormModal;

 LForm.HideShadow;

 LForm.MakeMainFilter;

 if Assigned(LForm.ViewFilterCurrent) then
  if Assigned(self.Views[LForm.ViewFilterCurrent]) then
   begin
    self.Views[LForm.ViewFilterCurrent].Visible := false;
    self.Views[LForm.ViewFilterCurrent].Parent := nil;
   end;

 LForm.ViewFilterCurrent := AView;
 self.Views[AView].Parent := LForm.multiFilter;
 self.Views[AView].Align := TAlignLayout.Client;
 self.Views[LForm.ViewFilterCurrent].Visible:= true;

 if ( (AView = TViewChat) or (AView = TViewOrderLog) ) then
  begin
   LForm.multiFilter.Width := LForm.Width*0.95;
   self.Views[LForm.ViewFilterCurrent].Caller := View.Base.CHAT_CALLER_ORDER;
  end
   else
  LForm.multiFilter.Width := LForm.Width*0.8;

 LForm.multiFilter.ShowMaster;

 //поскольку выезжает менюха 250 мс (по умолчанию), то ждем это время (+ 10 мс на всякий), потом AfterShow
 TThread.CreateAnonymousThread(
            procedure()
              begin
                Sleep(260);
                TThread.Synchronize(TThread.CurrentThread,
                procedure
                  begin
                    self.Views[AView].AfterShow;
                  end);
              end).Start;

end;



procedure TfrmMain.ShowHospitals;
var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Больницы');
  LViewParams.ViewClass := 'TViewHospitals';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abBack, abOK];
  LViewParams.MainMenuVisible := true;
  LViewParams.Caller := ACaller;
  self.ShowView(LViewParams);
end;


procedure TfrmMain.ShowNewMessageForOrderNotification;
var
  LNotification: TNotification;
begin
  if not self.Views[TViewChat].Visible then
     begin
       self.circleOrderChat.Visible := true;
       self.PlaySound(Form.Main.SOUND_CHAT);

       {$ifdef ANDROID}
         LNotification := self.FNotificationCenter.CreateNotification;
          try
            LNotification.Name := 'medins_notification';
            LNotification.AlertBody := 'Новое сообщение';
            LNotification.Number := 126;
            LNotification.AlertAction := 'Показать';
            LNotification.HasAction := True;
            LNotification.EnableSound:=false;
            self.FNotificationCenter.PresentNotification(LNotification);
            self.FNotificationCenter.OnReceiveLocalNotification:=self.NotificationOrdersClicked;
          finally
            LNotification.DisposeOf;
          end;
       {$endif}
     end;
end;

procedure TfrmMain.ShowNewMessageNotification;
 var
  LNotification: TNotification;
begin
  if not self.Views[TViewChat].Visible then
     begin
       self.circleNewChatMessages.Visible := true;
       self.lbNewChatMessages.Tag := self.lbNewChatMessages.Tag + 1;
       self.lbNewChatMessages.Text := self.lbNewChatMessages.Tag.ToString;
       self.PlaySound(Form.Main.SOUND_CHAT);

       {$ifdef ANDROID}
         LNotification := self.FNotificationCenter.CreateNotification;
          try
            LNotification.Name := 'medins_notification';
            LNotification.AlertBody := 'Новое сообщение';
            LNotification.Number := 125;
            LNotification.AlertAction := 'Показать';
            LNotification.HasAction := True;
            LNotification.EnableSound:=false;
            self.FNotificationCenter.PresentNotification(LNotification);
            self.FNotificationCenter.OnReceiveLocalNotification:=self.NotificationClicked;
          finally
            LNotification.DisposeOf;
          end;
       {$endif}
     end;
end;

procedure TfrmMain.ShowNewOrder;
var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Новый вызов');
  LViewParams.ViewClass := 'TViewNewOrder';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abCancel, abOK];
  LViewParams.MainMenuVisible := true;
  //self.ShowView(LViewParams);

  if not Assigned(FormNewOrder) then
   FormNewOrder := TFormNewOrder.Create(self);

  FormNewOrder.Title := LViewParams.Title;
  FormNewOrder.ActionButtons := LViewParams.ActionButtons;

  FormNewOrder.vsbScrollContent.Parent := FormNewOrder.ltFormContentWrapper;

  FormModal.Close;
  FormNewOrder.Clear;

  if not Assigned(ModelDataGlobal.Hospitals) then
   begin
     self.ShowHospitals;
     self.Views[TViewHospitals].CloseModal;
   end;

  FormNewOrder.Show;
end;

procedure TfrmMain.ShowNewOrder2;
var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Новый вызов');
  LViewParams.ViewClass := 'TViewNewOrder2';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abCancel, abOK];
  LViewParams.MainMenuVisible := true;
  self.ShowView(LViewParams);
end;

procedure TfrmMain.ShowShadow;
begin
 self.rectOverShadow.Position.X := 0;
 self.rectOverShadow.Position.Y := 0;
 self.rectOverShadow.Width := self.Width;
 self.rectOverShadow.Height := self.Height;
 //self.wbMap.Visible := false;
 self.rectOverShadow.Visible := true;
end;

procedure TfrmMain.ShowSKList(ACaller: integer = 1);
var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Список страховых');
  LViewParams.ViewClass := 'TViewSKList';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abBack];
  LViewParams.MainMenuVisible := true;
  LViewParams.Caller := ACaller;
  self.ShowView(LViewParams);
end;

procedure TfrmMain.ShowView(AView: ClassViewBase; AFormLook:TFormLook = flToolBarMenu);
begin
 self.HideShadow;

 if Assigned(self.ViewCurrent) then
  if Assigned(self.Views[self.ViewCurrent]) then
   begin
    self.Views[self.ViewCurrent].BeforeClose;
    self.Views[self.ViewCurrent].Visible := false;
    self.Views[self.ViewCurrent].Parent := nil;
   end;

 self.ViewCurrent := AView;
 self.Views[AView].Parent := self.ltFormContentWrapper;
 self.Views[AView].Align := TAlignLayout.Client;
 self.Views[self.ViewCurrent].Visible:= true;

 FormModal.FormLook := AFormLook;

 self.Views[AView].AfterShow;
end;

procedure TfrmMain.ShowView(AViewParams: TViewParams);
begin
 inherited;
end;

procedure TfrmMain.ShowViewAuth;
var
 LViewParams:TViewParams;
begin
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate(Model.LangConsts.LANG_LOG_IN);
  LViewParams.ViewClass := 'TViewAuth';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abNone];
  LViewParams.MainMenuVisible := false;
  self.ShowView(LViewParams);
end;

procedure TfrmMain.ShowViewOrders;
var
 LViewParams: TViewParams;
begin
  inherited;

  self.circleOrderChat.Visible := false;

  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate(Model.LangConsts.LANG_CALLS);
  LViewParams.ViewClass := 'TViewOrders';
  LViewParams.IsModal := false;
  LViewParams.ActionButtons := [abAdd, abRefresh];
  LViewParams.MainMenuVisible := true;
  LViewParams.Caller := ACaller;

  self.ShowView(LViewParams);
end;

procedure TfrmMain.StopSound(AWav: integer);
begin

   case AWav of
     Form.Main.SOUND_CLIENT_FROM_SK:
      begin
       self.timSoundSKClient.Enabled := false;
       self.mpSoundSKClient.Tag := -1;
      end;
   end;

 end;


procedure TfrmMain.timBlinkSKClientTimer(Sender: TObject);
begin
  inherited;

  if self.timBlinkSKClient.Tag = -1 then
   begin
    self.PlaySound(Form.Main.SOUND_CLIENT_FROM_SK);
    self.timBlinkSKClient.Tag := 0;
    self.circleSKOrder.Visible := true;
   end;

  self.lbSKOrder.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];

  if self.timBlinkSKClient.Tag = 1 then
    begin
      self.circleSKOrder.Fill.Color := TAlphaColorRec.Crimson;
      self.lbSKOrder.TextSettings.FontColor := TAlphaColorRec.White;
      self.timBlinkSKClient.Tag := 0;
    end
     else
    begin
      self.circleSKOrder.Fill.Color := TAlphaColorRec.Aliceblue;
      self.lbSKOrder.TextSettings.FontColor := TAlphaColorRec.Black;
      self.timBlinkSKClient.Tag := 1;
    end;
  //todo
end;

procedure TfrmMain.timCheckTCPClientTimer(Sender: TObject);
var
 LConnected: boolean;
begin
  inherited;

  try

   self.timCheckTCPClient.Enabled := false;
    try
      begin
        if not Assigned(ModelDataGlobal.AppSettings.SMPService) then exit;

        if ModelDataGlobal.AppSettings.SMPService.IP = '' then exit;

        LConnected := self.FTcpClient.Connected;

          if not LConnected then
            begin
              try
                begin
                self.FTcpClient.Connected := true;
                end;
              except on E:Exception do
                TLogger.Save('[TfrmMain.timCheckTCPClientTimer] err:'+e.Message, TLogLevel.Error);
              end;
            end;

      end;

    finally
     self.timCheckTCPClient.Enabled := true;
    end;

  Except on E:Exception do

  end;

end;

procedure TfrmMain.ShowViewMap(ADisableAfterShow: boolean = false);
var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate(Model.LangConsts.LANG_MAP);
  LViewParams.ViewClass := 'TViewMap';
  LViewParams.IsModal := false;
  LViewParams.ActionButtons := [abRefresh];
  LViewParams.MainMenuVisible := true;
  LViewParams.DisableAfterShow := ADisableAfterShow;
  self.ShowView(LViewParams);
end;

procedure TfrmMain.ShowViewModal(Sender:TObject; AView: ClassViewBase; APosition: TModalPosition = mpCenter;  AFormLook: TFormLook = flToolBarMenu);
begin
 //todo
 self.ShowShadow;

 FormModal.ModalSender := Sender;
 {$IF DEFINED(ANDROID) OR DEFINED(IOS)}
   FormModal.Left := 0;
   FormModal.Top := 0;
 {$ENDIF}

 if Assigned(FormModal.ViewCurrent) then
  begin
   self.Views[FormModal.ViewCurrent].BeforeClose;
   self.Views[FormModal.ViewCurrent].Parent := nil;
   self.Views[FormModal.ViewCurrent].Visible := false;
  end;

 FormModal.Width := round(self.Views[AView].Width);
 FormModal.Height := round(self.Views[AView].Height);
 FormModal.ModalPosition := APosition;
 FormModal.ViewCurrent := AView;
 self.Views[AView].Parent := FormModal.ltFormContentWrapper;
 self.Views[AView].Align := TAlignLayout.Client;
 self.Views[AView].Visible := true;

 FormModal.FormLook := AFormLook;

 FormModal.Show;
 self.Views[AView].AfterShow;

 if AView = TViewChatRecpt then
  begin
    self.Views[TViewCars].Parent := (self.Views[AView] as TViewChatRecpt).TabItem1;
    self.Views[TViewSKList].Parent := (self.Views[AView] as TViewChatRecpt).TabItem2;
    self.Views[TViewCars].Caller := View.Base.VIEWCARS_CALLER_CHAT_RECPT;
    self.Views[TViewSKList].Caller:= View.Base.SKLIST_CALLER_CHAT_RECPT;
    self.Views[TViewCars].AfterShow;
    self.Views[TViewSKList].AfterShow;
  end;

end;

procedure TfrmMain.ApplicationEventMessageHandler(const Sender: TObject;
  const M: TMessage);
begin
  {$ifdef ANDROID}
    if M is TApplicationEventMessage then
    begin
      case TApplicationEventMessage(M).Value.Event of
        TApplicationEvent.BecameActive:
          TAndroidHelperEx.EnableWakeLock(False);
        TApplicationEvent.WillBecomeInactive:
          TAndroidHelperEx.EnableWakeLock(True);
      end;
    end;
  {$endif}
end;

procedure TfrmMain.btnArchiveClick(Sender: TObject);
begin
 self.ShowViewOrders;
end;

procedure TfrmMain.Timer1Timer(Sender: TObject);
begin
 self.Timer1.Enabled := false;
 if self.FIsFirstRun then
  self.ShowViewModal(self, TViewAuth, mpCenter, flSimple);
end;

procedure TfrmMain.timSoundSKClientTimer(Sender: TObject);
var
 LFileName: string;
begin
  inherited;
  if self.mpSoundSKClient.Tag = -1 then exit;

  self.timSoundSKClient.Enabled := false;

   try
    {$ifdef MSWINDOWS}
     LFileName := ExtractFilePath(ParamStr(0))+'sound\ClientFromSK.wav';
    {$endif}
    {$ifdef ANDROID}
     LFileName := System.IOUtils.TPath.Combine(System.IOUtils.TPath.GetDocumentsPath, 'ClientFromSK.wav');
    {$endif}

    if not (FileExists(LFileName)) then
     begin
      TLogger.Save('[TfrmMain.timSoundSKClientTimer] LFileName not found '+LFileName);
      exit;
     end;

    {$ifdef ANDROID}
     if self.mpSoundSKClient.FileName.Trim = '' then
    {$endif}
      self.mpSoundSKClient.FileName := LFileName;
    self.mpSoundSKClient.Play;

   finally
    self.timSoundSKClient.Enabled := true;
   end;
end;

procedure TfrmMain.SetBounds(ALeft, ATop, AWidth, AHeight: integer);
 begin
   inherited;
   //это чтоб модальное окошко (которое на самом деле просто StayOnTop) двигалось вместе с гл. формой
   if Assigned(FormModal) then FormModal.InvalidateForm;
 end;

end.
