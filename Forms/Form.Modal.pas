﻿unit Form.Modal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts, View.Base,
  FMX.Controls.Presentation, FMX.StdCtrls, Form.Base;

type

  /// <summary>
  ///   Позиция модального окна:
  ///   mpCenter - будет центрироваться относительно гл. формы,
  ///   mpSender - будет стараться прижаться к контролу, ее вызвавшему
  /// </summary>
  TModalPosition = (mpCenter, mpControl);

  TFormModal = class(TFormBase)
  private
   FModalPosition: TModalPosition;
   /// <summary>
   ///   Кто вызвал мод. окошко. Форма или контрол.
   /// </summary>
   FModalSender: TObject;
   procedure SetModalPosition(AValue:TModalPosition);
    { Private declarations }
  public
    { Public declarations }
    property ModalPosition:TModalPosition read FModalPosition write SetModalPosition;
    property ModalSender:TObject read FModalSender write FModalSender;
    procedure InvalidateForm;
  end;

var
  FormModal: TFormModal;

implementation

 uses
  Form.Main;

{$R *.fmx}

{ TFormModal }

procedure TFormModal.InvalidateForm;
begin
 self.SetModalPosition(self.FModalPosition);
end;

procedure TFormModal.SetModalPosition;
var
 LForm:TForm;
begin

 self.FModalPosition := AValue;

 if (not Assigned(ModalSender)) then exit;

 if (not Assigned(ModalSender)) or (not ModalSender.InheritsFrom(TControl)) then FModalPosition := mpCenter;

 if (ModalSender is TForm) then LForm := ModalSender as TForm;

 case FModalPosition of
  mpCenter:
   begin
     {$if defined(MSWINDOWS) or defined(MACOS) }
      self.Left := LForm.Left + LForm.Width div 2 - self.Width div 2;
      self.Top := LForm.Top + LForm.Height div 2 - self.Height div 2;
     {$else}
      self.Left := 0;
      self.Top := 0;
      self.Width := LForm.Width;
      self.Height := LForm.Height;
     {$endif}
   end;
  mpControl:
   begin
     //лень писать нормальную логику, учитывающую уезжание за границы формы или экрана. Для демки достаточно.
     self.Left := round((ModalSender as TControl).Position.X - 200);
     self.Top := round((ModalSender as TControl).Position.Y + 30);
   end;
 end;
end;

end.
