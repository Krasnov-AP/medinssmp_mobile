unit Form.NewOrder;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.ListBox,
  FMX.Edit, FMX.DateTimeCtrls, FMX.StdCtrls, FMX.Controls.Presentation,
  FMX.Layouts, Form.Base, FMX.Memo.Types, FMX.EditBox, FMX.NumberBox,
  FMX.ScrollBox, FMX.Memo, FMX.Objects, Core.Common, Helper.Yandex.Adr.Autocomplete,
  System.DateUtils, Model.Global, Helper.SKAutocomplete, Helper.Hospitals.Autocomplete,
  Model.Hospital, FMX.ListView.Appearances, Controller.Yandex.Geocode, Model.Yandex.Geocode,
  Controller.AnyData, Controller.Orders, Model.Orders, Model.SkList;

type
  TFormNewOrder = class(TFormBase)
    vsbScrollContent: TVertScrollBox;
    ltScrollableContent: TLayout;
    ltAdr: TLayout;
    lbAdr: TLabel;
    edAddressAutocomplete: TEdit;
    ltAdrDetail: TLayout;
    lbAdrDetail: TLabel;
    edAdrDetail: TEdit;
    ltBirthday: TLayout;
    GridPanelLayout1: TGridPanelLayout;
    lbBirthDay: TLabel;
    lbAge: TLabel;
    dtBirthday: TDateEdit;
    edAge: TEdit;
    ltCallType: TLayout;
    cbCallType: TComboBox;
    ltContacts: TLayout;
    lbContacts: TLabel;
    edContacts: TEdit;
    ltFIO: TLayout;
    lbFIO: TLabel;
    edFIO: TEdit;
    ltIndications: TLayout;
    lbIndications: TLabel;
    ltMKADRean: TLayout;
    GridPanelLayout2: TGridPanelLayout;
    lbMKADDistance: TLabel;
    edMKADDistance: TNumberBox;
    cbRean: TCheckBox;
    ltPayType: TLayout;
    lbPayType: TLabel;
    cbPayType: TComboBox;
    ltPolisNum: TLayout;
    lbPolisNum: TLabel;
    edPolisNum: TEdit;
    ltTransportation: TLayout;
    Layout1: TLayout;
    lbTransportationTo: TLabel;
    edTransportationTo: TEdit;
    Layout3: TLayout;
    lbTransportationAdr: TLabel;
    edTransportationAdr: TEdit;
    Timer1: TTimer;
    edIndications: TEdit;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    ltCustomer: TLayout;
    Label1: TLabel;
    edCustomer: TEdit;
    Image8: TImage;
    Image9: TImage;
    Layout2: TLayout;
    Label2: TLabel;
    edCustomerAgent: TEdit;
    Image10: TImage;
    Layout4: TLayout;
    Label3: TLabel;
    edNormTime: TNumberBox;
    Layout5: TLayout;
    Label4: TLabel;
    edOrderCost: TNumberBox;
    Layout6: TLayout;
    Label5: TLabel;
    Image11: TImage;
    edComment: TEdit;
    Image12: TImage;
    ltHelpTypes: TLayout;
    Label6: TLabel;
    cbHelpType0: TCheckBox;
    cbHelpType1: TCheckBox;
    cbHelpType2: TCheckBox;
    cbHelpType3: TCheckBox;
    cbHelpType4: TCheckBox;
    cbHelpType5: TCheckBox;
    cbHelpType6: TCheckBox;
    cbHelpType7: TCheckBox;
    timAdr: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure vsbScrollContentTap(Sender: TObject; const Point: TPointF);
    procedure btnClearClick(Sender: TObject);
    procedure cbCallTypeChange(Sender: TObject);
    procedure dtBirthdayChange(Sender: TObject);
    procedure Image9Click(Sender: TObject);
    procedure Image7Click(Sender: TObject);
    procedure timAdrTimer(Sender: TObject);
    procedure edAdrDetailChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  strict private
   FCurrentControl: TFmxObject;
   FMousePos: TPointF;
   FKBBounds: TRect;
   FCoords: string;
   FControllerOrder: TControllerOrder;
  strict private
   function GetControl(AParent:TLayout; ACoords: TPointF; AWithOffset: boolean = true): TLayout;
   procedure OnSelectHospital(Sender: TObject);
   procedure OnSelectAdr(Sender: TObject);
   procedure OnYandexGeocodeSuccess2(Sender: TObject);
   procedure OnYandexGeocodeFail2(Sender: TObject);
   procedure OnOrderSuccess;
   procedure OnOrderFail;
   function CollectHelpTypes: string;
   procedure ClearHelpTypes;
  public
   procedure Clear;
   procedure ApplySKUnit;
   procedure ApplyHospital;
   procedure GetGeocodeData(address: string);
   procedure SaveOrder;

  end;

var
  FormNewOrder: TFormNewOrder;

implementation

{$R *.fmx}

procedure TFormNewOrder.ApplyHospital;
begin
 self.edTransportationTo.Text := ModelDataGlobal.CurrentHospital.Name;
 self.edTransportationTo.TagObject := ModelDataGlobal.CurrentHospital;
 self.edTransportationAdr.Text := ModelDataGlobal.CurrentHospital.Address;
 if not self.Visible then self.Show;
end;

procedure TFormNewOrder.ApplySKUnit;
begin
 self.edCustomer.Text := ModelDataGlobal.CurrentSKUnit.Name;
 self.edCustomer.TagObject := ModelDataGlobal.CurrentSKUnit;
 SKAutocomplete.PrevValue := self.edCustomer.Text;
 if not self.Visible then self.Show;
end;

procedure TFormNewOrder.btnClearClick(Sender: TObject);
begin
 if (Sender is TButton) or (Sender is TImage) then
  if (Sender as TFmxObject).Parent is TEdit then
   ((Sender as TFmxObject).Parent as TEdit).Text := '';
end;

procedure TFormNewOrder.Button1Click(Sender: TObject);
begin
  inherited;
  TToast.Show(self.CollectHelpTypes);
end;

procedure TFormNewOrder.cbCallTypeChange(Sender: TObject);
begin
  inherited;
  if self.cbCallType.ItemIndex = 1 then
   begin
    self.edTransportationTo.Enabled := true;
    self.edTransportationAdr.Enabled := true;
    self.lbTransportationTo.FontColor := TAlphaColorRec.White;
    self.lbTransportationAdr.FontColor := TAlphaColorRec.White;
   end
    else
   begin
    self.edTransportationTo.Enabled := false;
    self.edTransportationAdr.Enabled := false;
    self.lbTransportationTo.FontColor := TAlphaColorRec.Gray;
    self.lbTransportationAdr.FontColor := TAlphaColorRec.Gray;
   end;
end;

procedure TFormNewOrder.Clear;
begin

 self.cbCallType.ItemIndex := 0;
 self.cbPayType.ItemIndex := -1;
 self.edAdrDetail.Text := '';
 self.edFIO.Text := '';
 self.dtBirthday.Text := '';
 self.edAge.Text := '';
 self.edAddressAutocomplete.Text := '';
 self.edContacts.Text := '';
 self.edIndications.Text := '';
 self.edMKADDistance.Text := '';
 self.cbRean.IsChecked := false;
 self.FCoords := '55.75322,37.622513';

 self.edMKADDistance.Text := '';
 self.edCustomer.Text := '';
 self.edCustomerAgent.Text := '';

 self.cbCallTypeChange(self);

 self.edNormTime.Text := '';
 self.edOrderCost.Text := '';

 self.dtBirthday.Text := '';
 self.edAge.Text := '';

 self.edTransportationTo.Text := '';
 self.edTransportationAdr.Text := '';

 self.ClearHelpTypes;

end;

procedure TFormNewOrder.ClearHelpTypes;
var
 i: integer;
begin
 for i:= 0 to self.ltHelpTypes.ChildrenCount - 1 do
  begin
   if self.ltHelpTypes.Children[i] is TCheckBox then
    begin
      (self.ltHelpTypes.Children[i] as TCheckBox).IsChecked := false;
    end;
  end;
end;

function TFormNewOrder.CollectHelpTypes: string;
var
 i: integer;
 LCheckBox: TCheckBox;
begin
 result := ';';
 for i:= 0 to self.ltHelpTypes.ChildrenCount - 1 do
  begin
   if self.ltHelpTypes.Children[i] is TCheckBox then
    begin
      LCheckBox := self.ltHelpTypes.Children[i] as TCheckBox;
      if LCheckBox.IsChecked then
        result := result + LCheckBox.Tag.ToString + ',';
    end;
  end;
end;

procedure TFormNewOrder.dtBirthdayChange(Sender: TObject);
var
 LDT: TDateTime;
 diff: integer;
begin
  inherited;
  LDT := self.dtBirthday.Date;
  diff := trunc(DaysBetween(now, LDT) / 365);
  self.edAge.Text := diff.ToString;
end;

procedure TFormNewOrder.edAdrDetailChange(Sender: TObject);
begin
  inherited;
  self.timAdr.Enabled := false;
  self.timAdr.Enabled := true;
end;

procedure TFormNewOrder.FormCreate(Sender: TObject);
begin
  FControlledScrollBox:=self.vsbScrollContent;
  YandexAdrAutocomplete.Owner := self;
  YandexAdrAutocomplete.Control := self.edAddressAutocomplete;
  YandexAdrAutocomplete.OnSelectItem := self.OnSelectAdr;
  SKAutocomplete.Owner := self;
  SKAutocomplete.Control := self.edCustomer;
  HospitalsAutocomplete.Owner := self;
  HospitalsAutocomplete.Control := self.edTransportationTo;
  HospitalsAutocomplete.OnSelectItem := self.OnSelectHospital;
  self.FDoCloseOnOKClicked := false;
  //KeyboardHelper.CaptureClicks(self.ltScrollableContent);
end;

procedure TFormNewOrder.FormVirtualKeyboardHidden(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  inherited;
  self.ltScrollableContent.Margins.Top := 0;
  if Assigned(self.FCurrentControl) then
   (self.FCurrentControl as TMemo).HitTest := false;
end;

procedure TFormNewOrder.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
var
 c: TControl;
 pt: TPointF;
begin

  if self.Focused = nil then exit;

  if not KeyboardVisible then exit;

  //������ ����������� ������ � ������ ���������� �������. ������ - � ������� ������� ����������, ������ - � ����������.
  //������� ������ ������ �� 150 ��, ����� ������������

  if self.Focused.GetObject.InheritsFrom(TControl) then
   begin

     pt := Screen.MousePos;
     FMousePos := Screen.MousePos;
     FKBBounds := Bounds;

     self.Timer1.Enabled := false;
     self.Timer1.Enabled := true;

   end;

end;

procedure TFormNewOrder.Timer1Timer(Sender: TObject);
var
 pt: TPointF;
begin
  self.Timer1.Enabled := false;
  pt := self.FMousePos;

   if pt.Y > (screen.Height - self.FKBBounds.Height) then
    begin
     self.ltScrollableContent.Margins.Top := abs((Screen.Height - self.FKBBounds.Height) - pt.Y)*-1;
    end;

end;

function TFormNewOrder.GetControl(AParent:TLayout; ACoords: TPointF; AWithOffset: boolean = true): TLayout;
var
 i:integer;
 LLayout: TLayout;
 LOffsetX, LOffsetY: single;
 LCoords: TPointF;
begin

 result := nil;

 if not Assigned(AParent) then exit;

 LOffsetX := 0;
 LOffsetY := 0;

 if AWithOffset then
  begin
   LOffsetX := self.vsbScrollContent.ViewportPosition.X;
   {$if defined(ANDROID) or defined(iOS)}
    LOffsetY := self.vsbScrollContent.ViewportPosition.Y - 50;
   {$else}
    LOffsetY := self.vsbScrollContent.ViewportPosition.Y;
   {$endif}
  end;

  ACoords.X := ACoords.X + LOffsetX;
  ACoords.Y := ACoords.Y + LOffsetY;

 for i := 0 to AParent.ChildrenCount - 1 do
   begin
     if (AParent.Children[i] is TLayout) then
      begin

      if (AParent.Children[i] is TLayout) then
       LLayout := AParent.Children[i] as TLayout
        else
         exit;

       if (ACoords.X >= LLayout.Position.X) and (ACoords.X <= (LLayout.Width + LLayout.Position.X) ) and
        (ACoords.Y >= LLayout.Position.Y) and (ACoords.Y <= (LLayout.Height + LLayout.Position.Y) ) then
         begin
          if AWithOffset then
            begin
             LCoords.X := ACoords.X - LLayout.Position.X;
             LCoords.Y := ACoords.Y - LLayout.Position.Y;
            end
             else
            begin
             LCoords := ACoords;
            end;

           result := self.GetControl(LLayout, LCoords, false);

           if result = nil then
            result := LLayout;

           exit;
         end;

      end;
   end;

end;

procedure TFormNewOrder.GetGeocodeData(address: string);
var
  LFilter: TStrings;
  LControllerYandexGeocode: TControllerYandexCoord;
begin

  LControllerYandexGeocode := TControllerYandexCoord.Create;

  LFilter := TStringList.Create;
  LFilter.add('address=' + address);
  LFilter.Add('apikey=' + Model.Global.YANDEX_API_KEY);

  LControllerYandexGeocode.OnRequestSuccess2 := self.OnYandexGeocodeSuccess2;
  LControllerYandexGeocode.OnRequestFail2 := self.OnYandexGeocodeFail2;
  LControllerYandexGeocode.RetrieveData(rmGET, LFilter);
end;

procedure TFormNewOrder.Image7Click(Sender: TObject);
begin
  inherited;
  self.SendMessage('show_hospitals_new_order');
end;

procedure TFormNewOrder.Image9Click(Sender: TObject);
begin
  inherited;
  self.SendMessage('show_sklist_new_order');
end;

procedure TFormNewOrder.OnOrderFail;
begin
 self.HideLoader;
 TToast.Show('������ ��������� �� �������:(');
end;

procedure TFormNewOrder.OnOrderSuccess;
begin
  //self.HideLoader;
  self.HideLoader;
  TToast.Show('����� ������� ��������');
  self.Close;
end;

procedure TFormNewOrder.OnSelectAdr(Sender: TObject);
begin
 self.GetGeocodeData(self.edAddressAutocomplete.Text);
end;

procedure TFormNewOrder.OnSelectHospital(Sender: TObject);
var
 LItem: TListViewItem;
 LHospital: THospital;
begin

 if not Assigned(Sender) then exit;

 if not (Sender is TListViewItem) then exit;

 LItem := Sender as TListViewItem;

 if not (LItem.TagObject is THospital) then exit;

 LHospital := LItem.TagObject as THospital;

 self.edTransportationAdr.Text := LHospital.Address;

end;

procedure TFormNewOrder.OnYandexGeocodeFail2(Sender: TObject);
begin
 self.FCoords := '55.75322,37.622513';
end;

procedure TFormNewOrder.OnYandexGeocodeSuccess2(Sender: TObject);
var
  ts: TStrings;
  ya: TYandexGeoData;
  i: integer;
  LControllerYandexGeocode: TControllerYandexCoord;
begin

  if not (Sender is TControllerYandexCoord) then exit;

  LControllerYandexGeocode := TControllerYandexCoord(Sender);

  try

    try
      LControllerYandexGeocode.YandexCoordData.ConvertFromJson(LControllerYandexGeocode.HttpRequest.Response);
    except
      on E: Exception do
      begin
        //self.MakeError(e.Message);
        TLogger.Save('[TFormNewOrder.OnYandexGeocodeSuccess2] err: ' + e.message);
        self.FCoords := '55.75322,37.622513';
        exit;
      end;
    end;

    if Assigned(LControllerYandexGeocode.YandexCoordData.Items) then
     if LControllerYandexGeocode.YandexCoordData.Items.Count > 0 then
      begin
       //self.edContacts.text := LControllerYandexGeocode.YandexCoordData.Items[0].Lon + ' ' + LControllerYandexGeocode.YandexCoordData.Items[0].Lat;
       self.FCoords := LControllerYandexGeocode.YandexCoordData.Items[0].Lon + ',' + LControllerYandexGeocode.YandexCoordData.Items[0].Lat;
      end
       else
        self.FCoords := '55.75322,37.622513';

  finally
    SafeFree(LControllerYandexGeocode);
  end;

end;

procedure TFormNewOrder.SaveOrder;
var
 o: TOrder;
 LSKUnit: TSKUnit;
 LHospital: THospital;
begin
  self.ShowLoader('���������� ������...');

  o := TOrder.Create;
  o.Name := self.edFIO.Text;
  o.Birthday := self.dtBirthday.Text;
  o.Address := self.edAddressAutocomplete.Text;
  o.Address2 := self.edAdrDetail.Text;
  o.Chat := '%current_time% - ������ ������� ('+ModelDataGlobal.CurrentUser.PersonalInfo.FirstName+' '+ModelDataGlobal.CurrentUser.PersonalInfo.LastName+')';
  o.SMPUserFIO := ModelDataGlobal.CurrentUser.PersonalInfo.FirstName+' '+ModelDataGlobal.CurrentUser.PersonalInfo.LastName;
  o.Coords := self.FCoords;
  o.Contacts := self.edContacts.Text;
  o.DocNum := self.edPolisNum.Text;
  o.Description := self.edIndications.Text;
  o.OrderNote := self.edComment.Text;

  if self.edCustomer.Text.Trim <> '' then
   begin
    if self.edCustomer.TagObject is TSKUnit then
     begin
       LSKUnit := TSKUnit(self.edCustomer.TagObject);
       o.SKCode := LSKUnit.Code;
       self.edCustomerAgent.Text := o.SKCode;
       o.ActionStateSMP := '2';
       o.SKName := LSKUnit.Name;
     end;
   end;

  if self.cbCallType.ItemIndex = 1 then
   begin
    if self.edTransportationTo.Text <> '' then
     begin
      o.TeamAction := '��������������� � '+self.edTransportationTo.Text+' ('+self.edTransportationAdr.Text+')';
      o.Chat := Format('%%current_time%% - ��������������� � '+self.edTransportationTo.Text+' ('+self.edTransportationAdr.Text+')', []) + sLineBreak + o.Chat;
      o.HospitalName := self.edTransportationTo.Text;
      if self.edTransportationTo.TagObject is THospital then
       begin
        LHospital := THospital(self.edTransportationTo.TagObject);
        o.HospitalCoords := LHospital.Coords;
        o.HospitalName := LHospital.Name;
       end;
     end;
   end
    else
   if self.cbCallType.ItemIndex = 2 then
    o.TeamAction := '���������';

  o.TypeId := (self.cbCallType.ItemIndex + 1).ToString;
  o.SMPId := ModelDataGlobal.AppSettings.SMPService.Id.ToString;
  o.State := '1';
  o.HelpTypes := self.CollectHelpTypes;

  if not Assigned(self.FControllerOrder) then
    self.FControllerOrder := TControllerOrder.Create;

  self.FControllerOrder.Order := O;

  self.FControllerOrder.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  self.FControllerOrder.OnRequestSuccess := self.OnOrderSuccess;
  self.FControllerOrder.OnRequestFail := self.OnOrderFail;
  self.FControllerOrder.SaveOrder(1050);
end;

procedure TFormNewOrder.timAdrTimer(Sender: TObject);
begin
  inherited;
  self.timAdr.Enabled := false;
  self.GetGeocodeData(self.edAddressAutocomplete.Text +' '+self.edAdrDetail.Text);
end;

procedure TFormNewOrder.vsbScrollContentTap(Sender: TObject; const Point: TPointF);
var
 L: TLayout;
 p: TPointF;
 LEdit: TEdit;
 LMemo: TMemo;
begin

 exit;

  try

     p.X := Point.X;
     p.Y := Point.Y;
     L := self.GetControl( self.ltScrollableContent, p);

     if Assigned(L.TagObject) then
      if L.TagObject <> nil then
       begin

        if (L.TagObject is TMemo) then
         begin
          self.FCurrentControl := (L.TagObject as TMemo);
          LMemo := L.TagObject as TMemo;
          //LMemo.CanFocus := true;
          LMemo.HitTest := true;
          LMemo.SetFocus;
          //TAppKeyboard.Show(LMemo);
          //KeyboardHelper.Show(L.TagObject as TMemo);
         end;
       end;

  except on E:Exception do
  end;

end;

end.
