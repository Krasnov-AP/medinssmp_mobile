﻿unit Form.ShowMessage;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Objects, Core.Common;

type
  TfrmMessage = class(TForm)
    GridPanelLayout1: TGridPanelLayout;
    lbMessage: TLabel;
    btnOK: TButton;
    Image1: TImage;
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
  public
   procedure ShowMessage(AMsg:string);
    { Public declarations }
  end;

var
  frmMessage: TfrmMessage;

implementation

{$R *.fmx}

{ TfrmMessage }

procedure TfrmMessage.btnOKClick(Sender: TObject);
begin
 self.Close;
end;

procedure TfrmMessage.ShowMessage(AMsg: string);
begin
 self.lbMessage.Text := TLangManager.Translate(AMsg);
 self.Show;
 self.BringToFront;
end;

end.
