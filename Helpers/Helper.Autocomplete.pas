﻿unit Helper.Autocomplete;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, FMX.Memo.Types,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo,
  FMX.Edit, System.SyncObjs,
  FMX.WebBrowser, Controller.AnyData, Controller.Yandex.Geocode, Core.Common;

type
  TAdr = record
    Adr:string;
    Region:string;
    Tag:integer;
  end;

  TAutocomplete = class(TFrame)
    ltAutocompleteContent: TLayout;
    lvAutocompleteItems: TListView;
    timAutocomplete: TTimer;
    timControl: TTimer;
    procedure timAutocompleteTimer(Sender: TObject);
    procedure timControlTimer(Sender: TObject);
  const
   POLLING_ATTEMPTS = 20; //кол-во попыток получить значение edLog, после исчерпания таймер polling останавливается принудительно
   IS_POPUP_KEYBOARD = false; //если true - то всплывашка при фокусе TEdit, если false - то autocomplete
  strict private
   FOnSelectItem: TNotifyEvent;
  strict protected
    FControl: TControl;
    FCritical: System.SyncObjs.TCriticalSection;
    FPrevValue:string;
    FPollingCount: integer;
    FLoader: TAniIndicator;
    FFisrtShow:boolean;
    FCurrentControl: TFmxObject;
    FOwner: TFmxObject;
    FVertPosition: double;
  strict protected
    procedure SetControl(AValue:TControl);
    procedure SetOwner(AValue: TFmxObject);
    procedure ControlChanged(Sender: TObject);
    procedure ControlKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
    procedure ControlTextTyping(Sender: TObject);

    procedure AppendLoader;
    procedure StartLoader;
    procedure StopLoader;

    procedure OnItemClick(const Sender: TObject; const AItem: TListViewItem);
    procedure CreateQuery; virtual;
  public
    property Control: TControl read FControl write SetControl;
    property Owner: TFmxObject read FOwner write SetOwner;
    property VertPosition: double read FVertPosition write FVertPosition;
    property PrevValue: string read FPrevValue write FPrevValue;
    property OnSelectItem: TNotifyEvent read FOnSelectItem write FOnSelectItem;
   public
    constructor Create(AOwner: TComponent); override;
    procedure Show;
    procedure Hide;
  end;

implementation

{$R *.fmx}

{ TAutocomplete }

procedure TAutocomplete.AppendLoader;
var
 LEdit: TEdit;
begin

  if not (self.Control is TEdit) then exit;

  if self.FLoader = nil then
   self.FLoader := TAniIndicator.Create(self.Control);

 self.FLoader.Parent := self.Control;
 self.FLoader.Align := TAlignLayout.Right;

 self.FLoader.Margins.Right := 1;
 self.FLoader.Margins.Top := 1;
 self.FLoader.Margins.Bottom := 1;

 self.FLoader.Visible := false;

end;

procedure TAutocomplete.ControlChanged(Sender: TObject);
begin
  self.timAutocomplete.Enabled := false;
  self.timAutocomplete.Enabled := true;
end;

procedure TAutocomplete.ControlKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
 {$ifdef MSWINDOWS}
  //self.ControlChanged(self.Control); //переехали в ControlTextTyping
 {$endif}
end;

procedure TAutocomplete.ControlTextTyping(Sender: TObject);
begin
 self.ControlChanged(self.FControl);
end;

constructor TAutocomplete.Create(AOwner: TComponent);
begin
  inherited;

   self.FCritical := System.SyncObjs.TCriticalSection.Create;
   self.FPollingCount := 1;

   self.lvAutocompleteItems.ItemAppearanceName := 'ImageListItemBottomDetail';
   self.lvAutocompleteItems.ItemAppearanceObjects.ItemObjects.Text.Font.Size := 18;
   self.lvAutocompleteItems.ItemAppearanceObjects.ItemObjects.Text.Font.Style := [TFontStyle.fsBold];
   self.lvAutocompleteItems.ItemAppearanceObjects.ItemObjects.Text.TextColor := TAlphaColorRec.White;
   self.lvAutocompleteItems.ItemAppearanceObjects.ItemObjects.Detail.TextColor := TAlphaColorRec.Darkgray;
   self.lvAutocompleteItems.ItemAppearanceObjects.ItemObjects.Detail.Visible := true;
   self.lvAutocompleteItems.ItemAppearanceObjects.ItemObjects.Image.Visible := false;
   self.lvAutocompleteItems.ItemAppearanceObjects.ItemObjects.Text.WordWrap := False;

   self.lvAutocompleteItems.OnItemClick := self.OnItemClick;
   self.FFisrtShow := true;
end;

procedure TAutocomplete.CreateQuery;
begin
 //todo
end;

procedure TAutocomplete.Hide;
begin
  self.Visible := false;
end;

procedure TAutocomplete.SetControl(AValue: TControl);
var
 LObj: TFmxObject;
 LY: double;
begin
  self.FControl := AValue;

  if self.FControl = nil then
   begin
    self.timAutocomplete.Enabled := false;
    self.Hide;
    exit;
   end;

  if false {self.IS_POPUP_KEYBOARD} then
   begin
    self.Parent := AValue.Parent;
    self.Align := TAlignLayout.MostTop;
    self.Visible := false;
   end
    else
   begin
       self.Parent := self.FOwner; // Screen.ActiveForm;
       self.FControl := AValue;
       self.Align := TAlignLayout.None;
       self.Width := (self.Parent as TForm).Width;
       self.Margins.Left := self.FControl.Margins.Left;
       self.Margins.Right := self.FControl.Margins.Right;
       LY := (AValue as IControl).LocalToScreen(TPointF.Create(0,0)).Y;
       self.Height := LY - self.FControl.Height;
       {$ifdef MSWINDOWS}
        self.Height := LY - self.FControl.Height - 28;
       {$endif}
       self.Position.Y := 0;
       self.Position.X := 0;
       self.Visible := false;
   end;

  self.ltAutocompleteContent.Margins := AValue.Margins;

  if (AValue is TEdit) then
   begin
    //(AValue as TEdit).OnKeyUp := self.ControlKeyUp;
    if self.IS_POPUP_KEYBOARD then
     (AValue as TEdit).OnTyping := self.ControlTextTyping
      else
       begin
         self.timControl.Enabled := true;
       end;

    self.FPrevValue := (AValue as TEdit).Text;
   end;

  self.AppendLoader;

end;

procedure TAutocomplete.SetOwner(AValue: TFmxObject);
begin
 self.FOwner := AValue;
end;

procedure TAutocomplete.Show;
var
 LY: double;
begin

    LY := self.FControl.Root.Focused.LocalToScreen(TPointF.Create(5,5)).Y - 30;
    self.Height := 250; //self.lvAutocompleteItems.Height;
    {$ifdef MSWINDOWS}
      //self.Height := self.lvAutocompleteItems.Height; //LY - self.FControl.Height - 28;
      LY := LY - 60;
    {$endif}
    self.Position.Y := LY - self.Height - 10;
    self.Position.X := 0;

    TLogger.Save('LY='+LY.ToString);

  self.Visible := true;
end;

procedure TAutocomplete.StartLoader;
begin
  // через синхронизацию только, иначе не работает
  TThread.Synchronize(nil, procedure begin
     self.FLoader.Visible := true;
     self.FLoader.Enabled := true;
   end);
end;

procedure TAutocomplete.StopLoader;
begin
  TThread.Synchronize(nil, procedure begin
     self.FLoader.Visible := false;
     self.FLoader.Enabled := false;
   end);
end;

procedure TAutocomplete.timAutocompleteTimer(Sender: TObject);
var
 LEdit: TEdit;
 LFilter: TStrings;
begin
 self.timAutocomplete.Enabled := false;

 if self.FControl = nil then exit;

 if (self.Control is TEdit) then
  begin
   LEdit := (self.Control as TEdit);

   if LEdit.Text.Trim = '' then
    begin
      self.Hide;
      exit;
    end
     else
      self.Show;

   if self.FPrevValue <> LEdit.Text then
    begin
      self.CreateQuery;
    end;
  end;
end;

procedure TAutocomplete.timControlTimer(Sender: TObject);
var
 LEdit: TEdit;
begin

 try

   self.timControl.Enabled := false;

   if self.IS_POPUP_KEYBOARD then exit;

   if Screen.FocusControl = nil then
     begin
      self.Hide;
      exit;
     end;

   if not (Screen.FocusControl.GetObject is TEdit) then
    begin
     self.Hide;
     exit;
    end;

   LEdit := (Screen.FocusControl.GetObject as TEdit);

   if (LEdit = nil) then
    begin
     self.Hide;
     exit;
    end;

   if self.timAutocomplete.Enabled then
    begin
     exit;
    end;

   if LEdit <> self.FControl then
    begin
     self.Hide;
    end
     else
    begin
      if LEdit.Text <> self.FPrevValue then
       begin

         //self.timControl.Enabled := false;
         //TLogger.Save('LEdit.Text='+LEdit.Text + ' FPrevValue='+self.FPrevValue);
         //self.FPrevValue := LEdit.Text;
         self.ControlChanged(LEdit);

      end;
    end;

  finally
    self.timControl.Enabled := true;
  end;

end;

procedure TAutocomplete.OnItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
 if not (self.Control is TEdit) then exit;

 (self.Control as TEdit).Text := AItem.Text; //+', '+AItem.Detail;
 self.Control.TagObject := AItem.TagObject;
 self.FPrevValue := AItem.Text;

 if Assigned(self.OnSelectItem) then self.OnSelectItem(AItem);

 self.Hide;
end;

end.
