﻿unit Helper.Hospitals.Autocomplete;

interface

 uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, FMX.Memo.Types,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo,
  FMX.Edit, System.SyncObjs,
  FMX.WebBrowser, Core.Common,
  Helper.Autocomplete, Model.Global;

type
  THospitalsAutocomplete = class(TAutocomplete)
  const
   POLLING_ATTEMPTS = 20; //кол-во попыток получить значение edLog, после исчерпания таймер polling останавливается принудительно
   IS_POPUP_KEYBOARD = false; //если true - то всплывашка при фокусе TEdit, если false - то autocomplete
  private
    { Private declarations }
  public
    { Public declarations }
   procedure CreateQuery; override;
  end;

var
 HospitalsAutocomplete: THospitalsAutocomplete;

implementation

{$R *.fmx}

{ TSKAutocomplete }

procedure THospitalsAutocomplete.CreateQuery;
var
 ts:TStrings;
 i, diff: integer;
 lbItem: TListViewItem;
 s: string;
 LEdit: TEdit;
begin

  if not (self.Control is TEdit) then exit;

  self.StartLoader;
  LEdit := self.Control as TEdit;

  try

   self.lvAutocompleteItems.BeginUpdate;
   self.lvAutocompleteItems.Items.Clear;

   ts := TStringList.Create;
     try

        diff := 1;

        for i := 0 to ModelDataGlobal.Hospitals.Items.Count - 1 do
         begin

           if ModelDataGlobal.Hospitals.Items[i].Name.ToUpper.Contains(LEdit.Text.ToUpper) then
            begin

               lbItem := self.lvAutocompleteItems.Items.Add;
               lbItem.Accessory := TAccessoryType.More;

               lbItem.Height := 60;

               lbItem.Text := ModelDataGlobal.Hospitals.Items[i].Name;
               lbItem.Detail := ModelDataGlobal.Hospitals.Items[i].Address;
               lbItem.TagObject := ModelDataGlobal.Hospitals.Items[i];

               lbItem.Tag := i;

               inc(diff);

               if diff > 5 then break;

            end;
         end;

     finally
      self.lvAutocompleteItems.EndUpdate;
      ts.DisposeOf;
     end;

  finally
   self.StopLoader;
   if self.lvAutocompleteItems.Height > 300 then self.lvAutocompleteItems.Height := 300;
  end;

end;

initialization
 HospitalsAutocomplete := THospitalsAutocomplete.Create(nil);


finalization
 if Assigned(HospitalsAutocomplete) then
  HospitalsAutocomplete.DisposeOf;

end.
