﻿unit Helper.Keyboard;

interface

uses

  {$IFDEF MSWINDOWS}
  Winapi.Windows, Winapi.Messages,
  {$ENDIF}
  System.Classes, System.Types, System.SysUtils,
  System.StrUtils, System.IOUtils, System.SyncObjs, System.Generics.Collections,
  System.Messaging, System.Math, FMX.Dialogs, FMX.Platform, FMX.VirtualKeyboard,
  FMX.Memo, FMX.Edit;

type

 TLogLevelHelper = class helper for TEdit
  public
    procedure ShowKeyboard;
    procedure HideKeyboard;
  end;

implementation

{ TLogLevelHelper }

procedure TLogLevelHelper.HideKeyboard;
begin
 //todo
end;

procedure TLogLevelHelper.ShowKeyboard;
begin
 //todo
end;

end.
