﻿/// <summary>
///  Модуль всплывашки для TEdit, TMemo.
/// Всплывающее окно, которое всегда сверху, чтоб клавитатура не перегораживала мему или эдит
/// Нужен для мобильных устройств
/// Для работы необходимо:
///
/// 1) В момент инициализации формы (в OnShow, например) или фрейма (в данном проекте в AfterShow, но можно в любом другом месте)
///    вызвать метод CaptureClicks глобальной переменной KeyboardHelper с указанием родительского контрола.
///  Например:
///  procedure TViewSetOnTrack.AfterShow;
///      begin
///         KeyboardHelper.CaptureClicks(self.ltMain);  //ltMain - главный слой, содержащий все остальные
///      end;
///  !!! Важно !!!
///  Нужно обернуть все слои и контролы в какой-либо слой (в данном случае ltMain).
///  Для всех memo или edit внутри этого контрола будет вызываться всплывашка при нажатии
///
/// 2) Реализовать (по желанию) у формы (контейнера фреймов) или фрейма затемнение содержимого при вызове всплывашки.
///    Для этого нужно перехватывать сообщения
///    show_shadow и hide_shadow
///  Например:
///  procedure TfrmMain.ProcessMessages(const Sender: TObject; const M: TMessage);
///   var
///    LMsg:string;
///     begin
///
///      LMsg := (M as TMessage<String>).Value;
///
///      if LMsg = 'show_shadow' then
///        begin
///         if Assigned(FormViewContainer.ViewCurrent) then
///           if Assigned(self.Views[FormViewContainer.ViewCurrent]) then
///            begin
///              self.Views[FormViewContainer.ViewCurrent].ShowShadow;
///            end;
///        end
///         else
///        if LMsg = 'hide_shadow' then
///         begin
///           if Assigned(FormViewContainer.ViewCurrent) then
///            if Assigned(self.Views[FormViewContainer.ViewCurrent]) then
///             begin
///              self.Views[FormViewContainer.ViewCurrent].HideShadow;
///             end;
///         end;
///   end;
///
/// 3) В методе FormVirtualKeyboardShown той формы, для который реализуется всплывашка или которая содержит фрейм
/// загнать в переменную KeyboardBounds значение области клавиатуры
/// Например:
///   procedure TfrmMain.FormVirtualKeyboardShown(Sender: TObject;
///    KeyboardVisible: Boolean; const Bounds: TRect);
///      begin
///       KeyboardHelper.KeyboardBounds := Bounds;
///      end;
/// 4) !!! У контролов, для которых будет вызываться всплывашка установить CanFocus в false
///
/// </summary>
/// <remarks>
///   <list type="table">
///     <listheader>
///       <term>Параметр</term>
///       <description>Значение</description>
///     </listheader>
///     <item>
///       <term>Версия приложения</term>
///       <description>0.1</description>
///     </item>
///     <item>
///       <term>Дата последнего изменения</term>
///       <description>10.Oct.2021</description>
///     </item>
///     <item>
///       <term>Поддерживаемые платформы разработки</term>
///       <description>Embarcadero Delphi 10.4 Sydney Update 2 и выше</description>
///     </item>
///     <item>
///       <term>Поддерживаемые ОС</term>
///       <description>
///         <p>Windows 7 (SP1+)/8.1/10 (x32/x64)</p>
///         <p>Windows Server 2012 R2/2016/2019 (x32/x64)</p>
///         <p>macOS 10.13/10.14/10.15/11.1 (x64)</p>
///         <p>Android 7.1+</p>
///         <p>IOS не тестировалось</p>
///       </description>
///     </item>
///   </list>
/// </remarks>
unit Helper.View.Input;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Edit, FMX.Controls.Presentation, FMX.Layouts, FMX.Objects, FMX.Memo,
  FMX.Memo.Types, FMX.ScrollBox, System.Messaging, FMX.VirtualKeyboard, FMX.Platform,
  FMX.ListBox, Helper.Yandex.Adr.Autocomplete;

type
  THelperViewInput = class(TFrame)
    rectHelper: TRectangle;
    ltHelperButtons: TLayout;
    gltHelperButtons: TGridPanelLayout;
    btnCancel: TButton;
    btnOK: TButton;
    ltHelperContent: TLayout;
    edText: TEdit;
    moText: TMemo;
    lbPlaceholder: TLabel;
    lbCaption: TLabel;
    btnClear: TButton;
    timCheckMemoEmpty: TTimer;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure timCheckMemoEmptyTimer(Sender: TObject);
    procedure FrameKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
  strict private
    FPrevText: string;
    FPrevControl: TFmxObject;
    FBossParent : TFmxObject;
    FKeyboardBounds: TRect;
  strict private
   procedure ComponentMouseUp(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Single);
   procedure ComponentTap(Sender: TObject; const Point: TPointF);

   function GetBossParent(AControl: TFmxObject): TFMxObject;
   procedure Hide;
   procedure SetKeyboardBounds(AValue:TRect);
   function GetClientHeight:single; //клиентская часть области, где показываем фрейм. За исключением Toolbar-а, если есть
   function GetPlaceholderText(AMemo: TFmxObject):string;
   procedure SendMessage(AMsg:String);
   procedure ShowKeyboard;
    { Private declarations }
  public
    property KeyboardBounds:TRect read FKeyboardBounds write SetKeyboardBounds; //область вирт. клавиатуры
    property ClientHeight:single read GetClientHeight;
  public
    { Public declarations }
    procedure CaptureClicks(AParentLayout:TControl);
    procedure Show(Sender:TControl);
    procedure Accept;
    procedure Decline;
  end;


var
 KeyboardHelper:THelperViewInput; //гл. переменная - сплывашка для TMemo и TEdit

implementation

{$R *.fmx}

{ THelperViewInput }

procedure THelperViewInput.Accept;
begin

 if self.FPrevControl is TEdit then
  begin
   (self.FPrevControl as TEdit).Text := self.edText.Text;
  end;

 if self.FPrevControl is TMemo then
  begin
    (self.FPrevControl as TMemo).Text := self.moText.Text;
  end;

 self.Hide;

end;

procedure THelperViewInput.btnCancelClick(Sender: TObject);
begin
 self.Decline;
end;

procedure THelperViewInput.btnClearClick(Sender: TObject);
begin
 if self.edText.Visible then self.edText.SetFocus;
 if self.moText.Visible then self.moText.SetFocus;

 self.moText.Text := '';
 self.edText.Text := '';
end;

procedure THelperViewInput.btnOKClick(Sender: TObject);
begin
 self.Accept;
end;

procedure THelperViewInput.CaptureClicks(AParentLayout: TControl);
var
 c:TComponent;
 i, cnt:integer;
 LLine: TLine;
begin

  cnt := AParentLayout.Children.Count;

  for I := 0 to cnt - 1 do
   begin
     if (AParentLayout.Children[i] is TLayout) then
      self.CaptureClicks(AParentLayout.Children[i] as TLayout);

     if (AParentLayout.Children[i] is TEdit) then
      begin
       //(AParentLayout.Children[i].Parent as TLayout).OnTap := self.ComponentTap;
       (AParentLayout.Children[i].Parent as TLayout).TagObject := (AParentLayout.Children[i] as TEdit);
       (AParentLayout.Children[i] as TEdit).HitTest := false;
       (AParentLayout.Children[i] as TEdit).CanFocus := true;
       (AParentLayout.Children[i] as TEdit).ControlType := TControlType.Styled;

       {LLine := TLine.Create((AParentLayout.Children[i] as TEdit));
       LLine.Parent := AParentLayout.Children[i];
       LLine.Stroke.Thickness := 2;
       LLine.Stroke.Color := TAlphaColorRec.Lightslategray;
       LLine.LineType := TLineType.Bottom;
       LLine.Align := TAlignLayout.Bottom;}

//       {$IF DEFINED(iOS) or DEFINED(ANDROID)}
//        (AParentLayout.Children[i] as TEdit).OnTap := self.ComponentTap;
//       {$else}
//        (AParentLayout.Children[i] as TEdit).OnMouseUp := ComponentMouseUp;
//       {$endif}
      end;

    if (AParentLayout.Children[i] is TMemo) then
      begin
      //(AParentLayout.Children[i].Parent as TLayout).OnTap := self.ComponentTap;
      (AParentLayout.Children[i] as TMemo).HitTest := false;
      (AParentLayout.Children[i] as TMemo).CanFocus := true;
      (AParentLayout.Children[i].Parent as TLayout).TagObject := (AParentLayout.Children[i] as TMemo);
      (AParentLayout.Children[i] as TMemo).ControlType := TControlType.Styled;

//      {$IF DEFINED(iOS) or DEFINED(ANDROID)}
//       (AParentLayout.Children[i] as TMemo).OnTap := self.ComponentTap;
//      {$else}
//       (AParentLayout.Children[i] as TMemo).OnMouseUp := ComponentMouseUp;
//      {$endif}
      end;

   end;
  //todo
end;

procedure THelperViewInput.ComponentMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
 //todo
 if Sender.InheritsFrom(TControl) then
   self.Show(Sender as TControl);
end;

procedure THelperViewInput.ComponentTap(Sender: TObject; const Point: TPointF);
var
 c:TControl;
begin
 //todo
  if Sender.InheritsFrom(TControl) then
   begin
    c := TControl(Sender);
    self.Show(c.TagObject as TControl);
   end;
end;

procedure THelperViewInput.Decline;
begin

 if self.FPrevControl is TEdit then
  begin
   (self.FPrevControl as TEdit).Text := self.FPrevText;
  end;

 if self.FPrevControl is TMemo then
  begin
    (self.FPrevControl as TMemo).Text := self.FPrevText;
  end;

 self.Hide;

end;

procedure THelperViewInput.FrameKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
var
  FService : IFMXVirtualKeyboardService;
begin
  {$ifdef ANDROID}

  if Key = vkHardwareBack then
  begin
    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(FService));
    if (FService <> nil) and (FMX.Types.TVirtualKeyboardState.Visible in FService.VirtualKeyBoardState) then
     begin
      FService.HideVirtualKeyboard;
      Self.Decline;
    end;
   end
    else
   if Key = vkReturn then
    begin
    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(FService));
    if (FService <> nil) and (FMX.Types.TVirtualKeyboardState.Visible in FService.VirtualKeyBoardState) then
     begin
      FService.HideVirtualKeyboard;
      Self.Accept;
    end;
   end;

  {$ENDIF}
end;

function THelperViewInput.GetBossParent(AControl: TFmxObject): TFMxObject;
begin

 result := AControl.Parent;

 if not ( (result = nil) or (result.InheritsFrom(TForm)) ) then
  result:= self.GetBossParent(result);

end;

function THelperViewInput.GetClientHeight: single;
var
 i:integer;
 LHeight: single;
begin

 if {self.Align = TAlignLayout.Contents} false then
  result := self.Height
   else
  begin
    //нижеследующий код нужен, если наш фрейм установлен в Align=Client, а не Align=Content
    for i := 0 to self.Parent.ComponentCount - 1 do
      begin
        if (self.Parent.Components[i] is TToolBar) then
         begin
           if (self.Parent.Components[i] as TToolBar).Align = TAlignLayout.Top then
            begin
              LHeight := (self.Parent.Components[i] as TToolBar).Height;
              result := Screen.Height - LHeight - 10;
              exit;
            end;
         end;
      end;
  end;

end;

function THelperViewInput.GetPlaceholderText(AMemo: TFmxObject): string;
var
 i, j:integer;
 LComponent: TComponent;
 LParent: TFmxObject;
begin
 result := '';

 if ((not Assigned(AMemo)) or (AMemo = nil)) then exit;

 if not (AMemo.InheritsFrom(TFmxObject)) then exit;

 LParent := self.GetBossParent(AMemo);

 for i := 0 to LParent.ComponentCount - 1 do
  begin
    if (LParent.Components[i] is TLabel) then
     begin
       if (LParent.Components[i] as TLabel).Parent.Parent = AMemo then   //Parent.Parent - т.к. первый предок - TScrollContent
        begin
          result := (LParent.Components[i] as TLabel).Text;
          exit;
        end;
     end;
  end;

 //этот код не работает:(
 {for i := 0 to AMemo.Children.Count - 1 do
  begin

   if not AMemo.Children[i].InheritsFrom(TControl) then continue;

   if AMemo.Children[i] is TLabel then
    begin
      result := (AMemo.Children[i] as TLabel).Text;
      exit;
    end;

   result := self.GetPlaceholderText(AMemo.Children[i]);

  end;}

end;

procedure THelperViewInput.Hide;
begin
 self.timCheckMemoEmpty.Enabled := false;
 self.Parent := nil;
 self.rectHelper.Visible := false;
 self.SendMessage('hide_shadow');
end;

procedure THelperViewInput.SendMessage(AMsg: String);
var
 Message: TMessage;
begin
 Message := TMessage<String>.Create(AMsg);
 TMessageManager.DefaultManager.SendMessage(self, Message, True);
end;

procedure THelperViewInput.SetKeyboardBounds(AValue: TRect);
begin

 self.FKeyboardBounds := AValue;
 if self.Visible and (self.Parent<>nil) then
  self.rectHelper.Height := self.ClientHeight - AValue.Height - 10;

end;

procedure THelperViewInput.Show(Sender: TControl);
var
 LParent:TFmxObject;
 LComponent: TComponent;
 LRectangle: TRectangle;
 LEdit: TEdit;
 LMemo: TMemo;
 s:string;
begin

 try

   self.SendMessage('show_shadow');

   self.moText.Visible := false;
   self.edText.Visible := false;

   LParent := self.GetBossParent(Sender);
   if LParent.InheritsFrom(TControl) then
    self.Width := (LParent as TControl).Width;
   self.Parent := LParent;
   self.Align := TAlignLayout.Contents;

   self.rectHelper.Visible := true;
   self.BringToFront;

   FPrevControl := Sender;

   if (Sender is TEdit) then
    begin
     LEdit := Sender as TEdit;
     FPrevText := LEdit.Text;
     self.moText.Visible := false;
     self.edText.Visible := true;
     self.edText.Text := LEdit.Text;
     self.edText.TextPrompt := LEdit.TextPrompt;
     self.lbCaption.Text := LEdit.Hint;
     self.edText.BringToFront;
     self.edText.SetFocus;
     self.edText.SelStart := self.edText.Text.Length;

     s := LEdit.Name;
     if s.Contains('Autocomplete') then
      YandexAdrAutocomplete.Control := self.edText
       else
      YandexAdrAutocomplete.Control := nil;

    end
     else
    if Sender is TMemo then
     begin
       self.timCheckMemoEmpty.Enabled := true;
       LMemo := Sender as TMemo;
       FPrevText := LMemo.Text;
       self.moText.Visible := true;
       self.moText.Text := LMemo.Text;
       self.edText.Visible := false;
       self.lbPlaceholder.Text := self.GetPlaceholderText(LMemo);
       self.lbCaption.Text := LMemo.Hint;
       //self.lbPlaceholder.BringToFront;
       self.moText.SetFocus;
       self.moText.SelStart := self.moText.Text.Length;
     end;

 finally

 end;

end;

procedure THelperViewInput.ShowKeyboard;
var
  FService : IFMXVirtualKeyboardService;
begin
  {$ifdef ANDROID}

    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(FService));
    if (FService <> nil) and (FMX.Types.TVirtualKeyboardState.Visible in FService.VirtualKeyBoardState) then
     begin

      if self.edText.Visible then
       FService.ShowVirtualKeyboard(self.edText)
        else
         if self.moText.Visible then
          FService.ShowVirtualKeyboard(self.moText);

     end;

  {$ENDIF}
end;

procedure THelperViewInput.timCheckMemoEmptyTimer(Sender: TObject);
begin
 self.lbPlaceholder.Visible := self.moText.Text = '';
end;

initialization
 KeyboardHelper := THelperViewInput.Create(nil);

finalization
if Assigned(KeyboardHelper) then
 KeyboardHelper.DisposeOf;

end.
