﻿unit Helper.Yandex.Adr.Autocomplete;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, FMX.Memo.Types,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo,
  FMX.Edit, System.SyncObjs,
  FMX.WebBrowser, Controller.AnyData, Controller.Yandex.Geocode, Core.Common,
  Helper.Autocomplete;

type
  TAdr = record
    Adr:string;
    Region:string;
    Tag:integer;
  end;

  TYandexAdrAutocomplete = class(TAutocomplete)
  const
   POLLING_ATTEMPTS = 20; //кол-во попыток получить значение edLog, после исчерпания таймер polling останавливается принудительно
   IS_POPUP_KEYBOARD = false; //если true - то всплывашка при фокусе TEdit, если false - то autocomplete
  strict private
    FControllerYandexGeocode: TControllerYandexGeocode;
  strict private
    procedure OnYandexGeocodeSuccess;
    procedure OnYandexGeocodeFail;
    procedure CreateQuery; override;
  public
    constructor Create(AOwner: TComponent); override;
  end;

var
 YandexAdrAutocomplete: TYandexAdrAutocomplete;

implementation

{$R *.fmx}

{ TYandexAdrAutocomplete }

constructor TYandexAdrAutocomplete.Create(AOwner: TComponent);
begin
  inherited;
  self.FControllerYandexGeocode := TControllerYandexGeocode.Create;
end;

procedure TYandexAdrAutocomplete.CreateQuery;
var
 LEdit: TEdit;
 LFilter: TStrings;
begin

   if not (self.Control is TEdit) then exit;

   LEdit := self.Control as TEdit;

   if self.FPrevValue <> LEdit.Text then
    begin
     self.FPrevValue := LEdit.Text;
     if not Assigned(self.FControllerYandexGeocode) then
      self.FControllerYandexGeocode := TControllerYandexGeocode.Create;

     LFilter := TStringList.Create;
     LFilter.add('address='+LEdit.Text);

     self.StartLoader;

     self.FControllerYandexGeocode.OnRequestSuccess := self.OnYandexGeocodeSuccess;
     self.FControllerYandexGeocode.OnRequestFail := self.OnYandexGeocodeFail;
     self.FControllerYandexGeocode.RetrieveData(rmGET, LFilter);
     self.FPollingCount := 1;
    end;
end;

procedure TYandexAdrAutocomplete.OnYandexGeocodeFail;
begin
 self.StopLoader;
end;

procedure TYandexAdrAutocomplete.OnYandexGeocodeSuccess;
var
 ts:TStrings;
 i, diff: integer;
 lbItem: TListViewItem;
 s: string;
begin

  try

    try
     self.FControllerYandexGeocode.YandexGeoData.ConvertFromJson(self.FControllerYandexGeocode.HttpRequest.Response);
    except on E:Exception do
     begin
      exit;
     end;
    end;

   self.lvAutocompleteItems.BeginUpdate;
   self.lvAutocompleteItems.Items.Clear;

   ts := TStringList.Create;
   try
     if Assigned(self.FControllerYandexGeocode.YandexGeoData.Items) then
      begin

        diff := self.FControllerYandexGeocode.YandexGeoData.Items.Count - 5;
        if diff<0 then diff := 0;


        for i := self.FControllerYandexGeocode.YandexGeoData.Items.Count - 1 downto diff do
         begin

           s := self.FControllerYandexGeocode.YandexGeoData.Items[i].Title;
            if s.IndexOf(',')>0 then
             s := s.Substring(0, s.IndexOf(','));

           if ts.IndexOf(s)>=0 then continue;
           

           lbItem := self.lvAutocompleteItems.Items.Add;
           lbItem.Accessory := TAccessoryType.More;

           lbItem.Height := 60;

           lbItem.Text := s;//self.FControllerYandexGeocode.YandexGeoData.Items[i].Title;
           lbItem.Detail := self.FControllerYandexGeocode.YandexGeoData.Items[i].Subtitle;

           if ts.IndexOf(s)<0 then ts.Add(s);
           

           lbItem.Tag := i;
         end;
      end;
   finally
    self.lvAutocompleteItems.EndUpdate;
    ts.DisposeOf;
   end;

  finally
   self.StopLoader;
   if self.lvAutocompleteItems.Height > 300 then self.lvAutocompleteItems.Height := 300;
  end;

end;

initialization
 YandexAdrAutocomplete := TYandexAdrAutocomplete.Create(nil);


finalization
 if Assigned(YandexAdrAutocomplete) then
  YandexAdrAutocomplete.DisposeOf;

end.
