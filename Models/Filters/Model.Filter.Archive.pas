﻿unit Model.Filter.Archive;

interface

uses
  Model.AnyData, XSuperObject, XSuperJson, Core.Common, System.SysUtils;

type

 TFilterArchive = class
   public
    DateFrom, DateTo: TDate;
    State: Integer;
    Active: boolean;
   public
    constructor Create;
 end;

implementation

{ TFilterArchive }

constructor TFilterArchive.Create;
begin
  self.DateFrom := now - 1;
  self.DateTo := now;
  self.State := -1;
  self.Active := false;
end;

end.
