﻿unit Model.AnyData;

interface

uses

 Core.Common,
 System.SyncObjs,
 System.Generics.Collections,
 System.Rtti, System.SysUtils, XSuperObject, XSuperJson;

type

 TClassModelAnyData = class of TModelAnyData;

 TModelAnyData = class
  strict private
   FDataString: string;
   FCritical: TCriticalSection;
  strict private
   procedure ClearFields;
  public
   procedure SetDataString(AValue:string);
   function GetDataString:string;

   function ConvertToJson:string; virtual;
   procedure ConvertFromJson(AValue:string); virtual;

  public
   procedure SetProperty(APropertyName: string; AValue: Variant);
   function GetProperty(APropertyName: string): string;
   procedure Clear; virtual;

  public
   constructor Create;
   destructor Destroy; override;

  public
   procedure Lock;
   procedure Unlock;
   procedure Assign(ASource: TModelAnyData);

 end;

implementation

{ TModelAnyData }

function TModelAnyData.ConvertToJson: string;
begin
 //todo
 result := self.AsJSON;
end;

constructor TModelAnyData.Create;
begin
 inherited;
 self.FDataString := '';
 self.FCritical := TCriticalSection.Create;
end;

destructor TModelAnyData.Destroy;
begin
 SafeFree(self.FCritical);
 inherited;
end;

procedure TModelAnyData.Assign(ASource: TModelAnyData);
var
  ctx: TRttiContext;
  t, t1: TRttiType;
  m: TRttiMethod;
  f: TRttiField;
  V: TValue;
begin
  ctx := TRttiContext.Create;

  t := ctx.GetType(self.ClassType);
  t1 := ctx.GetType(ASource.ClassType);

  //self.Lock; //вываливается Lock object not owned на андроиде:(

  try

    for f in t.GetFields do
      begin

       try
        f.SetValue(self, t1.GetField(f.Name).GetValue(ASource));
       except on E:Exception do
        begin
          TLogger.Save('[TModelAnyData.Assign] Class: '+self.ClassName+' Field: '+f.Name+' err: '+e.Message);
        end;

       end;
      end;

  finally
   //self.Unlock;
  end;

end;

procedure TModelAnyData.Clear;
begin
 self.ClearFields;
end;

procedure TModelAnyData.ClearFields;
var
  ctx: TRttiContext;
  t: TRttiType;
  m: TRttiMethod;
  f: TRttiField;
  V: TValue;
begin
  ctx := TRttiContext.Create;

  t := ctx.GetType(self.ClassType);

  for f in t.GetFields do
    begin
      if f.Name.StartsWith('_') then
        begin
          v := v.FromVariant('');
            try
             f.SetValue(self, v);
            except on E:Exception do
             TLogger.Save('[TModelAnyData.ClearFields] err: '+e.Message, TLogLevel.Error);
            end;
        end;
    end;

end;

procedure TModelAnyData.ConvertFromJson(AValue: string);
begin
 if AValue.Trim = '' then exit;
 self.AssignFromJSON(AValue)
end;

function TModelAnyData.GetDataString: string;
begin
  self.FCritical.Acquire;
  try
    result := self.FDataString;
  finally
    self.FCritical.Release;
  end;
end;

function TModelAnyData.GetProperty(APropertyName: string): string;
var
  ctx: TRttiContext;
  t: TRttiType;
  m: TRttiMethod;
  f: TRttiField;
  p: TRttiProperty;
  V: TValue;
begin
  result := '';
  ctx := TRttiContext.Create;

  t := ctx.GetType(self.ClassType);

  for p in t.GetProperties do
    begin
      if p.Name.ToUpper = APropertyName.ToUpper then
        begin
            try
             result := p.GetValue(self).ToString;
            except on E:Exception do
             TToast.Show(e.Message);
            end;
          exit;
        end;
    end;

end;

procedure TModelAnyData.Lock;
begin
 {$ifdef MSWINDOWS}
  if Assigned(self.FCritical) then
   self.FCritical.Acquire;
 {$endif}
end;

procedure TModelAnyData.SetDataString(AValue: string);
begin
  self.FCritical.Acquire;
  try
    self.FDataString := AValue;
  finally
    self.FCritical.Release;
  end;
end;

procedure TModelAnyData.SetProperty(APropertyName: string; AValue: Variant);
var
  ctx: TRttiContext;
  t: TRttiType;
  m: TRttiMethod;
  f: TRttiField;
  p: TRttiProperty;
  V: TValue;
begin
  ctx := TRttiContext.Create;

  t := ctx.GetType(self.ClassType);

  for p in t.GetProperties do
    begin
      if p.Name.ToUpper = APropertyName.ToUpper then
        begin
          v := v.FromVariant(AValue);
            try
             p.SetValue(self, v);
            except on E:Exception do
             //игнорим
            end;
          exit;
        end;
    end;

end;

procedure TModelAnyData.Unlock;
begin
 {$ifdef MSWINDOWS}
  if Assigned(Self.FCritical) then
   self.FCritical.Release;
 {$endif}
end;

end.
