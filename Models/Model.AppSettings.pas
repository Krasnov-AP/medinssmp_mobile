﻿unit Model.AppSettings;

interface

uses

 System.Generics.Collections, System.Types, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common, Model.SMPService, Model.LangConsts, Model.Filter.Archive;

const
 MAIN_MENU_COUNT = 3; //кол-во пунктов гл. меню


type

  TMainMenuItem =  record
      Title:string;
      Descr:string;
      Tag:byte;
      Visible:boolean;
     end;

 TMainMenuList = TDictionary<byte, TMainMenuItem>;

 TCarsSettings = class
   strict private
    FOnDutyOnly: boolean;
   public
    property OnDutyOnly: boolean read FOnDutyOnly write FOnDutyOnly;
  end;

  TAppSettings = class(TModelAnyData)
    strict private
     [weak] FSMPService:TSMPService;
     [weak] FMainMenuList:TMainMenuList;
     [weak] FimgLogo: TResourceStream;
     [weak] FCarsSettings: TCarsSettings;
     FLocale:string;
     FDoSaveSettings: boolean;
     FFilterArchive: TFilterArchive;
    strict private
     procedure SetLocale(AValue:string);
    public
     [DISABLE]  //для того, чтобы XSuperObject не запихивал это свойство в json при сериализации и сохранении в БД
     property MainMenuList:TMainMenuList read FMainMenuList; //список пунктов гл. меню
     property SMPService:TSMPService read FSMPService write FSMPService; //текущая служба
     [DISABLE]
     property imgLogo:TResourceStream read FimgLogo; //лого Мединс для отображения в гл. меню или еще где
     property Locale:string read FLocale write SetLocale;
     property CarsSettings: TCarsSettings read FCarsSettings write FCarsSettings;
     property DoSaveSettings: boolean read FDoSaveSettings write FDoSaveSettings default false;
     [DISABLE]
     property FilterArchive: TFilterArchive read FFilterArchive write FFilterArchive;
    public
     procedure FillMainMenuList;
     constructor Create;
  end;

implementation

{ TAppSettings }

constructor TAppSettings.Create;
begin
 //todo
 self.FLocale := 'RU';

  if FindResource(0, PChar('logo'), PChar(RT_RCDATA)) <> 0 then
      self.FimgLogo := TResourceStream.Create(0,'logo',PChar(RT_RCDATA));

 self.FillMainMenuList;
 self.FCarsSettings := TCarsSettings.Create;
 self.FCarsSettings.OnDutyOnly := true;
 self.FilterArchive := TFilterArchive.Create;

end;

procedure TAppSettings.FillMainMenuList;
var
 LMainMenuItem:TMainMenuItem;
 i:byte;
begin

 if not Assigned(self.FMainMenuList) then
  self.FMainMenuList := TMainMenuList.Create
   else
  self.FMainMenuList.Clear;

 i := 1;

 LMainMenuItem.Title := TLangManager.Translate(Model.LangConsts.LANG_ARCHIVE);
 LMainMenuItem.Descr := TLangManager.Translate(Model.LangConsts.LANG_CALLS_ARCHIVE);
 LMainMenuItem.Tag := i;
 LMainMenuItem.Visible := true;
 self.FMainMenuList.Add(i-1, LMainMenuItem);
 inc(i);

 LMainMenuItem.Title := TLangManager.Translate(Model.LangConsts.LANG_SETTINGS);
 LMainMenuItem.Descr := TLangManager.Translate(Model.LangConsts.LANG_APP_SETTINGS);
 LMainMenuItem.Tag := i;
 LMainMenuItem.Visible := true;
 self.FMainMenuList.Add(i-1, LMainMenuItem);
 inc(i);

 LMainMenuItem.Title := TLangManager.Translate(Model.LangConsts.LANG_LOGOFF);
 LMainMenuItem.Descr := TLangManager.Translate(Model.LangConsts.LANG_APP_LOGOFF);
 LMainMenuItem.Tag := i;
 LMainMenuItem.Visible := true;
 self.FMainMenuList.Add(i-1, LMainMenuItem);

end;

procedure TAppSettings.SetLocale(AValue: string);
begin
 if self.FLocale <> AValue then
  begin
   TLangManager.Locale := AValue;
   self.FLocale := AValue;
   self.FillMainMenuList;
  end;
end;

end.
