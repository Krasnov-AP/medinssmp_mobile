﻿unit Model.Cars;

interface

 uses
  System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
  System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
  Model.AnyData, Core.Common, Core.Crypt, Core.Funcs, System.Rtti, System.Messaging;

 type

  TCar = class(TModelAnyData)
  const
    FIELD_NAMES = 'Id=580,Login=448,Pwrd=448,Session=448,Name=448,IsDeleted=500,Lat=448,Lng=448,'+
                  'LastUpdateDt=510,Type=500,State=500,OrderId=496,TaskCount=496,DriverId=496,Doctor1Id=496,'+
                  'Doctor2Id=496,Doctor1Types=448,Doctor2Types=448,IsError=500,Number=448,WorkingTime=496,'+
                  'TimeCarLogin=510,Hours=496,Station=448,PrevOrderId=496,Code=496,TimeReturnToBase=510,SKCode=448,'+
                  'Phone=448,TimeDinerStart=510,TimeDinerFinish=510,Visible=500,ActionCode=496,PNDDoctorId=496,'+
                  'OnDuty=500,FilterGroupId=496,TSId=448,Speed=480,TSType=500,RegistrationId=448,PartiallyFreeState=496,'+
                  'NextOrderId=580,ReleaseDateTime=510';
   strict private
      //поля, начинающиеся с "_", управляются автоматически (т.е. очистка и проч.). Остальные - на усмотрение разработчика.
     _FId: string; {0}
     _FLogin: string; {1}
     _FPwrd: string; {2}
     _FSession: string; {3}
     _FName: string; {4}
     _FIsDeleted: string; {5}
     _FLat: string; {6}
     _FLng: string; {7}
     _FLastUpdateDt: string; {8}
     _FType: string; {9}
     _FState: string; {10}
     _FOrderId: string; {11}
     _FTaskCount: string; {12}
     _FDriverId: string; {13}
     _FDoctor1Id: string; {14}
     _FDoctor2Id: string; {15}
     _FDoctor1Types: string; {16}
     _FDoctor2Types: string; {17}
     _FIsError: string; {18}
     _FNumber: string; {19}
     _FWorkingTime: string; {20}
     _FTimeCarLogin: string; {21}
     _FHours: string; {22}
     _FStation: string; {23}
     _FPrevOrderId: string; {24}
     _FCode: string; {25}
     _FTimeReturnToBase: string; {26}
     _FSKCode: string; {27}
     _FPhone: string; {28}
     _FTimeDinerStart: string; {29}
     _FTimeDinerFinish: string; {30}
     _FVisible: string; {31}
     _FActionCode: string; {32}
     _FPNDDoctorId: string; {33}
     _FOnDuty: string; {34}
     _FFilterGroupId: string; {35}
     _FTSId: string; {36}
     _FSpeed: string; {37}
     _FTSType: string; {38}
     _FRegistrationId: string; {39}
     _FPartiallyFreeState: string; {40}
     _FNextOrderId: string; {41}
     _FReleaseDateTime: string; {42}
   strict private
     FFields: TStrings;
     FFullInfo: string;
     FRecId: integer;
     FIsOnMap: boolean;
     FInfo: string;
     FSMPName: string;
     FTypeStr: string;
     FVisibleOnMap: boolean;
   strict private
     function GetTypeStr: string;
     function GetStateEx: integer;
     function GetStateIdForHTML(aCarId: integer; aState: smallint; aIsError: boolean = false; aOrderId: integer = -1): smallint;
     procedure SetVisibleOnMap(AValue: boolean);
     procedure SendMessage(AMsg: string);
   public
     property Id: string read _FId write _FId;
     property Login: string read _FLogin write _FLogin;
     property Pwrd: string read _FPwrd write _FPwrd;
     property Session: string read _FSession write _FSession;
     property Name: string read _FName write _FName;
     property IsDeleted: string read _FIsDeleted write _FIsDeleted;
     property Lat: string read _FLat write _FLat;
     property Lng: string read _FLng write _FLng;
     property LastUpdateDt: string read _FLastUpdateDt write _FLastUpdateDt;
     property PType: string read _FType write _FType;
     property State: string read _FState write _FState;
     property OrderId: string read _FOrderId write _FOrderId;
     property TaskCount: string read _FTaskCount write _FTaskCount;
     property DriverId: string read _FDriverId write _FDriverId;
     property Doctor1Id: string read _FDoctor1Id write _FDoctor1Id;
     property Doctor2Id: string read _FDoctor2Id write _FDoctor2Id;
     property Doctor1Types: string read _FDoctor1Types write _FDoctor1Types;
     property Doctor2Types: string read _FDoctor2Types write _FDoctor2Types;
     property IsError: string read _FIsError write _FIsError;
     property Number: string read _FNumber write _FNumber;
     property WorkingTime: string read _FWorkingTime write _FWorkingTime;
     property TimeCarLogin: string read _FTimeCarLogin write _FTimeCarLogin;
     property Hours: string read _FHours write _FHours;
     property Station: string read _FStation write _FStation;
     property PrevOrderId: string read _FPrevOrderId write _FPrevOrderId;
     property Code: string read _FCode write _FCode;
     property TimeReturnToBase: string read _FTimeReturnToBase write _FTimeReturnToBase;
     property SKCode: string read _FSKCode write _FSKCode;
     property Phone: string read _FPhone write _FPhone;
     property TimeDinerStart: string read _FTimeDinerStart write _FTimeDinerStart;
     property TimeDinerFinish: string read _FTimeDinerFinish write _FTimeDinerFinish;
     property Visible: string read _FVisible write _FVisible;
     property ActionCode: string read _FActionCode write _FActionCode;
     property PNDDoctorId: string read _FPNDDoctorId write _FPNDDoctorId;
     property OnDuty: string read _FOnDuty write _FOnDuty;
     property FilterGroupId: string read _FFilterGroupId write _FFilterGroupId;
     property TSId: string read _FTSId write _FTSId;
     property Speed: string read _FSpeed write _FSpeed;
     property TSType: string read _FTSType write _FTSType;
     property RegistrationId: string read _FRegistrationId write _FRegistrationId;
     property PartiallyFreeState: string read _FPartiallyFreeState write _FPartiallyFreeState;
     property NextOrderId: string read _FNextOrderId write _FNextOrderId;
     property ReleaseDateTime: string read _FReleaseDateTime write _FReleaseDateTime;
   public
    property FullInfo: string read FFullInfo write FFullInfo;
    property Fields: TStrings read FFields write FFields;
    property RecId: integer read FRecId write FRecId;
    property IsOnMap: boolean read FIsOnMap write FIsOnMap default false;
    property Info: string read FInfo write FInfo;
    property SMPName: string read FSMPName write FSMPName;
    property TypeStr: string read GetTypeStr;
    property StateEx: integer read GetStateEx;
    property VisibleOnMap: boolean read FVisibleOnMap write SetVisibleOnMap default false;
   public
    constructor Create;
    procedure ConvertFromJson(AValue: string); override;
  end;

   TCars = class(TModelAnyData)
    strict private
     FItems: TList<TCar>;
    strict private
     function GetFields(AValue:string):TStrings;
    public
     property Items:TList<TCar> read FItems write FItems;
    public
     procedure ConvertFromJson(AValue:string); override;
     constructor Create;
     function GetById(ACarId: string):TCar;
     procedure SetById(ACarId: string; ACar: TCar);
  end;

implementation

{ TCar }

procedure TCar.ConvertFromJson(AValue: string);
var
  ts:TStrings;
  i:integer;
  s: string;
begin

  ts := TStringList.Create;
   try
     ts.Delimiter := ',';
     ts.DelimitedText := AValue;

     for i:=0 to ts.Count-1 do
       begin
        try

         if self.Fields[i].ToUpper = 'TYPE' then
          self.PType := Base64DecodeAnsi(ts[i])
           else
          self.SetProperty(self.Fields[i], Base64DecodeAnsi(ts[i]));

         s := s + self.Fields[i]+'='+Base64DecodeAnsi(ts[i]) + ',';
        except on E:Exception do
         TLogger.Save('[TCar.ConvertFromJson] setField err: '+e.Message, TLogLevel.Error);
        end;
       end;

     self.FullInfo := ts.Text;

   finally
     ts.DisposeOf;
   end;
end;

constructor TCar.Create;
begin
 inherited;

 self.FFields := TStringList.Create;
 self.Clear;

 self.IsOnMap := false;
 self.FVisibleOnMap := false;
end;

function TCar.GetStateEx: integer;
var
 i: integer;
begin
   if self.IsError = '1' then
        result := 101
      else
     begin
      try
        i := StrToIntDef(self.PartiallyFreeState, 0);
        case i of
          0: result := GetStateIdForHTML(self.Id.ToInteger(), self.State.ToInteger, false, self.OrderId.ToInteger);
          1: result := 0;
          2,3: result := 2;
        end;
       except on E: Exception do
        result := 0;
      end;
     end;
end;

function TCar.GetStateIdForHTML(aCarId: integer; aState: smallint;
  aIsError: boolean; aOrderId: integer): smallint;
var
  orderId: integer;
  orderTeamAction: string;
begin
  try
    if aIsError then
      result := 101
    else if (aState = 12) then
    begin
      if aOrderId >= 0 then
        orderId := aOrderId
      else
      begin

      end;
      result := 2;
    end
    else if aState in [6,7,13] then
      Result := 5
    else if aState in [3,4,9] then
      result := 2
    else if aState = 1 then
      Result := 100
    else if aState in [11] then
      result := 0
    else
      result := aState;
  except
    result := aState;
  end;
end;

function TCar.GetTypeStr: string;
var
 LType: byte;
begin
  result := '';
  LType := StrToIntDef(self.PType, -1);

  if LType = -1 then exit;

  case LType of
    1: result := 'amb';
    2: result := 'reanim';
    3: result := 'pnd';
  end;

end;

procedure TCar.SendMessage(AMsg: string);
 var
 Message: TMessage;
begin
 Message := TMessage<String>.Create(AMsg);
 TMessageManager.DefaultManager.SendMessage(self, Message, True);
end;

procedure TCar.SetVisibleOnMap(AValue: boolean);
begin
  if self.FVisibleOnMap = AValue then exit;

  self.FVisibleOnMap := AValue;
  if self.FVisibleOnMap then
   self.SendMessage('set_car_visible='+self.Id)
    else
   self.SendMessage('set_car_invisible='+self.Id)
end;

{ TCars }

procedure TCars.ConvertFromJson(AValue: string);
var
 LCar:TCar;
 ts:TStrings;
 i: integer;
 LFields:TStrings;
begin

  try
    self.SetDataString(TCrypter.Decrypt(AValue));

     ts := TStringList.Create;
      try
        ts.Text := self.GetDataString;
        self.FItems.Clear;

         //собираем инфу по полям
         LFields := self.GetFields(ts[0]);
         for i := 1 to ts.Count - 1 do
           begin
             LCar := Tcar.Create;
             LCar.Fields := LFields;
             LCar.SetDataString(ts[i]);
             LCar.ConvertFromJson(ts[i]);
             self.FItems.Add(LCar);
           end;

      finally
        ts.DisposeOf;
      end;
    except on E:Exception do
     begin
      raise Exception.Create(e.Message);
     end;
  end;
end;

constructor TCars.Create;
begin
 inherited;
 self.FItems := TList<TCar>.Create;
end;

function TCars.GetById(ACarId: string): TCar;
var
 i:integer;
 LCar: TCar;
begin
  result := nil;
  for i := 0 to self.Items.Count - 1 do
   begin
     LCar := self.Items[i];
     if LCar.Id = ACarId then exit(LCar);
   end;
end;

function TCars.GetFields(AValue: string): TStrings;
var
 s:string;
 i, k:integer;
begin
 result := TStringList.Create;
 result.Delimiter := ',';
 result.DelimitedText := AValue;

  for i := 0 to result.Count - 1 do
    begin
      s := result[i];
      k := s.IndexOf('=');
      s := s.Substring(0, k);
      result[i] := s;
    end;
end;

procedure TCars.SetById(ACarId: string; ACar: TCar);
var
 i:integer;
 LCar: TCar;
begin

  for i := 0 to self.Items.Count - 1 do
   begin
    if self.Items[i].Id = ACarId then
     begin
      self.Items[i].Assign(ACar);
      exit;
     end;
   end;

  //если дошли сюда, значит машинку добавляем
  LCar := TCar.Create;
  LCar.Assign(ACar);

  self.Items.Add(LCar);

end;

end.
