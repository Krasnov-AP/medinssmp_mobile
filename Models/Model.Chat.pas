﻿//http://83.166.242.253:54002/?command=insertData&tableName=chat&SKCode=SKRed_Test Params=Id=580,User=448,Msg=448,IsDeleted=590,OwnerId=580,IsReaded=590,DtCreate=570,OwnerName=448,OwnerCode=448,OwnerType=580,OtherCode=448,FilterGroupId=580,OrderId=580
//,0JzQtdC00LjQvdGBINCh0JzQnyAo0JDQtNC80LjQvdC40YHRgtGA0LDRgtC+0YAp,dGVzdA==,,MTU0NQ==,RmFsc2U=,,U0tSZWRfVGVzdA==,U0tSZWRfVGVzdA==,Mw==,TUVESU5T,,MA==
unit Model.Chat;

interface

 uses
  System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
  System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
  Model.AnyData, Core.Common, Core.Crypt, Core.Funcs, System.Rtti, System.Messaging;

 type

  TChat = class(TModelAnyData)
   const
    FIELD_NAMES = 'Id=580,User=448,Msg=448,IsDeleted=500,OwnerId=496,IsReaded=500,DtCreate=510,OwnerName=448,'+
    'OwnerCode=448,OwnerType=500,OtherCode=448,FilterGroupId=496,UserId=580,OrderId=496,SKOrderId=496,OrderName=448';
   strict private
     _FId: string; {0}
     _FUser: string; {1}
     _FMsg: string; {2}
     _FIsDeleted: string; {3}
     _FOwnerId: string; {4}
     _FIsReaded: string; {5}
     _FDtCreate: string; {6}
     _FOwnerName: string; {7}
     _FOwnerCode: string; {8}
     _FOwnerType: string; {9}
     _FOtherCode: string; {10}
     _FFilterGroupId: string; {11}
     _FUserId: string; {12}
     _FOrderId: string; {13}
     _FSKOrderId: string; {14}
     _FOrderName: string; {15}

   strict private
     FFields: TStrings;
   strict private
     function GetFields(AValue:string):TStrings;
   public
     property Id: string read _FId write _FId;
     property User: string read _FUser write _FUser;
     property Msg: string read _FMsg write _FMsg;
     property IsDeleted: string read _FIsDeleted write _FIsDeleted;
     property OwnerId: string read _FOwnerId write _FOwnerId;
     property IsReaded: string read _FIsReaded write _FIsReaded;
     property DtCreate: string read _FDtCreate write _FDtCreate;
     property OwnerName: string read _FOwnerName write _FOwnerName;
     property OwnerCode: string read _FOwnerCode write _FOwnerCode;
     property OwnerType: string read _FOwnerType write _FOwnerType;
     property OtherCode: string read _FOtherCode write _FOtherCode;
     property FilterGroupId: string read _FFilterGroupId write _FFilterGroupId;
     property UserId: string read _FUserId write _FUserId;
     property OrderId: string read _FOrderId write _FOrderId;
     property SKOrderId: string read _FSKOrderId write _FSKOrderId;
     property OrderName: string read _FOrderName write _FOrderName;
   public
     property Fields: TStrings read FFields write FFields;
   public
     constructor Create;
     function ConvertToJson:string; override;
     procedure ConvertFromJson(AValue:string); override;
  end;

  TChats = class(TModelAnyData)
    strict private
     FItems: TList<TChat>;
    strict private
     function GetFields(AValue:string):TStrings;
    public
     property Items:TList<TChat> read FItems write FItems;
    public
     procedure ConvertFromJson(AValue:string); override;
     function GetById(AChatId: string): TChat;
     procedure SetById(AChatId: string; AChat: TChat);
     constructor Create;
  end;


implementation

{ TChat }

procedure TChat.ConvertFromJson(AValue: string);
var
  ts:TStrings;
  i:integer;
  s: string;
begin

  ts := TStringList.Create;
   try
     ts.Delimiter := ',';
     ts.DelimitedText := AValue;

     for i:=0 to ts.Count-1 do
       begin
        try
         self.SetProperty(self.Fields[i], Base64DecodeAnsi(ts[i]));
         s := s + self.Fields[i]+'='+Base64DecodeAnsi(ts[i]) + ',';
        except on E:Exception do
         TLogger.Save('[TChat.ConvertFromJson] setField err: '+e.Message, TLogLevel.Error);
        end;
       end;

      TLogger.Save('TChat.ConvertFromJson '+s);

   finally
     ts.DisposeOf;
   end;
end;

function TChat.ConvertToJson: string;
var
 ts_fields, ts_values, ts_result: TStrings;
 i:integer;
 LKeyValue: TKeyValue;
begin
 try

  result := '';

  ts_fields := TStringList.Create;
  ts_fields.Delimiter := ',';
  ts_fields.DelimitedText := self.FIELD_NAMES;

  ts_values := TStringList.Create;
  ts_values.Delimiter := ',';
  ts_values.DelimitedText := self.FIELD_NAMES;

  ts_result := TStringList.Create;

  try

    for I := 0 to ts_fields.Count - 1 do
      begin
        LKeyValue := ExtractKeyValue(ts_fields[i]);
        ts_fields[i] := LKeyValue.Key;
        ts_values[i] := Core.Funcs.Base64Encode(self.GetProperty(LKeyValue.Key));
      end;

   ts_result.Add(TCrypter.Encrypt(self.FIELD_NAMES));
   ts_result.Add(TCrypter.Encrypt(ts_values.DelimitedText));

   result := ts_result.Text;

  finally

    ts_fields.DisposeOf;
    ts_values.DisposeOf;
    ts_result.DisposeOf;

  end;
 except on E:Exception do
  TLogger.Save('[TChat.ConvertToJson err: ]'+e.Message, TLogLevel.Error);
 end;

  //todo
end;

constructor TChat.Create;
begin
 inherited;

 self.FFields := TStringList.Create;
 self.Fields := self.GetFields(self.FIELD_NAMES);
 self.Clear;

end;

function TChat.GetFields(AValue: string): TStrings;
var
 s:string;
 i, k:integer;
begin
 result := TStringList.Create;
 result.Delimiter := ',';
 result.DelimitedText := AValue;

  for i := 0 to result.Count - 1 do
    begin
      s := result[i];
      k := s.IndexOf('=');
      s := s.Substring(0, k);
      result[i] := s;
    end;
end;
{ TChats }

procedure TChats.ConvertFromJson(AValue: string);
var
 LChat: TChat;
 ts:TStrings;
 i: integer;
 LFields:TStrings;
begin

  try
    self.SetDataString(TCrypter.Decrypt(AValue));

     ts := TStringList.Create;
      try
        ts.Text := self.GetDataString;

        self.FItems.Clear;

         //собираем инфу по полям
         LFields := self.GetFields(ts[0]);
         //TLogger.Save('[TChats.ConvertFromJson] fields.Text='+LFields.Text);
         for i := 1 to ts.Count - 1 do
           begin
             LChat := TChat.Create;
             LChat.Fields := LFields;
             LChat.SetDataString(ts[i]);
             LChat.ConvertFromJson(ts[i]);
             self.FItems.Add(LChat);
           end;

      finally
        ts.DisposeOf;
      end;
    except on E:Exception do
     begin
      raise Exception.Create(e.Message);
     end;
  end;
end;

constructor TChats.Create;
begin
 inherited;
 self.FItems := TList<TChat>.Create;
end;

function TChats.GetById(AChatId: string): TChat;
var
 i:integer;
begin
  result := nil;
  for i := 0 to self.Items.Count - 1 do
   begin
    //TLogger.Save('[TOrders.GetById] i='+i.ToString+' AOrderId='+AOrderId+' self.Items[i].Id='+self.Items[i].Id);
    if self.Items[i].Id = AChatId then exit(self.Items[i]);
   end;
end;

function TChats.GetFields(AValue: string): TStrings;
var
 s:string;
 i, k:integer;
begin
 result := TStringList.Create;
 result.Delimiter := ',';
 result.DelimitedText := AValue;

  for i := 0 to result.Count - 1 do
    begin
      s := result[i];
      k := s.IndexOf('=');
      s := s.Substring(0, k);
      result[i] := s;
    end;
end;

procedure TChats.SetById(AChatId: string; AChat: TChat);
var
 i:integer;
 LChat: TChat;
begin

  for i := 0 to self.Items.Count - 1 do
   begin
    if self.Items[i].Id = AChatId then
     begin
      self.Items[i].Lock;
       try
        self.Items[i].Assign(AChat);
       finally
        self.Items[i].Unlock;
       end;
      exit;
     end;
   end;

  //если дошли сюда, значит чат добавляем
  LChat := TChat.Create;
  LChat.Assign(AChat);

  self.Lock;
   try
    self.Items.Add(LChat);
   finally
     self.Unlock;
   end;

end;

end.
