﻿/// <summary>
///   Модуль с глобальными данными, доступные любому модулю приложения
/// </summary>
/// <remarks>
///   <list type="table">
///     <listheader>
///       <term>Параметр</term>
///       <description>Значение</description>
///     </listheader>
///     <item>
///       <term>Версия приложения</term>
///       <description>0.1</description>
///     </item>
///     <item>
///       <term>Дата последнего изменения</term>
///       <description>10.Oct.2021</description>
///     </item>
///     <item>
///       <term>Поддерживаемые платформы разработки</term>
///       <description>Embarcadero Delphi 10.4 Sydney Update 2 и выше</description>
///     </item>
///     <item>
///       <term>Поддерживаемые ОС</term>
///       <description>
///         <p>Windows 7 (SP1+)/8.1/10 (x32/x64)</p>
///         <p>Windows Server 2012 R2/2016/2019 (x32/x64)</p>
///         <p>macOS 10.13/10.14/10.15/11.1 (x64)</p>
///       </description>
///     </item>
///   </list>
/// </remarks>
unit Model.Global;

interface

uses
  Model.AnyData, Model.User, Model.AppSettings,
  Core.Common, System.SyncObjs, Model.Orders, System.Generics.Collections,
  System.Classes, Model.Cars, Model.SkList, Model.Hospital;

const

  HOST_HTTP = 'http://medins.softerium.ru';
  PORT_HTTP = '54001';
  HOST_WEBSOCKET = ''; //bntkjgf
  PORT_WEBSOCKET = '8081';

   {$if defined(MSWINDOWS) or defined(MACOS) }
    CLIENT_TYPE = 'desktop';
    TOKEN_CLIENT = '9C9486BE-ECC8-448A-B8B8-56991281CB4A';
   {$else}
    CLIENT_TYPE = 'mobile';
    TOKEN_CLIENT = '';
       /// <summary>
       /// У нас может быть 2 модели отправки координат:
       ///  1) По изменению координат от gps-датчика.
       ///     Поскольку координаты могут "прыгать" в зависмотсти от кол-ва видимых спутников, то такой способ может нагрузить запросами сервер.
       ///  2) По таймеру. Раз в GEODATA_REFRESH_INTERVAL будем отправлять данные.
       ///  Дополнительно, чтоб отсечь близкие gps-данные будем отправлять координаты, если расстояние между прошлыми координатами
       ///  превысило MIN_DISTANCE_TO_REPORT
       /// </summary>
    GEODATA_REFRESH_INTERVAL = 5000; //ms
    MIN_DISTANCE_TO_REPORT = 100; //метры
   {$endif}

   YANDEX_API_KEY = '521a59c0-5d51-4075-af7a-1843a353dc3d';

   MAX_ARCHIVE_DAYS = 14;

type

  TModelNetSettings = class
  strict private
    FHost, FPort: string;
  end;

 TModelDataGlobal = class
  strict private
    [volatile] FCurrentUser: TUser;
    FAppSettings: TAppSettings;
    FCritical: TCriticalSection;
    [volatile] FCurrentOrder: TOrder;
    FCurrentOrderArchive: TOrder;
    FCars: TCars;
    FOrders: TOrders;
    FOrdersArchive: TOrders;
    FSKList: TSKList;
    FHospitals: THospitals;
    [volatile] FCurrentCar: TCar;
    [volatile] FCommands: TDictionary<string, string>;
    FCurrentSKUnit: TSKUnit;
    FCurrentHospital: THospital;
    [weak] FBlinkingOrderIds: TStrings;
    FSurveyValue: integer;
    procedure SetCurrentUser(AValue: TUser);
    function GetCurrentUser: TUser;
  public
    property CurrentUser: TUser read GetCurrentUser write SetCurrentUser;
    property CurrentCar: TCar read FCurrentCar write FCurrentCar;
    property AppSettings:TAppSettings read FAppSettings write FAppSettings;
    property CurrentOrder: TOrder read FCurrentOrder write FCurrentOrder;
    property CurrentOrderArchive: TOrder read FCurrentOrderArchive write FCurrentOrderArchive;
    property CurrentSKUnit: TSKUnit read FCurrentSKUnit write FCurrentSKUnit;
    property Cars: TCars read FCars write FCars;
    property Orders: TOrders read FOrders write FOrders;
    property OrdersArchive: TOrders read FOrdersArchive write FOrdersArchive;
    property SKList: TSKList read FSKList write FSKList;
    property Hospitals: THospitals read FHospitals write FHospitals;
    property CurrentHospital: THospital read FCurrentHospital write FCurrentHospital;
    property BlinkingOrderIds: TStrings read FBlinkingOrderIds write FBlinkingOrderIds;
    property SurveyValue: integer read FSurveyValue write FSurveyValue;
    constructor Create;
    destructor Destroy; override;
  public
    procedure AddCommand(AKey, AValue: string);
    function GetCommand(AKey: string) :string;

    procedure Lock;
    procedure Unlock;
  end;

var
  ModelDataGlobal: TModelDataGlobal;
  ChatIsFirstRun: boolean = true;

implementation

{ TAuthDataGlobal }

procedure TModelDataGlobal.AddCommand(AKey, AValue: string);
var
 ts: TStrings;
 s:string;
begin

 TLogger.Save('[TModelDataGlobal.AddCommand] '+Akey+'='+AValue);

 if Assigned(self.FCritical) then
  self.FCritical.Acquire;

 ts := TStringList.Create;

  try

   if not self.FCommands.TryGetValue(AKey, s) then
    begin
     self.FCommands.Add(AKey, AValue);
    end
     else
    begin
     ts.Delimiter := '|';
     ts.DelimitedText := s;
     ts.Add(AValue);
     self.FCommands.Items[AKey] := ts.DelimitedText;
    end;

  finally
   begin
    ts.Free;
    if Assigned(Self.FCritical) then
     self.FCritical.Release;
   end;
  end;

end;

constructor TModelDataGlobal.Create;
begin
  self.FCritical := TCriticalSection.Create;
  self.AppSettings := TAppSettings.Create;
  self.CurrentUser := TUser.Create;
  self.FCommands := TDictionary<string, string>.Create;
  self.FCars := TCars.Create;
  self.FOrders := TOrders.Create;
  self.FOrdersArchive := TOrders.Create;
  self.FSKList := TSKList.Create;
  self.FBlinkingOrderIds := TStringList.Create;
end;

destructor TModelDataGlobal.Destroy;
begin
  self.FBlinkingOrderIds.DisposeOf;
  SafeFree(self.FCritical);
  inherited;
end;

//возвращаем команду и очищаем ее
function TModelDataGlobal.GetCommand(AKey: string): string;
var
 s: string;
begin
 self.Lock;
 result := '';
 try
  if self.FCommands.TryGetValue(AKey, s) then
   begin
    result := s;
    self.FCommands.Items[AKey] := '';
   end;
 finally
  self.Unlock;
 end;
end;

function TModelDataGlobal.GetCurrentUser: TUser;
begin
  self.Lock;
  try
    result := self.FCurrentUser;
  finally
    self.Unlock;
  end;
end;

procedure TModelDataGlobal.Lock;
begin
 {$ifdef MSWINDOWS}
  if Assigned(self.FCritical) then
   self.FCritical.Acquire;
 {$endif}
end;

procedure TModelDataGlobal.SetCurrentUser(AValue: TUser);
begin
  self.Lock;
  try
    self.FCurrentUser := AValue;
  finally
    self.Unlock;
  end;
end;

procedure TModelDataGlobal.Unlock;
begin
 {$ifdef MSWINDOWS}
  if Assigned(self.FCritical) then
   self.FCritical.Release;
 {$endif}
end;

end.

