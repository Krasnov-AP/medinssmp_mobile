﻿unit Model.Hospital;

interface

uses
  System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
  System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
  Model.AnyData, Core.Common, Core.Crypt, Core.Funcs, System.Rtti, System.Messaging;

type

THospital = class(TModelAnyData)
 const
   FIELD_NAMES = 'Id=580,IsDeleted=500,Name=448,Address=448,Coords=448,HospitalId=448,CityId=496,RegionId=496,IsActive=500';
 strict private
   _FId: string; {0}
   _FIsDeleted: string; {1}
   _FName: string; {2}
   _FAddress: string; {3}
   _FCoords: string; {4}
   _FHospitalId: string; {5}
   _FCityId: string; {6}
   _FRegionId: string; {7}
   _FIsActive: string; {8}
 strict private
    FFields: TStrings;
    FFullInfo: string;
 public
   property Id: string read _FId write _FId;
   property IsDeleted: string read _FIsDeleted write _FIsDeleted;
   property Name: string read _FName write _FName;
   property Address: string read _FAddress write _FAddress;
   property Coords: string read _FCoords write _FCoords;
   property HospitalId: string read _FHospitalId write _FHospitalId;
   property CityId: string read _FCityId write _FCityId;
   property RegionId: string read _FRegionId write _FRegionId;
   property IsActive: string read _FIsActive write _FIsActive;
 public
   property FullInfo: string read FFullInfo write FFullInfo;
   property Fields: TStrings read FFields write FFields;
 public
   procedure ConvertFromJson(AValue: string); override;
   constructor Create;
end;

THospitals = class(TModelAnyData)
   strict private
     FItems: TList<THospital>;
    strict private
     function GetFields(AValue:string):TStrings;
    public
     property Items:TList<THospital> read FItems write FItems;
    public
     procedure ConvertFromJson(AValue:string); override;
     constructor Create;
end;

implementation

{ TSKUnit }

procedure THospital.ConvertFromJson(AValue: string);
var
  ts:TStrings;
  i:integer;
  s: string;
begin

  ts := TStringList.Create;
   try
     ts.Delimiter := ',';
     ts.DelimitedText := AValue;

     for i:=0 to ts.Count-1 do
       begin
        try

         self.SetProperty(self.Fields[i], Base64DecodeAnsi(ts[i]));

         s := s + self.Fields[i]+'='+Base64DecodeAnsi(ts[i]) + ',';
        except on E:Exception do
         TLogger.Save('[THospital.ConvertFromJson] setField err: '+e.Message, TLogLevel.Error);
        end;
       end;

     self.FullInfo := ts.Text;

   finally
     ts.DisposeOf;
   end;

end;

constructor THospital.Create;
begin
  inherited;

  self.FFields := TStringList.Create;
  self.Clear;
end;

{ THospitals }

procedure THospitals.ConvertFromJson(AValue: string);
var
 LHospital:THospital;
 ts:TStrings;
 i: integer;
 LFields:TStrings;
begin

  try
    self.SetDataString(TCrypter.Decrypt(AValue));

     ts := TStringList.Create;
      try
        ts.Text := self.GetDataString;
        self.FItems.Clear;

         //собираем инфу по полям
         LFields := self.GetFields(ts[0]);
         for i := 1 to ts.Count - 1 do
           begin
             LHospital := THospital.Create;
             LHospital.Fields := LFields;
             LHospital.SetDataString(ts[i]);
             LHospital.ConvertFromJson(ts[i]);
             self.FItems.Add(LHospital);
           end;

      finally
        ts.DisposeOf;
      end;
    except on E:Exception do
     begin
      raise Exception.Create(e.Message);
     end;
  end;
end;

constructor THospitals.Create;
begin
  inherited;
  self.FItems := TList<THospital>.Create;
end;

function THospitals.GetFields(AValue: string): TStrings;
 var
 s:string;
 i, k:integer;
begin
 result := TStringList.Create;
 result.Delimiter := ',';
 result.DelimitedText := AValue;

  for i := 0 to result.Count - 1 do
    begin
      s := result[i];
      k := s.IndexOf('=');
      s := s.Substring(0, k);
      result[i] := s;
    end;
end;

end.
