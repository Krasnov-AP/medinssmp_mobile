﻿unit Model.LangConsts;

interface

 const

  LANG_LOGIN = 'login';
  LANG_PASSWORD = 'password';
  LANG_COMPANY = 'company';
  LANG_SIGN_IN = 'sign_in';
  LANG_CHOOSE_COMPANY = 'choose_company';
  LANG_SOMETHING_WENT_WRONG = 'something_went_wrong';
  LANG_AUTH_IS_GOING_ON = 'auth_is_going_on';
  LANG_UNKNOWN_COMMAND = 'Неизвестная команда!';
  LANG_UNKNOWN_ERROR = 'unknown_error';
  LANG_ERROR = 'error';
  LANG_SETTINGS = 'settings';
  LANG_APP_SETTINGS = 'app_settings';
  LANG_LOGOFF = 'logoff';
  LANG_APP_LOGOFF = 'app_logoff';
  LANG_LOADING = 'loading';
  LANG_UNKNOWN_DB_PATH = 'unknown_db_path';

  LANG_MENU = 'menu';
  LANG_CALLS = 'calls';
  LANG_CALLS_ARCHIVE = 'calls_archive';
  LANG_ARCHIVE = 'archive';
  LANG_MAP = 'map';
  LANG_CHAT = 'chat';
  LANG_TEAMS = 'teams';

  LANG_DO_LOGOFF_APP = 'do_logoff_app';
  LANG_LOG_IN = 'log_in';


implementation

end.
