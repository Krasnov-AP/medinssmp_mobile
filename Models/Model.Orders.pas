﻿unit Model.Orders;

interface

 uses
  System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
  System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
  Model.AnyData, Core.Common, Core.Crypt, Core.Funcs, System.Rtti;

 const
  csCancelled = 2;
  csInProcess = 3;
  csDone = 4;
  csOnAgreement = 1;
  csNew = 0;
  csDetectMore = 5; //нужно доп. анализировать
  csSurvey = 10;
  csSurvey2 = 11;

 type

  TOrder = class(TModelAnyData)
   const
    FIELD_NAMES = 'Id=580,SKOrderId=580,Name=448,BirthDay=570,Address=448,Contacts=448,Description=448,'+
    'SKName=448,DocNum=448,Coords=448,IsDeleted=590,UserLogin=448,CreatorLogin=448,State=580,SKCode=448,'+
    'SKUserName=448,SMPName=448,SMPUserName=448,Price=448,Team=448,Diagnose=448,TeamAction=448,Chat=448,'+
    'PriceCode=448,CarId=580,TimeCall=570,TimeDep=570,TimeArr=570,TimeFinish=570,SMPUserFIO=448,SKUserFIO=448,'+
    'SMPId=580,IsVIP=590,MKADDistance=448,UserNote=448,OrderNote=448,MainUserId=580,WorkingTime=580,TimeCarLogin=570,'+
    'MonthlyId=580,TimeCarGetOrder=570,DriverId=580,Doctor1Id=580,Doctor2Id=580,IsManual=590,CarStation=448,'+
    'CarNumber=448,TimeCarMoveToBase=570,ClosedSKUser=448,Address2=448,HospitalName=448,TypeId=580,HelpTypes=448,'+
    'CategoryId=580,NormalTime=580,ShiftId=580,DriverName=448,IsPlan=590,PlanDate=570,PlanNotifyDt=570,PaymentType=580,'+
    'ClosedSMPUserId=580,HospitalCoords=448,HospitalAddress=448,CallCosts=480,TimeArrivingToHospital=570,Distance=580,'+
    'Years=580,CarName=448,CarPhone=448,CarCode=580,Version=580,WasCalculated=580,RangeForSort=580,DateForSort=570,'+
    'TypeStr=448,IsReanimation=590,TimeReturn=570,AccountIsBilled=590,PlanTime=560,ActionStateSMP=580,IsSKAgent=590,'+
    'IsClosed=590,IsWait=590,WaitFrom=560,WaitTo=560,WaitNote=448,Stairs=580,IsAutoCalcCallCosts=590,SKCarId=580,'+
    'AccountableUserId=580,IsHospitalCoordination=590,IsHide=590,DailyId=580,NumberCalc=448,RegionId=580,BSO=448,'+
    'FilterGroupId=580,SecFilterGroupId=580,OutfilOMS=448,ArchiveDateCall=570,TimeDepToHospital=570,TimeWork=448';

  {case aTypeNum of
    500, 580, 496: result := stInteger;
    448: result := stString;
    590: result := stBoolean;
    510: result := stDateTime;
    560: result := stTime;
    570: result := stDate;
    480: result := stFloat;
    else
      result := stString;
  end}

   strict private
     _FId: string; {0}
     _FSKOrderId: string; {1}
     _FName: string; {2}
     _FBirthDay: string; {3}
     _FAddress: string; {4}
     _FContacts: string; {5}
     _FDescription: string; {6}
     _FSKName: string; {7}
     _FDocNum: string; {8}
     _FCoords: string; {9}
     _FIsDeleted: string; {10}
     _FUserLogin: string; {11}
     _FCreatorLogin: string; {12}
     _FState: string; {13}
     _FSKCode: string; {14}
     _FSKUserName: string; {15}
     _FSMPName: string; {16}
     _FSMPUserName: string; {17}
     _FPrice: string; {18}
     _FTeam: string; {19}
     _FDiagnose: string; {20}
     _FTeamAction: string; {21}
     _FChat: string; {22}
     _FPriceCode: string; {23}
     _FCarId: string; {24}
     _FTimeCall: string; {25}
     _FTimeDep: string; {26}
     _FTimeArr: string; {27}
     _FTimeFinish: string; {28}
     _FSMPUserFIO: string; {29}
     _FSKUserFIO: string; {30}
     _FSMPId: string; {31}
     _FIsVIP: string; {32}
     _FMKADDistance: string; {33}
     _FUserNote: string; {34}
     _FOrderNote: string; {35}
     _FMainUserId: string; {36}
     _FWorkingTime: string; {37}
     _FTimeCarLogin: string; {38}
     _FMonthlyId: string; {39}
     _FTimeCarGetOrder: string; {40}
     _FDriverId: string; {41}
     _FDoctor1Id: string; {42}
     _FDoctor2Id: string; {43}
     _FIsManual: string; {44}
     _FCarStation: string; {45}
     _FCarNumber: string; {46}
     _FTimeCarMoveToBase: string; {47}
     _FClosedSKUser: string; {48}
     _FAddress2: string; {49}
     _FHospitalName: string; {50}
     _FTypeId: string; {51}
     _FHelpTypes: string; {52}
     _FCategoryId: string; {53}
     _FNormalTime: string; {54}
     _FShiftId: string; {55}
     _FDriverName: string; {56}
     _FIsPlan: string; {57}
     _FPlanDate: string; {58}
     _FPlanNotifyDt: string; {59}
     _FPaymentType: string; {60}
     _FClosedSMPUserId: string; {61}
     _FHospitalCoords: string; {62}
     _FHospitalAddress: string; {63}
     _FCallCosts: string; {64}
     _FTimeArrivingToHospital: string; {65}
     _FDistance: string; {66}
     _FYears: string; {67}
     _FCarName: string; {68}
     _FCarPhone: string; {69}
     _FCarCode: string; {70}
     _FVersion: string; {71}
     _FWasCalculated: string; {72}
     _FRangeForSort: string; {73}
     _FDateForSort: string; {74}
     _FTypeStr: string; {75}
     _FIsReanimation: string; {76}
     _FTimeReturn: string; {77}
     _FAccountIsBilled: string; {78}
     _FPlanTime: string; {79}
     _FActionStateSMP: string; {80}
     _FIsSKAgent: string; {81}
     _FIsClosed: string; {82}
     _FIsWait: string; {83}
     _FWaitFrom: string; {84}
     _FWaitTo: string; {85}
     _FWaitNote: string; {86}
     _FStairs: string; {87}
     _FIsAutoCalcCallCosts: string; {88}
     _FSKCarId: string; {89}
     _FAccountableUserId: string; {90}
     _FIsHospitalCoordination: string; {91}
     _FIsHide: string; {92}
     _FDailyId: string; {93}
     _FNumberCalc: string; {94}
     _FRegionId: string; {95}
     _FBSO: string; {96}
     _FFilterGroupId: string; {97}
     _FSecFilterGroupId: string; {98}
     _FOutfilOMS: string; {99}
     _FArchiveDateCall: string; {100}
     _FTimeDepToHospital: string; {101}
     _FTimeWork: string; {102}

   strict private
     FFields: TStrings;
     FFullInfo: string;
     FRecId: integer;
   strict private
    function GetFullInfo:string;
   public
     property Id: string read _FId write _FId;
     property SKOrderId: string read _FSKOrderId write _FSKOrderId;
     property Name: string read _FName write _FName;
     property BirthDay: string read _FBirthDay write _FBirthDay;
     property Address: string read _FAddress write _FAddress;
     property Contacts: string read _FContacts write _FContacts;
     property Description: string read _FDescription write _FDescription;
     property SKName: string read _FSKName write _FSKName;
     property DocNum: string read _FDocNum write _FDocNum;
     property Coords: string read _FCoords write _FCoords;
     property IsDeleted: string read _FIsDeleted write _FIsDeleted;
     property UserLogin: string read _FUserLogin write _FUserLogin;
     property CreatorLogin: string read _FCreatorLogin write _FCreatorLogin;
     property State: string read _FState write _FState;
     property SKCode: string read _FSKCode write _FSKCode;
     property SKUserName: string read _FSKUserName write _FSKUserName;
     property SMPName: string read _FSMPName write _FSMPName;
     property SMPUserName: string read _FSMPUserName write _FSMPUserName;
     property Price: string read _FPrice write _FPrice;
     property Team: string read _FTeam write _FTeam;
     property Diagnose: string read _FDiagnose write _FDiagnose;
     property TeamAction: string read _FTeamAction write _FTeamAction;
     property Chat: string read _FChat write _FChat;
     property PriceCode: string read _FPriceCode write _FPriceCode;
     property CarId: string read _FCarId write _FCarId;
     property TimeCall: string read _FTimeCall write _FTimeCall;
     property TimeDep: string read _FTimeDep write _FTimeDep;
     property TimeArr: string read _FTimeArr write _FTimeArr;
     property TimeFinish: string read _FTimeFinish write _FTimeFinish;
     property SMPUserFIO: string read _FSMPUserFIO write _FSMPUserFIO;
     property SKUserFIO: string read _FSKUserFIO write _FSKUserFIO;
     property SMPId: string read _FSMPId write _FSMPId;
     property IsVIP: string read _FIsVIP write _FIsVIP;
     property MKADDistance: string read _FMKADDistance write _FMKADDistance;
     property UserNote: string read _FUserNote write _FUserNote;
     property OrderNote: string read _FOrderNote write _FOrderNote;
     property MainUserId: string read _FMainUserId write _FMainUserId;
     property WorkingTime: string read _FWorkingTime write _FWorkingTime;
     property TimeCarLogin: string read _FTimeCarLogin write _FTimeCarLogin;
     property MonthlyId: string read _FMonthlyId write _FMonthlyId;
     property TimeCarGetOrder: string read _FTimeCarGetOrder write _FTimeCarGetOrder;
     property DriverId: string read _FDriverId write _FDriverId;
     property Doctor1Id: string read _FDoctor1Id write _FDoctor1Id;
     property Doctor2Id: string read _FDoctor2Id write _FDoctor2Id;
     property IsManual: string read _FIsManual write _FIsManual;
     property CarStation: string read _FCarStation write _FCarStation;
     property CarNumber: string read _FCarNumber write _FCarNumber;
     property TimeCarMoveToBase: string read _FTimeCarMoveToBase write _FTimeCarMoveToBase;
     property ClosedSKUser: string read _FClosedSKUser write _FClosedSKUser;
     property Address2: string read _FAddress2 write _FAddress2;
     property HospitalName: string read _FHospitalName write _FHospitalName;
     property TypeId: string read _FTypeId write _FTypeId;
     property HelpTypes: string read _FHelpTypes write _FHelpTypes;
     property CategoryId: string read _FCategoryId write _FCategoryId;
     property NormalTime: string read _FNormalTime write _FNormalTime;
     property ShiftId: string read _FShiftId write _FShiftId;
     property DriverName: string read _FDriverName write _FDriverName;
     property IsPlan: string read _FIsPlan write _FIsPlan;
     property PlanDate: string read _FPlanDate write _FPlanDate;
     property PlanNotifyDt: string read _FPlanNotifyDt write _FPlanNotifyDt;
     property PaymentType: string read _FPaymentType write _FPaymentType;
     property ClosedSMPUserId: string read _FClosedSMPUserId write _FClosedSMPUserId;
     property HospitalCoords: string read _FHospitalCoords write _FHospitalCoords;
     property HospitalAddress: string read _FHospitalAddress write _FHospitalAddress;
     property CallCosts: string read _FCallCosts write _FCallCosts;
     property TimeArrivingToHospital: string read _FTimeArrivingToHospital write _FTimeArrivingToHospital;
     property Distance: string read _FDistance write _FDistance;
     property Years: string read _FYears write _FYears;
     property CarName: string read _FCarName write _FCarName;
     property CarPhone: string read _FCarPhone write _FCarPhone;
     property CarCode: string read _FCarCode write _FCarCode;
     property Version: string read _FVersion write _FVersion;
     property WasCalculated: string read _FWasCalculated write _FWasCalculated;
     property RangeForSort: string read _FRangeForSort write _FRangeForSort;
     property DateForSort: string read _FDateForSort write _FDateForSort;
     property TypeStr: string read _FTypeStr write _FTypeStr;
     property IsReanimation: string read _FIsReanimation write _FIsReanimation;
     property TimeReturn: string read _FTimeReturn write _FTimeReturn;
     property AccountIsBilled: string read _FAccountIsBilled write _FAccountIsBilled;
     property PlanTime: string read _FPlanTime write _FPlanTime;
     property ActionStateSMP: string read _FActionStateSMP write _FActionStateSMP;
     property IsSKAgent: string read _FIsSKAgent write _FIsSKAgent;
     property IsClosed: string read _FIsClosed write _FIsClosed;
     property IsWait: string read _FIsWait write _FIsWait;
     property WaitFrom: string read _FWaitFrom write _FWaitFrom;
     property WaitTo: string read _FWaitTo write _FWaitTo;
     property WaitNote: string read _FWaitNote write _FWaitNote;
     property Stairs: string read _FStairs write _FStairs;
     property IsAutoCalcCallCosts: string read _FIsAutoCalcCallCosts write _FIsAutoCalcCallCosts;
     property SKCarId: string read _FSKCarId write _FSKCarId;
     property AccountableUserId: string read _FAccountableUserId write _FAccountableUserId;
     property IsHospitalCoordination: string read _FIsHospitalCoordination write _FIsHospitalCoordination;
     property IsHide: string read _FIsHide write _FIsHide;
     property DailyId: string read _FDailyId write _FDailyId;
     property NumberCalc: string read _FNumberCalc write _FNumberCalc;
     property RegionId: string read _FRegionId write _FRegionId;
     property BSO: string read _FBSO write _FBSO;
     property FilterGroupId: string read _FFilterGroupId write _FFilterGroupId;
     property SecFilterGroupId: string read _FSecFilterGroupId write _FSecFilterGroupId;
     property OutfilOMS: string read _FOutfilOMS write _FOutfilOMS;
     property ArchiveDateCall: string read _FArchiveDateCall write _FArchiveDateCall;
     property TimeDepToHospital: string read _FTimeDepToHospital write _FTimeDepToHospital;
     property TimeWork: string read _FTimeWork write _FTimeWork;
   public
    property FullInfo: string read FFullInfo write FFullInfo;
    property Fields: TStrings read FFields write FFields;
    property RecId: integer read FRecId write FRecId;
   public
    procedure ConvertFromJson(AValue:string); override;
    function ConvertToJson:string; override;
    constructor Create;
    procedure Clear; override;
  end;

  TOrders = class(TModelAnyData)
    strict private
     FItems: TList<TOrder>;
    strict private
     function GetFields(AValue:string):TStrings;
    public
     property Items:TList<TOrder> read FItems write FItems;
    public
     procedure ConvertFromJson(AValue:string); override;
     function GetById(AOrderId: string): TOrder;
     procedure SetById(AOrderId: string; AOrder: TOrder);
     constructor Create;
  end;

implementation

{ TCall }

procedure TOrder.Clear;
begin
  inherited;
  self.State := '0';
  self.MKADDistance := '0';
  self.PlanDate := DateToStr(now);
  self.WasCalculated := '1';
  self.RangeForSort := '3';
  self.DateForSort := '30.12.1899';
  self.NumberCalc := '0';
  self.FilterGroupId := '0';
  self.ArchiveDateCall := '30.12.1899';
  self.TimeWork := '00:00';
  self.TypeStr := 'В';
  self.TypeId := '1';
  self.CarName := '-';
  self.Coords:='0,0';
end;

procedure TOrder.ConvertFromJson(AValue: string);
var
  ts:TStrings;
  i:integer;
  s: string;
begin

  ts := TStringList.Create;
   try
     ts.Delimiter := ',';
     ts.DelimitedText := AValue;

     for i:=0 to ts.Count-1 do
       begin
        try
         self.SetProperty(self.Fields[i], Base64DecodeAnsi(ts[i]));
         s := s + self.Fields[i]+'='+Base64DecodeAnsi(ts[i]) + '|';
        except on E:Exception do
         TLogger.Save('[TOrder.ConvertFromJson] setField err: '+e.Message, TLogLevel.Error);
        end;
       end;

      TLogger.Save('TOrder.ConvertFromJson '+s);
      self.FullInfo := s;

   finally
     ts.DisposeOf;
   end;
end;

function TOrder.ConvertToJson: string;
var
 ts_fields, ts_values, ts_result: TStrings;
 i:integer;
 LKeyValue: TKeyValue;
begin
 try

  result := '';

  ts_fields := TStringList.Create;
  ts_fields.Delimiter := ',';
  ts_fields.DelimitedText := self.FIELD_NAMES;

  ts_values := TStringList.Create;
  ts_values.Delimiter := ',';
  ts_values.DelimitedText := self.FIELD_NAMES;

  ts_result := TStringList.Create;

  try

    for I := 0 to ts_fields.Count - 1 do
      begin
        LKeyValue := ExtractKeyValue(ts_fields[i]);
        ts_fields[i] := LKeyValue.Key;
        ts_values[i] := Core.Funcs.Base64Encode(self.GetProperty(LKeyValue.Key));
        //TLogger.Save('i='+i.ToString+' '+LKeyValue.Key+'='+self.GetProperty(LKeyValue.Key));
      end;

   TLogger.Save('fieldNames='+self.FIELD_NAMES);
   TLogger.Save('Values='+ts_values.DelimitedText);

   ts_result.Add(TCrypter.Encrypt(self.FIELD_NAMES));
   ts_result.Add(TCrypter.Encrypt(ts_values.DelimitedText));

   result := ts_result.Text;

  finally

    ts_fields.DisposeOf;
    ts_values.DisposeOf;
    ts_result.DisposeOf;

  end;
 except on E:Exception do
  TLogger.Save('[TOrder.ConvertToJson err: ]'+e.Message, TLogLevel.Error);
 end;

  //todo
end;

constructor TOrder.Create;
begin
 inherited;

 self.FFields := TStringList.Create;
 self.Clear;

end;

function TOrder.GetFullInfo: string;
var
 i:integer;
 ts:TStrings;
begin

 ts := TStringList.Create;
 ts.Delimiter := ',';
 ts.DelimitedText := self.GetDataString;
 result := '';

  try
   for i := 0 to ts.Count - 1 do
    begin
     result := result + self.FFields[i]+'('+i.ToString+')'+'='+Base64DecodeAnsi(ts[i])+' | ';
    end;
  finally
   ts.DisposeOf;
  end;

end;

{ TOrders }

procedure TOrders.ConvertFromJson(AValue: string);
var
 LCall:TOrder;
 ts:TStrings;
 i: integer;
 LFields:TStrings;
begin

  try
    self.SetDataString(TCrypter.Decrypt(AValue));

     ts := TStringList.Create;
      try
        ts.Text := self.GetDataString;
        self.FItems.Clear;

         //собираем инфу по полям
         LFields := self.GetFields(ts[0]);
         for i := 1 to ts.Count - 1 do
           begin
             LCall := TOrder.Create;
             LCall.Fields := LFields;
             LCall.SetDataString(ts[i]);
             LCall.ConvertFromJson(ts[i]);
             self.FItems.Add(LCall);
           end;

      finally
        ts.DisposeOf;
      end;
    except on E:Exception do
     begin
      raise Exception.Create(e.Message);
     end;
  end;
end;

constructor TOrders.Create;
begin
  inherited;
  self.FItems := TList<TOrder>.Create;
end;

function TOrders.GetById(AOrderId: string): TOrder;
var
 i:integer;
begin
  result := nil;
  TLogger.Save('[TOrders.GetById] enter cnt='+self.Items.Count.ToString);
  for i := 0 to self.Items.Count - 1 do
   begin
    TLogger.Save('[TOrders.GetById] i='+i.ToString+' AOrderId='+AOrderId+' self.Items[i].Id='+self.Items[i].Id);
    if self.Items[i].Id = AOrderId then exit(self.Items[i]);
   end;
end;


function TOrders.GetFields(AValue:string): TStrings;
var
 s:string;
 i, k:integer;
begin
 result := TStringList.Create;
 result.Delimiter := ',';
 result.DelimitedText := AValue;

  for i := 0 to result.Count - 1 do
    begin
      s := result[i];
      k := s.IndexOf('=');
      s := s.Substring(0, k);
      result[i] := s;
    end;
end;

procedure TOrders.SetById(AOrderId: string; AOrder: TOrder);
var
 i:integer;
 LOrder: TOrder;
begin

  for i := 0 to self.Items.Count - 1 do
   begin
    if self.Items[i].Id = AOrderId then
     begin
      self.Items[i].Lock;
       try
        self.Items[i].Assign(AOrder);
       finally
        self.Items[i].Unlock;
       end;
      exit;
     end;
   end;

  //если дошли сюда, значит вызов добавляем
  LOrder := TOrder.Create;
  LOrder.Assign(AOrder);

  self.Lock;
   try
    self.Items.Add(LOrder);
   finally
     self.Unlock;
   end;

end;
end.
