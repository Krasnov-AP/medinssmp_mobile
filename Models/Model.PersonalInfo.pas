﻿unit Model.PersonalInfo;

interface

uses
  System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
  System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
  Model.AnyData, Core.Common, Core.Crypt, Core.Funcs;

type

 TPersonalInfoItem = class(TModelAnyData)
  strict private
    FFirstName, FLastName, FMiddleName: string;
    FRoleId:integer;
    FPhone, FEmail: string;
    FHelpTypes: string;
    FId: integer;
    FLogin: string;
    FFullInfo: string;
  strict private
   function GetFullName: string;
  public
   property FirstName:string read FFirstName write FFirstName;
   property LastName:string read FLastName write FLastName;
   property MiddleName:string read FMiddleName write FMiddleName;
   property RoleId: integer read FRoleId write FRoleId;
   property Phone: string read FPhone write FPhone;
   property Email: string read FEmail write FEmail;
   property HelpTypes: string read FHelpTypes write FHelpTypes;
   property Id: integer read FId write FId;
   property Login: string read FLogin write FLogin;
   property FullInfo: string read FFullInfo write FFullInfo;
   property FullName: string read GetFullName;
  private
    FFullName: string;
  private
    procedure SetFullName(const Value: string);
  public
   procedure ConvertFromJson(AValue:string); override;
   constructor Create;
   destructor Destroy; override;
 end;

 TPersonalInfo = class(TModelAnyData)
   strict private
     FItems: TList<TPersonalInfoItem>;
    strict private
     function GetFields(AValue:string):TStrings;
    public
     property Items:TList<TPersonalInfoItem> read FItems write FItems;
    public
     procedure ConvertFromJson(AValue:string); override;
     constructor Create;
     function GetUserInfo(Login: string): TPersonalInfoItem;
 end;

implementation

{ TPersonalInfo }

procedure TPersonalInfoItem.ConvertFromJson(AValue: string);
var
  ts:TStrings;
  i:integer;
begin

  ts := TStringList.Create;
   try
     ts.Delimiter := ',';
     ts.DelimitedText := AValue;

     self.Id := StrToIntDef(Base64DecodeAnsi(ts[0]), -1);
     if ts.Count>=1 then
      self.Login := Base64DecodeAnsi(ts[1]);
     if ts.Count>=4 then
      self.FirstName := Base64DecodeAnsi(ts[3]);
     if ts.Count>=5 then
      self.LastName := Base64DecodeAnsi(ts[4]);
     if ts.Count>=6 then
      self.MiddleName := Base64DecodeAnsi(ts[5]);
     if ts.Count>=7 then
      self.RoleId := StrToIntDef(Base64DecodeAnsi(ts[6]), -1);
     if ts.Count>=9 then
      self.Phone := Base64DecodeAnsi(ts[8]);
     if ts.Count>=10 then
      self.Email := Base64DecodeAnsi(ts[9]);
     if ts.Count>=12 then
      self.HelpTypes := Base64DecodeAnsi(ts[11]);

     self.FullInfo := ts.Text;

   finally
     ts.DisposeOf;
   end;
end;

constructor TPersonalInfoItem.Create;
begin
 inherited;
 self.FFirstName := '';
 self.FLastName := '';
 self.FMiddleName := '';
end;

destructor TPersonalInfoItem.Destroy;
begin

  inherited;
end;

function TPersonalInfoItem.GetFullName: string;
begin
 result := self.FFirstName.Trim+' '+self.FMiddleName.Trim+' '+self.FLastName.Trim;
 result := result.Trim;
end;

procedure TPersonalInfoItem.SetFullName(const Value: string);
begin
  FFullName := Value;
end;

{ TPersonalInfo }

procedure TPersonalInfo.ConvertFromJson(AValue: string);
var
 LPersonalInfoItem: TPersonalInfoItem;
 ts:TStrings;
 i: integer;
 LFields:TStrings;
begin

  self.SetDataString(TCrypter.Decrypt(AValue));

   ts := TStringList.Create;
    try
      ts.Text := self.GetDataString;
      self.FItems.Clear;

       //собираем инфу по полям
       LFields := self.GetFields(ts[0]);
       for i := 1 to ts.Count - 1 do
         begin
           LPersonalInfoItem := TPersonalInfoItem.Create;
           LPersonalInfoItem.SetDataString(ts[i]);
           LPersonalInfoItem.ConvertFromJson(ts[i]);
           self.FItems.Add(LPersonalInfoItem);
         end;

    finally
      ts.DisposeOf;
    end;
end;

constructor TPersonalInfo.Create;
begin
  inherited;
  self.FItems := TList<TPersonalInfoItem>.Create;
end;

function TPersonalInfo.GetFields(AValue: string): TStrings;
begin
  result := nil;
end;

function TPersonalInfo.GetUserInfo(Login: string): TPersonalInfoItem;
var
 i: integer;
begin

  result := nil;

  if not Assigned(self.Items) then exit(Result);
  if (self.Items.Count = 0) then exit(Result);
  
  for i:= 0 to self.Items.Count - 1 do
   begin
     if self.Items[i].Login.Trim = Login.Trim then
      begin
        result := self.Items[i];
        exit;
      end;

   end;
end;

end.
