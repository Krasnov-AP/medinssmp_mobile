﻿unit Model.SMPService;

interface

uses
 System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
 System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
 Model.AnyData, Core.Common;

type

 TSMPService = class(TModelAnyData)
  strict private
   FId:Integer;
   FName, FIP, FCode:string;
  public
   property Id:Integer read FId write FId;
   property Name:string read FName write FName;
   property IP:string read FIP write FIP;
   property Code:string read FCode write FCode;
   //procedure Assign(ASrc:TSMPService);
   function ConvertToJson:string; override;
 end;

 TCompanyListData = record
   Company_List: TList<TSMPService>;
 end;

 TSMPServices = class(TModelAnyData)
  strict private
   FData: TCompanyListData;
   FSuccess:string;
  public
   property Data:TCompanyListData read FData write FData;
   property Success:string read FSuccess write FSuccess;

   procedure ConvertFromJson(AValue:string); override;
   function ConvertToJson:string; override;
 end;

implementation

{ TSMPServices }

procedure TSMPServices.ConvertFromJson(AValue: string);
var
 X:ISuperObject;
 LIsSuccess: boolean;
begin

 X := SO(AValue);

 LIsSuccess := X.S['success'] = 'true';

 if not LIsSuccess then
  begin
    raise Exception.Create(TLangManager.Translate('smp_services_load_error'));
    exit;
  end;

 self.AssignFromJSON(AValue);

end;

function TSMPServices.ConvertToJson: string;
begin
 result := TJson.Stringify<TSMPServices>(self);  //или result := self.AsJson;
end;

{ TSMPService }

function TSMPService.ConvertToJson: string;
begin
  result := self.AsJSON;
end;

end.
