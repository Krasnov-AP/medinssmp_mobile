﻿unit Model.SkList;

interface

uses
  System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
  System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
  Model.AnyData, Core.Common, Core.Crypt, Core.Funcs, System.Rtti, System.Messaging;

type

TSKUnit = class(TModelAnyData)
 const
   FIELD_NAMES = 'Id=580,Name=448,Code=448,CallTime=496,IsDeleted=500,PriceId=496,IsDefault=500,UseRadarMed=500';
 strict private
   _FId: string; {0}
   _FName: string; {1}
   _FCode: string; {2}
   _FCallTime: string; {3}
   _FIsDeleted: string; {4}
   _FPriceId: string; {5}
   _FIsDefault: string; {6}
   _FUseRadarMed: string; {7}
 strict private
    FFields: TStrings;
    FFullInfo: string;
 public
   property Id: string read _FId write _FId;
   property Name: string read _FName write _FName;
   property Code: string read _FCode write _FCode;
   property CallTime: string read _FCallTime write _FCallTime;
   property IsDeleted: string read _FIsDeleted write _FIsDeleted;
   property PriceId: string read _FPriceId write _FPriceId;
   property IsDefault: string read _FIsDefault write _FIsDefault;
   property UseRadarMed: string read _FUseRadarMed write _FUseRadarMed;
 public
  property FullInfo: string read FFullInfo write FFullInfo;
  property Fields: TStrings read FFields write FFields;
 public
   procedure ConvertFromJson(AValue: string); override;
   constructor Create;
end;

TSkList = class(TModelAnyData)
   strict private
     FItems: TList<TSKUnit>;
    strict private
     function GetFields(AValue:string):TStrings;
    public
     property Items:TList<TSKUnit> read FItems write FItems;
    public
     procedure ConvertFromJson(AValue:string); override;
     constructor Create;
end;

implementation

{ SKUnit }

procedure TSKUnit.ConvertFromJson(AValue: string);
var
  ts:TStrings;
  i:integer;
  s: string;
begin

  ts := TStringList.Create;
   try
     ts.Delimiter := ',';
     ts.DelimitedText := AValue;

     for i:=0 to ts.Count-1 do
       begin
        try

         self.SetProperty(self.Fields[i], Base64DecodeAnsi(ts[i]));

         s := s + self.Fields[i]+'='+Base64DecodeAnsi(ts[i]) + ',';
        except on E:Exception do
         TLogger.Save('[TCar.ConvertFromJson] setField err: '+e.Message, TLogLevel.Error);
        end;
       end;

     self.FullInfo := ts.Text;

   finally
     ts.DisposeOf;
   end;
end;

constructor TSKUnit.Create;
begin

 inherited;

 self.FFields := TStringList.Create;
 self.Clear;

end;

{ TSkList }

procedure TSkList.ConvertFromJson(AValue: string);
var
 LSKUnit:TSKUnit;
 ts:TStrings;
 i: integer;
 LFields:TStrings;
begin

  try
    self.SetDataString(TCrypter.Decrypt(AValue));

     ts := TStringList.Create;
      try
        ts.Text := self.GetDataString;
        self.FItems.Clear;

         //собираем инфу по полям
         LFields := self.GetFields(ts[0]);
         for i := 1 to ts.Count - 1 do
           begin
             LSKUnit := TSKUnit.Create;
             LSKUnit.Fields := LFields;
             LSKUnit.SetDataString(ts[i]);
             LSKUnit.ConvertFromJson(ts[i]);
             self.FItems.Add(LSKUnit);
           end;

      finally
        ts.DisposeOf;
      end;
    except on E:Exception do
     begin
      raise Exception.Create(e.Message);
     end;
  end;
end;

constructor TSkList.Create;
begin
 inherited;
 self.FItems := TList<TSKUnit>.Create;
end;

function TSkList.GetFields(AValue: string): TStrings;
var
 s:string;
 i, k:integer;
begin
 result := TStringList.Create;
 result.Delimiter := ',';
 result.DelimitedText := AValue;

  for i := 0 to result.Count - 1 do
    begin
      s := result[i];
      k := s.IndexOf('=');
      s := s.Substring(0, k);
      result[i] := s;
    end;
end;

end.
