﻿unit Model.User.GeoData;

interface

uses

System.Classes, System.SysUtils, System.SyncObjs, System.Generics.Collections, System.Math,
Core.Common, Model.AnyData, XSuperObject, XSuperJson, System.NetEncoding;

type

 TModelUserGeoData = class
   strict private
    FLon, FLat:string;
    FUserToken:string;
   public
    property Lon:string read FLon write FLon;
    property Lat:string read FLat write FLat;
    property UserToken:string read FUserToken write FUserToken;
    procedure AssignUser(AUser:TModelUserGeoData);
 end;

 TUserGeoData = class(TModelAnyData)
  public
   function ConvertToJson:string; override;
   procedure ConvertFromJson(AValue:string); override;
   constructor Create;
   destructor Destroy; override;
 end;

implementation

{ TModelUserGeoData }

procedure TModelUserGeoData.AssignUser(AUser: TModelUserGeoData);
begin
 //todo
end;

{ TUserGeoData }

function TUserGeoData.ConvertToJson: string;
begin
 //todo
end;

constructor TUserGeoData.Create;
begin
 //todo
end;

destructor TUserGeoData.Destroy;
begin

  inherited;
end;

procedure TUserGeoData.ConvertFromJson(AValue: string);
begin
  inherited;

end;

end.
