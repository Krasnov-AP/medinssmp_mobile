﻿unit Model.User;

interface

uses

System.Classes, System.SysUtils, System.SyncObjs, System.Generics.Collections, System.Math,
Core.Common, Model.AnyData, XSuperObject, XSuperJson, System.NetEncoding, FMX.Dialogs, Core.Crypt,
Model.LangConsts, Model.PersonalInfo;

type

// TModelUser = class
//   strict private
//    FName:string;
//    FPassword:string;
//    FToken:string;
//   public
//    property Name:string read FName write FName;
//    property Password:string read FPassword write FPassword;
//    property Token:string read FToken write FToken;
//    procedure AssignUser(AUser:TModelUser);
// end;

 TUser = class(TModelAnyData)
 strict private
    FName:string;
    FPassword:string;
    FToken:string;
    FId:integer;
    FPersonalInfo: TPersonalInfoItem;
  public
   property Name:string read FName write FName;
   property Password:string read FPassword write FPassword;
   property Token:string read FToken write FToken;
   property Id:integer read FId write FId;
   property PersonalInfo: TPersonalInfoItem read FPersonalInfo write FPersonalInfo;
   function ConvertToJson:string; override;
   procedure ConvertFromJson(AValue:string); override;
   procedure AssignUser(AUser:TUser);
   constructor Create;
   destructor Destroy; override;
 end;

implementation

{ TUser }

function TUser.ConvertToJson: string;
var
 X:ISuperObject;
 //LUser:TModelUser;
begin
  //todo

 result := '';

 X := SO;
 X.S['name'] := TNetEncoding.Base64.Encode(self.Name);
 X.S['password'] := TNetEncoding.Base64.Encode(self.Password);
 X.S['token'] := self.Token;

 result := X.AsJson;

end;

procedure TUser.AssignUser(AUser: TUser);
begin

  self.Name := AUser.Name;
  self.Password := AUser.Password;
  self.Token := AUser.Token;
  self.Id  := AUser.Id;

end;

constructor TUser.Create;
begin
 inherited;
 self.FPersonalInfo := TPersonalInfoItem.Create;
end;

destructor TUser.Destroy;
begin
 inherited;
end;

procedure TUser.ConvertFromJson(AValue: string);
var
 X:ISuperObject;
 s:string;
 LId:integer;
 ts:TStrings;
begin

 s := TCRypter.Decrypt(AValue);

  try
   LId := StrToInt(s);
   self.Id := LId;
  except on E:Exception do
    begin
     ts := TStringList.Create;
      try
        ts.CommaText := s;
        if ts.Count>0 then
         raise Exception.Create(TLangManager.Translate(ts.Values['msg']))
          else
         raise Exception.Create(TLangManager.Translate(Model.LangConsts.LANG_UNKNOWN_ERROR));
      finally
        ts.DisposeOf;
      end;
    end;
  end;


 {X := SO(AValue);

 self.Name := TNetEncoding.Base64.Decode(X.S['name']);
 self.Password := TNetEncoding.Base64.Decode(X.S['password']);
 self.Token := X.S['token'];}

end;

end.
