﻿unit Model.ViewParams;

interface

 uses
  Model.AnyData, XSuperObject, XSuperJson, Core.Common, FMX.Types;

 type

   TActionButtons = set of (abYesNo, abBack, abNone, abAdd, abOK, abCancel, abRefresh, abFilter);

   TViewParams = class(TModelAnyData)
    strict private
     FTitle:string;
     FViewClass:string;
     FLocale:string;
     FIsModal:boolean;
     FFormat:string;
     FActionButtons:TActionButtons;
     FMainMenuVisible:boolean;
     FCaller: integer; //кто вызывал вьюху
     FDisableAfterShow: boolean;
     //FParent: string;
    public
     property Title:string read FTitle write FTitle;
     property ViewClass:string read FViewClass write FViewClass;
     property Locale:string read FLocale write FLocale;
     property IsModal:boolean read FIsModal write FIsModal;
     property Format:string read FFormat write FFormat;
     property ActionButtons: TActionButtons read FActionButtons write FActionButtons;
     property MainMenuVisible:boolean read FMainMenuVisible write FMainMenuVisible;
     property Caller: integer read FCaller write FCaller;
     property DisableAfterShow: boolean read FDisableAfterShow write FDisableAfterShow;
     //property Parent: string read FParent write FParent;
    public
     function ConvertToJson:string; virtual;
     procedure ConvertFromJson(AValue:string); virtual;
     constructor Create;
   end;

implementation

{ TViewParams }

procedure TViewParams.ConvertFromJson(AValue: string);
begin
 inherited;
end;

function TViewParams.ConvertToJson: string;
begin
 result := self.AsJSON();
end;

constructor TViewParams.Create;
begin
 self.FFormat := 'JSON';
 self.FMainMenuVisible := true;
 self.Locale := TLangManager.Locale;
 self.Caller := 1;
 self.FDisableAfterShow := false;
end;

end.
