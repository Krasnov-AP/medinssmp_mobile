﻿unit Model.Yandex.Geocode;

interface

uses
  System.Generics.Collections, XSuperObject, XSuperJson, System.NetEncoding,
  System.Internal.ExcUtils, System.SysUtils, System.Classes, FMX.Dialogs,
  Model.AnyData, Core.Common, Core.Crypt, Core.Funcs;

type

 {TYandexGeoItem = class (TModelAnyData)
   strict private
    FTitle, FSubtitle: string;
   public
    property Title: string read FTitle write FTitle;
    property Subtitle: string read FSubtitle write FSubTitle;
 end;}

 TYandexGeoItem = record
   Title, Subtitle: string;
 end;

 TYandexGeoData = class(TModelAnyData)
  strict private
   FItems: TList<TYandexGeoItem>;
  public
   property Items: TList<TYandexGeoItem> read FItems write FItems;
  public
   procedure ConvertFromJson(AValue:string); override;
   constructor Create;
 end;

 TYandexCoordItem = record
   Lat, Lon: string;
 end;

 TYandexCoordData = class(TModelAnyData)
  strict private
   FItems: TList<TYandexCoordItem>;
  public
   property Items: TList<TYandexCoordItem> read FItems write FItems;
  public
   procedure ConvertFromJson(AValue:string); override;
   constructor Create;
 end;

implementation

{ TYandexGeoData }

procedure TYandexGeoData.ConvertFromJson(AValue: string);
var
 LItem: TYandexGeoItem;
 X:ISuperObject;
 i: Integer;
 ts: TStrings;
begin

  X := SO(AValue);

  ts := TStringList.Create;
  ts.Delimiter := ',';

  self.FItems.Clear;

  try

  for i := 0 to X.A['results'].Length - 1 do
    begin
      ts.DelimitedText := X.A['results'].O[i].S['text'];
      LItem.Title := X.A['results'].O[i].O['title'].S['text'];
      LItem.SubTitle := X.A['results'].O[i].O['subtitle'].S['text'];
      self.FItems.Add(LItem);
    end;

  finally

    ts.Free;
  end;
end;

constructor TYandexGeoData.Create;
begin
  inherited;
  self.FItems := TList<TYandexGeoItem>.Create;
end;

{ TYandexCoordData }

procedure TYandexCoordData.ConvertFromJson(AValue: string);
var
 LItem: TYandexCoordItem;
 X:ISuperObject;
 i: Integer;
 ts: TStrings;
begin
  X := SO(AValue);

  ts := TStringList.Create;
  ts.Delimiter := ' ';

  self.FItems.Clear;

  try

  for i := 0 to X.O['response'].O['GeoObjectCollection'].A['featureMember'].Length - 1 do
    begin
      ts.DelimitedText :=X.O['response'].O['GeoObjectCollection'].A['featureMember'].O[i].O['GeoObject'].O['Point'].S['pos'];
      if ts.Count>1 then
        begin
         LItem.Lat := ts[0];
         LItem.Lon := ts[1];
        end;
      self.FItems.Add(LItem);
    end;

  finally

    ts.Free;
  end;
end;

constructor TYandexCoordData.Create;
begin
 inherited;
 self.FItems := TList<TYandexCoordItem>.Create;
end;

end.
