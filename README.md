Мобильный клиент Мединс АРМ СМП.
Поддерживаемые платформы:
 - Android 7.1+
 - IOS (не тестировалось)
 - Windows 7-11 (x32, x64) - без карты
 - MacOS ?
 
Сборка проекта:
1) Тестировалось и собиралось под Delphi XE10.4.2 и Android SDK 25.2.5. По идее должно скомпилироваться под Delpi XE7+ (но не тестировалось). Под D11 для сборки нужно внести незначительные изменения при работе с потоками. 
Важно! При сборке под Delphi ниже версии  10.3.3 не будет поддержки Android 11!
1) Никаких сторонних компонентов, требующих дополнительной установки, не требуется. Все сторонние модули располагаются в папке 3dparty
2) Выходные исполняемые файлы будут лежать в папке Output рядом с папкой проекта (ну или переназначить выходную папку в свойствах проекта).
3) После первой компиляции под Win перенести из корня проекта каталоги  data\ и sound\ в output\Win64\debug и компильнуть еще раз
4) Под Win карта работать не будет (т.к. стандартный TWebBrowser использует древний движок IE9 даже после пропатчивания). Chromium не внедрял, т.к. проект делается под моб. платформы в первую очередь. 
5) В Library Path (Tools->Options->Language->Delphi->Library) прописать пути к сторонним модулям:
  - \3dparty\ScriptGate
  - \3dparty\KastriFree\API
  - \3dparty\KastriFree\Include
  - \3dparty\KastriFree\Core
 
