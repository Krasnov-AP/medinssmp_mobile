﻿unit View.Filter.Archive;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, View.Base,
  FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.DateTimeCtrls, Core.Common,
  Model.Global, FMX.ListBox;

type
  TViewFilterOrdersArchive = class(TViewBase)
    Label1: TLabel;
    Button2: TButton;
    btnOK: TButton;
    GridPanelLayout1: TGridPanelLayout;
    lbDateFrom: TLabel;
    edDateFrom: TDateEdit;
    lbDateTo: TLabel;
    edDateTo: TDateEdit;
    Layout1: TLayout;
    lbStatus: TLabel;
    cbStatus: TComboBox;
    procedure Button2Click(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    function CheckForErrors: boolean;
    procedure AfterShow; override;
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TViewFilterOrdersArchive.AfterShow;
begin
  inherited;

  self.edDateFrom.Date := ModelDataGlobal.AppSettings.FilterArchive.DateFrom;
  self.edDateTo.Date := ModelDataGlobal.AppSettings.FilterArchive.DateTo;
  if ModelDataGlobal.AppSettings.FilterArchive.State = - 1 then
   self.cbStatus.ItemIndex := 0
    else
     if ModelDataGlobal.AppSettings.FilterArchive.State = 4 then
      self.cbStatus.ItemIndex := 1
       else
        self.cbStatus.ItemIndex := 2;


end;

procedure TViewFilterOrdersArchive.btnOKClick(Sender: TObject);
begin
  inherited;

  if not CheckForErrors then exit;

  ModelDataGlobal.AppSettings.FilterArchive.DateFrom := self.edDateFrom.Date;
  ModelDataGlobal.AppSettings.FilterArchive.DateTo := self.edDateTo.Date;

  case self.cbStatus.ItemIndex of
    0: ModelDataGlobal.AppSettings.FilterArchive.State := -1;
    1: ModelDataGlobal.AppSettings.FilterArchive.State := 4;
    2: ModelDataGlobal.AppSettings.FilterArchive.State := 22;
  end;

  ModelDataGlobal.AppSettings.FilterArchive.Active := true;

  self.HideFilter;

  TThread.CreateAnonymousThread(
            procedure()
              begin
                Sleep(260);
                TThread.Synchronize(TThread.CurrentThread,
                procedure
                  begin
                    self.SendMessage('refresh_data');
                  end);
              end).Start;

end;

procedure TViewFilterOrdersArchive.Button2Click(Sender: TObject);
begin
  inherited;
  self.HideFilter;
end;

function TViewFilterOrdersArchive.CheckForErrors: boolean;
var
 LDiff: integer;
begin

 result := false;

 LDiff := abs(Round(self.edDateFrom.Date - self.edDateTo.Date));

 if (LDiff > Model.Global.MAX_ARCHIVE_DAYS) then
   begin
     TToast.Show(format('Укажите период не более %d дней.', [Model.Global.MAX_ARCHIVE_DAYS]));
     exit;
   end;

 result := true;

end;

end.
