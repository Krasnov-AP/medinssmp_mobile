﻿unit View.Filter.Cars;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Layouts, FMX.Controls.Presentation, View.Base, FMX.Objects, Model.Global, Model.Cars;

type
  TViewFilterCars = class(TViewBase)
    cbAll: TRadioButton;
    Layout1: TLayout;
    lbCaption: TLabel;
    cbOnShift: TRadioButton;
    btnOK: TButton;
    Button2: TButton;
    ltShifts: TLayout;
    procedure btnOKClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AfterShow; override;
    procedure UpdateCarVisibility;
  end;

implementation

{$R *.fmx}

{ TViewFilterCars }

procedure TViewFilterCars.AfterShow;
begin
  inherited;
  if ModelDataGlobal.AppSettings.CarsSettings.OnDutyOnly then
   self.cbOnShift.IsChecked := true
    else
   self.cbAll.IsChecked := true;
end;

procedure TViewFilterCars.btnOKClick(Sender: TObject);
begin
  inherited;
  if ModelDataGlobal.AppSettings.DoSaveSettings then
   begin
     ModelDataGlobal.AppSettings.CarsSettings.OnDutyOnly := self.cbOnShift.IsChecked;
   end;
  self.HideFilter;
  self.UpdateCarVisibility;
  self.SendMessage('save_settings');
end;

procedure TViewFilterCars.Button2Click(Sender: TObject);
begin
  inherited;
  self.HideFilter;
end;

procedure TViewFilterCars.UpdateCarVisibility;
var
 i: integer;
 LCar: TCar;
begin
 //todo
 for i := 0 to ModelDataGlobal.Cars.Items.Count - 1 do
  begin
    LCar := ModelDataGlobal.Cars.Items[i];
    if not ModelDataGlobal.AppSettings.CarsSettings.OnDutyOnly then
     LCar.VisibleOnMap := true
      else
    LCar.VisibleOnMap := LCar.OnDuty.Trim = '1';
  end;
end;

end.
