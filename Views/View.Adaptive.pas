﻿unit View.Adaptive;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.TabControl,
  FMX.ListBox, Core.Common, System.Generics.Collections;

type
  TViewAdaptive = class(TViewBase)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    TabItem4: TTabItem;
    TabItem5: TTabItem;
    lbTab5: TListBox;
    lbTab1: TListBox;
    lbTab2: TListBox;
    lbTab3: TListBox;
    lbTab4: TListBox;
    Button1: TButton;
    Layout1: TLayout;
    Button2: TButton;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
  strict private
    FIsAdaptive: boolean;

  strict protected
   property IsAdaptive:boolean read FIsAdaptive write FIsAdaptive;
    { Private declarations }
  strict protected
   procedure BuildInputs;
  public
    procedure AfterShow; override;
    { Public declarations }
  end;

implementation

{$R *.fmx}

{ TViewAdaptive }

procedure TViewAdaptive.AfterShow;
var
 i: integer;
begin
  inherited;

   for i:=0 to self.lbTab1.Items.Count - 1 do
    self.lbTab1.ListItems[i].Selectable := false;

   if self.IsAdaptive then self.BuildInputs;


end;

procedure TViewAdaptive.BuildInputs;
var
 LCurTab: integer;
 i, LTotalHeight, LCurHeight, LLastId: integer;
 LArr: array of TList<TListBoxItem>;
begin
 //todo

 LCurTab := 0;
 LTotalHeight := 0;
 LCurHeight := 0;
 LLastId := -1;

 self.TabControl1.BeginUpdate;
 self.lbTab1.BeginUpdate;
 self.lbTab2.BeginUpdate;
 self.lbTab1.Items.BeginUpdate;

 try

   //сначала выясняем индекс элемента, начиная с которого начнем переносить эелементы на другие закладки
   for i := 0 to self.lbTab1.Items.Count - 1 do
     begin
      if (LTotalHeight + round(self.lbTab1.ListItems[i].Height + self.lbTab1.ListItems[i].Margins.Bottom + self.lbTab1.ListItems[i].Margins.Top)) >= self.lbTab1.Height -10 then
       begin
         LLastId := i;
         break;
       end;
       LTotalHeight := LTotalHeight + round(self.lbTab1.ListItems[i].Height + self.lbTab1.ListItems[i].Margins.Bottom + self.lbTab1.ListItems[i].Margins.Top);
     end;

   //если такого элемента нет - отваливаем
   if LLastId = -1 then exit;

   if self.lbTab1.Items.Count < LLastId then exit;

   inc(LCurTab);
   dec(LLastId);
   while (Assigned(self.lbTab1.ListItems[LLastId + 1])) do
    begin

      if LCurHeight + round(self.lbTab1.ListItems[LLastId + 1].Height + self.lbTab1.ListItems[LLastId + 1].Margins.Bottom + self.lbTab1.ListItems[LLastId + 1].Margins.Top) >= self.lbTab1.Height - 10 then
         begin
          inc(LCurTab);
          LCurHeight := 0;
         end
          else
            LCurHeight := LCurHeight + round(self.lbTab1.ListItems[LLastId + 1].Height + self.lbTab1.ListItems[LLastId + 1].Margins.Bottom + self.lbTab1.ListItems[LLastId + 1].Margins.Top);

      self.lbTab1.ListItems[LLastId + 1].Parent := (self.FindComponent('lbTab'+(LCurTab+1).ToString) as TListBox);
    end;



   {for i:=0 to self.lbTab1.Items.Count - 1 do
     begin

      if (LTotalHeight + round(self.lbTab1.ListItems[i].Height)) >= self.Height then
       begin
        if LCurTab = 0 then inc(LCurTab);
        self.lbTab1.ListItems[i].Parent := (self.FindComponent('lbTab'+(LCurTab+1).ToString) as TListBox);
        if LCurHeight + round(self.lbTab1.ListItems[i].Height) >= self.Height then
         begin
          inc(LCurTab);
          LCurHeight := 0;
         end
          else LCurHeight := LCurHeight + round(self.lbTab1.ListItems[i].Height);
        //LTotalHeight := 0;
       end;

      LTotalHeight := LTotalHeight + round(self.lbTab1.ListItems[i].Height);
      TLogger.Save('LTotalHeight='+LTotalHeight.ToString+' self.Height='+self.Height.ToString+ ' self.lbTab1.Items.Count='+self.lbTab1.Items.Count.ToString);

     end;}

 finally
   self.lbTab1.Items.EndUpdate;
   self.TabControl1.EndUpdate;
   self.lbTab1.EndUpdate;
   self.lbTab2.EndUpdate;
 end;

end;

procedure TViewAdaptive.Button1Click(Sender: TObject);
begin
  inherited;
  TToast.Show(self.lbTab1.Height.ToString+ ' '+self.Height.ToString);
end;

end.
