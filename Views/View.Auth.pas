﻿unit View.Auth;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, FMX.Layouts, FMX.Controls.Presentation, FMX.Objects, FMX.Edit,
  Controller.Auth, Model.User, Core.Common, Model.Global, FMX.ListBox,
  Model.SMPService, Controller.SMPService, XSuperObject, XSuperJson,
  System.ImageList, FMX.ImgList, FMX.MultiResBitmap, ElAES, System.NetEncoding,
  Core.Crypt, System.Hash, Controller.AnyData, data.main, System.IOUtils,
  Model.LangConsts, Model.ViewParams, FMX.Memo.Types, FMX.ScrollBox, FMX.Memo,
  Core.Funcs, Helper.Yandex.Adr.Autocomplete, FMX.WebBrowser, SG.ScriptGate,
  Model.Yandex.Geocode, Controller.Yandex.Geocode, Controller.PersonalInfo,
  EncdDecd, IdCoderMIME, IdGlobal, IdCoder, Model.Orders, Controller.Orders,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  Controller.Cars, Form.Log, Model.SkList, Controller.SKList;

type
  TViewAuth = class(TViewBase)
    btnOK: TButton;
    btnExit: TButton;
    GridPanelLayout1: TGridPanelLayout;
    Rectangle1: TRectangle;
    Layout1: TLayout;
    lbLogin: TLabel;
    edName: TEdit;
    Layout2: TLayout;
    lbPassword: TLabel;
    edPassword: TEdit;
    Layout3: TLayout;
    Layout4: TLayout;
    aniLoader: TAniIndicator;
    Layout5: TLayout;
    lbError: TLabel;
    Rectangle2: TRectangle;
    Label1: TLabel;
    Layout6: TLayout;
    GridPanelLayout2: TGridPanelLayout;
    lbCompany: TLabel;
    cbCompanyList: TComboBox;
    btnSignIn: TButton;
    cbLang: TComboBox;
    imgLang: TImage;
    GridPanelLayout3: TGridPanelLayout;
    Layout7: TLayout;
    imglLangs: TImageList;
    Image1: TImage;
    lbCompanyIP: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Memo1: TMemo;
    IdHTTP1: TIdHTTP;
    Layout8: TLayout;
    cbSaveSettings: TCheckBox;
    btnRefresh: TSpeedButton;
    imgRefresh: TImage;
    btnLog: TButton;
    Line1: TLine;
    Line2: TLine;
    Button5: TButton;
    procedure btnOKClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnSignInClick(Sender: TObject);
    procedure cbLangChange(Sender: TObject);
    procedure cbCompanyListChange(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure edNameChange(Sender: TObject);
    procedure edNameEnter(Sender: TObject);
    procedure edNameTap(Sender: TObject; const Point: TPointF);
    procedure edNameMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure btnRefreshClick(Sender: TObject);
    procedure btnLogClick(Sender: TObject);
    procedure edPasswordMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure edPasswordTap(Sender: TObject; const Point: TPointF);
    procedure Layout1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
    procedure Layout1Tap(Sender: TObject; const Point: TPointF);
    procedure timControlTimer(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    FControllerAuth: TControllerAuth;
    FControllerSMPService: TControllerSMPService;
    FControllerYandexGeocode: TControllerYandexCoord;
    FControllerPersonalInfo: TControllerPersonalInfo;
    FControllerOrder: TControllerOrder;
    FControllerCars: TControllerCars;
    FControllerSKList: TControllerSKList;
    FPrevControl: TFmxObject;
    procedure MakeError(AMsg: string); override;
    procedure LoadImages;
  public
    property ControllerAuth: TControllerAuth read FControllerAuth write FControllerAuth;
    property ControllerSMPService: TControllerSMPService read FControllerSMPService write FControllerSMPService;
    procedure AfterShow; override;
    procedure BeforeClose; override;
    procedure OnAuthSucces;
    procedure OnAuthFail;
    procedure OnSMPServiceSuccess;
    procedure OnSMPServiceFail;
    procedure OnYandexGeocodeSuccess;
    procedure OnYandexGeocodeFail;
    procedure OnPersonalInfoSuccess;
    procedure OnPersonalInfoFail;
    procedure OnOrderSuccess;
    procedure OnOrderFail;
    procedure OnCarsSuccess;
    procedure OnCarsFail;
    procedure OnSKListSuccess;
    procedure OnSKListFail;
    procedure LoadCompanyList;
    procedure Translate; override;
    procedure AddressDone(address: string);
    procedure GetGeocodeData(address: string);
    { Public declarations }
  end;

implementation

{$R *.fmx}
const
  SampleHTML = '<html>' + '<header>' + '<script type="text/JavaScript">' + 'function Callback(param) {return param+" gnus";}' + 'function func(param) { return Callback(param); }' + // Call from Delphi
    'function aa() {window.location.href = "medins:AddressDone(''call by JS'')";}' + '</script>' + '</head>' + '<body>' + '<br><br>' + // Call Delphi Method
    '<a href="javascript:void(0)" onclick="aa()">Call Delphi noparam procedure</a>' + '<br><br>' + '<a href="YourOrgScheme:Add(''Calc: 30 + 12 = '', 30, 12)">Call Delphi procedure</a>' + '<body>' + '</html>';

procedure TViewAuth.AddressDone(address: string);
begin
 //ShowMessage(TNetEncoding.Base64.Decode(address));
  ShowMessage(address);
end;

procedure TViewAuth.AfterShow;
var
  r: extended;
begin
  inherited;
  //TLogger.Save('[TViewAuth.AfterShow] begin');
  self.ControllerAuth := TControllerAuth.Create;
  self.LoadImages;
  self.LoadCompanyList;
  self.FPrevControl := nil;
  //YandexAdrAutocomplete.Control := self.edName;
end;

procedure TViewAuth.BeforeClose;
begin
  inherited;
end;

procedure TViewAuth.btnExitClick(Sender: TObject);
begin
  inherited;
  //self.CloseApp;
  self.CloseModal;
end;

procedure TViewAuth.btnOKClick(Sender: TObject);
var
  LUser: TUser;
begin
  inherited;

  if self.cbCompanyList.ItemIndex < 1 then
  begin
    TToast.Show(Model.LangConsts.LANG_CHOOSE_COMPANY);
    exit;
  end;

  self.ShowLoader(TLangManager.Translate(Model.LangConsts.LANG_AUTH_IS_GOING_ON));

  LUser := TUser.Create;

  LUser.Name := self.edName.Text;
  LUser.Password := self.edPassword.Text;
  LUser.Token := TOKEN_CLIENT;

  self.ControllerAuth.User := LUser;

  self.ControllerAuth.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  self.ControllerAuth.OnRequestSuccess := self.OnAuthSucces;
  self.ControllerAuth.OnRequestFail := self.OnAuthFail;
  self.ControllerAuth.RetrieveData;

end;

procedure TViewAuth.btnRefreshClick(Sender: TObject);
begin
  inherited;
  self.ShowLoader;
  self.LoadCompanyList;
end;

procedure TViewAuth.btnSignInClick(Sender: TObject);
var
  LViewParams: TViewParams;
begin
  if self.cbSaveSettings.IsChecked then
  begin
    ModelDataGlobal.CurrentUser.Name := self.edName.Text;
    ModelDataGlobal.CurrentUser.Password := self.edPassword.Text;
    ModelDataGlobal.AppSettings.DoSaveSettings := true;
    DataMain.SaveUser;
    DataMain.SaveSettings;
  end;
  self.btnOKClick(self);

end;

procedure TViewAuth.Button3Click(Sender: TObject);
var
  ts, ts2: TStrings;
  ya: TYandexGeoData;
  ya2: TYandexCoordData;
  i, j: integer;
  s: string;
  keyval: TKeyValue;
  o: TOrder;
  cli: TIdHTTP;
  kv: TKeyValue;
begin

  self.SendMessage('play_client_from_sk');
  exit;

  s := '';
  ShowMessage(s.ToInteger.ToString);
  exit;


//  YandexAdrAutocomplete.Control := self.edName;
//
//  exit;
//
//  TAppKeyBoard.Show(self.edName);
//  s := BoolToStr(TAppKeyboard.Visible, True);
//
//  self.Memo1.Lines.Add(s);

//  self.Memo1.Lines.Add(ModelDataGlobal.AppSettings.ConvertToJson);
//
//  exit;
//
//  s := '03000000000000001F7622D9059F533FE6389743563F468B';
//
//  //kv := Core.Funcs.ExtractKeyValue(s);
//
//  ShowMessage(TCrypter.Decrypt(s));
//  exit;
//  {o := TOrder.Create;
//  o.Name := 'Пациент 778';
//  o.Birthday := '12.12.1981';
//  o.Address := 'Масквабад';
//  o.Chat := '%current_time% - Заявка создана (Администратор)';
//  o.SMPUserFIO := 'Администратор';
//  o.Coords := '55.75322,37.622513';
//
//  //self.IdHTTP1.URL.Port := '54002';
//  //self.IdHTTP1.URL.Host := 'http://83.166.242.253';
//
//  cli := TIdHTTP.Create(nil);
//
//  ts := TStringList.Create;
//  ts.Text := o.ConvertToJson;
//  ShowMessage(cli.Post('http://83.166.242.253:54002/?command=insertData&tableName=orders', ts));
//  exit;}
//
//  o := TOrder.Create;
//  o.Name := 'Пациент 790';
//  o.Birthday := '12.12.1981';
//  o.Address := 'Масквабад';
//  o.Chat := '%current_time% - Заявка создана (Администратор)';
//  o.SMPUserFIO := 'Администратор';
//  o.Coords := '55.75322,37.622513';
//
//
//  if not Assigned(self.FControllerOrder) then
//    self.FControllerOrder := TControllerOrder.Create;
//
//  self.FControllerOrder.Order := o;
//
//  self.FControllerOrder.Host := ModelDataGlobal.AppSettings.SMPService.IP;
//
//  self.FControllerOrder.OnRequestSuccess := self.OnOrderSuccess;
//  self.FControllerOrder.OnRequestFail := self.OnOrderFail;
//  self.FControllerOrder.SaveOrder(1050);
//
//  exit;
//
//  //ShowMessage(o.GetProperty('address'));
//  //ShowMessage(o.ConvertToJson);
//  self.Memo1.Lines.Add(o.ConvertToJson);
//  exit;
//
//  ts := TStringList.Create;
//  ts.Add('item1');
//  ts.Add('item2');
//  ts.Add('item3');
//
//  ts.Delimiter := ',';
//  ShowMessage(ts.DelimitedText);
//  exit;
//
//
//  s := 'govno=kal';
//   keyval := ExtractKeyValue(s);
//   ShowMessage(keyval.Key+' | '+keyval.Value);
//
//
////  ShowMessage(TNetEncoding.Base64.Encode('Пациент 33'));
////  exit;
//
//
//  {if not Assigned(self.FControllerPersonalInfo) then
//    self.FControllerPersonalInfo := TControllerPersonalInfo.Create;
//
//  self.FControllerPersonalInfo.Host := '83.166.242.253'; //ModelDataGlobal.AppSettings.SMPService.IP;
//
//  self.FControllerPersonalInfo.OnRequestSuccess := self.OnPersonalInfoSuccess;
//  self.FControllerPersonalInfo.OnRequestFail := self.OnPersonalInfoFail;
//  ts := TStringList.Create;
//  ts.Add('Login=1');
//  self.FControllerPersonalInfo.RetrieveData(rmGET, ts);
//
//  exit; }

  ts := TStringList.Create;
  ts.LoadFromFile('c:\1\test.txt');
  {s := 'CC00000000000000C9411DF11856F5A5D385B73393A5B60533CACCBB2EF495E3CDAA8E086869'+
  'BDF82FB2FEBE7FC0F2EC52F3B328A8908B48A95A68CC3596BEDE1FF7E4A9886715F81D048EDCAEB2D0CC5F216'+
  '11566109038EBDD2FDC1DDAD3534BC044555371DF932F3E76F8957D4335F199A3D0B83C5C5C78C2256AF934DB4'+
  'C3F7FCE96779B6EE2304D7A0DBB448010B2E7D9187157F58FB92B354280928EF1AB1FCFB828CC8ACF61ADBDA7F41E7'+
  '8F0365BD9CFC720B0E308844CE0A4CECAD426F0CF3C2257DC5797D7D96CA4E0E007D24672BEABB56777';}
  s := TCRypter.Decrypt(ts.Text);
  ts.Text := s;

  for i := 1 to ts.Count do
  begin

  end;

  self.Memo1.Lines.Text := s;

  exit;

  ts.Delimiter := ',';
  for i := 1 to self.Memo1.Lines.Count - 1 do
  begin
    ts.DelimitedText := self.Memo1.Lines[i];
    s := '';
    for j := 0 to ts.Count - 1 do
      s := s + Base64DecodeAnsi(ts[j]) + ' | ';
    self.Memo1.Lines.Add(s);
  end;

  exit;

  self.GetGeocodeData('голованова');
  exit;

  ts := TStringList.Create;
  ts.LoadFromFile('ya2.json');

  ya2 := TYandexCoordData.Create;
  ya2.ConvertFromJson(ts.Text);

  ShowMessage(ya2.Items[0].Lat + ' ' + ya2.Items[0].Lon);

  exit;

  YandexAdrAutocomplete.Control := self.edName;

  exit;

  inherited;

end;

procedure TViewAuth.Button4Click(Sender: TObject);
var
  s: string;
  sa: AnsiString;
  Base64: TBase64Encoding;
  a: ArrayOfByte;
  t: TOrder;
  i: string;
begin

  t := TOrder.Create;
  ShowMessage(t.FIELD_NAMES);

  exit;

  s := 'мединс';
 //s := Base64EncodeAnsi(s);
   //s := TIdEncoderMIME.EncodeBytes(ToBytes(s));
   //s := TIdEncoder.Encode(s);
   //Base64 := TBase64Encoding.Create(0);
   //s := Base64.Encode(s);
   //a := StrTobytes(s);
   //s := Core.Funcs.EncodeBase64(s);

  s := TIdEncoderMIME.EncodeString(s, IndyTextEncoding_UTF8);

 //s := Core.Funcs.Base64Encode(ToBytes(s));

  ShowMessage(s);
  sa := s;
  ShowMessage(TCrypter.Encrypt((s)));
 //  base64String:=TIdEncoderMIME.EncodeBytes(ToBytes(Base));

end;

procedure TViewAuth.Button5Click(Sender: TObject);
begin
  inherited;
  self.SendMessage('stop_playing_client_from_sk') ;
end;

procedure TViewAuth.btnLogClick(Sender: TObject);
begin
  inherited;
  frmLog.Show;
end;

procedure TViewAuth.cbCompanyListChange(Sender: TObject);
begin
  inherited;

  ModelDataGlobal.AppSettings.SMPService := self.ControllerSMPService.SMPServices.Data.Company_List[self.cbCompanyList.ItemIndex - 1];
  self.SendMessage('smp_changed');

  {$ifdef DEBUG}
  if self.cbCompanyList.ItemIndex > 0 then
    if Assigned(self.ControllerSMPService.SMPServices.Data.Company_List[self.cbCompanyList.ItemIndex - 1]) then
    begin
      self.lbCompanyIP.Text := ModelDataGlobal.AppSettings.SMPService.IP;
      self.lbCompanyIP.Visible := true;
    end;
  {$endif}
end;

procedure TViewAuth.cbLangChange(Sender: TObject);
begin
  inherited;
  case self.cbLang.ItemIndex of
    0: //RU
      begin
        self.imgLang.Bitmap.LoadFromStream(imgFlagRU);
        Model.Global.ModelDataGlobal.AppSettings.Locale := 'RU';
        TLangManager.Locale := 'RU';
        self.Translate;
      end;
    1: //EN
      begin
        self.imgLang.Bitmap.LoadFromStream(imgFlagGB);
        Model.Global.ModelDataGlobal.AppSettings.Locale := 'EN';
        TLangManager.Locale := 'EN';
        self.Translate;
      end;
  end;
end;

procedure TViewAuth.edNameChange(Sender: TObject);
begin
  inherited;
  //self.GetGeocodeData(self.edName.Text);
end;

procedure TViewAuth.edNameEnter(Sender: TObject);
begin
  inherited;
  //YandexAdrAutocomplete.Control := self.edName;
  //TToast.Show( (Sender as TEdit).Position.Y);
end;

procedure TViewAuth.edNameMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  inherited;
  self.Memo1.Lines.Add('active control = '+(Screen.FocusControl.GetObject.Name)); //  as TForm).ActiveControl.Name);
  self.Memo1.Lines.Add('name mouseup y=' + Y.ToString);
end;

procedure TViewAuth.edNameTap(Sender: TObject; const Point: TPointF);
begin
  inherited;
  self.Memo1.Lines.Add('name tap y=' + Point.Y.ToString);
end;

procedure TViewAuth.edPasswordMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  inherited;
  self.Memo1.Lines.Add('pass mouseup y=' + Y.ToString);
end;

procedure TViewAuth.edPasswordTap(Sender: TObject; const Point: TPointF);
begin
  inherited;
  self.Memo1.Lines.Add('pass tap y=' + Point.Y.ToString);
end;

procedure TViewAuth.GetGeocodeData(address: string);
var
  LFilter: TStrings;
begin

  if not Assigned(self.FControllerYandexGeocode) then
    self.FControllerYandexGeocode := TControllerYandexCoord.Create;

  LFilter := TStringList.Create;
  LFilter.add('address=' + address);
  LFilter.Add('apikey=' + Model.Global.YANDEX_API_KEY);

  self.FControllerYandexGeocode.OnRequestSuccess := self.OnYandexGeocodeSuccess;
  self.FControllerYandexGeocode.OnRequestFail := self.OnYandexGeocodeFail;
  self.FControllerYandexGeocode.RetrieveData(rmGET, LFilter);
end;

procedure TViewAuth.Layout1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  inherited;
  self.Memo1.Lines.Add('name layout mouseup y=' + Y.ToString);
end;

procedure TViewAuth.Layout1Tap(Sender: TObject; const Point: TPointF);
begin
  inherited;
  self.Memo1.Lines.Add('name layout tap y=' + Point.Y.ToString);
end;

procedure TViewAuth.LoadCompanyList;
var
  LControllerSMPService: TControllerSMPService;
begin
  //if not Assigned(self.ControllerSMPService) then
  begin
    self.ShowLoader();
    self.ControllerSMPService := TControllerSMPService.Create;
    self.ControllerSMPService.OnRequestSuccess := self.OnSMPServiceSuccess;
    self.ControllerSMPService.OnRequestFail := self.OnSMPServiceFail;
    self.ControllerSMPService.RetrieveData;
  end;

  {if not Assigned(LControllerSMPService) then
  begin
    //ShowLoader();
    LControllerSMPService := TControllerSMPService.Create;
    //LControllerSMPService.OnRequestSuccess := OnSMPServiceSuccess;
    //LControllerSMPService.OnRequestFail := OnSMPServiceFail;
    LControllerSMPService.RetrieveData;
  end;}

end;

procedure TViewAuth.LoadImages;
begin
  if FindResource(0, 'flag_gb', PChar(RT_RCDATA)) <> 0 then
    imgFlagGB := TResourceStream.Create(0, 'flag_gb', PChar(RT_RCDATA));
  if FindResource(0, 'flag_ru', PChar(RT_RCDATA)) <> 0 then
    imgFlagRU := TResourceStream.Create(0, 'flag_ru', PChar(RT_RCDATA));
end;

procedure TViewAuth.MakeError(AMsg: string);
begin
  inherited;
end;

procedure TViewAuth.OnAuthFail;
begin
  self.HideLoader;
  self.MakeError(Model.LangConsts.LANG_SOMETHING_WENT_WRONG);
end;

procedure TViewAuth.OnAuthSucces;
var
  LViewParams: TViewParams;
  ts: TStrings;
begin

  try
    TLogger.Save('[TViewAuth.OnAuthSucces] start');

    try
      self.ControllerAuth.User.ConvertFromJson(self.ControllerAuth.HttpRequest.Response);
     //TLogger.Save('[TViewAuth.OnAuthSucces] response='+self.ControllerAuth.HttpRequest.Response);
    except
      on E: Exception do
      begin
        self.MakeError(e.Message);
        TLogger.Save('[TViewAuth.OnAuthSucces] err: ' + e.message);
        exit;
      end;
    end;

    if not Assigned(ModelDataGlobal.CurrentUser) then
      ModelDataGlobal.CurrentUser := TUser.Create;

    ModelDataGlobal.CurrentUser.AssignUser(self.ControllerAuth.User);

    try
      if not Assigned(self.FControllerPersonalInfo) then
        self.FControllerPersonalInfo := TControllerPersonalInfo.Create;

      self.FControllerPersonalInfo.Host := ModelDataGlobal.AppSettings.SMPService.IP;

      self.FControllerPersonalInfo.OnRequestSuccess := self.OnPersonalInfoSuccess;
      self.FControllerPersonalInfo.OnRequestFail := self.OnPersonalInfoFail;
      ts := TStringList.Create;
      ts.Add('Login=' + ModelDataGlobal.CurrentUser.Name);
      self.FControllerPersonalInfo.RetrieveData(rmGET, ts);
    finally
      ts.Free;
    end;

    try
      if not Assigned(self.FControllerCars) then
        self.FControllerCars := TControllerCars.Create;

      self.FControllerCars.Host := ModelDataGlobal.AppSettings.SMPService.IP;

      self.FControllerCars.OnRequestSuccess := self.OnCarsSuccess;
      self.FControllerCars.OnRequestFail := self.OnCarsFail;
      self.FControllerCars.RetrieveData();
    finally

    end;

    try
      if not Assigned(self.FControllerSKList) then
        self.FControllerSKList := TControllerSKList.Create;

      self.FControllerSKList.Host := ModelDataGlobal.AppSettings.SMPService.IP;

      self.FControllerSKList.OnRequestSuccess := self.OnSKListSuccess;
      self.FControllerSKList.OnRequestFail := self.OnSKListFail;
      self.FControllerSKList.RetrieveData();
    finally

    end;

    self.CloseModal;
    //показываем карту
    LViewParams := TViewParams.Create;
    LViewParams.Title := TLangManager.Translate(Model.LangConsts.LANG_MAP);
    LViewParams.Locale := Model.Global.ModelDataGlobal.AppSettings.Locale;
    LViewParams.ViewClass := 'TViewMap';
    LViewParams.IsModal := false;
    LViewParams.ActionButtons := [abRefresh];
    self.ShowView(LViewParams);

  finally
    //self.HideLoader;
  end;

end;

procedure TViewAuth.OnCarsFail;
begin
 //todo
  TToast.Show('Не удалось загрузить список бригад');
end;

procedure TViewAuth.OnCarsSuccess;
begin

  try
    self.FControllerCars.Cars.ConvertFromJson(self.FControllerCars.HttpRequest.Response);
  except
    on E: Exception do
    begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewAuth.OnCarsSuccess] err: ' + e.message);
      exit;
    end;
  end;

  ModelDataGlobal.Cars := self.FControllerCars.Cars;
end;

procedure TViewAuth.OnOrderFail;
begin
  self.HideLoader;
  self.MakeError(Model.LangConsts.LANG_SOMETHING_WENT_WRONG);
end;

procedure TViewAuth.OnOrderSuccess;
begin
  try
     //self.FControllerPersonalInfo.PersonalInfo.ConvertFromJson(self.FControllerPersonalInfo.HttpRequest.Response);
    ShowMessage(self.FControllerOrder.HttpRequest.Response + ' ' + TCrypter.Decrypt(self.FControllerOrder.HttpRequest.Response));
  except
    on E: Exception do
    begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewAuth.OnOrderSuccess] err: ' + e.message);
      exit;
    end;
  end;
end;

procedure TViewAuth.OnPersonalInfoFail;
begin
  self.Memo1.Lines.Add('OnPersonalInfoFail');
end;

procedure TViewAuth.OnPersonalInfoSuccess;
var
  ts: TStrings;
  i: integer;
begin

  try
    self.FControllerPersonalInfo.PersonalInfo.ConvertFromJson(self.FControllerPersonalInfo.HttpRequest.Response);
  except
    on E: Exception do
    begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewAuth.OnPersonalInfoSuccess] err: ' + e.message);
      exit;
    end;
  end;

  ModelDataGlobal.CurrentUser.PersonalInfo := self.FControllerPersonalInfo.PersonalInfo.GetUserInfo(ModelDataGlobal.CurrentUser.Name);

  {if Assigned(self.FControllerPersonalInfo.PersonalInfo.Items) then
   begin
    for i := 0 to self.FControllerPersonalInfo.PersonalInfo.Items.Count - 1 do
     self.Memo1.Lines.Add(self.FControllerPersonalInfo.PersonalInfo.Items[i].FirstName +
                          ' '+ self.FControllerPersonalInfo.PersonalInfo.Items[i].LastName);
   end;}

end;

procedure TViewAuth.OnSKListFail;
begin
 try
  TToast.Show('Не удалось загрузить список страховых компаний');
 finally
  self.HideLoader;
 end;
end;

procedure TViewAuth.OnSKListSuccess;
begin

 try

  try
    self.FControllerSKList.SKList.ConvertFromJson(self.FControllerSKList.HttpRequest.Response);
  except
    on E: Exception do
    begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewAuth.OnSKListSuccess] err: ' + e.message);
      exit;
    end;
  end;

  ModelDataGlobal.SKList := self.FControllerSKList.SKList;
 finally
  self.HideLoader;
 end;

end;

procedure TViewAuth.OnSMPServiceFail;
begin
 //todo
  self.HideLoader;
  self.MakeError(Model.LangConsts.LANG_SOMETHING_WENT_WRONG);
end;

procedure TViewAuth.OnSMPServiceSuccess;
var
  i: integer;
  ts: TStrings;
begin
 //todo
  try

    try
      self.ControllerSMPService.SMPServices.ConvertFromJson(self.ControllerSMPService.HttpRequest.Response);
    except
      on E: Exception do
      begin
        self.MakeError(e.Message);
        TLogger.Save('[TViewAuth.OnSMPServiceSuccess] err: ' + e.message);
        exit;
      end;
    end;

    //грузим настройки
    if DataMain.LoadSettings then
    begin
      self.cbSaveSettings.IsChecked := ModelDataGlobal.AppSettings.DoSaveSettings;
      if DataMain.LoadUser then
      begin
        self.edName.Text := ModelDataGlobal.CurrentUser.Name;
        self.edPassword.Text := ModelDataGlobal.CurrentUser.Password;
      end;
    end;

    if Assigned(self.ControllerSMPService.SMPServices.Data.Company_List) then
    begin
      try
        self.cbCompanyList.BeginUpdate;
        self.cbCompanyList.Clear;
        self.cbCompanyList.Items.Add(TLangManager.Translate('choose_company'));
        for i := 0 to self.ControllerSMPService.SMPServices.Data.Company_List.Count - 1 do
        begin
          self.cbCompanyList.Items.Add(self.ControllerSMPService.SMPServices.Data.Company_List[i].Name);

          if ModelDataGlobal.AppSettings.DoSaveSettings then
            if self.ControllerSMPService.SMPServices.Data.Company_List[i].Id = ModelDataGlobal.AppSettings.SMPService.Id then
              self.cbCompanyList.ItemIndex := i + 1;

        end;
        if not ModelDataGlobal.AppSettings.DoSaveSettings then
          self.cbCompanyList.ItemIndex := 0;
      finally
        self.cbCompanyList.EndUpdate;
      end;
    end;

  finally
    self.HideLoader;
   {ifdef TEST}
//     self.edName.Text := '1';
//     self.edPassword.Text := '1';
//     self.SendMessage('smp_load_finished');
     //self.btnOKClick(self);
   {endif}
  end;

end;

procedure TViewAuth.OnYandexGeocodeFail;
begin
end;

procedure TViewAuth.OnYandexGeocodeSuccess;
var
  ts: TStrings;
  ya: TYandexGeoData;
  i: integer;
begin

  try
    self.FControllerYandexGeocode.YandexCoordData.ConvertFromJson(self.FControllerYandexGeocode.HttpRequest.Response);
  except
    on E: Exception do
    begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewAuth.OnYandexGeocodeSuccess] err: ' + e.message);
      exit;
    end;
  end;

  self.Memo1.Lines.Clear;

  if Assigned(self.FControllerYandexGeocode.YandexCoordData.Items) then
  begin
    for i := 0 to self.FControllerYandexGeocode.YandexCoordData.Items.Count - 1 do
      self.Memo1.Lines.Add(self.FControllerYandexGeocode.YandexCoordData.Items[i].Lat + ' ' + self.FControllerYandexGeocode.YandexCoordData.Items[i].Lon);
  end;

end;

procedure TViewAuth.timControlTimer(Sender: TObject);
var
  LObj: TFmxObject;
begin
  inherited;

  if Screen.FocusControl = nil then exit;

  try
    LObj := Screen.FocusControl.GetObject;

    if (LObj = nil) then exit;

    if LObj <> self.FPrevControl then
     begin
      self.Memo1.Lines.Add('LObj name= '+LObj.Name+' y='+Screen.FocusControl.LocalToScreen(TPointF.Create(0,0)).Y.ToString);
      self.FPrevControl := LObj;
     end;

  except on E:Exception do

    TLogger.Save('[TViewAuth.timControlTimer] err: '+e.Message, TLogLevel.Error);

  end;
//
//  exit;
//
//  if self.Parent.InheritsFrom(TForm) then
//  begin
//    //if (self.Parent as TForm) then
//    LForm := self.Parent as TForm;
//    if Assigned(LForm.ActiveControl) then
//    begin
//      if LForm.ActiveControl <> self.FPrevControl then
//      begin
//        if LForm.ActiveControl.InheritsFrom(TEdit) then
//        begin
//          self.FPrevControl := LForm.ActiveControl;
//          self.Memo1.Lines.Add('timControl new y = ' + self.FPrevControl.Position.Y.ToString);
//        end;
//      end;
//    end;
//  end;
end;

procedure TViewAuth.Translate;
begin
  inherited;

  self.lbLogin.Text := TLangManager.Translate(Model.LangConsts.LANG_LOGIN);
  self.lbPassword.Text := TLangManager.Translate(Model.LangConsts.LANG_PASSWORD);
  self.lbCompany.Text := TLangManager.Translate(Model.LangConsts.LANG_COMPANY);
  self.btnSignIn.Text := TLangManager.Translate(Model.LangConsts.LANG_SIGN_IN);

  self.cbCompanyList.BeginUpdate;
  try
    self.cbCompanyList.Items[0] := TLangManager.Translate(Model.LangConsts.LANG_CHOOSE_COMPANY);
  finally
    self.cbCompanyList.EndUpdate;
  end;
end;

end.

