﻿/// <summary>
///   Модуль базового визуального представления
/// </summary>
/// <remarks>
///   <list type="table">
///     <listheader>
///       <term>Параметр</term>
///       <description>Значение</description>
///     </listheader>
///     <item>
///       <term>Версия приложения</term>
///       <description>0.1</description>
///     </item>
///     <item>
///       <term>Дата последнего изменения</term>
///       <description>10.Oct.2021</description>
///     </item>
///     <item>
///       <term>Поддерживаемые платформы разработки</term>
///       <description>Embarcadero Delphi 10.4 Sydney Update 2 и выше</description>
///     </item>
///     <item>
///       <term>Поддерживаемые ОС</term>
///       <description>
///         <p>Windows 7 (SP1+)/8.1/10 (x32/x64)</p>
///         <p>Windows Server 2012 R2/2016/2019 (x32/x64)</p>
///         <p>macOS 10.13/10.14/10.15/11.1 (x64)</p>
///       </description>
///     </item>
///   </list>
/// </remarks>
unit View.Base;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, System.Messaging,
  FMX.Layouts, FMX.Objects, FMX.Controls.Presentation, Core.Common, Model.LangConsts,
  Model.ViewParams, Model.Global, FMX.Edit, FMX.Memo;

const
 VIEWCARS_CALLER_CONFIRM_CAR = 1;
 VIEWCARS_CALLER_CHAT_RECPT = 2;
 SKLIST_CALLER_CHAT_RECPT = 1;
 SKLIST_CALLER_NEW_ORDER = 2;
 HOSPITALS_CALLER_NEW_ORDER = 1;
 CHAT_CALLER_MAIN = 1;
 CHAT_CALLER_ORDER = 2;
 VIEW_CALLER_SILENT = -1;  //открываем вьюху с загрузкой данных, но без отрисовки
 VIEW_CALLER_DEFAULT = 1;
 ORDER_DETAIL_CALLER_SURVEY = 2;

type
  ClassViewBase = class of TViewBase;

  TViewBase = class(TFrame)
    ltHeader: TLayout;
    ltFooter: TLayout;
    ltContent: TLayout;
    rectLoader: TRectangle;
    ltDescr: TLayout;
    AniIndicator1: TAniIndicator;
    lbDesr: TLabel;
    timCommands: TTimer;
    timKeyboard: TTimer;
    procedure timCommandsTimer(Sender: TObject);
    procedure timKeyboardTimer(Sender: TObject);
  const
   COMMAND_TYPES = ''; //в наследниках определить, например 'WEBCONSOLE,SENDLOCATION''
  strict protected
   FCOMMAND_TYPES: string; //если нужно обрабатывать команды, то в потомках, в AfterShow, перед inherited прописать self.FCOMMAND_TYPES := self.COMMAND_TYPES
   FNeedToProcessMessages: boolean;
   FCaller: integer;
   FDataLoaded: boolean; //флажок, грузили ли мы уже данные. Нужно для некторых вьюх.
   FCurrentMessage: string;
   FDisableAfterShow: boolean;
   FViewParams: TViewParams;
  strict protected
    procedure SendMessage(AMsg:String); overload;
    procedure SendMessage(Sender: TObject; AMsg: string); overload;
    procedure ProcessCommands; virtual;
    procedure ProcessCommand(ACommandType, ACommand: string); virtual; //переопределяем в потомках
    procedure ProcessMessages(const Sender: TObject; const M: TMessage); virtual;
    procedure Subscribe; virtual;
    procedure Unsubscribe; virtual;
  public
    property Caller: integer read FCaller write FCaller; //кто вызвал вьюху
    property DataLoaded:boolean read FDataLoaded write FDataLoaded default false;
    property DisableAfterShow: boolean read FDisableAfterShow write FDisableAfterShow default false;
    property ViewParams: TViewParams read FViewParams write FViewParams;
  public
    { Public declarations }
    procedure CloseModal;
    procedure CloseApp;
    procedure AfterShow; virtual;
    procedure BeforeClose; virtual;
    procedure ShowLoader(ADescr:string = ''); virtual;
    procedure HideLoader;
    procedure ShowShadow;
    procedure HideShadow;
    procedure MakeError(AMsg:string); virtual;
    procedure Translate; virtual;
    procedure ShowView(AViewParams:TViewParams);
    procedure RefreshData; virtual;
    procedure ShowFilter(AView: ClassViewBase);
    procedure HideFilter;
  end;

var
 imgFlagGB, imgFlagRU:TResourceStream;

implementation

{$R *.fmx}

{ TViewBase }

procedure TViewBase.AfterShow;
begin
 //todo
 //self.timKeyboard.Enabled := true;
 self.timCommands.Enabled := true;
 self.FCurrentMessage := '';
 self.FNeedToProcessMessages := true;
 self.Subscribe;
end;

procedure TViewBase.BeforeClose;
begin
 //todo
 //self.timKeyboard.Enabled := false;
 self.timCommands.Enabled := false;
 self.Unsubscribe;
end;

procedure TViewBase.CloseApp;
begin
  self.SendMessage('close_app');
end;

procedure TViewBase.CloseModal;
begin
 self.SendMessage('close_modal');
end;

procedure TViewBase.HideFilter;
begin
 self.SendMessage('hide_filter');
end;

procedure TViewBase.HideLoader;
begin
 self.AniIndicator1.Enabled := false;
 self.rectLoader.Visible := false;
end;

procedure TViewBase.HideShadow;
begin
 self.rectLoader.Visible := false;
end;

procedure TViewBase.MakeError(AMsg: string);
begin
 self.HideLoader;
 TToast.Show(AMsg);
end;

procedure TViewBase.ProcessCommand(ACommandType, ACommand: string);
begin
 //todo переопределяем в потомках
end;

procedure TViewBase.ProcessCommands;
var
 LCommandTypes: TStrings;
 LCommand: string;
 i: integer;
begin
  inherited;

  self.timCommands.Enabled := false;

  LCommandTypes := TStringList.Create;
  LCommandTypes.Delimiter := ',';
  LCommandTypes.DelimitedText := self.FCOMMAND_TYPES.ToUpper;

  try

   for i:= 0 to LCommandTypes.Count - 1 do
    begin

      LCommand:= ModelDataGlobal.GetCommand(LCommandTypes[i].ToUpper);
      self.ProcessCommand(LCommandTypes[i].ToUpper, LCommand);

    end; //for i


  finally
    LCommandTypes.DisposeOf;
    self.timCommands.Enabled := true;
  end;

end;

procedure TViewBase.ProcessMessages(const Sender: TObject; const M: TMessage);
var
 LMsg: string;
begin
  LMsg := (M as TMessage<String>).Value;

  self.FNeedToProcessMessages := not ((LMsg = self.FCurrentMessage) or (LMsg.Trim = ''));

  if self.FNeedToProcessMessages then self.FCurrentMessage := LMsg;
end;

procedure TViewBase.RefreshData;
begin
 //todo
end;

procedure TViewBase.SendMessage(AMsg: String);
var
 Message: TMessage;
begin
 Message := TMessage<String>.Create(AMsg);
 TMessageManager.DefaultManager.SendMessage(self, Message, True);
end;

procedure TViewBase.SendMessage(Sender: TObject; AMsg: string);
var
 Message: TMessage;
begin
 Message := TMessage<String>.Create(AMsg);
 TMessageManager.DefaultManager.SendMessage(Sender, Message, True);
end;

procedure TViewBase.ShowFilter(AView: ClassViewBase);
begin
 self.SendMessage('show_filter_menu='+AView.ClassName);
end;

procedure TViewBase.ShowLoader(ADescr: string);
var
 LDescr:string;
begin

  if self.Caller = View.Base.VIEW_CALLER_SILENT then exit;
  

  if ADescr<>'' then
   LDescr := TLangManager.Translate(ADescr)
    else
   LDescr := TLangManager.Translate(Model.LangConsts.LANG_LOADING);

  self.AniIndicator1.Enabled := true;
  self.rectLoader.Visible := true;
  self.lbDesr.Text := LDescr;
  
end;

procedure TViewBase.ShowShadow;
begin

   if self.Caller = View.Base.VIEW_CALLER_SILENT then exit;

   self.rectLoader.Opacity := 0.95;
   self.rectLoader.Visible := true;
   self.lbDesr.Visible := false;
   self.AniIndicator1.Enabled := false;
   self.AniIndicator1.Visible := false;
end;

procedure TViewBase.ShowView(AViewParams: TViewParams);
begin
 self.SendMessage(AViewParams.ConvertToJson);
end;

procedure TViewBase.Subscribe;
begin
   TMessageManager.DefaultManager.SubscribeToMessage(TMessage<String>, self.ProcessMessages);
end;

procedure TViewBase.timCommandsTimer(Sender: TObject);
begin
 self.timCommands.Enabled := false;
  try
    self.ProcessCommands;
  finally
    self.timCommands.Enabled := true;
  end;
end;

procedure TViewBase.timKeyboardTimer(Sender: TObject);
begin
  if self.Root.Focused is TEdit then
   TAppKeyboard.Show(self.Root.Focused as TEdit)
    else
   if self.Root.Focused is TMemo then
     TAppKeyboard.Show(self.Root.Focused as TMemo)
     else
     TAppKeyboard.Hide;
end;

procedure TViewBase.Translate;
begin
 //todo
end;

procedure TViewBase.Unsubscribe;
begin
 TMessageManager.DefaultManager.Unsubscribe(TMessage<String>, self.ProcessMessages, True);
end;

end.
