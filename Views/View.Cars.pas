﻿unit View.Cars;

interface

uses
   System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, FMX.Layouts, FMX.Controls.Presentation, FMX.Objects, FMX.Edit,
  Controller.Auth, Model.User, Core.Common, Model.Global, FMX.ListBox,
  Model.SMPService, Controller.SMPService, XSuperObject, XSuperJson,
  System.ImageList, FMX.ImgList, FMX.MultiResBitmap, ElAES,
  System.NetEncoding, Core.Crypt, System.Hash, Controller.AnyData, data.main,
  System.IOUtils, Model.LangConsts, Model.ViewParams, FMX.Memo.Types,
  FMX.ScrollBox, FMX.Memo, Core.Funcs, Controller.Cars, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView,
  FMX.TabControl, Model.Cars, View.Scrollable, System.Generics.Collections;

type

   TCarItem = class
   strict private
    FCar: TCar;
    FLabelName, FLabelNumber, FLabelNormalTime: TLabel;
    //FRectState: TRectangle;
    FImageState: TImage;
    FListBoxItem: TListBoxItem;
   strict private
    procedure SetCar(AValue: TCar);
   public
    FRectWrapper: TRectangle;
    OffsetY: integer;
   public
    property Car: TCar read FCar write SetCar;
   public
    procedure MakeCarBlock(AParent: TFmxObject);
    procedure UpdateCarBlock;
    constructor Create;
  end;

  TCarItems = class
   strict private
    FItems: TDictionary<integer, TCarItem>;
    FOnDutyOnly: boolean;
   public
    property Items: TDictionary<integer, TCarItem> read FItems write FItems;
    property OnDutyOnly: boolean read FOnDutyOnly write FOnDutyOnly default true;
   public
    procedure Add(AParent:TFmxObject; AItem: TCarItem);
    procedure Update(ACarId:integer; AItem: TCarItem);
    procedure Clear;
    constructor Create;
  end;

  TViewCars = class(TViewBase)
    lbCars: TListBox;
    Button1: TButton;
    timDblClick: TTimer;
    rbAll: TRadioButton;
    rbOnDuty: TRadioButton;
    procedure Button1Click(Sender: TObject);
    procedure timCommandsTimer(Sender: TObject);
    procedure lbCarsItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure timDblClickTimer(Sender: TObject);
    procedure rbAllClick(Sender: TObject);
    procedure rbOnDutyClick(Sender: TObject);
  strict private
   //FControllerCars: TControllerCars;
   FCarItems: TCarItems;
  strict private
   procedure OnCarsSuccess2(Sender: TObject);
   procedure OnCarsFail2(Sender: TObject);
   procedure ProcessMsgDlg(const AResult: TModalResult);
    { Private declarations }
  public
   //property ControllerCars: TControllerCars read FControllerCars write FControllerCars;

  public
    { Public declarations }
    procedure AfterShow; override;
    procedure RefreshCars(ASilent: boolean = false);
    procedure SelectCar;
    procedure ConfirmCar;
    procedure ConfirmCarForChat;
    procedure RefreshData; override;
  end;

implementation

{$R *.fmx}

{ TCarItem }

constructor TCarItem.Create;
begin
 inherited;
 self.FCar := TCar.Create;
end;

procedure TCarItem.MakeCarBlock(AParent: TFmxObject);
var
 LRect:TRectangle;
 LLine:TLine;
 LLayout:TLayout;
 LLabel:TLabel;
 LImage: TImage;
 lbITem: TListBoxItem;
begin


 try
   lbItem := TListBoxItem.Create(AParent);
   lbItem.Parent := AParent;
   lbItem.Height := 50;

   self.FListBoxItem := lbItem;

   try
    lbItem.Tag := self.Car.Id.ToInteger;
   except on E:Exception do
    lbItem.Tag := -1;
   end;

   LRect := TRectangle.Create(lbItem);
   LRect.Parent := lbItem;
   //LRect.Position.Y := 10000;
   LRect.Align := TAlignLayout.Client;
   LRect.Height := 50;
   LRect.Margins.Bottom := 0;
   LRect.Fill.Color := TAlphaColorRec.Black;
   LRect.HitTest := false;
   LRect.CanFocus := false;


   self.FRectWrapper := LRect;

   LLine := TLine.Create(LRect);
   LLine.Parent := LRect;
   LLine.Height := 1;
   LLine.Stroke.Color := TAlphaColorRec.Lightgray;
   LLine.Align := TAlignLayout.MostBottom;

   //слой с картинкой
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.MostLeft;
   LLayout.Width := 0;
   LLayout.Margins.Bottom := 2;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;

   LImage := TImage.Create(LLayout);
   LImage.Parent := LLayout;
   LImage.Align := TAlignLayout.Center;
   LImage.Width :=16;
   LImage.Height := 16;

     {case self.Order.State.ToInteger of
      csCancelled: LImage.Bitmap := bmpOrderCancelled;
      csInProcess: LImage.Bitmap := bmpOrderInProcess;
      csDone: LImage.Bitmap := bmpOrderDone;
      csNew: LImage.Bitmap := bmpOrderNew;
      csOnAgreement: LImage.Bitmap := bmpOnAgreement;
     end;}

   self.FImageState := LImage;

   //слой с основной меткой (Название бригады)
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Top;
   LLayout.Height := 25;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 18;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   LLabel.Text := self.Car.Name; // + ' ('+self.Car.Id+', State='+self.Car.State+', OnDuty='+self.Car.OnDuty+' OrderId='+self.Car.OrderId+')';
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelName := LLabel;

   //слой с гос. номером
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Top;
   LLayout.Height := 25;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 14;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   LLabel.Text := self.Car.Number;
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelNumber := LLabel;

   //слой с норм. временем
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Left;
   LLayout.Height := 25;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 12;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];
   if Assigned(ModelDataGlobal.CurrentOrder) then
    LLabel.Text := ModelDataGlobal.CurrentOrder.NormalTime + ' мин';
   LLabel.Width := (AParent as TControl).Width / 2;
   LLabel.Margins.Left := 5;
   self.FLabelNormalTime := LLabel;

   //слой с меткой бригады
   {LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Right;
   LLayout.Height := 25;

   LRect := TRectangle.Create(LLayout);
   LRect.Parent := LLayout;
   LRect.Stroke.Color := TAlphaColorRec.Lightgray;
   LRect.Align := TAlignLayout.Right;
   LRect.Width := (AParent as TControl).Width * 0.4;
   LRect.Margins.Top := 5;
   LRect.Margins.Bottom := 5;
   LRect.Margins.Right := 10;
   if self.Order.State.ToInteger<>4 then
    LRect.Fill.Color := TAlphaColorRec.Orangered
     else
    LRect.Fill.Color := TAlphaColorRec.Green;
   self.FRectState := LRect;

   LLabel := TLabel.Create(LRect);
   LLabel.Parent := LRect;
   LLabel.Align := TAlignLayout.Center;
   LLabel.TextSettings.Font.Size := 12;
   LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   if (self.Order.CarId<>'') and (self.Order.CarId<>'0') then
    LLabel.Text := self.Order.Team;
   LLabel.Width := LRect.Width - 10;
   LLabel.Margins.Left := 5;
   self.FLabelTeam := LLabel;}

 finally
 end;

end;

procedure TViewCars.SelectCar;
var
 LCarItem: TCarItem;
 b: boolean;
 LId: integer;
begin

 try

    //сначала снимаем выделение у предыдущего выделенного элемента
    if Assigned(ModelDataGlobal.CurrentCar) then
     begin
      b := self.FCarItems.Items.TryGetValue(ModelDataGlobal.CurrentCar.Id.ToInteger, LCarItem);
      if Assigned(LCarItem) then
       LCarItem.FRectWrapper.Fill.Color := TAlphaColorRec.Black
     end;

    //теперь выделяем текущий и загоняем в глобальное поле
    LId := -1;
    LId := self.lbCars.Selected.Tag;

     b := self.FCarItems.Items.TryGetValue(LId, LCarItem);

     if b then
      begin
       ModelDataGlobal.CurrentCar := LCarItem.Car;
       LCarItem.FRectWrapper.Fill.Color := TAlphaColorRec.SaddleBrown;
      end;

 except on E:Exception do
   TLogger.Save('[TViewCars.SelectCar] err: '+e.Message);
 end;

end;

procedure TCarItem.SetCar(AValue: TCar);
begin
 self.FCar := AValue;
 self.UpdateCarBlock;
end;

procedure TCarItem.UpdateCarBlock;
begin
 //todo
end;

{ TCarItems }

procedure TCarItems.Add(AParent: TFmxObject; AItem: TCarItem);
begin
  self.FItems.Add(AItem.Car.Id.ToInteger, AItem);

  //if ModelDataGlobal.AppSettings.CarsSettings.OnDutyOnly then
  if self.OnDutyOnly then
   if AItem.Car.OnDuty<>'1' then exit;

  AItem.MakeCarBlock(AParent);
end;

procedure TCarItems.Clear;
begin
 self.FItems.Clear;
end;

constructor TCarItems.Create;
begin
 self.FItems := TDictionary<integer, TCarItem>.Create;
 self.OnDutyOnly := true;
end;

procedure TCarItems.Update(ACarId: integer; AItem: TCarItem);
var
 LItem: TCarItem;
begin
 if self.FItems.TryGetValue(ACarId, LItem) then
  begin
    LItem.Car := AItem.Car;
  end;
end;

{ TViewCars }

procedure TViewCars.AfterShow;
begin
 inherited;

 self.FCarItems := TCarItems.Create;

 self.rbOnDuty.IsChecked := false;
 self.rbOnDuty.IsChecked := false;

 if ModelDataGlobal.AppSettings.CarsSettings.OnDutyOnly then
  self.rbOnDuty.IsChecked := true
   else
  self.rbAll.IsChecked := true;

 self.FCarItems.OnDutyOnly := ModelDataGlobal.AppSettings.CarsSettings.OnDutyOnly;

 self.RefreshCars;

end;

procedure TViewCars.Button1Click(Sender: TObject);
begin
 self.RefreshCars;
end;

procedure TViewCars.ConfirmCar;
begin
 case self.Caller of
  View.Base.VIEWCARS_CALLER_CONFIRM_CAR:
                         MessageDlg('Назначить вызов на бригаду "'+ModelDataGlobal.CurrentCar.Name+'"?',
                         TMsgDlgType.mtInformation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0, ProcessMsgDlg);
  View.Base.VIEWCARS_CALLER_CHAT_RECPT:
                         self.ConfirmCarForChat;
 end;

 //MessageDlg('Назначить вызов на бригаду "'+ModelDataGlobal.CurrentCar.Name+'"?',
 //          TMsgDlgType.mtInformation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0, ProcessMsgDlg);
end;

procedure TViewCars.ConfirmCarForChat;
begin
  self.CloseModal;
  self.SendMessage('confirm_car_for_chat');
end;

procedure TViewCars.lbCarsItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
  inherited;
  //self.SelectCar;

  if self.timDblClick.Tag = Item.Tag then
   begin
     self.timDblClick.Enabled := false;
     self.timDblClick.Tag := -1;
     self.ConfirmCar;
   end;

  self.SelectCar;

  self.timDblClick.Tag := Item.Tag;
  self.timDblClick.Enabled := true;
end;

procedure TViewCars.OnCarsFail2(Sender: TObject);
begin
 try
  self.MakeError('Ошибка при загрузке списка бригад');
 finally
  TControllerCars(Sender).Destroy;
 end;
end;

procedure TViewCars.OnCarsSuccess2(Sender: TObject);
var
 i:integer;
 s:string;
 LParent:TListBox;
 LCarItem: TCarItem;
 LShowStates: set of byte;
 LControllerCars: TControllerCars;
 LCar: TCar;
begin
 try
    TLogger.Save('[TViewCars.OnCarsSuccess2] start');

    if not (Sender is TControllerCars) then
     begin
      TLogger.Save('[TViewCars.OnCarsSuccess2] not TControllerCars! actual Sender is '+Sender.ClassName, TLogLevel.Error);
      exit;
     end;

    LControllerCars := TControllerCars(Sender);

    try
     LControllerCars.Cars.ConvertFromJson(LControllerCars.HttpRequest.Response);
    except on E:Exception do
     begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewCars.OnCarSuccess2] err: '+e.message);
      exit;
     end;
    end;

   TLogger.Save('LControllerCars.Cars.Items.Count='+LControllerCars.Cars.Items.Count.ToString);

   if not Assigned(ModelDataGlobal.Cars) then
     ModelDataGlobal.Cars := TCars.Create;

   ModelDataGlobal.Cars.Lock;
   self.lbCars.BeginUpdate;

   self.lbCars.Content.DeleteChildren;

   self.FCarItems.Clear;

   ModelDataGlobal.Cars.Items.Clear;

   try

     LParent:= self.lbCars;

     for i:=0 to LControllerCars.Cars.Items.Count - 1 do
      begin


         begin
          try
           LCarItem := TCarItem.Create;
           LCarItem.Car.Assign(LControllerCars.Cars.Items[i]);

           //LCarItem.Car.Lock;
            try
             self.FCarItems.Add(LParent, LCarItem);
             ModelDataGlobal.Cars.Items.Add(LCarItem.Car);
            finally
              //LCarItem.Car.Unlock;
            end;
          except on E:Exception do
           TLogger.Save('i='+i.ToString+' '+e.Message);
          end;
         end;

      end;

   finally
     ModelDataGlobal.Cars.Unlock;
     self.lbCars.EndUpdate;
   end;

  finally
   self.HideLoader;
   LControllerCars.Destroy;
   {$ifdef ANDROID}
    TToast.Show('Двойное нажатие - выбор бригады.');
   {$endif}
  end;
end;

procedure TViewCars.ProcessMsgDlg(const AResult: TModalResult);
begin
  case AResult of
   mrYes:
    begin
     self.CloseModal;
     sleep(500);
     self.SendMessage('confirm_car');
    end;
   mrNo: begin end;
 end;
end;

procedure TViewCars.rbAllClick(Sender: TObject);
begin
  inherited;
  self.FCarItems.OnDutyOnly := false;
  self.RefreshCars(false);
end;

procedure TViewCars.rbOnDutyClick(Sender: TObject);
begin
  inherited;
  self.FCarItems.OnDutyOnly := true;
  self.RefreshCars(false);
end;

procedure TViewCars.RefreshCars(ASilent: boolean = false);
var
 LControllerCars: TControllerCars;
begin
  if not ASilent then
   self.ShowLoader;

  LControllerCars := TControllerCars.Create;

  LControllerCars.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  LControllerCars.OnRequestSuccess2 := self.OnCarsSuccess2;
  LControllerCars.OnRequestFail2 := self.OnCarsFail2;
  LControllerCars.RetrieveData();
end;

procedure TViewCars.RefreshData;
begin
  inherited;
  self.RefreshCars(true);
end;

procedure TViewCars.timCommandsTimer(Sender: TObject);
begin
 inherited;
 //todo
end;

procedure TViewCars.timDblClickTimer(Sender: TObject);
begin
  inherited;
  self.timDblClick.Enabled := false;
  self.timDblClick.Tag := -1;
end;

end.
