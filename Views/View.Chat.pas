﻿unit View.Chat;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, View.Base,
  FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.Edit, System.Messaging,
  Helper.View.Input,  Model.SkList, Model.Cars, Core.Common, Core.Crypt, Core.Funcs,
  Model.Global, Model.Chat, Controller.Chat, FMX.ListBox, System.Generics.Collections,
  Controller.AnyData, System.Math, System.Math.Vectors, FMX.Controls3D,
  FMX.Layers3D;

const
  COLOR_GREEN_LIGHT = $20817E62;
  COLOR_GREEN_DARK = $00234200;
  COLOR_GREEN_DOLLAR = $0001E712;
  COLOR_GREEN_VERY_LIGHT = $00C4FFC4;

  COLOR_BLUE_DARK = $006F5B3C;
  COLOR_BLUE_LIGHT = $00C5B194;
  COLOR_BLUE_NORMAL = $00FF001A;

  COLOR_YELOW_LIGHT = $0080FFFF;

  COLOR_GRAY_LIGHT = $00E4DFDC;
  COLOR_GRAY_DARK = $005A6377;
  COLOR_GRAY_VERY_DARK = $00616161;

  COLOR_INCOMING_ORDER = $00D2D2FF;
  COLOR_INCOMING_MESSAGE = COLOR_INCOMING_ORDER;

  COLOR_RED_LIGHT = $008383A5;
  COLOR_RED_DARK = $000000B0;//$00363567;
  COLOR_RED_VERY_DARK = $00363567;

  COLOR_LIGHT_GREEN = $0080FF80;

  COLOR_REANIMATION = $008080FF;
  CHAT_OWNER_PREFIX = '    ';

  OWNER_COLOR: array[0..3] of Integer = (
    TAlphaColorRec.Black,
    TAlphaColorRec.Darkred,
    TAlphaColorRec.Blue,
    TAlphaColorRec.Black
  );


type

  TChatItem = class
   strict private
    FChat: TChat;
   strict private
    procedure SetChat(AValue: TChat);
   public
    FRectWrapper: TRectangle;
    FLabelSender, FLabelMsg, FLabelDate, FLabelTeam: TLabel;
    FRectState: TRectangle;
    FImageState: TImage;
    FListBoxItem: TListBoxItem;
    OffsetY: integer;
   public
    property Chat: TChat read FChat write SetChat;
   public
    procedure MakeChatBlock(AParent: TFmxObject);
    procedure UpdateChatBlock;
    constructor Create;
  end;

  TChatItems = class
   strict private
    FItems: TDictionary<integer, TChatItem>;
    FCaller: integer;
   public
    property Items: TDictionary<integer, TChatItem> read FItems write FItems;
    property Caller: integer read FCaller write FCaller;
   public
    procedure Add(AParent:TFmxObject; AItem: TChatItem);
    procedure Update(AChatId:integer; AItem: TChatItem);
    procedure Clear;
    constructor Create;
  end;


  TViewChat = class(TViewBase)
    edChatMessage: TEdit;
    Image1: TImage;
    btnSendMessage: TButton;
    edRecpt: TEdit;
    btnChatRecpt: TButton;
    Image2: TImage;
    aniSendMessage: TAniIndicator;
    lbChats: TListBox;
    timDblClick: TTimer;
    lbChooseRecptPromt: TLabel;
    lbMessagePromt: TLabel;
    timCheckPromt: TTimer;
    Layout3D1: TLayout3D;
    procedure btnChatRecptClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnSendMessageClick(Sender: TObject);
    procedure edRecptMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure edRecptChange(Sender: TObject);
    procedure edChatMessageChange(Sender: TObject);
    procedure timCheckPromtTimer(Sender: TObject);
  const
    COMMAND_TYPES = 'updateChat';
  strict private
    { Private declarations }
    FControllerChat: TControllerChat;
    FChatLoaded: boolean;
    FChatItems: TChatItems;
    FChatIds: TStrings;
  strict protected
   procedure OnChatsSuccess2(Sender: TObject);
   procedure OnChatsFail2(Sender: TObject);
   procedure OnChatSuccess2(Sender: TObject);
   procedure OnChatFail2(Sender: TObject);
  strict protected
   procedure ProcessMessages(const Sender: TObject; const M: TMessage); override;
   procedure ShowMessageLoader;
   procedure HideMessageLoader;
   procedure OnChatMessageSuccess;
   procedure OnChatMessageFail;
   procedure ProcessCommand(ACommandType, ACommand: string); override;
   procedure UpdateChat(AChatId: string);
  public
    { Public declarations }
   procedure AfterShow; override;
   procedure BeforeClose; override;
   procedure SendChatMessage;
   procedure RefreshData; override;
   function GetSKUnit: TSKUnit;
  end;

var
 LastChatId: integer;

implementation

{$R *.fmx}

{ TViewChat }

procedure TViewChat.AfterShow;
var
 LChatItem: TChatItem;
 LSKUnit: TSKUnit;
begin

  self.FCOMMAND_TYPES := self.COMMAND_TYPES;

  inherited;

  self.aniSendMessage.Enabled := false;
  self.aniSendMessage.Visible := false;

  if Assigned(self.FChatIds) then self.FChatIds.DisposeOf;

  self.FChatIds := TStringList.Create;

  self.edRecpt.Text := '';
  self.edChatMessage.Text := '';

  if self.Caller = View.Base.CHAT_CALLER_ORDER then
   begin
    LSKUnit := self.GetSKUnit;

    if LSKUnit <> nil then
      begin
       self.edRecpt.Text := LSKUnit.Name;
       self.edRecpt.TagObject := LSKUnit;
      end;
   end;

  self.RefreshData;

  self.timCheckPromt.Enabled := true;

  {if not self.FChatLoaded then
   self.RefreshData
    else
     begin
      LChatItem := TChatItem.Create;
      if self.FChatItems.Items.TryGetValue(LastChatId, LChatItem) then
        self.lbChats.ScrollToItem(LChatItem.FListBoxItem);
     end;}

end;

procedure TViewChat.BeforeClose;
begin
 //inherited;
 self.timCheckPromt.Enabled := false;
 //self.Unsubscribe;
end;

procedure TViewChat.btnChatRecptClick(Sender: TObject);
begin
  inherited;
  self.SendMessage('show_chat_recpt');
end;

procedure TViewChat.btnSendMessageClick(Sender: TObject);
begin
  inherited;
  self.SendChatMessage;
end;

procedure TViewChat.Button2Click(Sender: TObject);
begin
  inherited;
  TToast.Show(ModelDataGlobal.AppSettings.SMPService.Id.ToString);
end;

procedure TViewChat.edChatMessageChange(Sender: TObject);
begin
  inherited;
  self.lbMessagePromt.Visible := self.edChatMessage.Text.Trim = '';
end;

procedure TViewChat.edRecptChange(Sender: TObject);
begin
  inherited;
  self.lbChooseRecptPromt.Visible := self.edRecpt.Text.Trim = '';
end;

procedure TViewChat.edRecptMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
  self.btnChatRecptClick(self);
end;

function TViewChat.GetSKUnit: TSKUnit;
var
 i: integer;
begin

  result := nil;
  if ModelDataGlobal.CurrentOrder.SKCode.Trim = '' then exit;

  for i := 0 to ModelDataGlobal.SKList.Items.Count - 1 do
   begin
     result := ModelDataGlobal.SKList.Items[i];
     if result.Code = ModelDataGlobal.CurrentOrder.SKCode then exit;
   end;

end;

procedure TViewChat.HideMessageLoader;
begin
 self.btnSendMessage.Visible := true;
 self.aniSendMessage.Enabled := false;
 self.aniSendMessage.Visible := false;
end;

procedure TViewChat.OnChatFail2(Sender: TObject);
begin
 //todo

end;

procedure TViewChat.OnChatMessageFail;
begin
 self.HideMessageLoader;
 self.MakeError('Не удалось отправить сообщение');
end;

procedure TViewChat.OnChatMessageSuccess;
var
 s: string;
begin

  self.HideMessageLoader;

   try
     //self.FControllerChat.Chat.ConvertFromJson() ControllerCars.Cars.ConvertFromJson(LControllerCars.HttpRequest.Response);
     //TLogger.Save(self.FControllerChat.HttpRequest.Response);
     s := TCrypter.Decrypt(self.FControllerChat.HttpRequest.Response);
     if not Core.Funcs.IsNumeric(s) then
      begin
       self.MakeError('Не удалось отправить сообщение');
       exit;
      end;

    except on E:Exception do
     begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewChat.OnChatMessageSuccess] err: '+e.message);
      exit;
     end;
    end;

   self.edChatMessage.Text := '';

end;

procedure TViewChat.OnChatsFail2(Sender: TObject);
begin
 self.HideLoader;
 self.MakeError('Не удалось загрузить список сообщений...');
 self.timCommands.Enabled := true;
end;

procedure TViewChat.OnChatsSuccess2(Sender: TObject);
var
 i:integer;
 s:string;
 LParent:TListBox;
 LChatItem: TChatItem;
 LChat: TChat;
 LShowStates: set of byte;
 LControllerChats: TControllerChats;
begin

 TLogger.Save('[TViewChat.OnChatsSuccess2] start');

 if not (Sender is TControllerChats) then exit;

 self.HideLoader;

 LControllerChats := TControllerChats(Sender);

 try
    self.FChatLoaded := true;

    try
     //TLogger.Save('[TViewChat.OnChatsSuccess2] response='+LControllerChats.HttpRequest.Response);
     LControllerChats.Chats.ConvertFromJson(LControllerChats.HttpRequest.Response);
    except on E:Exception do
     begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewChat.OnChatsSuccess2] err: '+e.message);
      exit;
     end;
    end;

   self.lbChats.BeginUpdate;
   self.lbChats.Content.DeleteChildren;

   LParent := self.lbChats;

   if not Assigned(self.FChatItems) then self.FChatItems := TChatItems.Create;
   self.FChatItems.Clear;

   try

    TLogger.Save('LControllerChats.Chats.Items.Count='+LControllerChats.Chats.Items.Count.ToString);

     for i:=0 to LControllerChats.Chats.Items.Count - 1 do
      begin

         try
           self.FChatIds.Add(LControllerChats.Chats.Items[i].Id);

           if self.Caller = View.Base.CHAT_CALLER_MAIN then
            if LControllerChats.Chats.Items[i].OrderId.Trim <> '0' then continue;

           if self.Caller = View.Base.CHAT_CALLER_ORDER then
             if LControllerChats.Chats.Items[i].OrderId.Trim  <> ModelDataGlobal.CurrentOrder.Id.Trim then continue;

           if LControllerChats.Chats.Items[i].User.Trim.Contains('Системное сообщение') then continue;


            try
             LChatItem :=TChatItem.Create;
             LChatItem.Chat.Assign(LControllerChats.Chats.Items[i]);

             LChat := TChat.Create;
             LChat.Assign(LControllerChats.Chats.Items[i]);

             //TLogger.Save('msg='+LChat.Msg);

             self.FChatItems.Caller := self.Caller;
             self.FChatItems.Add(LParent, LChatItem);

            except on E:Exception do
             TToast.Show(e.Message);
            end;

         except on E:Exception do
             TLogger.Save('[self.FChatItems.Add] err: '+e.Message);
         end;

      end;

    finally
       self.lbChats.EndUpdate;
       if self.Visible then
        self.lbChats.ScrollToItem(LChatItem.FListBoxItem);
    end;

  finally
   LControllerChats.Destroy;
   self.HideLoader;
   self.timCommands.Enabled := true;
   //self.lbChats
  end;
end;


procedure TViewChat.OnChatSuccess2(Sender: TObject);
var
 i:integer;
 s:string;
 LParent:TListBox;
 LChatItem: TChatItem;
 LChat: TChat;
 LShowStates: set of byte;
 LControllerChat: TControllerChat;
 ts: TStrings;
begin

 TLogger.Save('[TViewChat.OnChatSuccess2] start');

 if not (Sender is TControllerChat) then exit;

 LControllerChat := TControllerChat(Sender);

 try

    ts := TStringList.Create;

    try
     //TLogger.Save('[TViewChat.OnChatSuccess2] response='+LControllerChats.HttpRequest.Response);
     s := TCrypter.Decrypt(LControllerChat.HttpRequest.Response);
     ts.Text := s;

     if ts.Count>1 then
       LControllerChat.Chat.ConvertFromJson(ts[1])
        else
       exit;
    except on E:Exception do
     begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewChat.OnChatSuccess2] err: '+e.message);
      exit;
     end;
    end;

   self.lbChats.BeginUpdate;

   LParent := self.lbChats;

   //TToast.Show('chat change');

    try

     self.FChatItems.Caller := self.Caller;

     if not self.FChatItems.Items.TryGetValue(LControllerChat.Chat.Id.ToInteger, LChatItem) then
        begin
         TLogger.Save('adding chat');
         LChatItem := TChatItem.Create;
         LChatItem.Chat.Assign(LControllerChat.Chat);
         self.FChatItems.Add(LParent, LChatItem);
        end
       else
        begin
         TLogger.Save('updating chat');
         //LChatItem :=TChatItem.Create;
         LChatItem.Chat.Assign(LControllerChat.Chat);
         //TLogger.Save('LControllerChat.Chat.IsReaded='+LControllerChat.Chat.IsReaded+' LChatItem.Chat.IsReaded')
         self.FChatItems.Update(LControllerChat.Chat.Id.ToInteger, LChatItem);
        end;
     except on E:Exception do
      TLogger.Save('[TViewChat.OnChatSuccess2] err: '+e.Message);
    end;

   if Assigned(self.FChatIds) then
     if self.FChatIds.IndexOf(LControllerChat.Chat.Id)<0 then
       begin
         self.FChatIds.Add(LControllerChat.Chat.Id);
         if (LControllerChat.Chat.OrderId = '0') or (LControllerChat.Chat.OrderId = '')  then
          self.SendMessage('new_chat_message')
           else
          self.SendMessage('new_chat_message_for_order');
      end;

    {try

    TLogger.Save('LControllerChats.Chats.Items.Count='+LControllerChats.Chats.Items.Count.ToString);

     for i:=0 to LControllerChats.Chats.Items.Count - 1 do
      begin

           if LControllerChats.Chats.Items[i].OrderId.Trim <> '0' then continue;
           if LControllerChats.Chats.Items[i].User.Trim.Contains('Системное сообщение') then continue;


           LChatItem :=TChatItem.Create;
           LChatItem.Chat.Assign(LControllerChats.Chats.Items[i]);

           LChat := TChat.Create;
           LChat.Assign(LControllerChats.Chats.Items[i]);

           TLogger.Save('msg='+LChat.Msg);

           LControllerChats.Lock;
            try
             self.FChatItems.Add(LParent, LChatItem);
            finally
             LControllerChats.Unlock;
            end;

      end;}

    finally
       ts.Free;
       self.lbChats.EndUpdate;
        if self.FChatItems.Items.TryGetValue(LControllerChat.Chat.Id.ToInteger, LChatItem) then
         self.lbChats.ScrollToItem(LChatItem.FListBoxItem);
       LControllerChat.Destroy;
    end;

 
end;

procedure TViewChat.ProcessCommand(ACommandType, ACommand: string);
var
 i:integer;
 LCommands: TStrings;
 LCommand: string;
 LId: Integer;
begin
  inherited;

  begin
      if ACommand<>'' then
         begin
           LCommands := TStringList.Create;

           try

             TLogger.Save('[TViewChat.ProcessCommand] command: '+ACommand);
             LCommands.Delimiter := '|';
             LCommands.DelimitedText := ACommand;

             for i := LCommands.Count - 1 downto 0 do
               begin

                LCommand := LCommands[i];
                if (LCommand<>'') then
                 begin
                  LId := StrToIntDef(LCommand, -1);
                  self.UpdateChat(LId.ToString);

                  {if self.Visible then
                   self.UpdateChat(LId.ToString)
                    else
                    if Assigned(self.FChatIds) then
                     if self.FChatIds.IndexOf(LId.ToString)<0 then
                      begin
                       self.FChatIds.Add(LId.ToString);
                       self.SendMessage('new_chat_message');
                      end;}

                 end;

               end;

           finally
            LCommands.DisposeOf;
           end;
      end;
   end;

end;

procedure TViewChat.ProcessMessages(const Sender: TObject; const M: TMessage);
var
 LMsg: string;
begin

  inherited;

  if not self.FNeedToProcessMessages then
   begin
    exit;
   end;

  LMsg := (M as TMessage<String>).Value;

  if Sender is THelperViewInput then
   begin
    if LMsg = 'keyboard_shown' then
     begin
      self.ltFooter.Height := KeyboardHelper.KeyboardBounds.Height + 10;// + 75;
     end
      else
    if LMsg = 'keyboard_hidden' then
     begin
      self.ltFooter.Height := 40;
     end
   end
    else
  if LMsg = 'confirm_skunit_for_chat' then
   begin
     self.edRecpt.Text := ModelDataGlobal.CurrentSKUnit.Name;
     self.edRecpt.TagObject := ModelDataGlobal.CurrentSKUnit;
   end
    else
  if LMsg = 'confirm_car_for_chat' then
   begin
     self.edRecpt.Text := ModelDataGlobal.CurrentCar.Name;
     self.edRecpt.TagObject := ModelDataGlobal.CurrentCar;
   end;

end;

procedure TViewChat.RefreshData;
var
 LControllerChats: TControllerChats;
begin

  self.ShowLoader('Загрузка сообщений...');
  self.timCommands.Enabled := false;

  LControllerChats := TControllerChats.Create;

  LControllerChats.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  LControllerChats.OnRequestSuccess2 := self.OnChatsSuccess2;
  LControllerChats.OnRequestFail2 := self.OnChatsFail2;
  LControllerChats.RetrieveData();

end;

procedure TViewChat.SendChatMessage;
var
 O:TChat;
 LSKUnit: TSKUnit;
 LCar: TCar;
begin
 //todo

  if ( (self.edRecpt.Text.Trim = '') or (not Assigned(self.edRecpt.TagObject)) ) then
   begin
     TToast.Show('Укажите получателя!');
     exit;
   end;


  self.ShowMessageLoader;

  o := TChat.Create;
  o.Id := '';
  o.User := ModelDataGlobal.AppSettings.SMPService.Name + '(' + ModelDataGlobal.CurrentUser.PersonalInfo.FullName + ')';
  o.Msg := self.edChatMessage.Text;
  o.IsDeleted := '';

  if self.edRecpt.TagObject is TCar then
    begin
     o.OwnerId := (1000000+(self.edRecpt.TagObject as TCar).Id.ToInteger).ToString;
     o.OwnerName := (self.edRecpt.TagObject as TCar).Name;
     o.OwnerCode := '';
    end;

  if (self.edRecpt.TagObject is TSKUnit) then
   begin
    o.OwnerId := (self.edRecpt.TagObject as TSKUnit).Id;
    o.OwnerName := (self.edRecpt.TagObject as TSKUnit).Name;
    o.OwnerCode := (self.edRecpt.TagObject as TSKUnit).code;
   end;

  o.IsReaded := 'False';
  o.OwnerType := '3';
  o.OtherCode := 'MEDINS';
  o.OrderId := '0';

  if not Assigned(self.FControllerChat) then
    self.FControllerChat := TControllerChat.Create;

  self.FControllerChat.Chat := o;

  self.FControllerChat.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  self.FControllerChat.OnRequestSuccess := self.OnChatMessageSuccess;
  self.FControllerChat.OnRequestFail := self.OnChatMessageFail;
  self.FControllerChat.SendChatMessage(o.OwnerCode);
end;

procedure TViewChat.ShowMessageLoader;
begin
 self.btnSendMessage.Visible := false;
 self.aniSendMessage.Enabled := true;
 self.aniSendMessage.Visible := true;
end;

procedure TViewChat.timCheckPromtTimer(Sender: TObject);
begin
  self.timCheckPromt.Enabled := false;
   try
    self.lbMessagePromt.Visible := self.edChatMessage.Text.Trim = '';
    self.lbChooseRecptPromt.Visible := self.edRecpt.Text.Trim = '';
   finally
     self.timCheckPromt.Enabled := true;
   end;
end;

procedure TViewChat.UpdateChat(AChatId: string);
var
 LControllerChat: TControllerChat;
 LFilter: TStrings;
begin

  LControllerChat := TControllerChat.Create;

  LControllerChat.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  try
    LFilter := TStringList.Create;
    LFilter.Add('chat_id='+AChatId);
    LControllerChat.OnRequestSuccess2 := self.OnChatSuccess2;
    LControllerChat.OnRequestFail2 := self.OnChatFail2;
    LControllerChat.RetrieveData(rmGET, LFilter);
  finally
    LFilter.DisposeOf;
  end;


end;

{ TChatItem }

constructor TChatItem.Create;
begin
 inherited;
 self.FChat := TChat.Create;
end;

procedure TChatItem.MakeChatBlock(AParent: TFmxObject);
var
 LRect:TCalloutRectangle;
 LLine:TLine;
 LLayout:TLayout;
 LLabel:TLabel;
 LImage: TImage;
 lbITem: TListBoxItem;
 LHeight, LWidthSender, LWidthMsg, LBlockWidth: double;
begin

 //if Parent.InheritsFrom(TControl) then
 // (Parent as TControl).BeginUpdate;

 TLogger.Save('[TChatItem.MakeChatBlock] begin Parent = '+AParent.Name);

 try

   lbItem := TListBoxItem.Create(AParent);
   lbItem.Parent := AParent;
   lbItem.Height := 100;
   lbItem.Margins.Top := 20;

   self.FListBoxItem := lbItem;

   try
    lbItem.Tag := self.Chat.Id.ToInteger;
   except on E:Exception do
    lbItem.Tag := -1;
   end;

   LRect := TCalloutRectangle.Create(lbItem);
   LRect.Parent := lbItem;
   //LRect.Position.Y := 10000;
   LRect.Align := TAlignLayout.Contents;
   LRect.Height := lbItem.Height;
   LRect.Margins.Bottom := 0;
   LRect.Fill.Color := TAlphaColorRec.White;
   LRect.HitTest := false;
   LRect.CanFocus := false;
   LRect.Margins.Right := 10;
   LRect.ClipChildren := true;

   if self.Chat.OwnerType.ToInteger = 3 then
    begin
     LRect.CalloutPosition := TCalloutPosition.Right;
     LRect.Align := TAlignLayout.Right;
     LRect.Width := (AParent as TScrollBox).Width * 0.8;
     //self.Chat.Msg := 'w='+LRect.Width.ToString+' '+ self.Chat.Msg;
    end
     else
    begin
     LRect.Align := TAlignLayout.Left;
     LRect.Width := (AParent as TScrollBox).Width*0.8;
     LRect.CalloutPosition := TCalloutPosition.Left;
     self.Chat.IsReaded := '1';
    end;

   LRect.XRadius := 5;
   LRect.YRadius := 5;
   LRect.CalloutWidth := 15;

   self.FRectWrapper := LRect;

   {LLine := TLine.Create(LRect);
   LLine.Parent := LRect;
   LLine.Height := 1;
   LLine.Stroke.Color := TAlphaColorRec.Lightgray;
   LLine.Align := TAlignLayout.MostBottom;}

   //слой с картинкой
   {LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.MostLeft;
   LLayout.Width := 0;
   LLayout.Margins.Bottom := 2;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;}

   {LImage := TImage.Create(LLayout);
   LImage.Parent := LLayout;
   LImage.Align := TAlignLayout.Center;
   LImage.Width :=16;
   LImage.Height := 16;

     case self.Order.State.ToInteger of
      csCancelled: LImage.Bitmap := bmpOrderCancelled;
      csInProcess: LImage.Bitmap := bmpOrderInProcess;
      csDone: LImage.Bitmap := bmpOrderDone;
      csNew: LImage.Bitmap := bmpOrderNew;
      csOnAgreement: LImage.Bitmap := bmpOnAgreement;
      csDetectMore:
       begin
         if (self.Order.HospitalCoords<>'') and (self.Order.Coords = self.Order.HospitalCoords) then
          LImage.Bitmap := bmpOrderInProcess
           else
         if (self.Order.IsHospitalCoordination <> '') then
          LImage.Bitmap := bmpVosklBlue
           else
          LImage.Bitmap := bmpOrderCancelled;
       end;
     end;

   self.FImageState := LImage;}

   //слой с основной меткой (отправитель)
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Top;
   LLayout.Height := 25;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;
   LLayout.Margins.Left := 20;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 14;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];

   LLabel.TextSettings.FontColor := OWNER_COLOR[self.Chat.OwnerType.ToInteger];

   if self.Chat.IsReaded.ToUpper <> '1' then
     LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;

   {if self.Chat.OwnerId.ToInteger > 1000000 then
    LLabel.TextSettings.FontColor := TAlphaColorRec.Green;}

   LLabel.Text := self.Chat.User; //self.Chat.OwnerName+ ' ('+self.Chat.OwnerId+' , '+self.Chat.User+' , '+self.Chat.IsReaded+')'; //+', '+self.Order.State+', '+self.Order.TimeArr+')';
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelSender := LLabel;

   //слой с сообщением
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Client;
   LLayout.Height := 40;
   LLayout.Margins.Left := 20;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Client;
   LLabel.AutoSize := true;
   LLabel.TextSettings.Font.Size := 18;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];
   LLabel.WordWrap := true;

   LLabel.TextSettings.FontColor := OWNER_COLOR[self.Chat.OwnerType.ToInteger];

   if self.Chat.IsReaded.ToUpper <> '1' then
     LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;

   {if self.Chat.OwnerId.ToInteger > 1000000 then
    LLabel.TextSettings.FontColor := TAlphaColorRec.Green;}

   LLabel.Text := self.Chat.Msg;
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelMsg := LLabel;

   //слой с временной меткой
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Bottom;
   LLayout.Height := 25;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Right;
   LLabel.TextSettings.Font.Size := 12;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];
   LLabel.Text := self.Chat.DtCreate;
   //LLabel.Width := (AParent as TControl).Width / 2;
   LLabel.Margins.Left := 5;
   self.FLabelDate := LLabel;

   LWidthMsg := self.FLabelMsg.Canvas.TextWidth(self.FLabelMsg.Text);
   LWidthSender := self.FLabelSender.Canvas.TextWidth(self.FLabelSender.Text);

   TLogger.Save('LWidthMsg='+LWidthMsg.ToString+' LWidthSender='+LWidthSender.ToString);

   LBlockWidth := Max(LWidthMsg, LWidthSender)*1.2 + 15 + 20 + 10;

   if LBlockWidth<200 then LBlockWidth := 200;
   

   if LBlockWidth < self.FRectWrapper.Width then self.FRectWrapper.Width := LBlockWidth;


   LHeight := trunc(LWidthMsg /self.FRectWrapper.Width) + 1;
   //self.FLabelMsg.Text := '(('+(LHeight).toString+')) ' + self.FLabelMsg.Text;

   lbItem.Height := (LHeight - 1)*40 + 50 + 50;

   LastChatId := self.Chat.Id.ToInteger;

//   //слой с меткой бригады
//   LLayout := TLayout.Create(LRect);
//   LLayout.Parent := LRect;
//   LLayout.Align := TAlignLayout.Right;
//   LLayout.Height := 25;
//
//   LRect := TRectangle.Create(LLayout);
//   LRect.Parent := LLayout;
//   LRect.Stroke.Color := TAlphaColorRec.Lightgray;
//   LRect.Align := TAlignLayout.Right;
//   LRect.Width := (AParent as TControl).Width * 0.4;
//   LRect.Margins.Top := 5;
//   LRect.Margins.Bottom := 5;
//   LRect.Margins.Right := 10;
//   if self.Order.State.ToInteger<>4 then
//    LRect.Fill.Color := TAlphaColorRec.Orangered
//     else
//    LRect.Fill.Color := TAlphaColorRec.Green;
//   self.FRectState := LRect;
//
//   LLabel := TLabel.Create(LRect);
//   LLabel.Parent := LRect;
//   LLabel.Align := TAlignLayout.Center;
//   LLabel.TextSettings.Font.Size := 12;
//   LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;
//   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
//   if (self.Order.CarId<>'') and (self.Order.CarId<>'0') then
//    LLabel.Text := self.Order.Team;
//   LLabel.Width := LRect.Width - 10;
//   LLabel.Margins.Left := 5;
//   self.FLabelTeam := LLabel;

 finally
  //if Parent.InheritsFrom(TControl) then
  // (Parent as TControl).EndUpdate;
 end;

end;

procedure TChatItem.SetChat(AValue: TChat);
begin
 self.FChat := AValue;
 self.UpdateChatBlock;
end;

procedure TChatItem.UpdateChatBlock;
begin
 if not Assigned(self.FLabelMsg) then exit;

 TLogger.Save('UpdateChatBlock self.Chat.Id='+self.Chat.Id+' self.Chat.OwnerType='+self.Chat.OwnerType+' self.Chat.IsReaded='+self.Chat.IsReaded);

 if self.Chat.IsReaded.ToUpper = '1' then
  begin
   TLogger.Save('setting font self.Chat.IsReaded='+self.Chat.IsReaded);
   self.FLabelSender.TextSettings.FontColor := OWNER_COLOR[self.Chat.OwnerType.ToInteger];
   self.FLabelMsg.TextSettings.FontColor := OWNER_COLOR[self.Chat.OwnerType.ToInteger];
   TLogger.Save('setting font done');
  end;

end;

{ TOrderItems }

procedure TChatItems.Add(AParent: TFmxObject; AItem: TChatItem);
begin
  TLogger.Save('AItem.Chat.Id='+AItem.Chat.Id);
  self.FItems.AddOrSetValue(AItem.Chat.Id.ToInteger, AItem);
  if self.Caller <> View.Base.VIEW_CALLER_SILENT then
    AItem.MakeChatBlock(AParent);
end;

procedure TChatItems.Clear;
begin
  self.FItems.Clear;
end;

constructor TChatItems.Create;
begin
  self.FItems := TDictionary<integer, TChatItem>.Create;
end;

procedure TChatItems.Update(AChatId: integer; AItem: TChatItem);
var
 LItem: TChatItem;
begin

  if self.Caller <> View.Base.VIEW_CALLER_SILENT then
   AItem.UpdateChatBlock;

 exit;

 if self.FItems.TryGetValue(AChatId, LItem) then
  begin
    LItem.Chat := AItem.Chat;
    TLogger.Save('LItem.Chat.IsReaded='+LItem.Chat.IsReaded+' AItem.Chat.IsReaded='+AItem.Chat.IsReaded);
    LItem.UpdateChatBlock;
  end;

end;

end.
