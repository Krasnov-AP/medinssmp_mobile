﻿unit View.Hospitals;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, Controller.Hospital, Model.Hospital, FMX.Controls.Presentation,
  FMX.Objects, FMX.Layouts, FMX.Edit, FMX.ListBox, System.Generics.Collections,
  Model.Global, Core.Common;

type

  THospitalItem = class
   strict private
    FHospital: THospital;
    FLabelName, FLabelNumber, FLabelNormalTime: TLabel;
    //FRectState: TRectangle;
    FImageState: TImage;
    FListBoxItem: TListBoxItem;
   public
    FRectWrapper: TRectangle;
    OffsetY: integer;
   public
    property Hospital: THospital read FHospital write FHospital;
    property ListBoxItem: TListBoxItem read FListBoxItem write FListBoxItem;
   public
    procedure MakeHospitalBlock(AParent: TFmxObject);
    constructor Create;
  end;

  THospitalItems = class
   strict private
    FItems: TDictionary<integer, THospitalItem>;
   public
    property Items: TDictionary<integer, THospitalItem> read FItems write FItems;
   public
    procedure Add(AParent:TFmxObject; AItem: THospitalItem; AWithCodeOnly: boolean = false);
    procedure Clear;
    constructor Create;
  end;

  TViewHospitals = class(TViewBase)
    edHospitalFilter: TEdit;
    imgSearch: TImage;
    imgClear: TImage;
    lbHospitals: TListBox;
    imgDone: TImage;
    timDblClick: TTimer;
    timFilterByName: TTimer;
    procedure lbHospitalsItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure timFilterByNameTimer(Sender: TObject);
    procedure edHospitalFilterTyping(Sender: TObject);
  strict private
    { Private declarations }
    FHospitalItems: THospitalItems;
  strict protected
    procedure OnHospitalsSuccess2(Sender: TObject);
    procedure OnHospitalsFail2(Sender: TObject);
  public
    { Public declarations }
    procedure AfterShow; override;
    procedure RefreshData; override;
    procedure SelectHospital;
    procedure ConfirmHospitalForNewOrder;
    procedure FilterByName;
  end;

var
 bmpImageDoneHospital: TBitmap;

implementation

{$R *.fmx}

{ TSKItems }

{ THospitalItem }

constructor THospitalItem.Create;
begin
 inherited;
 self.FHospital := THospital.Create;
end;

procedure THospitalItem.MakeHospitalBlock(AParent: TFmxObject);
var
 LRect:TRectangle;
 LLine:TLine;
 LLayout:TLayout;
 LLabel:TLabel;
 LImage: TImage;
 lbITem: TListBoxItem;
begin


 try
   lbItem := TListBoxItem.Create(AParent);
   lbItem.Parent := AParent;
   lbItem.Height := 50;

   self.FListBoxItem := lbItem;

   try
    lbItem.Tag := self.Hospital.Id.ToInteger;
   except on E:Exception do
    lbItem.Tag := -1;
   end;

   LRect := TRectangle.Create(lbItem);
   LRect.Parent := lbItem;
   //LRect.Position.Y := 10000;
   LRect.Align := TAlignLayout.Client;
   LRect.Height := 80;
   LRect.Margins.Bottom := 0;
   LRect.Fill.Color := TAlphaColorRec.Black;
   LRect.HitTest := false;
   LRect.CanFocus := false;


   self.FRectWrapper := LRect;

   LLine := TLine.Create(LRect);
   LLine.Parent := LRect;
   LLine.Height := 1;
   LLine.Stroke.Color := TAlphaColorRec.Lightgray;
   LLine.Align := TAlignLayout.MostBottom;

   //слой с картинкой
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.MostLeft;
   LLayout.Width := 40;
   LLayout.Margins.Bottom := 2;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;

   LImage := TImage.Create(LLayout);
   LImage.Parent := LLayout;
   LImage.Align := TAlignLayout.Center;
   LImage.Width :=16;
   LImage.Height := 16;

   LImage.Bitmap := bmpImageDoneHospital;

     {case self.Order.State.ToInteger of
      csCancelled: LImage.Bitmap := bmpOrderCancelled;
      csInProcess: LImage.Bitmap := bmpOrderInProcess;
      csDone: LImage.Bitmap := bmpOrderDone;
      csNew: LImage.Bitmap := bmpOrderNew;
      csOnAgreement: LImage.Bitmap := bmpOnAgreement;
     end;

   self.FImageState := LImage; }

   //слой с основной меткой (Название больницы)
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Top;
   LLayout.Height := 25;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 18;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   LLabel.Text := self.Hospital.Name; // + '('+self.SKUnit.Id+')'; // + ' ('+self.Car.Id+', State='+self.Car.State+', OnDuty='+self.Car.OnDuty+' OrderId='+self.Car.OrderId+')';
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelName := LLabel;

   //слой с адресом
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Top;
   LLayout.Height := 25;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 14;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   LLabel.Text := self.Hospital.Address;
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelNumber := LLabel;

 finally
 end;

end;


{ THospitalItems }

procedure THospitalItems.Add(AParent: TFmxObject; AItem: THospitalItem;
  AWithCodeOnly: boolean);
var
 LDoAdd: boolean;
begin

  LDoAdd := true;

  if LDoAdd then
   begin
    self.FItems.Add(AItem.Hospital.Id.ToInteger, AItem);
    AItem.MakeHospitalBlock(AParent);
   end;

end;

procedure THospitalItems.Clear;
begin
 self.FItems.Clear;
end;

constructor THospitalItems.Create;
begin
 self.FItems := TDictionary<integer, THospitalItem>.Create;
end;

{ TViewHospitals }

procedure TViewHospitals.AfterShow;
begin
  inherited;

  bmpImageDoneHospital := self.imgDone.Bitmap;

  if not self.DataLoaded then
   begin
    self.FHospitalItems := THospitalItems.Create;
    self.RefreshData;
   end;

end;

procedure TViewHospitals.ConfirmHospitalForNewOrder;
 begin
 self.SendMessage('confirm_hospital_for_new_order');
 self.CloseModal;
end;

procedure TViewHospitals.edHospitalFilterTyping(Sender: TObject);
begin
  inherited;
  self.timFilterByName.Enabled := false;
  self.timFilterByName.Enabled := true;
end;

procedure TViewHospitals.FilterByName;
var
 i:integer;
 LUpperText: string;
begin

 LUpperText := self.edHospitalFilter.Text.Trim.ToUpper;

   try
    self.lbHospitals.BeginUpdate;

     for i in self.FHospitalItems.Items.Keys do
       begin

        if not (LUpperText = '')  then
         begin
          if self.FHospitalItems.Items[i].Hospital.Name.ToUpper.Contains(LUpperText) then
            self.FHospitalItems.Items[i].ListBoxItem.Height := 50
             else
            self.FHospitalItems.Items[i].ListBoxItem.Height := 0;
         end
          else
           self.FHospitalItems.Items[i].ListBoxItem.Height := 50;
       end;
   finally
    self.lbHospitals.EndUpdate;
   end;

end;

procedure TViewHospitals.lbHospitalsItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
 inherited;

  if self.timDblClick.Tag = Item.Tag then
   begin
     self.timDblClick.Enabled := false;
     self.timDblClick.Tag := -1;

     case self.Caller of
       View.Base.HOSPITALS_CALLER_NEW_ORDER: self.ConfirmHospitalForNewOrder;
       //View.Base.SKLIST_CALLER_NEW_ORDER: self.ConfirmSKUnitForNewOrder;
     end;
   end;

  self.SelectHospital;

  self.timDblClick.Tag := Item.Tag;
  self.timDblClick.Enabled := true;
end;

procedure TViewHospitals.OnHospitalsFail2(Sender: TObject);
begin
 self.HideLoader;
 self.MakeError('Не удалось загрузить список больниц');
end;

procedure TViewHospitals.OnHospitalsSuccess2(Sender: TObject);
var
 i:integer;
 s:string;
 LParent:TListBox;
 LHospitalItem: THospitalItem;
 LHospital: THospital;
 LControllerHospitals: TControllerHospitals;
begin

 try

   if not Assigned(ModelDataGlobal.Hospitals) then
     ModelDataGlobal.Hospitals := THospitals.Create;


    TLogger.Save('[TViewHospitals.OnHospitalsSuccess2] start');

    if not (Sender is TControllerHospitals) then
     begin
      TLogger.Save('[TViewHospitals.OnHospitalsSuccess2] Sender is not TControllerHospitals. Actual Sender is '+Sender.ClassName);
      exit;
     end;

    LControllerHospitals := TControllerHospitals(Sender);


    try
     TLogger.Save('[TViewHospitals.OnHospitalsSuccess2] response='+LControllerHospitals.HttpRequest.Response);
     LControllerHospitals.Hospitals.ConvertFromJson(LControllerHospitals.HttpRequest.Response);
    except on E:Exception do
     begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewHospitals.OnHospitalsSuccess2] err: '+e.message);
      exit;
     end;
    end;

   TLogger.Save('LControllerHospitals.Hospitals.Items.Count='+LControllerHospitals.Hospitals.Items.Count.ToString);

    try
     ModelDataGlobal.Hospitals.Lock;
     ModelDataGlobal.Hospitals.Items.Clear;
     self.lbHospitals.BeginUpdate;

     self.lbHospitals.Content.DeleteChildren;

     self.FHospitalItems.Clear;

     try

       LParent:= self.lbHospitals;

       for i:=0 to LControllerHospitals.Hospitals.Items.Count - 1 do
        begin

           begin
            try
             LHospitalItem := THospitalItem.Create;
             LHospitalItem.Hospital.Assign(LControllerHospitals.Hospitals.Items[i]);

             //LCarItem.Car.Lock;
              try
               self.FHospitalItems.Add(LParent, LHospitalItem);
               ModelDataGlobal.Hospitals.Items.Add(LHospitalItem.Hospital);
              finally
                //LCarItem.Car.Unlock;
              end;
            except on E:Exception do
             TLogger.Save('i='+i.ToString+' '+e.Message);
            end;
           end;

        end;

     finally
       ModelDataGlobal.Hospitals.Unlock;
       self.lbHospitals.EndUpdate;
     end;

    except on E:Exception do
     TLogger.Save('[[TViewHospitals.OnHospitalsSuccess2] 2 err: ]'+e.Message);
    end;

  finally
   self.HideLoader;
   self.FDataLoaded := true;
  end;

end;

procedure TViewHospitals.RefreshData;
var
 LControllerHospitals: TControllerHospitals;
begin

  self.ShowLoader('Загрузка списка больниц...');
  self.timCommands.Enabled := false;

  LControllerHospitals := TControllerHospitals.Create;

  LControllerHospitals.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  LControllerHospitals.OnRequestSuccess2 := self.OnHospitalsSuccess2;
  LControllerHospitals.OnRequestFail2 := self.OnHospitalsFail2;
  LControllerHospitals.RetrieveData();

end;

procedure TViewHospitals.SelectHospital;
var
 LHospitalItem: THospitalItem;
 b: boolean;
 LId: integer;
begin

 try

    //сначала снимаем выделение у предыдущего выделенного элемента
    if Assigned(ModelDataGlobal.CurrentHospital) then
     begin
      b := self.FHospitalItems.Items.TryGetValue(ModelDataGlobal.CurrentHospital.Id.ToInteger, LHospitalItem);
      if Assigned(LHospitalItem) then
       LHospitalItem.FRectWrapper.Fill.Color := TAlphaColorRec.Black
     end;

    //теперь выделяем текущий и загоняем в глобальное поле
    LId := -1;
    LId := self.lbHospitals.Selected.Tag;

     b := self.FHospitalItems.Items.TryGetValue(LId, LHospitalItem);

     if b then
      begin
       ModelDataGlobal.CurrentHospital := LHospitalItem.Hospital;
       LHospitalItem.FRectWrapper.Fill.Color := TAlphaColorRec.SaddleBrown;
      end;

 except on E:Exception do
   TLogger.Save('[TViewHospitals.SelectHospital] err: '+e.Message);
 end;

end;

procedure TViewHospitals.timFilterByNameTimer(Sender: TObject);
begin
  inherited;
  self.timFilterByName.Enabled := false;
  self.FilterByName;
end;

end.
