﻿unit View.Map;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, Core.Common,
  View.Base, FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.WebBrowser,
  Core.WebConsole, Core.Funcs, Model.Global, Model.Cars, System.StrUtils,
  Controller.Cars, Controller.AnyData, FMX.Memo.Types, FMX.ScrollBox, FMX.Memo,
  View.Filter.Cars, System.Messaging, Model.Orders, Controller.Orders;

type
  TViewMap = class(TViewBase)
   wbMap: TWebBrowser;
  const
   COMMAND_TYPES = 'WEBCONSOLE,SENDLOCATION,SETSTATE,UPDATECARS';
   MAP_PAGE = 'http://176.122.25.189/md/map/template.html?r=';
   {MAP_PAGE = 'https://yandex.ru/maps/21624/zcherbinka/?ll=37.571270%2C55.516572&'+
   'mode=routes&rtext=55.381342%2C37.363520~55.650048%2C37.724571&rtt=auto&ruri='+
   'ymapsbm1%3A%2F%2Fgeo%3Fll%3D37.364%252C55.381%26spn%3D0.017%252C0.008%26text%3D%25D0%25A0%25D0%'+
   '25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%25D0%259C%25D0%25BE%25D1%2581%25D0%25BA%25D0%'+
   '25B2%25D0%25B0%252C%2520%25D0%25BF%25D0%25BE%25D1%2581%25D0%25B5%25D0%25BB%25D0%25B5%25D0%25BD%25D0'+
   '%25B8%25D0%25B5%2520%25D0%25A9%25D0%25B0%25D0%25BF%25D0%25BE%25D0%25B2%25D1%2581%25D0%25BA%25D0%25BE%25D0'+
   '%25B5%252C%2520%25D1%2582%25D0%25B5%25D1%2580%25D1%2580%25D0%25B8%25D1%2582%25D0%25BE%25D1%2580%25D0%25B8%25D1%'+
   '258F%2520%25D0%2594%25D0%259D%25D0%259F%2520%25D0%25A1%25D0%25B0%25D1%2582%25D0%25B8%25D0%25BD%25D1%2581%25D0%'+
   '25BA%25D0%25B8%25D0%25B5%2520%25D0%259F%25D1%2580%25D1%2583%25D0%25B4%25D1%258B~ymapsbm1%3A%2F%2Fgeo%3Fll%3D37.725%'+
   '252C55.650%26spn%3D0.001%252C0.001%26text%3D%25D0%25A0%25D0%25BE%25D1%2581%25D1%2581%25D0%25B8%25D1%258F%252C%2520%2'+
   '5D0%259C%25D0%25BE%25D1%2581%25D0%25BA%25D0%25B2%25D0%25B0%252C%2520%25D1%2583%25D0%25BB%25D0%25B8%25D1%2586%25D0%25B0%2520%25D0%259C%25D0%25B0%25D1%2580%25D1%2588%25D0%25B0%25D0%25BB%25D0%25B0%2520%'+
   '25D0%2593%25D0%25BE%25D0%25BB%25D0%25BE%25D0%25B2%25D0%25B0%25D0%25BD%25D0%25BE%25D0%25B2%25D0%25B0%252C%25204%25D0%2590&'+
   'utm_source=main_stripe_big&z=11.08&R=';}
  strict protected
   procedure ProcessCommand(ACommandType, ACommand: string); override;
   procedure ProcessMessages(const Sender: TObject; const M: TMessage); override;
  strict private
   FMapLoaded:boolean;
   FWebConsole: TWebConsole;
   FControllerCars: TControllerCars;
   FChatFirstRun: boolean;
    { Private declarations }
  strict private
   procedure OnMapLoaded(Sender:TObject);
   procedure OnCarsSuccess;
   procedure OnCarsSuccessWithMapRefresh;
   procedure OnCarsFail;
   procedure OnCarsFailWithMapRefresh;
   procedure OnOrdersSuccess2(Sender: TObject);
   procedure OnOrdersFail2(Sender: TObject);
  public
    { Public declarations }
    procedure AfterShow; override;
    constructor Create(AOwner:TComponent); override;
    procedure RefreshData; override;
    procedure ShowCarsOnMap(ACarId: integer = -1);
    procedure SetLocation(ACommand: string);
    procedure SetState(ACar:TCar);
    procedure UpdateCarStates(ACarIds: string; ASilent: boolean = true);
    procedure SetCarBaloon(ACarId:string);
    procedure SetCarVisibleOnMap(ACarId: string; AVisible: boolean = false);
    procedure AddCarToMap(out ACar: TCar);
    procedure RefreshOrders(ASilent: boolean = false);
  end;

implementation

{$R *.fmx}

{ TViewMap }

procedure TViewMap.AddCarToMap(out ACar: TCar);
var
  orderId, nextOrderId, stateEx: Integer;
  partiallyFreeState: integer;
  releaseDateTime: TDateTime;
  timeArrived: string;
  fTimeArr: TDateTime;
  fIsError: integer;
  s: string;
  LOrder: TOrder;
begin
 

  fIsError := 0;

  LOrder := ModelDataGlobal.Orders.GetById(ACar.OrderId);

  if LOrder<>nil then
   begin
    try
     fTimeArr := StrToDateTime(LOrder.TimeArr);
     TLogger.Save('ACar.OrderId='+ACar.OrderId+' TimeArr='+LOrder.TimeArr);
    except on E:Exception do
     fTimeArr := 0;
    end;
   end
    else
     TLogger.Save('[TViewMap.AddCarToMap] LOrder=NIL! ACar.OrderId='+ACar.OrderId);

  if fTimeArr > 0 then
    timeArrived := FormatDateTime('mm/dd/yyyy hh:mm:ss', fTimeArr).Replace('.', '/');

  if fIsError = 1 then
    stateEx := 101
  else stateEx := 0; {
  case partiallyFreeState of
    0: stateEx := GetStateIdForHTML(CarId, State);
    1: stateEx := 102;
    2: stateEx := 103;
    3: stateEx := 104;
  end; }

  if (self.wbMap <> nil) and (self.FWebConsole <> nil) then
   begin
    s := format('addNewCar(%s, ''%s'', ''%s'', ''%s'', %d, ''%s'', ''%s'', ''%s'', %d, %d, ''%s'', ''%s'')',
                                              [ACar.Id, ACar.TypeStr, ACar.Lat + ',' + Acar.Lng, Acar.Name, ACar.StateEx, ACar.SMPName,
                                              Acar.Name, ACar.Number, ACar.IsError.ToInteger, ACar.OrderId.ToInteger, timeArrived,
                                              AnsiReplaceStr(FormatDateTime('yyyy-mm-dd hh:mm:ss', releaseDateTime), ' ', 'T')]);
    self.FWebConsole.ExecJs(s);
   end;

  ACar.IsOnMap := true;
end;

procedure TViewMap.AfterShow;
var
 LIsHospitalization: boolean;
 LAddress: string;
begin

  TLogger.Save('TViewMap.AfterShow');

  self.FCOMMAND_TYPES := self.COMMAND_TYPES;

  inherited;

  if self.DisableAfterShow then exit;
 
  if not self.FMapLoaded then
    begin
      self.ShowLoader;

      if not Assigned(self.WbMap) then
       begin
         self.WbMap := TWebBrowser.Create(self);
         self.WbMap.Parent := self.ltContent;
         self.WbMap.Align := TAlignLayOut.Client;
         //TMessageManager.DefaultManager.SubscribeToMessage(TCar, self.ProcessMessages);
         //TMessageManager.DefaultManager.SubscribeToMessage(TMessage<String>, self.ProcessMessages);
         //self.wbMap.Height := 300;
       end;

      self.wbMap.Visible := true;

      self.RefreshData;

    end
     else
    begin
     if Assigned(ModelDataGlobal.CurrentOrder) then
      begin

        self.FWebConsole.SetJsParam('PrevSelectedCar', IntToStr(0));

        if ModelDataGlobal.CurrentOrder.State.ToInteger in [4, 6] then
         self.FWebConsole.SetJsParam('ClientCoords', '')
          else
         self.FWebConsole.SetJsParam('ClientCoords', ModelDataGlobal.CurrentOrder.Coords);

        if ModelDataGlobal.CurrentOrder.Coords = ModelDataGlobal.CurrentOrder.HospitalCoords then
          begin
            LAddress := ModelDataGlobal.CurrentOrder.HospitalAddress;
            LIsHospitalization := true;
          end
           else
          begin
            LAddress := ModelDataGlobal.CurrentOrder.Address;
            LIsHospitalization := false;
          end;

          self.FWebConsole.ExecJs(format('setClientLabel(''%s'', ''%s'', ''%s'')', [LAddress, ModelDataGlobal.CurrentOrder.Coords, AnsiLowerCase(BoolToStr(LIsHospitalization, true))]));

      end;
    end;

    if not Assigned(self.FControllerCars) then
     self.FControllerCars := TControllerCars.Create;

end;

constructor TViewMap.Create(AOwner: TComponent);
begin
  inherited;
  self.FMapLoaded := false;
end;

procedure TViewMap.OnCarsFail;
begin
 //todo
 self.HideLoader;
end;

procedure TViewMap.OnCarsFailWithMapRefresh;
begin
 self.MakeError('Ошибка при обновлении карты/бригад');
 if not self.wbMap.Visible then
  self.wbMap.Visible := true;
end;

procedure TViewMap.OnCarsSuccess;
var
 i: integer;
 LCar: TCar;
begin
    try
     self.FControllerCars.Cars.ConvertFromJson(self.FControllerCars.HttpRequest.Response);
    except on E:Exception do
     begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewMap.OnCarsSuccess] err: '+e.message);
      exit;
     end;
    end;

  if not Assigned(ModelDataGlobal.Cars) then
   ModelDataGlobal.Cars := TCars.Create;

  if Assigned(ModelDataGlobal.Cars) then
   begin
     for i:=0 to self.FControllerCars.Cars.Items.Count - 1 do
      begin
       LCar := self.FControllerCars.Cars.Items[i];
       ModelDataGlobal.Cars.SetById(LCar.Id, LCar);

       if not ModelDataGlobal.AppSettings.CarsSettings.OnDutyOnly then
        LCar.VisibleOnMap := true
          else
        LCar.VisibleOnMap := LCar.OnDuty.Trim = '1';

       self.SetState(LCar);
      end;
   end;

end;

procedure TViewMap.OnCarsSuccessWithMapRefresh;
var
 i: integer;
 LCar: TCar;
begin

    try
     self.FControllerCars.Cars.ConvertFromJson(self.FControllerCars.HttpRequest.Response);
    except on E:Exception do
     begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewMap.OnCarsSuccessWithMapRefresh] err: '+e.message);
      exit;
     end;
    end;

  if not Assigned(ModelDataGlobal.Cars) then
   ModelDataGlobal.Cars := TCars.Create;

  if Assigned(ModelDataGlobal.Cars) then
   begin
     for i:=0 to self.FControllerCars.Cars.Items.Count - 1 do
      begin
       LCar := self.FControllerCars.Cars.Items[i];
       ModelDataGlobal.Cars.SetById(LCar.Id, LCar);
       //self.SetState(LCar); //карта прогрузится позже, так что здесь убираю
      end;
   end;

   randomize;
   var r := random(10000);
   self.wbMap.OnDidFinishLoad := self.OnMapLoaded;
   self.wbMap.OnDidFailLoadWithError := self.OnMapLoaded;
   self.wbMap.EnableCaching := false;
   self.wbMap.URL := SELF.MAP_PAGE+r.ToString;
   self.FMapLoaded := true;

end;

procedure TViewMap.OnMapLoaded(Sender: TObject);
begin

 self.HideLoader;

 self.wbMap.Visible := true;
 self.SendMessage('map_loaded');

 if not Assigned(Self.FWebConsole) then
       self.FWebConsole := TWebConsole.Create(self.wbMap);

end;


procedure TViewMap.OnOrdersFail2(Sender: TObject);
begin
  self.MakeError('Ошибка при полученнии списка вызовов');
end;

procedure TViewMap.OnOrdersSuccess2(Sender: TObject);
var
 i:integer;
 s:string;
 LOrder: TOrder;
 LShowStates: set of byte;
 LControllerOrders: TControllerOrders;
begin

 TLogger.Save('[TViewMap.OnOrdersSuccess2] start');

 if not (Sender is TControllerOrders) then exit;

 LControllerOrders := TControllerOrders(Sender);

 try

    try
     LControllerOrders.Orders.ConvertFromJson(LControllerOrders.HttpRequest.Response);
    except on E:Exception do
     begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewMap.OnOrdersSuccess2] err: '+e.message);
      exit;
     end;
    end;

   ModelDataGlobal.Orders.Lock;
   ModelDataGlobal.Orders.Items.Clear;

   try

     for i:=0 to LControllerOrders.Orders.Items.Count - 1 do
      begin

       LShowStates := [2, 4, 22];

       if not (LControllerOrders.Orders.Items[i].State.ToInteger in LShowStates) then
         begin

           LOrder := TOrder.Create;
           LOrder.Assign(LControllerOrders.Orders.Items[i]);
           ModelDataGlobal.Orders.Items.Add(LOrder);
           TLogger.Save('ModelDataGlobal.Orders.Items.Add(LOrder)');

         end;

      end;
     finally
       ModelDataGlobal.Orders.Unlock;
     end;

     if not Assigned(self.FControllerCars) then
      self.FControllerCars := TControllerCars.Create;

     self.FControllerCars.Host := ModelDataGlobal.AppSettings.SMPService.IP;

     self.FControllerCars.OnRequestSuccess := self.OnCarsSuccessWithMapRefresh;
     self.FControllerCars.OnRequestFail := self.OnCarsFailWithMapRefresh;
     self.FControllerCars.RetrieveData;

  finally
  end;
end;


procedure TViewMap.ProcessCommand(ACommandType, ACommand: string);
var
  i:integer;
  LCommands: TStrings;
  LCommand: string;
  LKV: TKeyValue;
   begin
      if ACommand<>'' then
         begin
           //TToast.Show(LCommand);
           LCommands := TStringList.Create;

           try

             TLogger.Save('[TViewMap.ProcessCommands] CommandType '+ACommandType+' command: '+ACommand);
             LCommands.Delimiter := '|';
             LCommands.DelimitedText := ACommand;
             //TLogger.Save()

             for i := LCommands.Count - 1 downto 0 do
               begin

                LCommand := LCommands[i];

                 if LCommand = 'LOADINGFINISHED' then
                  begin


                   if ChatIsFirstRun then
                     begin
                      self.SendMessage('chat_first_load');
                      ChatIsFirstRun := false;
                     end;

                    //при сообщении chat_first_load происходит показ формы с чатом (а то таймер на форме чата работать не будет)
                    //и мгновенное переключение обратно на вьюху с картой. Поэтому подождем немного (150 мс), а потом обработаем команду
                    TThread.CreateAnonymousThread(
                      procedure()
                        begin
                          Sleep(150);
                          TThread.Synchronize(TThread.CurrentThread,
                          procedure
                            begin

                             try
                              self.FWebConsole.SetJsParam('UseCluster', '1');
                              self.FWebConsole.SetJsParam('MapControl', '1');

                              //cars routines here
                               try
                                self.ShowCarsOnMap();
                               except on E: Exception do
                                TLogger.Save('[TViewMap.ProcessCommand] LOADINGFINISHED err: '+e.Message);
                               end;

                              self.FWebConsole.SetJsParam('Cluster', True);
                              //if Assigned(ModelDataGlobal.Cars.Items[0]) then
                              UpdateCarStates('');
                             except on E:Exception do
                              TLogger.Save(e.Message);
                             end;

                            end);
                        end).Start;

                  end
                   else
                 if LCommand = 'GETMAPCENTER' then
                  begin
                    self.FWebConsole.SetJsParam('MapCenterByCoords', '55.75322,37.622513'); //ставим пока Москву
                  end
                   else
                 if ACommandType = 'SENDLOCATION' then
                  begin
                    self.SetLocation(LCommand);
                  end
                   else
                 if ACommandType = 'SETSTATE' then
                  begin
                    self.UpdateCarStates(LCommand);
                  end
                  else
                 if ACommandType = 'UPDATECARS' then
                  begin
                    self.UpdateCarStates('');
                  end
                   else
                  if LCommand.Contains('GETCARBALOON') then
                   begin
                    //getCarBaloon&carId=123
                    LKV := Core.Funcs.ExtractKeyValue(LCommand);
                    self.SetCarBaloon(LKV.Value);
                   end
                  else
                 if LCommand.Contains('OPENFILTERDIALOG') then
                   begin
                     self.ShowFilter(TViewFilterCars);
                   end;
               end;

           finally
            LCommands.DisposeOf;
           end;
      end;
   end;

procedure TViewMap.ProcessMessages(const Sender: TObject; const M: TMessage);
begin
end;

procedure TViewMap.RefreshData;
var
 i: integer;
begin
   inherited;

   self.wbMap.Visible := false;
   self.ShowLoader();

  if Assigned(ModelDataGlobal.Cars.Items) then
    for i:=0 to ModelDataGlobal.Cars.Items.Count - 1 do
     ModelDataGlobal.Cars.Items[i].IsOnMap := false;

  self.RefreshOrders;

end;

procedure TViewMap.RefreshOrders(ASilent: boolean = false);
var
 LControllerOrders: TControllerOrders;
begin

 if not ASilent then
  self.ShowLoader;

  LControllerOrders := TControllerOrders.Create;

  LControllerOrders.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  LControllerOrders.OnRequestSuccess2 := self.OnOrdersSuccess2;
  LControllerOrders.OnRequestFail2 := self.OnOrdersFail2;
  LControllerOrders.RetrieveData();

end;

procedure TViewMap.SetCarBaloon(ACarId: string);
var
 LOrderId, s: string;
 LFlag: boolean;
begin
  //todo
  LOrderId := ModelDataGlobal.CurrentOrder.Id;
  LFlag := (ModelDataGlobal.CurrentOrder.SKOrderId.ToInteger > 0) and (ModelDataGlobal.CurrentOrder.State = '0') and (ModelDataGlobal.CurrentOrder.MainUserId.ToInteger <= 0);
  s := format('setCarBaloon(%s, ''%s'', %d, %s)', [ACarId, ModelDataGlobal.CurrentOrder.Coords, LOrderId.ToInteger(), AnsiLowerCase(BoolToStr(LFlag, true))]);
  self.FWebConsole.ExecJs(s);
end;

procedure TViewMap.SetCarVisibleOnMap(ACarId: string; AVisible: boolean);
var
 LCar: TCar;
begin
 if not AVisible then
  self.FWebConsole.ExecJs(format('setCarVisible(%s, ''%s'')', [ACarId, 'hideCar']))
   else
   begin
    LCar := ModelDataGlobal.Cars.GetById(ACarId);
    if LCar<>nil then
     if not LCar.IsOnMap then
      self.AddCarToMap(LCar);

      self.FWebConsole.ExecJs(format('setCarVisible(%s, ''%s'')', [ACarId, '']));
   end;
end;

procedure TViewMap.SetLocation(ACommand: string); //ACommand=carId,lat,lng
var
 ts: TStrings;
 LCar: TCar;
 Llat, Llng, LCarId: string;
begin
 //todo

  ts := TStringList.Create;

  try
    ts.Delimiter := ',';
    ts.DelimitedText := ACommand;
    if ts.Count>=3 then
     begin
       LCarId := ts[0];
       LLat := ts[1];
       LLng := ts[2];

       LCar := ModelDataGlobal.Cars.GetById(LCarId);
       if LCar <> nil then
        begin
          LCar.Lat := LLat;
          LCar.Lng := LLng;
          self.FWebConsole.ExecJs(format('addNewCarWayPoint(%s, ''%s'')', [LCarId, Llat + ',' + Llng]));
        end;
     end;
  finally
    ts.DisposeOf;
  end;
end;

procedure TViewMap.SetState(ACar: TCar);
var
 LCar: TCar;
 s: string;
begin

  s := '';
  if ACar.ReleaseDateTime.Trim <> '' then
   begin
     try
       s := AnsiReplaceStr(FormatDateTime('yyyy-mm-dd hh:mm:ss', StrToDateTime(ACar.ReleaseDateTime)), ' ', 'T');
     except on E:Exception do
       s := '';
     end;
   end;

  self.FWebConsole.ExecJs(format('setCarState(%s, %d, %s, ''%s'')', [ACar.Id, ACar.stateEx, ACar.OrderId, s]));
  self.FWebConsole.SetJsParam('Cluster', True);

end;

procedure TViewMap.ShowCarsOnMap;
var
 i:integer;
 LCar: TCar;
 LVisible: boolean;
begin
 if Assigned(ModelDataGlobal.Cars.Items) then
   begin

    for i := 0 to ModelDataGlobal.Cars.Items.Count - 1 do
     begin

      LCar := ModelDataGlobal.Cars.Items[i];

      if not LCar.IsOnMap then
       begin
        if not ModelDataGlobal.AppSettings.CarsSettings.OnDutyOnly then
         LVisible := true
          else
         LVisible := LCar.OnDuty.Trim = '1';
        if LVisible then
         self.AddCarToMap(LCar);
       end
        else
       begin

       end;

     end;

   end;
end;

procedure TViewMap.UpdateCarStates(ACarIds: string; ASilent: boolean = true);
var
 ts: TStrings;
begin

  self.FControllerCars.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  ts := TStringList.Create;
  if ACarIds<>'' then
    begin
     ts := TStringList.Create;
     ts.Text := ACarIds.Replace('|', ',');
     ts.Text := 'and "Id" in ('+ts.Text+')';
    end
     else
      ts.Text := '';

  self.FControllerCars.OnRequestSuccess := self.OnCarsSuccess;
  self.FControllerCars.OnRequestFail := self.OnCarsFail;
  self.FControllerCars.RetrieveData(rmGET, ts);
end;

end.
