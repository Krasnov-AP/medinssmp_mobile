﻿unit View.NewOrder;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.TabControl,
  Helper.View.Input, FMX.ListBox, FMX.Edit, FMX.DateTimeCtrls, FMX.Memo.Types,
  FMX.ScrollBox, FMX.Memo, FMX.EditBox, FMX.NumberBox, View.Scrollable, Model.Orders,
  Controller.Orders, Model.Global, Core.Common, Core.Funcs, Controller.Cars,
  System.Messaging;

type
  TViewNewOrder = class(TViewScrollable)
    ltAdr: TLayout;
    lbAdr: TLabel;
    edAddressAutocomplete: TEdit;
    ltAdrDetail: TLayout;
    lbAdrDetail: TLabel;
    edAdrDetail: TEdit;
    ltBirthday: TLayout;
    GridPanelLayout1: TGridPanelLayout;
    lbBirthDay: TLabel;
    lbAge: TLabel;
    dtBirthday: TDateEdit;
    edAge: TEdit;
    ltCallType: TLayout;
    cbCallType: TComboBox;
    ltContacts: TLayout;
    lbContacts: TLabel;
    ltFIO: TLayout;
    lbFIO: TLabel;
    edFIO: TEdit;
    ltIndications: TLayout;
    lbIndications: TLabel;
    moIndications: TMemo;
    ltMKADRean: TLayout;
    GridPanelLayout2: TGridPanelLayout;
    lbMKADDistance: TLabel;
    edMKADDistance: TNumberBox;
    cbRean: TCheckBox;
    ltPayType: TLayout;
    lbPayType: TLabel;
    cbPayType: TComboBox;
    ltPolisNum: TLayout;
    lbPolisNum: TLabel;
    edPolisNum: TEdit;
    ltTransportation: TLayout;
    Layout1: TLayout;
    lbTransportationTo: TLabel;
    edTransportationTo: TEdit;
    SearchEditButton2: TSearchEditButton;
    Layout3: TLayout;
    Label2: TLabel;
    edTransportationAdr: TEdit;
    Layout2: TLayout;
    Label1: TLabel;
    cbTeam: TComboBox;
    edContacts: TMemo;
    procedure cbCallTypeChange(Sender: TObject);
    procedure cbTeamMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  strict protected
    procedure ProcessMessages(const Sender: TObject; const M: TMessage); override;
  private
    FControllerOrder: TControllerOrder;
    { Private declarations }
  public
    { Public declarations }
    procedure AfterShow; override;
    procedure SaveOrder;
    procedure OnOrderSuccess;
    procedure OnOrderFail;
  end;

implementation

{$R *.fmx}

{ TViewNewCall }

procedure TViewNewOrder.AfterShow;
begin
  if not Assigned(self.FControllerOrder) then
   self.FControllerOrder := TControllerOrder.Create;
  inherited;
end;


procedure TViewNewOrder.cbCallTypeChange(Sender: TObject);
begin
  inherited;
  self.ltTransportation.Visible := self.cbCallType.ItemIndex = 1;

end;

procedure TViewNewOrder.cbTeamMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  inherited;
//  self.ShowLoader();
//  TThread.CreateAnonymousThread(
//            procedure()
//            begin
//                Sleep(3000);
//                TThread.Synchronize(TThread.CurrentThread,
//                procedure
//                begin
//                  TToast.Show('Ошибка загрузки списка бригад err: 10061 Connection Timeout');
//                  self.HideLoader;
//                end);
//            end).Start;
end;

procedure TViewNewOrder.OnOrderFail;
begin
  self.HideLoader;
end;

procedure TViewNewOrder.OnOrderSuccess;
begin
  self.HideLoader;
  TToast.Show('Вызов успешно сохранен');
  self.CloseModal;
end;

procedure TViewNewOrder.ProcessMessages(const Sender: TObject;
  const M: TMessage);
var
 LMsg: string;
begin

  inherited;

  exit;

  if not self.FNeedToProcessMessages then exit;

  LMsg := (M as TMessage<String>).Value;

  if Sender is THelperViewInput then
   begin
    //TToast.Show(LMsg+' '+Sender.ClassName+ ' '+ KeyboardHelper.KeyboardBounds.Height.ToString);
    if LMsg = 'keyboard_shown' then
     begin
      //self.ltFooter.Height := KeyboardHelper.KeyboardBounds.Height + 75;
      if self.FCurrentPoint.Y > KeyboardHelper.KeyboardBounds.Height + 75 then
       self.ltContent.Margins.Top := - (KeyboardHelper.KeyboardBounds.Height + 75);

     end
      else
    if LMsg = 'keyboard_hidden' then
     begin
      //self.ltFooter.Height := 55;
      self.ltContent.Margins.Top := 0;
     end
   end;
end;

procedure TViewNewOrder.SaveOrder;
var
 O:TOrder;
begin
 //todo
  self.ShowLoader('Сохранение заявки...');

  o := TOrder.Create;
  o.Name := self.edFIO.Text;
  o.Birthday := self.dtBirthday.Text;
  o.Address := self.edAddressAutocomplete.Text;
  o.Chat := '%current_time% - Заявка создана ('+ModelDataGlobal.CurrentUser.PersonalInfo.FirstName+' '+ModelDataGlobal.CurrentUser.PersonalInfo.LastName+')';
  o.SMPUserFIO := ModelDataGlobal.CurrentUser.PersonalInfo.FirstName+' '+ModelDataGlobal.CurrentUser.PersonalInfo.LastName;
  o.Coords := '55.75322,37.622513';
  o.Contacts := self.edContacts.Text;

  if not Assigned(self.FControllerOrder) then
    self.FControllerOrder := TControllerOrder.Create;

  self.FControllerOrder.Order := O;

  self.FControllerOrder.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  self.FControllerOrder.OnRequestSuccess := self.OnOrderSuccess;
  self.FControllerOrder.OnRequestFail := self.OnOrderFail;
  self.FControllerOrder.SaveOrder(1050);
end;

end.
