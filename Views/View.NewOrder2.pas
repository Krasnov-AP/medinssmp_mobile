﻿unit View.NewOrder2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Adaptive, FMX.ListBox, FMX.Controls.Presentation, FMX.Objects,
  FMX.Layouts, FMX.TabControl, FMX.Edit, FMX.DateTimeCtrls, Helper.Yandex.Adr.Autocomplete,
  FMX.Memo.Types, FMX.ScrollBox, FMX.Memo, FMX.EditBox, FMX.NumberBox, System.DateUtils;

type
  TViewNewOrder2 = class(TViewAdaptive)
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    ListBoxItem4: TListBoxItem;
    ListBoxItem5: TListBoxItem;
    ListBoxItem6: TListBoxItem;
    ListBoxItem7: TListBoxItem;
    cbCallType: TComboBox;
    lbPayType: TLabel;
    cbPayType: TComboBox;
    edFIO: TEdit;
    lbFIO: TLabel;
    GridPanelLayout1: TGridPanelLayout;
    lbBirthDay: TLabel;
    lbAge: TLabel;
    dtBirthday: TDateEdit;
    edAge: TEdit;
    edPolisNum: TEdit;
    lbPolisNum: TLabel;
    edAddressAutocomplete: TEdit;
    lbAdr: TLabel;
    lbAdrDetail: TLabel;
    edAdrDetail: TEdit;
    ListBoxItem8: TListBoxItem;
    ListBoxItem9: TListBoxItem;
    ListBoxItem10: TListBoxItem;
    edContacts: TMemo;
    lbContacts: TLabel;
    moIndications: TMemo;
    lbIndications: TLabel;
    GridPanelLayout2: TGridPanelLayout;
    lbMKADDistance: TLabel;
    edMKADDistance: TNumberBox;
    cbRean: TCheckBox;
    ListBoxItem11: TListBoxItem;
    ListBoxItem12: TListBoxItem;
    cbTeam: TComboBox;
    Label1: TLabel;
    btnSendMessage: TButton;
    Image1: TImage;
    procedure dtBirthdayChange(Sender: TObject);
  private
    { Private declarations }
  public
    procedure AfterShow; override;
    { Public declarations }
  end;

implementation

{$R *.fmx}

{ TViewNewOrder2 }

procedure TViewNewOrder2.AfterShow;
var
 i:integer;
begin
  YandexAdrAutocomplete.Control := self.edAddressAutocomplete;
  self.isAdaptive := false;
  inherited;
end;

procedure TViewNewOrder2.dtBirthdayChange(Sender: TObject);
var
 LDT: TDateTime;
 diff: integer;
begin
  inherited;
  LDT := self.dtBirthday.Date;
  diff := trunc(DaysBetween(now, LDT) / 365);
  self.edAge.Text := diff.ToString;
end;

end.
