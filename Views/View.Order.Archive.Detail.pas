﻿unit View.Order.Archive.Detail;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, View.Base,
  FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.TabControl,
  FMX.ListBox, FMX.Edit, FMX.DateTimeCtrls, Model.Global;

type
  TViewOrderArchiveDetail = class(TViewBase)
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    TabItem3: TTabItem;
    TabItem4: TTabItem;
    TabItem5: TTabItem;
    Layout1: TLayout;
    lbStatus: TLabel;
    cbStatus: TComboBox;
    Layout2: TLayout;
    Label1: TLabel;
    edNum: TEdit;
    Layout3: TLayout;
    Lfnf: TLabel;
    Layout4: TLayout;
    Label3: TLabel;
    edFIO: TEdit;
    edBirthday: TDateEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Layout5: TLayout;
    Label4: TLabel;
    edAddress: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AfterShow; override;
  end;

implementation

{$R *.fmx}

{ TViewOrderArchiveDetail }

procedure TViewOrderArchiveDetail.AfterShow;
begin
  inherited;
  self.edNum.Text := ModelDataGlobal.CurrentOrderArchive.MonthlyId;
  self.edFIO.Text := ModelDataGlobal.CurrentOrderArchive.Name;
  self.edBirthday.Text := ModelDataGlobal.CurrentOrderArchive.BirthDay;
  self.edAddress.Text := ModelDataGlobal.CurrentOrderArchive.Address;
end;

end.
