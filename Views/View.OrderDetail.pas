﻿unit View.OrderDetail;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, View.Base,
  FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, Model.Global, Model.Cars,
  Model.Orders, Controller.Orders, Core.Common, Model.ViewParams, Controller.Cars,
  Model.AnyData, Controller.AnyData, Core.Crypt, FMX.MultiView, View.Cars,
  View.Chat, View.OrderLog, FMX.TabControl, View.Survey, Controller.Survey,
  System.Messaging;

type
  TViewOrderDetail = class(TViewBase)
    btnConfirmOrder: TButton;
    btnConfirmCar: TButton;
    GridPanelLayout1: TGridPanelLayout;
    Circle1: TCircle;
    Circle2: TCircle;
    Label1: TLabel;
    Label2: TLabel;
    imgMap: TImage;
    imgOrderDetail: TImage;
    Label3: TLabel;
    lbFIO: TLabel;
    Label4: TLabel;
    lbAddress: TLabel;
    Label5: TLabel;
    btnCancelOrder: TButton;
    btnCancelCar: TButton;
    Layout1: TLayout;
    Circle3: TCircle;
    Image1: TImage;
    Circle4: TCircle;
    Image2: TImage;
    Label6: TLabel;
    Label7: TLabel;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    GridPanelLayout2: TGridPanelLayout;
    Circle5: TCircle;
    Image3: TImage;
    Circle6: TCircle;
    Image4: TImage;
    Label8: TLabel;
    Label9: TLabel;
    Layout2: TLayout;
    Label10: TLabel;
    Label11: TLabel;
    lbAddress2: TLabel;
    lbFIO2: TLabel;
    Label14: TLabel;
    lbBirthday2: TLabel;
    lbContacts2: TLabel;
    lbCustomer2: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label12: TLabel;
    lbDescription2: TLabel;
    Label13: TLabel;
    lbComment2: TLabel;
    procedure btnConfirmOrderClick(Sender: TObject);
    procedure btnConfirmCarClick(Sender: TObject);
    procedure btnCancelCarClick(Sender: TObject);
    procedure Circle3Click(Sender: TObject);
    procedure Circle4Click(Sender: TObject);
    procedure Circle6Click(Sender: TObject);
    procedure Circle5Click(Sender: TObject);
  strict protected
   procedure ProcessMessages(const Sender: TObject; const M: TMessage); override;
  strict private
   FControllerOrder: TControllerOrder;
   FControllerCars: TControllerCars;
  private
    { Private declarations }
    procedure OnOrderUpdateSuccess;
    procedure OnOrderUpdateFail;
    procedure OnCarUpdateSuccess;
    procedure OnCarUpdateFail;
    procedure OnOrderAcceptSuccess2(Sender: TObject);
    procedure OnOrderAcceptFail2(Sender: TObject);
    procedure OnSurveySuccess2(Sender: TObject);
    procedure OnSurveyFail2(Sender: TObject);
    procedure ProcessMsgDlg(const AResult: TModalResult);

    procedure ShowCars;
    procedure SetSurveyState(AState: string; AValue: string = '');
    procedure CancelSurvey;
    procedure ConfirmSurvey;
  public
    { Public declarations }
    procedure AfterShow; override;
    procedure GetOrderToWork;
    procedure UpdateCar(ACommand: string);
    procedure ConfirmCar;
    procedure CancelCar;
    procedure AcceptOrder;

  end;

implementation

{$R *.fmx}

{ TViewOrderDetail }

procedure TViewOrderDetail.AcceptOrder;
var
 LControllerOrder: TControllerOrder;
 LOrder: TOrder;
begin
  if ModelDataGlobal.CurrentOrder.SKCode <> '' then
   if ( (ModelDataGlobal.CurrentOrder.ActionStateSMP = '0') or (ModelDataGlobal.CurrentOrder.ActionStateSMP.Trim = '') ) then
    if ( (ModelDataGlobal.CurrentOrder.SKOrderId <> '0') and (ModelDataGlobal.CurrentOrder.SKOrderId.Trim <> '') ) then
     begin
      LControllerOrder := TControllerOrder.Create;

      self.ShowLoader('Обновление данных...');

      LOrder := ModelDataGlobal.CurrentOrder;
      LOrder.ActionStateSMP := '1';

      LOrder.Chat := Format('%%current_time%% - Принята диспетчером', []) + sLineBreak + LOrder.Chat;

      LControllerOrder.Order := LOrder;

      LControllerOrder.Host := ModelDataGlobal.AppSettings.SMPService.IP;

      LControllerOrder.OnRequestSuccess2 := self.OnOrderAcceptSuccess2;
      LControllerOrder.OnRequestFail2 := self.OnOrderAcceptFail2;
      LControllerOrder.UpdateOrder(LOrder.Id.ToInteger);

     end;
end;

procedure TViewOrderDetail.AfterShow;
begin
  inherited;

  self.lbFIO.Text := '';
  self.lbAddress.Text := '';

  self.btnConfirmOrder.Enabled := false;
  self.btnConfirmCar.Enabled := false;
  self.btnCancelOrder.Enabled := true;
  self.btnCancelCar.Enabled := false;

  if Assigned(ModelDataGlobal.CurrentOrder) then
   begin
     self.lbFIO.Text := ModelDataGlobal.CurrentOrder.Name;
     self.lbAddress.Text := ModelDataGlobal.CurrentOrder.Address+ ' '+ModelDataGlobal.CurrentOrder.Address2;

     self.lbFIO2.Text := self.lbFIO.Text;
     self.lbAddress2.Text := self.lbAddress.Text;
     self.lbCustomer2.Text := ModelDataGlobal.CurrentOrder.SKName;
     self.lbDescription2.Text := ModelDataGlobal.CurrentOrder.Description;
     self.lbBirthday2.Text := ModelDataGlobal.CurrentOrder.BirthDay;
     self.lbContacts2.Text := ModelDataGlobal.CurrentOrder.Contacts;
     self.lbComment2.Text := ModelDataGlobal.CurrentOrder.OrderNote;

     if ModelDataGlobal.CurrentOrder.SMPName = '' then
      self.btnConfirmOrder.Enabled := true
       else
        begin
         if (ModelDataGlobal.CurrentOrder.Team = '') or (ModelDataGlobal.CurrentOrder.State = '0') then
           self.btnConfirmCar.Enabled := true
            else
           self.btnCancelCar.Enabled := true;
        end;

        if self.Caller = View.Base.ORDER_DETAIL_CALLER_SURVEY then
         self.TabControl1.ActiveTab := self.TabItem2
          else
         self.TabControl1.ActiveTab := self.TabItem1;

   end;

   if not Assigned(self.FControllerOrder) then
    self.FControllerOrder := TControllerOrder.Create;

  self.AcceptOrder; //для заявок от СК мы должны подтвердить, что приняли. А то пиликать будет без конца....

end;

procedure TViewOrderDetail.OnCarUpdateFail;
begin
 self.MakeError('Ошибка. Бригада не назначена');
end;

procedure TViewOrderDetail.OnCarUpdateSuccess;
var
 s: string;
begin
 self.HideLoader;

   try
     //self.ControllerCars.Cars.ConvertFromJson(self.ControllerCars.HttpRequest.Response);
     s := self.FControllerCars.HttpRequest.Response;

     if s.Trim = '' then
      begin
        self.MakeError('Сервер вернул пустой ответ. Возможно, ошибка!');
        exit;
      end;


     s := TCrypter.Decrypt(s);
     if s.Contains('=ok') then
      begin
        TToast.Show('Операция выполнена успешно');
        self.CloseModal;
      end;

    except on E:Exception do
     begin
      self.MakeError('Ошибка! '+e.Message);
      TLogger.Save('[TViewOrderDetail.OnCarSuccess] err: '+e.message);
      exit;
     end;
    end;

end;

procedure TViewOrderDetail.OnOrderAcceptFail2(Sender: TObject);
begin
 //todo
 self.HideLoader;
 self.MakeError('Не удалось обновить статус заявки:(');
end;

procedure TViewOrderDetail.OnOrderAcceptSuccess2(Sender: TObject);
begin
 self.HideLoader;
end;

procedure TViewOrderDetail.OnOrderUpdateFail;
begin
 //todo
 self.HideLoader;
 self.MakeError('Ошибка! Не удалось обновить вызов.');
end;

procedure TViewOrderDetail.OnOrderUpdateSuccess;
begin
 self.HideLoader;
 TToast.Show('Вызов успешно сохранен/изменен');
 self.CloseModal;
 sleep(10);
 self.SendMessage('refresh_orders');
end;

procedure TViewOrderDetail.OnSurveyFail2(Sender: TObject);
begin
 self.HideLoader;
 self.MakeError('Ошибка...');
end;

procedure TViewOrderDetail.OnSurveySuccess2(Sender: TObject);
var
 LControllerSurvey: TControllerSurvey;
 s: string;
begin
  self.HideLoader;

  if not (Sender is TControllerSurvey) then exit;

  try
    LControllerSurvey := TControllerSurvey(Sender);

     try
       s := LControllerSurvey.HttpRequest.Response;

       if s.Trim = '' then
        begin
          self.MakeError('Сервер вернул пустой ответ. Возможно, ошибка!');
          exit;
        end;

       s := TCrypter.Decrypt(s);
       if s.Contains('=ok') then
        begin
          TToast.Show('Операция выполнена успешно');
          self.CloseModal;
        end;

      except on E:Exception do
       begin
        self.MakeError('Ошибка! '+e.Message);
        TLogger.Save('[TViewOrderDetail.OnSurveyCancelSuccess2] err: '+e.message);
        exit;
       end;
      end;

  finally
   SafeFree(LControllerSurvey);
  end;

end;

procedure TViewOrderDetail.ProcessMessages(const Sender: TObject;
  const M: TMessage);
var
 LMsg: string;
begin
  inherited;

  if not self.FNeedToProcessMessages then exit;

  LMsg := (M as TMessage<String>).Value;

  if Sender is TViewSurvey then
   if LMsg = 'confirm_survey' then
    self.ConfirmSurvey;

end;

procedure TViewOrderDetail.ProcessMsgDlg(const AResult: TModalResult);
var
 LControllerSurvey: TControllerSurvey;
begin
 self.CancelSurvey;
end;

procedure TViewOrderDetail.SetSurveyState(AState: string; AValue: string = '');
var
 LControllerSurvey: TControllerSurvey;
 ts: TStrings;
begin

 LControllerSurvey := TControllerSurvey.Create;
 ts := TStringList.Create;

 try

   try

     LControllerSurvey.Host := ModelDataGlobal.AppSettings.SMPService.IP;

     ts.Add('orderId='+ModelDataGlobal.CurrentOrder.Id);
     ts.Add('state='+AState);
     ts.Add('value='+AValue);

     LControllerSurvey.OnRequestSuccess2 := self.OnSurveySuccess2;
     LControllerSurvey.OnRequestFail2 := self.OnSurveyFail2;
     LControllerSurvey.RetrieveData(rmGet, ts);

   except on E:Exception do
    begin
     self.MakeError(e.Message);
     SafeFree(LControllerSurvey);
    end;
   end;
 finally
   ts.DisposeOf;
 end;

end;

procedure TViewOrderDetail.ShowCars;
var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Бригады');
  LViewParams.ViewClass := 'TViewCars';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abCancel];
  LViewParams.MainMenuVisible := true;
  LViewParams.Caller := View.Base.VIEWCARS_CALLER_CONFIRM_CAR;
  self.ShowView(LViewParams);
end;

procedure TViewOrderDetail.UpdateCar(ACommand: string);
var
 ts: TStrings;
begin
  //self.ShowLoader('Сохранение данных');

  if not Assigned(Self.FControllerCars) then
   self.FControllerCars := TControllerCars.Create;

  self.FControllerCars.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  ts := TStringList.Create;
  ts.Add('command='+ACommand);
  ts.Add('connectionUserId='+ModelDataGlobal.CurrentUser.id.ToString);
  ts.Add('userLogin='+ModelDataGlobal.CurrentUser.Name);
  ts.Add('carId='+ModelDataGlobal.CurrentCar.Id);
  ts.Add('orderId='+ModelDataGlobal.CurrentOrder.Id);

  try
   self.FControllerCars.OnRequestSuccess := self.OnCarUpdateSuccess;
   self.FControllerCars.OnRequestFail := self.OnCarUpdateFail;
   self.FControllerCars.RetrieveData(rmGet, ts);
  finally
   ts.Free;
  end;
end;

procedure TViewOrderDetail.btnCancelCarClick(Sender: TObject);
begin
  inherited;
  TToast.Show('Пока не реализовано..');
end;

procedure TViewOrderDetail.btnConfirmCarClick(Sender: TObject);
begin
  inherited;
  self.ShowCars;
end;

procedure TViewOrderDetail.btnConfirmOrderClick(Sender: TObject);
begin
  inherited;
  self.GetOrderToWork;
end;

procedure TViewOrderDetail.CancelCar;
begin
  self.UpdateCar('cancelCar');
end;

procedure TViewOrderDetail.CancelSurvey;
begin
 self.SetSurveyState('2');
end;

procedure TViewOrderDetail.Circle3Click(Sender: TObject);
var
 LViewParams: TViewParams;
begin
  inherited;

  self.ShowFilter(TViewChat);
  exit;

  LViewParams := TViewParams.Create;
  //LViewParams.Title := TLangManager.Translate('Архив');
  LViewParams.ViewClass := 'TViewChat';
  LViewParams.IsModal := false;
  LViewParams.ActionButtons := [];
  LViewParams.MainMenuVisible := true;
  //LViewParams.Parent := 'pnlPopupView';
  self.ShowView(LViewParams);
end;

procedure TViewOrderDetail.Circle4Click(Sender: TObject);
begin
  inherited;

  self.ShowFilter(TViewOrderLog);
  exit;
end;

procedure TViewOrderDetail.Circle5Click(Sender: TObject);
begin
  inherited;
   MessageDlg('Действительно хотите отказать?', TMsgDlgType.mtInformation, [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], 0, ProcessMsgDlg);
end;

procedure TViewOrderDetail.Circle6Click(Sender: TObject);
begin
  inherited;
  self.ShowFilter(TViewSurvey);
end;

procedure TViewOrderDetail.ConfirmCar;
begin
 self.UpdateCar('confirmCar');
end;

procedure TViewOrderDetail.ConfirmSurvey;
begin
  self.SetSurveyState('2', ModelDataGlobal.SurveyValue.ToString);
end;

procedure TViewOrderDetail.GetOrderToWork;
var
 LOrder: TOrder;
begin

   self.ShowLoader('Сохранение данных');

   LOrder := ModelDataGlobal.CurrentOrder;

   LOrder.IsPlan := 'False';
   LOrder.MainUserId := ModelDataGlobal.CurrentUser.Id.ToString;
   LOrder.SMPUserFIO := ModelDataGlobal.CurrentUser.PersonalInfo.FullName;
   LOrder.SMPUserName := ModelDataGlobal.CurrentUser.Name;
   LOrder.ActionStateSMP := '2';
   LOrder.Chat := Format('%%current_time%% - Взята в работу', []) + sLineBreak + LOrder.Chat;
   LOrder.TimeCall := '';
   LOrder.SKUserName := ModelDataGlobal.CurrentUser.PersonalInfo.FullName;
   LOrder.AccountableUserId := LOrder.MainUserId;
   LOrder.State := '0';
   LOrder.SMPName := ModelDataGlobal.AppSettings.SMPService.Name;

   if not Assigned(self.FControllerOrder) then
      self.FControllerOrder := TControllerOrder.Create;

    self.FControllerOrder.Order := LOrder;

    self.FControllerOrder.Host := ModelDataGlobal.AppSettings.SMPService.IP;

    self.FControllerOrder.OnRequestSuccess := self.OnOrderUpdateSuccess;
    self.FControllerOrder.OnRequestFail := self.OnOrderUpdateFail;
    self.FControllerOrder.UpdateOrder(LOrder.Id.ToInteger);

end;

end.
