﻿unit View.OrderLog;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, FMX.Controls.Presentation, FMX.Objects, FMX.Layouts,
  FMX.Memo.Types, FMX.ScrollBox, FMX.Memo, Model.Global, System.Messaging,
  Core.Common;

type
  TViewOrderLog = class(TViewBase)
    Label1: TLabel;
    moLog: TMemo;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
  strict protected
   procedure ProcessMessages(const Sender: TObject; const M: TMessage); override;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AfterShow; override;
    procedure refreshData; override;
  end;

implementation

{$R *.fmx}

procedure TViewOrderLog.AfterShow;
begin
 inherited;
 self.RefreshData;
end;

procedure TViewOrderLog.Button2Click(Sender: TObject);
begin
  inherited;
  self.HideFilter;
end;

procedure TViewOrderLog.ProcessMessages(const Sender: TObject;
  const M: TMessage);
var
 LMsg: string;
begin
  inherited;

  if not self.FNeedToProcessMessages then exit;

  LMsg := (M as TMessage<String>).Value;

  if LMsg = 'update_order_log' then
   begin
    {$ifdef ANDROID}
     //TToast.Show('update_order_log recieved');
    {$endif}
    if self.Visible then self.RefreshData;
   end;

end;

procedure TViewOrderLog.refreshData;
begin
  if Assigned(ModelDataGlobal.CurrentOrder) then
   begin

      TThread.CreateAnonymousThread(
            procedure()
              begin
                Sleep(250);
                TThread.Synchronize(TThread.CurrentThread,
                procedure
                  begin
                    self.moLog.BeginUpdate;
                    self.moLog.Lines.Text := ModelDataGlobal.CurrentOrder.Chat;
                    self.moLog.EndUpdate;
                  end);
              end).Start;

   end;
end;

end.
