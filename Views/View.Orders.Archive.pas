﻿unit View.Orders.Archive;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.ListBox,
  View.Orders, Controller.Orders.Archive, Model.LangConsts, Model.Orders,
  Core.Common, Model.Global, Controller.AnyData, Model.ViewParams;

type

  TViewOrdersArchive = class(TViewBase)
    lbArchive: TListBox;
    timDblClick: TTimer;
    procedure lbArchiveItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
  strict private
   FOrderItems: TOrderItems;
  strict private
   procedure OnOrdersSuccess2(Sender: TObject);
   procedure OnOrdersFail2(Sender: TObject);
  public
   procedure RefreshOrders(ASilent: boolean = false);
   procedure RefreshData; override;
   procedure AfterShow; override;
   procedure ShowOrderDetail;
   procedure SelectOrder;
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TViewOrdersArchive.AfterShow;
begin
 inherited;
 if not Assigned(self.FOrderItems) then
  self.FOrderItems := TOrderItems.Create;
 self.RefreshOrders();
end;

procedure TViewOrdersArchive.lbArchiveItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
 var
 LOrderItem: TOrderItem;
begin
  inherited;

  if self.timDblClick.Tag = Item.Tag then
   begin
     self.timDblClick.Enabled := false;
     self.timDblClick.Tag := -1;
     self.ShowOrderDetail;
   end;

  self.SelectOrder;

  self.timDblClick.Tag := Item.Tag;
  self.timDblClick.Enabled := true;
  //TToast.Show(Item.Tag.ToString);
end;


procedure TViewOrdersArchive.OnOrdersFail2(Sender: TObject);
begin
 try
   self.HideLoader;
   self.MakeError(Model.LangConsts.LANG_SOMETHING_WENT_WRONG);
 finally
   if Sender is TControllerOrdersArchive then
    TControllerOrdersArchive(Sender).Destroy;
 end;
end;

procedure TViewOrdersArchive.OnOrdersSuccess2(Sender: TObject);
var
 i:integer;
 s:string;
 LParent:TListBox;
 LOrderItem: TOrderItem;
 LOrder: TOrder;
 LShowStates: set of byte;
 LControllerOrders: TControllerOrdersArchive;
 LDoShow: boolean;
begin

 TLogger.Save('[TViewOrdersArchive.OnOrdersSuccess2] start');

 if not (Sender is TControllerOrdersArchive) then exit;

 LControllerOrders := TControllerOrdersArchive(Sender);

 try

    try
     LControllerOrders.Orders.ConvertFromJson(LControllerOrders.HttpRequest.Response);
    except on E:Exception do
     begin
      self.MakeError('Нет данных для отображения');
      TLogger.Save('[TViewOrdersArchive.OnOrdersSuccess2] err: '+e.message);
      exit;
     end;
    end;

   self.lbArchive.BeginUpdate;
   self.lbArchive.Content.DeleteChildren;

   self.FOrderItems.Clear;
   ModelDataGlobal.OrdersArchive.Lock;
   ModelDataGlobal.OrdersArchive.Items.Clear;

   try

     for i:=0 to LControllerOrders.Orders.Items.Count - 1 do
      begin

       LDoShow := true;
       if ModelDataGlobal.AppSettings.FilterArchive.Active then
         begin
          if ModelDataGlobal.AppSettings.FilterArchive.State <> -1 then
            begin
              LShowStates := [ModelDataGlobal.AppSettings.FilterArchive.State];
              LDoShow := LControllerOrders.Orders.Items[i].State.ToInteger in LShowStates;
            end;
         end;

       //LShowStates := [2, 4, 22];
       LParent:= self.lbArchive;

       //if not (LControllerOrders.Orders.Items[i].State.ToInteger in LShowStates) then
       if LDoShow then
         begin
           LOrderItem := TOrderItem.Create;
           //LOrderItem.Order := LControllerOrders.Orders.Items[i];
           LOrderItem.Order.Assign(LControllerOrders.Orders.Items[i]);

           LOrder := TOrder.Create;
           LOrder.Assign(LControllerOrders.Orders.Items[i]);
           ModelDataGlobal.OrdersArchive.Items.Add(LOrder);
           TLogger.Save('ModelDataGlobal.OrdersArchive.Items.Add(LOrder)');

           LOrderItem.OffsetY := 80*i;
           LControllerOrders.Lock;
            try
             self.FOrderItems.Add(LParent, LOrderItem);
            finally
             LControllerOrders.Unlock;
            end;
         end;

      end;
     finally
       ModelDataGlobal.OrdersArchive.Unlock;
       self.lbArchive.EndUpdate;
      end;

  finally
   LControllerOrders.Destroy;
   self.HideLoader;
  end;
end;

procedure TViewOrdersArchive.RefreshData;
begin
 inherited;
 self.RefreshOrders();
end;

procedure TViewOrdersArchive.RefreshOrders;
var
 LControllerOrders: TControllerOrdersArchive;
 ts: TStrings;
begin

 if not ASilent then
  self.ShowLoader;

  LControllerOrders := TControllerOrdersArchive.Create;

  LControllerOrders.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  if ModelDataGlobal.AppSettings.FilterArchive.Active then
   begin
    ts := TStringList.Create;
    ts.Add('condition=(not+("IsPlan"=1+AND+"State"=0)+AND+("IsDeleted"=0))+and+("TimeCall"+between+%27'+
      DateToStr(ModelDataGlobal.AppSettings.FilterArchive.DateFrom)+'%27+and+dateadd(day%2C+1%2C+cast(%27'+
      DateToStr(ModelDataGlobal.AppSettings.FilterArchive.DateTo)+'%27+as+date)))');
    //(not+("IsPlan"=1+AND+"State"=0)+AND+("IsDeleted"=0))+and+("TimeCall"+between+%2720.12.2021%27+and+dateadd(day%2C+1%2C+cast(%2728.12.2021%27+as+date)))
   end else ts := nil;

  LControllerOrders.OnRequestSuccess2 := self.OnOrdersSuccess2;
  LControllerOrders.OnRequestFail2 := self.OnOrdersFail2;
  LControllerOrders.RetrieveData(rmGet, ts);

end;

procedure TViewOrdersArchive.SelectOrder;
var
 LOrderItem: TOrderItem;
 b: boolean;
 LId: integer;
begin

 try

    //сначала снимаем выделение у предыдущего выделенного элемента
    if Assigned(ModelDataGlobal.CurrentOrderArchive) then
     begin
      b := self.FOrderItems.Items.TryGetValue(ModelDataGlobal.CurrentOrderArchive.Id.ToInteger, LOrderItem);
      if Assigned(LOrderItem) then
       LOrderItem.FRectWrapper.Fill.Color := TAlphaColorRec.Black
     end;

    //теперь выделяем текущий и загоняем в глобальное поле
    LId := -1;
    LId := self.lbArchive.Selected.Tag;

     b := self.FOrderItems.Items.TryGetValue(LId, LOrderItem);

     if b then
      begin
       ModelDataGlobal.CurrentOrderArchive := LOrderItem.Order;
       LOrderItem.FRectWrapper.Fill.Color := TAlphaColorRec.SaddleBrown;
      end;

 except on E:Exception do
   TLogger.Save('[TViewOrdersArchive.SelectOrder] err: '+e.Message);
 end;

end;

procedure TViewOrdersArchive.ShowOrderDetail;
 var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Карта вызова №'+ModelDataGlobal.CurrentOrderArchive.MonthlyId);
  LViewParams.ViewClass := 'TViewOrderArchiveDetail';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abCancel];
  LViewParams.MainMenuVisible := true;
  self.ShowView(LViewParams);

  TToast.Show('Пока не реализовано');
end;

end.
