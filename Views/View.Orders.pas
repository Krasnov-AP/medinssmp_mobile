﻿unit View.Orders;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, FMX.Layouts, FMX.Controls.Presentation, FMX.Objects, FMX.Edit,
  Controller.Auth, Model.User, Core.Common, Model.Global, FMX.ListBox,
  Model.SMPService, Controller.SMPService, XSuperObject, XSuperJson,
  System.ImageList, FMX.ImgList, FMX.MultiResBitmap, ElAES,
  System.NetEncoding, Core.Crypt, System.Hash, Controller.AnyData, data.main,
  System.IOUtils, Model.LangConsts, Model.ViewParams, FMX.Memo.Types,
  FMX.ScrollBox, FMX.Memo, Core.Funcs, Controller.Orders, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView,
  FMX.TabControl, Model.Orders, View.Scrollable, System.Generics.Collections;

type

  TOrderItem = class
   strict private
    FOrder: TOrder;
   strict private
    procedure SetOrder(AValue: TOrder);
   public
    FRectWrapper: TRectangle;
    FLabelFIO, FLabelAdrdress, FLabelDate, FLabelTeam: TLabel;
    FRectState: TRectangle;
    FImageState: TImage;
    FListBoxItem: TListBoxItem;
    OffsetY: integer;
   public
    property Order: TOrder read FOrder write SetOrder;
   public
    procedure MakeOrderBlock(AParent: TFmxObject);
    procedure UpdateCallBlock;
    constructor Create;
  end;

  TOrderItems = class
   strict private
    FItems: TDictionary<integer, TOrderItem>;
    FCaller: integer;
   public
    property Items: TDictionary<integer, TOrderItem> read FItems write FItems;
    property Caller: integer read FCaller write FCaller;
   public
    procedure Add(AParent:TFmxObject; AItem: TOrderItem);
    procedure Update(AOrderId:integer; AItem: TOrderItem);
    procedure Clear;
    constructor Create;
  end;

  TViewOrders = class(TViewBase)
    btnRefresh: TButton;
    VertScrollBox1: TVertScrollBox;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    imgOrderNew: TImage;
    imgOnAgreement: TImage;
    imgOrderInProcess: TImage;
    imgOrderCancelled: TImage;
    imgOrderDone: TImage;
    lbInProcess: TListBox;
    lbNew: TListBox;
    timDblClick: TTimer;
    imgVosklBlue: TImage;
    imgSKOrder: TImage;
    timBlinking: TTimer;
    TabItem3: TTabItem;
    lbSurvey: TListBox;
    imgSurvey: TImage;
    procedure btnRefreshClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure vsbNewMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure lbInProcessItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure lbNewItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure timDblClickTimer(Sender: TObject);
    procedure timBlinkingTimer(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
  const
   COMMAND_TYPES = 'UpdateOrder,setOrderState,updateOrderPart1,updateOrderPart2,insertOrder,updateAndShowOrder,deleteSurvey';
  strict private
    //FControllerOrders:TControllerOrders;
    FOrdersLoaded:boolean;
    FOrderItems: TOrderItems;
    //FUpdatingOrderId: integer;
  strict protected
   procedure ProcessCommand(ACommandType, ACommand: string); override;
   procedure RefreshOrdersCount;
  private
    { Private declarations }
    procedure OnOrdersSuccess2(Sender: TObject);
    procedure OnOrdersFail2(Sender: TObject);
    //procedure OnOrdersSuccess;
    //procedure OnOrdersFail;
    //procedure OnUpdateOrderSuccess;
    //procedure OnUpdateOrderFail;
    procedure OnUpdateOrderSuccess2(Sender: TObject);
    procedure OnUpdateOrderFail2(Sender: TObject);

  public
    //property ControllerOrders:TControllerOrders read FControllerOrders write FControllerOrders;
  public
    { Public declarations }
    procedure AfterShow; override;
    procedure BeforeClose; override;
    procedure UpdateOrder(AOrderId:integer);
    function GetControl(AParent:TVertScrollBox; ACoords: TPointF; AWithOffset: boolean = true): TRectangle;
    procedure RefreshOrders(ASilent: boolean = false);
    procedure SelectOrder;
    procedure RefreshData; override;
    procedure ShowOrderDetail;
    procedure StopBlinking;
    procedure DeleteOrder(AOrderId:string);
    procedure StopSurveyBlinking;
  end;

var
  bmpOrderCancelled, bmpOrderInProcess, bmpOrderDone,
  bmpOrderNew, bmpOnAgreement, bmpVosklBlue {19}, bmpSKOrder{18}, bmpSurvey: TBitmap;

implementation

{$R *.fmx}

{ TViewCalls }

procedure TViewOrders.AfterShow;
begin

  self.FCOMMAND_TYPES := self.COMMAND_TYPES;

  inherited;

  bmpOrderCancelled := self.imgOrderCancelled.Bitmap;
  bmpOrderInProcess := self.imgOrderInProcess.Bitmap;
  bmpOrderDone := self.imgOrderDone.Bitmap;
  bmpOrderNew := self.imgOrderNew.Bitmap;
  bmpOnAgreement := self.imgOnAgreement.Bitmap;
  bmpVosklBlue := self.imgVosklBlue.Bitmap;
  bmpSKOrder := self.imgSKOrder.Bitmap;
  bmpSurvey := self.imgSurvey.Bitmap;

  self.TabItem3.Visible := self.lbSurvey.Items.Count > 0;

  if not Assigned(self.TabControl1.ActiveTab) then
   self.TabControl1.ActiveTab := self.TabItem2
    else
     begin
      if self.TabControl1.ActiveTab = self.TabItem3 then
       self.StopSurveyBlinking;
     end;

  if not Assigned(self.FOrderItems) then
    self.FOrderItems := TOrderItems.Create;

  if not self.FOrdersLoaded then
    begin
      self.RefreshOrders;
    end;

   {$ifdef ANDROID}
    if self.Caller <> View.Base.VIEW_CALLER_SILENT then
     TToast.Show('Двойное нажатие - действия с вызовом.');
   {$endif}

   self.timBlinking.Enabled := true;
end;

procedure TViewOrders.BeforeClose;
begin
  //inherited;
  //self.timBlinking.Enabled := false;
end;

procedure TViewOrders.btnRefreshClick(Sender: TObject);
var
 ts: TStrings;
begin

  //self.RefreshOrders();

  self.Button1Click(self);


end;

procedure TViewOrders.Button1Click(Sender: TObject);
var
 LOrderItem: TOrderItem;
begin
  ShowMessage(self.lbSurvey.Items.Count.ToString);
end;

procedure TViewOrders.DeleteOrder(AOrderId: string);
var
 i:integer;
 LOrderItem: TOrderItem;
 LOrder: TOrder;
 b: boolean;
begin

 try
  try
   b := self.FOrderItems.Items.TryGetValue(AOrderId.ToInteger, LOrderItem);

   if b then
    if LOrderItem.Order.Id = AOrderId then
     begin
      LOrderItem.FListBoxItem.Parent := nil;
      i := ModelDataGlobal.BlinkingOrderIds.IndexOf(AOrderId);
      if i>=0 then
       ModelDataGlobal.BlinkingOrderIds.Delete(i);
     end;

  except on E:Exception do
   TLogger.Save('[TViewOrders.DeleteOrder] err: '+e.Message);
  end;
 finally
  self.RefreshOrdersCount;
 end;

 {
 try
  for i := 0 to self.FOrderItems.Items.Count - 1 do
   begin
    LOrder := self.FOrderItems.Items[i].Order;

    //if LOrder.State.ToInteger in [10, 11] then
     if LOrder.Id = AOrderId then
      begin
        self.FOrderItems.Items[i].FListBoxItem.Parent := nil;
        exit;
      end;

   end;
 except on E:Exception do
   TLogger.Save('[TViewOrders.DeleteOrder] err: '+e.Message);
 end;}

end;

procedure TViewOrders.OnOrdersFail2(Sender: TObject);
begin
 try
   self.HideLoader;
   self.MakeError(Model.LangConsts.LANG_SOMETHING_WENT_WRONG);
 finally
   if Sender is TControllerOrders then
    TControllerOrders(Sender).Destroy;
 end;
end;

procedure TViewOrders.OnOrdersSuccess2(Sender: TObject);
var
 i, k:integer;
 s:string;
 LParent:TListBox;
 LOrderItem: TOrderItem;
 LOrder: TOrder;
 LShowStates: set of byte;
 LControllerOrders: TControllerOrders;
 LCNT: integer;
 LNeedToDelete: boolean;
begin

   TLogger.Save('[TViewOrders.OnOrdersSuccess2] start');

   if not (Sender is TControllerOrders) then exit;

   self.HideLoader;
   LControllerOrders := TControllerOrders(Sender);

   try

     try
      if self.Caller <> View.Base.VIEW_CALLER_SILENT then self.FOrdersLoaded := true;

      try
       LControllerOrders.Orders.ConvertFromJson(LControllerOrders.HttpRequest.Response);
      except on E:Exception do
       begin
        self.MakeError(e.Message);
        TLogger.Save('[TViewOrders.OnOrdersSuccess2] err: '+e.message);
        exit;
       end;
      end;

     self.lbInProcess.BeginUpdate;
     self.lbNew.BeginUpdate;
     self.lbSurvey.BeginUpdate;

     self.lbInProcess.Content.DeleteChildren;
     self.lbSurvey.Content.DeleteChildren;
     self.lbNew.Content.DeleteChildren;

     self.FOrderItems.Clear;
     ModelDataGlobal.Orders.Lock;
     ModelDataGlobal.Orders.Items.Clear;

     LCNT := ModelDataGlobal.BlinkingOrderIds.Count;

     try

       for i:=0 to LControllerOrders.Orders.Items.Count - 1 do
        begin

         LShowStates := [2, 4, 22]; //или [2, 22] - чтоб отображать завершенные
         LParent:= self.lbInProcess;


         if not ( (LControllerOrders.Orders.Items[i].ActionStateSMP = '2') and (LControllerOrders.Orders.Items[i].SMPName.Trim <> '') ) {(LControllerOrders.Orders.Items[i].State = '0')} then
          begin
           //if  (LControllerOrders.Orders.Items[i].SMPName.Trim = '') {(LControllerOrders.Orders.Items[i].ActionStateSMP) <> '2'} then
            begin
             LParent := self.lbNew;
             LShowStates := [2, 4, 22];
            end;
          end;

         if (LControllerOrders.Orders.Items[i].State.ToInteger in [10, 11]) then
          begin
           LParent := self.lbSurvey;
          end;


         if (not (LControllerOrders.Orders.Items[i].State.ToInteger in LShowStates) ) and (not (LControllerOrders.Orders.Items[i].IsHide = '1')) then
         //{or
         // ( (LControllerOrders.Orders.Items[i].UserLogin.Trim<>'') and (LControllerOrders.Orders.Items[i].SKOrderId.Trim='0') )} then
           begin
             LOrderItem :=TOrderItem.Create;
             //LOrderItem.Order := LControllerOrders.Orders.Items[i];
             LOrderItem.Order.Assign(LControllerOrders.Orders.Items[i]);

             LOrder := TOrder.Create;
             LOrder.Assign(LControllerOrders.Orders.Items[i]);
             ModelDataGlobal.Orders.Items.Add(LOrder);
             TLogger.Save('ModelDataGlobal.Orders.Items.Add(LOrder)');


             //заносим id-шники, которые будут мелькать в глобальный лист
             LNeedToDelete := true;
             k := ModelDataGlobal.BlinkingOrderIds.IndexOf(LOrder.Id);
             if ( LOrder.SKCode <> '' ) then
              if ( (LOrder.ActionStateSMP = '0') or (LOrder.ActionStateSMP.Trim = '') ) then
                if ( (LOrder.SKOrderId <> '0') and (LOrder.SKOrderId.Trim <> '') ) then
                 begin
                  if k < 0 then ModelDataGlobal.BlinkingOrderIds.AddObject(LOrder.Id, LOrder);
                  LNeedToDelete := false;
                 end;

             if LOrder.State.ToInteger in [10, 11] then
              begin
               if k < 0 then ModelDataGlobal.BlinkingOrderIds.AddObject(LOrder.Id, LOrder);
               LNeedToDelete := false;
              end;

             if (LNeedToDelete) and (k >=0) then
              ModelDataGlobal.BlinkingOrderIds.Delete(k);

               LOrderItem.OffsetY := 80*i;
               LControllerOrders.Lock;
                try
                 self.FOrderItems.Caller := self.Caller;
                 self.FOrderItems.Add(LParent, LOrderItem);
                finally
                 LControllerOrders.Unlock;
                end;
           end;

           if Assigned(ModelDataGlobal.CurrentOrder) then
             begin
              if ModelDataGlobal.CurrentOrder.Id = LOrder.Id then
               ModelDataGlobal.CurrentOrder.Assign(LOrder);
             end;

        end; //for i

         if ModelDataGlobal.BlinkingOrderIds.Count = 0 then
            begin
             self.StopBlinking;
            end
             else
            begin
              if LCNT < 1 then  //если до этого не трезвонили
               self.SendMessage('start_blinking_client_from_sk');
            end;

       finally
         ModelDataGlobal.Orders.Unlock;
         self.lbInProcess.EndUpdate;
         self.lbNew.EndUpdate;
         self.lbSurvey.EndUpdate;
       end;

     except on E: Exception do
       TToast.Show(e.Message);
     end;

    finally
     LControllerOrders.Destroy;
     self.RefreshOrdersCount;
     self.HideLoader;
    end;
end;

procedure TViewOrders.OnUpdateOrderFail2(Sender: TObject);
begin
 try

 finally
   if Sender is TControllerOrders then
     TControllerOrders(Sender).Destroy;
 end;
end;

procedure TViewOrders.OnUpdateOrderSuccess2(Sender: TObject);
var
 i, k:integer;
 s:string;
 LParent:TListBox;
 LOrder, LCurrentOrder: TOrder;
 LOrderItem: TOrderItem;
 LNeedToChangeParent, LNeedToDelete: boolean;
 LControllerOrders: TControllerOrders;
 LCNT: integer;
 LDoHide: boolean;
begin

  if not (Sender is TControllerOrders) then exit;

  LControllerOrders := TControllerOrders(Sender);

  try

    TLogger.Save('[TViewOrders.OnUpdateOrderSuccess2] start');

    try
     LControllerOrders.Orders.ConvertFromJson(LControllerOrders.HttpRequest.Response);
    except on E:Exception do
     begin
      self.MakeError(e.Message);
      TLogger.Save('[TViewOrders.OnUpdateOrderSuccess2] err: '+e.message, TLogLevel.Error);
      exit;
     end;
    end;

   try
     if LControllerOrders.Orders.Items.Count > 0 then
      begin

       LCNT := ModelDataGlobal.BlinkingOrderIds.Count;
       TLogger.Save('LCNT='+LCNT.ToString);

       for i := 0 to LControllerOrders.Orders.Items.Count-1 do
        begin

         //LOrder := TOrder.Create;
         //LOrder.Assign(LControllerOrders.Orders.Items[i]);
         LOrder := LControllerOrders.Orders.Items[i];

         TLogger.Save('step0');

         //заносим id-шники, которые будут мелькать в глобальный лист
         LNeedToDelete := true;
         k := ModelDataGlobal.BlinkingOrderIds.IndexOf(LOrder.Id);
         //от СК
         if ( LOrder.SKCode <> '' ) then
          if ( (LOrder.ActionStateSMP = '0') or (LOrder.ActionStateSMP.Trim = '') ) then
            if ( (LOrder.SKOrderId <> '0') and (LOrder.SKOrderId.Trim <> '') ) then
             begin
              if k < 0 then ModelDataGlobal.BlinkingOrderIds.AddObject(LOrder.Id, LOrder);
              LNeedToDelete := false;
             end;

         //опросы
         if LOrder.State.ToInteger in [10, 11] then
            begin
             if k < 0 then ModelDataGlobal.BlinkingOrderIds.AddObject(LOrder.Id, LOrder);
             LNeedToDelete := false;
            end;

         if (LNeedToDelete) and (k >=0) then
          ModelDataGlobal.BlinkingOrderIds.Delete(k);


         //LControllerOrders.Lock;
         try

          self.FOrderItems.Caller := self.Caller;

          if self.FOrderItems.Items.TryGetValue({self.FUpdatingOrderId} LOrder.Id.ToInteger, LOrderItem) then
            begin

             //LNeedToChangeParent := (TOrder(LControllerOrders.Orders.Items[0]).SMPName <> '') and (LOrderitem.Order.SMPName = ''); //неправильно - 31.01.2022
             LNeedToChangeParent := ( (LControllerOrders.Orders.Items[i].ActionStateSMP = '2') and (LControllerOrders.Orders.Items[i].SMPName.Trim <> '') );

             LOrderItem.Order := LControllerOrders.Orders.Items[i];
             if LNeedToChangeParent then
               LOrderItem.FListBoxItem.Parent := self.lbInProcess;

             if (LControllerOrders.Orders.Items[i].State.ToInteger in [10, 11]) then
              begin
               self.TabItem3.Visible := true;
               LOrderItem.FListBoxItem.Parent := self.lbSurvey;
              end;

             LDoHide := LOrderItem.Order.State.ToInteger in [2, 4, 22];
             if LDoHide then
               if (LOrderItem.Order.State.ToInteger = 4) then
                if (LOrderItem.Order.IsHide <> '1')  then
                  LDoHide := false;

             if LDoHide then
              begin
               LOrderItem.FListBoxItem.Parent := nil;
              end
               else
                begin
                 self.FOrderItems.Update({self.FUpdatingOrderId}LOrderItem.Order.Id.ToInteger, LOrderItem);
                end;
           end
            else
           begin
             LParent:= self.lbInProcess;
             LOrderItem := TOrderItem.Create;
             LOrderItem.Order := LControllerOrders.Orders.Items[i];

             if LOrderItem.Order.SMPName.Trim = '' then LParent := self.lbNew;

             if LOrderItem.Order.State.ToInteger in [10, 11] then
              begin
               self.TabItem3.Visible := true;
               LParent := self.lbSurvey;
              end;

             if LOrderItem.Order.State.ToInteger in [2, 4, 22] then continue;
             self.FOrderItems.Add(LParent, LOrderItem);

           end;

          try
            ModelDataGlobal.Orders.SetById(LOrder.Id, LOrder);
            if Assigned(ModelDataGlobal.CurrentOrder) then
             begin
              if ModelDataGlobal.CurrentOrder.Id = LOrder.Id then
               LCurrentOrder := LOrder;
               //ModelDataGlobal.CurrentOrder.Assign(LOrder);
             end;
          except on E:Exception do
           TLogger.Save('[TViewOrders.OnUpdateOrderSuccess2] err3: '+e.Message, TLogLevel.Error);
          end;

          finally
           //LControllerOrders.Unlock;
          end;
        end; //for i


      end
       else
        TLogger.Save('[TViewOrders.OnUpdateOrderSuccess2] Count<1]');
   except on E:Exception do
    TLogger.Save('[TViewOrders.OnUpdateOrderSuccess2] err2: '+e.Message, TLogLevel.Error);
   end;

  finally
    self.RefreshOrdersCount;
    LControllerOrders.Destroy;

      TThread.CreateAnonymousThread(
            procedure()
              begin
                Sleep(250);
                TThread.Synchronize(TThread.CurrentThread,
                procedure
                  begin
                   if Assigned(LCurrentOrder) then
                    begin
                     ModelDataGlobal.CurrentOrder.Assign(LCurrentOrder);
                     self.SendMessage('update_order_log');
                      {$ifdef ANDROID}
                       //TToast.Show('update_order_log sent');
                      {$endif}
                    end;
                  end);
              end).Start;

         if ModelDataGlobal.BlinkingOrderIds.Count = 0 then
          begin
           //self.SendMessage('stop_blinking_client_from_sk');
           self.StopBlinking;
          end
           else
          begin
            if LCNT < 1 then  //если до этого не трезвонили
             self.SendMessage('start_blinking_client_from_sk');
          end;

  end;

end;

procedure TViewOrders.ProcessCommand;
var
 i:integer;
 LCommands: TStrings;
 LCommand: string;
 LId: Integer;
begin
  inherited;

  begin
      if ACommand<>'' then
         begin
           LCommands := TStringList.Create;

           try

             TLogger.Save('[TViewOrders.ProcessCommands] command: '+ACommand);
             LCommands.Delimiter := '|';
             LCommands.DelimitedText := ACommand;

             for i := LCommands.Count - 1 downto 0 do
               begin

                LCommand := LCommands[i];
                if (LCommand<>'') then
                 begin
                  LId := StrToIntDef(LCommand, -1);
                  if ACommandType.ToUpper <> 'DELETESURVEY' then
                   self.UpdateOrder(LId)
                    else
                   self.DeleteOrder(LId.ToString);
                 end;

               end;

           finally
            LCommands.DisposeOf;
           end;
      end;
   end;

end;

procedure TViewOrders.RefreshData;
begin
  inherited;
  self.RefreshOrders;
end;

procedure TViewOrders.RefreshOrders;
var
 LControllerOrders: TControllerOrders;
begin

 if not ASilent then
  self.ShowLoader;

  self.StopBlinking;

  LControllerOrders := TControllerOrders.Create;

  LControllerOrders.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  LControllerOrders.OnRequestSuccess2 := self.OnOrdersSuccess2;
  LControllerOrders.OnRequestFail2 := self.OnOrdersFail2;
  LControllerOrders.RetrieveData();

end;

procedure TViewOrders.RefreshOrdersCount;
begin
  self.TabItem1.Text := 'В работе ('+self.lbInProcess.Items.Count.ToString+')';
  self.TabItem2.Text := 'Новые ('+self.lbNew.Items.Count.ToString+')';
  self.TabItem3.Text := 'Опрос ('+self.lbSurvey.Items.Count.ToString+')';

  if self.TabControl1.ActiveTab = self.TabItem3 then
     self.StopSurveyBlinking;

  self.TabItem3.Visible := self.lbSurvey.Items.Count > 0;

  if (not self.TabControl1.ActiveTab.Visible) or (not Assigned(Self.TabControl1.ActiveTab))  then
   self.TabControl1.ActiveTab := self.TabItem1;
end;

procedure TViewOrders.SelectOrder;
var
 LOrderItem: TOrderItem;
 b: boolean;
 LId: integer;
begin

  try

    //сначала снимаем выделение у предыдущего выделенного элемента
    if Assigned(ModelDataGlobal.CurrentOrder) then
     begin
      b := self.FOrderItems.Items.TryGetValue(ModelDataGlobal.CurrentOrder.Id.ToInteger, LOrderItem);
      if Assigned(LOrderItem) then
       LOrderItem.FRectWrapper.Fill.Color := TAlphaColorRec.Black
     end;

    //теперь выделяем текущий и загоняем в глобальное поле
    LId := -1;
    if self.TabControl1.ActiveTab = self.TabItem1 then
      LId := self.lbInProcess.Selected.Tag
       else if self.TabControl1.ActiveTab = self.TabItem2 then
        LId := self.lbNew.Selected.Tag
         else
          LId := self.lbSurvey.Selected.Tag;

     b := self.FOrderItems.Items.TryGetValue(LId, LOrderItem);

     if b then
      begin
       ModelDataGlobal.CurrentOrder := LOrderItem.Order;
       LOrderItem.FRectWrapper.Fill.Color := TAlphaColorRec.SaddleBrown;
      end;

  except on E:Exception do
   TLogger.Save('[TViewOrders.SelectOrder] err: '+e.Message);
  end;

end;

procedure TViewOrders.ShowOrderDetail;
 var
 LViewParams: TViewParams;
begin
  inherited;
  LViewParams := TViewParams.Create;
  LViewParams.Title := TLangManager.Translate('Действия по вызову');
  LViewParams.ViewClass := 'TViewOrderDetail';
  LViewParams.IsModal := true;
  LViewParams.ActionButtons := [abCancel];
  LViewParams.MainMenuVisible := true;
  //если опрос
  if self.TabControl1.ActiveTab = self.TabItem3 then
   LViewParams.Caller := View.Base.ORDER_DETAIL_CALLER_SURVEY;
  self.ShowView(LViewParams);

end;

procedure TViewOrders.StopBlinking;
var
 k:integer;
begin

 if self.Caller = View.Base.VIEW_CALLER_SILENT then exit;

 try
  self.timBlinking.Tag := -1;
  self.TabItem2.TextSettings.FontColor := TAlphaColorRec.White;
  self.TabItem3.TextSettings.FontColor := TAlphaColorRec.White;

  self.TabItem2.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
  self.TabItem3.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];

 if self.Caller <> View.Base.VIEW_CALLER_SILENT then
  for k in self.FOrderItems.Items.Keys do
   begin
    self.FOrderItems.Items[k].FLabelFIO.TextSettings.FontColor := TAlphaColorRec.White;
    self.FOrderItems.Items[k].FLabelAdrdress.TextSettings.FontColor := TAlphaColorRec.White;
   end;

  self.SendMessage('stop_blinking_client_from_sk');
 except on E:Exception do
  TLogger.Save('stop blinking error '+e.Message, TLogLevel.Error);
 end;

end;

procedure TViewOrders.StopSurveyBlinking;
var
 i: integer;
 LOrder: TOrder;
begin

  inherited;

  self.timBlinking.Enabled := false;

  try

  if self.TabControl1.ActiveTab = self.TabItem3 then
   begin
    i := 0;
    self.TabItem3.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];

     while i <= ModelDataGlobal.BlinkingOrderIds.Count - 1 do
      begin
       if (ModelDataGlobal.BlinkingOrderIds.Objects[i] is TOrder) then
        begin
         LOrder := (ModelDataGlobal.BlinkingOrderIds.Objects[i] as TOrder);
          if LOrder.State.ToInteger in [10, 11] then
           ModelDataGlobal.BlinkingOrderIds.Delete(i)
            else inc(i);
        end
         else inc(i);

        if i > 1000 then break; //на всякий

      end; //while
   end;

  finally
    self.timBlinking.Enabled := true;
    if ModelDataGlobal.BlinkingOrderIds.Count = 0 then
          begin
           self.StopBlinking;
           //self.SendMessage('stop_blinking_client_from_sk');
          end
  end;

end;

procedure TViewOrders.TabControl1Change(Sender: TObject);
begin
  self.StopSurveyBlinking;
end;

procedure TViewOrders.timBlinkingTimer(Sender: TObject);
var
 i: integer;
 LOrderItem: TOrderItem;
 LId: integer;
 b, LStopBlinking: boolean;
 LOrder: TOrder;
begin
  inherited;

  self.timBlinking.Enabled:= false;
  LStopBlinking := self.timBlinking.Tag = -1;

  if self.timBlinking.Tag = 0 then self.timBlinking.Tag := 1 else self.timBlinking.Tag := 0;
   try
     if ModelDataGlobal.BlinkingOrderIds.Count = 0 then exit;
     ModelDataGlobal.Lock;
      try
       for i := 0 to ModelDataGlobal.BlinkingOrderIds.Count - 1 do
         begin
          try
            LId := ModelDataGlobal.BlinkingOrderIds[i].ToInteger;
          except on E: Exception do
            continue;
          end;

           b := self.FOrderItems.Items.TryGetValue(LId, LOrderItem);
           if not b then continue;

           LOrderItem.FLabelFIO.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];
           LOrderItem.FLabelAdrdress.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];
           self.TabItem2.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];
           self.TabItem3.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];

           LOrder := nil;
           if Assigned(ModelDataGlobal.BlinkingOrderIds.Objects[i]) then
            if (ModelDataGlobal.BlinkingOrderIds.Objects[i] is TOrder) then
             LOrder := ModelDataGlobal.BlinkingOrderIds.Objects[i] as TOrder;


           if self.timBlinking.Tag = 1 then
            begin
             LOrderItem.FLabelFIO.TextSettings.FontColor := TAlphaColorRec.Red;
             LOrderItem.FLabelAdrdress.TextSettings.FontColor := TAlphaColorRec.Red;
             if LOrder = nil then
              self.TabItem2.TextSettings.FontColor := TAlphaColorRec.Red
               else
                begin
                 if LOrder.State.ToInteger in [10, 11] then
                  self.TabItem3.TextSettings.FontColor := TAlphaColorRec.Red
                   else
                  self.TabItem2.TextSettings.FontColor := TAlphaColorRec.Red;
                end;
            end
             else
            begin
             LOrderItem.FLabelFIO.TextSettings.FontColor := TAlphaColorRec.White;
             LOrderItem.FLabelAdrdress.TextSettings.FontColor := TAlphaColorRec.White;
             if LOrder = nil then
              self.TabItem2.TextSettings.FontColor := TAlphaColorRec.White
               else
                begin
                 if LOrder.State.ToInteger in [10, 11] then
                  self.TabItem3.TextSettings.FontColor := TAlphaColorRec.White
                   else
                  self.TabItem2.TextSettings.FontColor := TAlphaColorRec.White;
                end;
            end;

         end;
      finally
        ModelDataGlobal.Unlock;
      end;
   finally
    {if not LStopBlinking then }self.timBlinking.Enabled := true;
   end;
end;

procedure TViewOrders.timDblClickTimer(Sender: TObject);
begin
  inherited;
  self.timDblClick.Enabled := false;
  self.timDblClick.Tag := -1;
end;

procedure TViewOrders.UpdateOrder(AOrderId: integer);
var
 ts: TStrings;
 LControllerOrders: TControllerOrders;
begin

 if (AOrderId = 0) or (AOrderId = -1) then
  begin
   TLogger.Save('[TViewOrders.UpdateOrder] before RefreshOrders');
   self.RefreshOrders(true);
   exit;
  end;

  if not self.FOrdersLoaded then exit;

  LControllerOrders := TControllerOrders.Create;
  
  LControllerOrders.Host := ModelDataGlobal.AppSettings.SMPService.IP;

  ts := TStringList.Create;
   try
    ts.Add(format('and ("Id"=%d)', [AOrderId]));

    LControllerOrders.OnRequestSuccess2 := self.OnUpdateOrderSuccess2;
    LControllerOrders.OnRequestFail2 := self.OnUpdateOrderFail2;
    LControllerOrders.RetrieveData(rmGET, ts);
   finally
     ts.DisposeOf;
   end;

end;

procedure TViewOrders.vsbNewMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
var
 L: TRectangle;
 p: TPointF;
begin
end;

{ TOrderItem }

constructor TOrderItem.Create;
begin
 inherited;
 self.FOrder := TOrder.Create;
end;

procedure TOrderItem.MakeOrderBlock(AParent: TFmxObject);
var
 LRect:TRectangle;
 LLine:TLine;
 LLayout:TLayout;
 LLabel:TLabel;
 LImage: TImage;
 lbITem: TListBoxItem;
begin

 //if Parent.InheritsFrom(TControl) then
 // (Parent as TControl).BeginUpdate;

 try

   lbItem := TListBoxItem.Create(AParent);
   lbItem.Parent := AParent;
   lbItem.Height := 80;
   //lbItem.Name := 'Order'+self.Order.Id; //коммент, а то в мобилках отображается

   self.FListBoxItem := lbItem;

   try
    lbItem.Tag := self.Order.Id.ToInteger;
   except on E:Exception do
    lbItem.Tag := -1;
   end;

   LRect := TRectangle.Create(lbItem);
   LRect.Parent := lbItem;
   //LRect.Position.Y := 10000;
   LRect.Align := TAlignLayout.Client;
   LRect.Height := 80;
   LRect.Margins.Bottom := 0;
   LRect.Fill.Color := TAlphaColorRec.Black;
   LRect.HitTest := false;
   LRect.CanFocus := false;


   self.FRectWrapper := LRect;

   LLine := TLine.Create(LRect);
   LLine.Parent := LRect;
   LLine.Height := 1;
   LLine.Stroke.Color := TAlphaColorRec.Lightgray;
   LLine.Align := TAlignLayout.MostBottom;

   //слой с картинкой
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.MostLeft;
   LLayout.Width := 40;
   LLayout.Margins.Bottom := 2;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;

   LImage := TImage.Create(LLayout);
   LImage.Parent := LLayout;
   LImage.Align := TAlignLayout.Center;
   LImage.Width :=16;
   LImage.Height := 16;

     case self.Order.State.ToInteger of
      csCancelled, 22: LImage.Bitmap := bmpOrderCancelled;
      csInProcess: LImage.Bitmap := bmpOrderInProcess;
      csDone: LImage.Bitmap := bmpOrderDone;
      csNew:
       if self.Order.SKOrderId.Trim<>'0' then
         LImage.Bitmap := bmpSKOrder
          else
         LImage.Bitmap := bmpOrderNew;
      csOnAgreement: LImage.Bitmap := bmpOnAgreement;
      csSurvey, csSurvey2: LImage.Bitmap := bmpSurvey;
      csDetectMore:
       begin
         if (self.Order.HospitalCoords<>'') and (self.Order.Coords = self.Order.HospitalCoords) then
          LImage.Bitmap := bmpOrderInProcess
           else
         if (self.Order.IsHospitalCoordination = '1') then
          LImage.Bitmap := bmpVosklBlue
           else
          LImage.Bitmap := bmpOrderCancelled;
       end;
     end;

   self.FImageState := LImage;

   //слой с основной меткой (Имя)
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Top;
   LLayout.Height := 25;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 18;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   LLabel.Text := self.Order.Name; // + ' ('+self.Order.IsHide+', ' +self.Order.ActionStateSMP+', '+self.Order.State+ ','+self.Order.SKOrderId+','+self.Order.SKName+ ', '+ self.Order.SMPName+')'; //, '+self.Order.TimeArr+', '+self.Order.IsHospitalCoordination+')';
   //TLogger.Save(self.Order.FullInfo);
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelFIO := LLabel;

   //слой с адресной меткой
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Top;
   LLayout.Height := 25;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 14;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   LLabel.Text := self.Order.Address+', '+self.Order.Address2; //+ '; '+self.Order.HelpTypes;
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelAdrdress := LLabel;

   //слой с временной меткой
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Left;
   LLayout.Height := 25;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 12;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];
   LLabel.Text := self.Order.TimeCall;
   LLabel.Width := (AParent as TControl).Width / 2;
   LLabel.Margins.Left := 5;
   self.FLabelDate := LLabel;

   //слой с меткой бригады
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Right;
   LLayout.Height := 25;

   LRect := TRectangle.Create(LLayout);
   LRect.Parent := LLayout;
   LRect.Stroke.Color := TAlphaColorRec.Lightgray;
   LRect.Align := TAlignLayout.Right;
   LRect.Width := (AParent as TControl).Width * 0.4;
   LRect.Margins.Top := 5;
   LRect.Margins.Bottom := 5;
   LRect.Margins.Right := 10;
   if self.Order.State.ToInteger<>4 then
    LRect.Fill.Color := TAlphaColorRec.Orangered
     else
    LRect.Fill.Color := TAlphaColorRec.Green;
   self.FRectState := LRect;

   LLabel := TLabel.Create(LRect);
   LLabel.Parent := LRect;
   LLabel.Align := TAlignLayout.Center;
   LLabel.TextSettings.Font.Size := 12;
   LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   if (self.Order.CarId<>'') and (self.Order.CarId<>'0') then
    LLabel.Text := self.Order.Team;
   LLabel.Width := LRect.Width - 10;
   LLabel.Margins.Left := 5;
   self.FLabelTeam := LLabel;

 finally
  //if Parent.InheritsFrom(TControl) then
  // (Parent as TControl).EndUpdate;
 end;

end;

procedure TOrderItem.SetOrder(AValue: TOrder);
begin
 self.FOrder := AValue;
 //self.UpdateCallBlock;
end;


procedure TOrderItem.UpdateCallBlock;
begin

 if not Assigned(self.FLabelFIO) then exit;

 //self.FLabelFIO.Text := self.Order.Name;
 self.FLabelFIO.Text := self.Order.Name; // + ' ('+self.Order.Id+', '+self.Order.State+ ','+self.Order.SKOrderId+','+self.Order.SKName+ ')'; //, '+self.Order.TimeArr+', '+self.Order.IsHospitalCoordination+')';
 self.FLabelAdrdress.Text := self.Order.Address+', '+self.Order.Address2;
 self.FLabelDate.Text := self.Order.TimeCall;
 self.FLabelTeam.Text := '';
 if self.Order.State <> '0' then
  self.FLabelTeam.Text := self.Order.Team;

     case self.Order.State.ToInteger of
      csCancelled, 22: self.FImageState.Bitmap := bmpOrderCancelled;
      csInProcess: self.FImageState.Bitmap := bmpOrderInProcess;
      csDone: self.FImageState.Bitmap := bmpOrderDone;
      csNew:
       if self.Order.SKOrderId.Trim<>'0' then
         self.FImageState.Bitmap := bmpSKOrder
          else
         self.FImageState.Bitmap := bmpOrderNew;
      csOnAgreement: self.FImageState.Bitmap := bmpOnAgreement;
      csSurvey, csSurvey2: self.FImageState.Bitmap := bmpSurvey;
      csDetectMore:
       begin
         if (self.Order.HospitalCoords<>'') and (self.Order.Coords = self.Order.HospitalCoords) then
          self.FImageState.Bitmap := bmpOrderInProcess
           else
         if (self.Order.IsHospitalCoordination = '1') then
          self.FImageState.Bitmap := bmpVosklBlue
           else
          self.FImageState.Bitmap := bmpOrderCancelled;
       end;
     end;

   if self.Order.State.ToInteger<>4 then
    self.FRectState.Fill.Color := TAlphaColorRec.Orangered
     else
    self.FRectState.Fill.Color := TAlphaColorRec.Green;
end;

{ TOrderItems }

procedure TOrderItems.Add(AParent:TFmxObject; AItem: TOrderItem);
begin
  self.FItems.AddOrSetValue(AItem.Order.Id.ToInteger, AItem);
   if self.Caller <> View.Base.VIEW_CALLER_SILENT then
    AItem.MakeOrderBlock(AParent);
end;

procedure TOrderItems.Clear;
begin
 self.FItems.Clear;
end;

constructor TOrderItems.Create;
begin
 self.FItems := TDictionary<integer, TOrderItem>.Create;
end;

procedure TOrderItems.Update(AOrderId:integer; AItem: TOrderItem);
var
 LItem: TOrderItem;
begin

 if self.FItems.TryGetValue(AOrderId, LItem) then
  begin
    LItem.Order := AItem.Order;
    if self.Caller <> View.Base.VIEW_CALLER_SILENT then
     LItem.UpdateCallBlock;
  end;

 //ModelDataGlobal.Orders.SetById(AOrderId.ToString, AItem.Order);
end;

function TViewOrders.GetControl(AParent:TVertScrollBox; ACoords: TPointF; AWithOffset: boolean = true): TRectangle;
begin

end;

procedure TViewOrders.lbInProcessItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
var
 LOrderItem: TOrderItem;
begin
  inherited;

  if self.timDblClick.Tag = Item.Tag then
   begin
     self.timDblClick.Enabled := false;
     self.timDblClick.Tag := -1;
     self.ShowOrderDetail;
   end;

  self.SelectOrder;

  self.timDblClick.Tag := Item.Tag;
  self.timDblClick.Enabled := true;
end;

procedure TViewOrders.lbNewItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
var
 LOrderItem: TOrderItem;
begin
  inherited;
  self.SelectOrder;
end;

end.
