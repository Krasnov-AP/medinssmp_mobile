﻿unit View.SKList;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  Core.Common,
  View.Base, FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.ListBox,
  Model.SkList, System.Generics.Collections, Model.Global, FMX.Edit,
  FMX.Memo.Types, FMX.ScrollBox, FMX.Memo;

type

   TSKItem = class
   strict private
    FSKUnit: TSKUnit;
    FLabelName, FLabelNumber, FLabelNormalTime: TLabel;
    //FRectState: TRectangle;
    FImageState: TImage;
    FListBoxItem: TListBoxItem;
   public
    FRectWrapper: TRectangle;
    OffsetY: integer;
   public
    property SKUnit: TSKUnit read FSKUnit write FSKUnit;
    property ListBoxItem: TListBoxItem read FListBoxItem write FListBoxItem;
   public
    procedure MakeSKUnitBlock(AParent: TFmxObject);
    constructor Create;
  end;

  TSKItems = class
   strict private
    FItems: TDictionary<integer, TSKItem>;
   public
    property Items: TDictionary<integer, TSKItem> read FItems write FItems;
   public
    procedure Add(AParent:TFmxObject; AItem: TSKItem; AWithCodeOnly: boolean = false);
    procedure Clear;
    constructor Create;
  end;


  TViewSKList = class(TViewBase)
    lbSKList: TListBox;
    edSKFilter: TEdit;
    imgSearch: TImage;
    imgDone: TImage;
    timDblClick: TTimer;
    imgClear: TImage;
    timFilterByName: TTimer;
    procedure timDblClickTimer(Sender: TObject);
    procedure lbSKListItemClick(const Sender: TCustomListBox;
      const Item: TListBoxItem);
    procedure timFilterByNameTimer(Sender: TObject);
    procedure edSKFilterTyping(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FSKItems: TSKItems;
  public
   procedure RefreshData; override;
   procedure AfterShow; override;
   procedure SelectSKUnit;
   procedure ConfirmSKUnitForNewOrder;
   procedure ConfirmSKUnitForChat;
   procedure FilterByName;
    { Public declarations }
  end;

var
 bmpImageDone: TBitmap;

implementation

{$R *.fmx}

{ TSKItem }

constructor TSKItem.Create;
begin
 inherited;
 self.FSKUnit := TSKUnit.Create;
end;

procedure TSKItem.MakeSKUnitBlock(AParent: TFmxObject);
var
 LRect:TRectangle;
 LLine:TLine;
 LLayout:TLayout;
 LLabel:TLabel;
 LImage: TImage;
 lbITem: TListBoxItem;
begin


 try
   lbItem := TListBoxItem.Create(AParent);
   lbItem.Parent := AParent;
   lbItem.Height := 50;

   self.FListBoxItem := lbItem;

   try
    lbItem.Tag := self.SKUnit.Id.ToInteger;
   except on E:Exception do
    lbItem.Tag := -1;
   end;

   LRect := TRectangle.Create(lbItem);
   LRect.Parent := lbItem;
   //LRect.Position.Y := 10000;
   LRect.Align := TAlignLayout.Client;
   LRect.Height := 80;
   LRect.Margins.Bottom := 0;
   LRect.Fill.Color := TAlphaColorRec.Black;
   LRect.HitTest := false;
   LRect.CanFocus := false;


   self.FRectWrapper := LRect;

   LLine := TLine.Create(LRect);
   LLine.Parent := LRect;
   LLine.Height := 1;
   LLine.Stroke.Color := TAlphaColorRec.Lightgray;
   LLine.Align := TAlignLayout.MostBottom;

   //слой с картинкой
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.MostLeft;
   LLayout.Width := 40;
   LLayout.Margins.Bottom := 2;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;

   LImage := TImage.Create(LLayout);
   LImage.Parent := LLayout;
   LImage.Align := TAlignLayout.Center;
   LImage.Width :=16;
   LImage.Height := 16;

   LImage.Bitmap := bmpImageDone;

     {case self.Order.State.ToInteger of
      csCancelled: LImage.Bitmap := bmpOrderCancelled;
      csInProcess: LImage.Bitmap := bmpOrderInProcess;
      csDone: LImage.Bitmap := bmpOrderDone;
      csNew: LImage.Bitmap := bmpOrderNew;
      csOnAgreement: LImage.Bitmap := bmpOnAgreement;
     end;

   self.FImageState := LImage; }

   //слой с основной меткой (Название СК)
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Top;
   LLayout.Height := 25;
   LLayout.HitTest := false;
   LLayout.CanFocus := false;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 18;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   LLabel.Text := self.SKUnit.Name; // + '('+self.SKUnit.Id+')'; // + ' ('+self.Car.Id+', State='+self.Car.State+', OnDuty='+self.Car.OnDuty+' OrderId='+self.Car.OrderId+')';
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelName := LLabel;

   //слой с кодом
   LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Top;
   LLayout.Height := 25;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 14;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   LLabel.Text := self.SKUnit.Code;
   if self.SKUnit.CallTime.Trim<>'' then LLabel.Text := LLabel.Text +' ('+self.SKUnit.CallTime+' мин. )';
   LLabel.Width := (AParent as TControl).Width;
   LLabel.Margins.Left := 5;
   self.FLabelNumber := LLabel;

   //слой с норм. временем
   {LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Left;
   LLayout.Height := 25;

   LLabel := TLabel.Create(LLayout);
   LLabel.Parent := LLayout;
   LLabel.Align := TAlignLayout.Left;
   LLabel.TextSettings.Font.Size := 12;
   LLabel.TextSettings.Font.Style := [TFontStyle.fsBold];
   LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style];
   if Assigned(ModelDataGlobal.CurrentOrder) then
    LLabel.Text := ModelDataGlobal.CurrentOrder.NormalTime + ' мин';
   LLabel.Width := (AParent as TControl).Width / 2;
   LLabel.Margins.Left := 5;
   self.FLabelNormalTime := LLabel;}

   //слой с меткой бригады
   {LLayout := TLayout.Create(LRect);
   LLayout.Parent := LRect;
   LLayout.Align := TAlignLayout.Right;
   LLayout.Height := 25;

   LRect := TRectangle.Create(LLayout);
   LRect.Parent := LLayout;
   LRect.Stroke.Color := TAlphaColorRec.Lightgray;
   LRect.Align := TAlignLayout.Right;
   LRect.Width := (AParent as TControl).Width * 0.4;
   LRect.Margins.Top := 5;
   LRect.Margins.Bottom := 5;
   LRect.Margins.Right := 10;
   if self.Order.State.ToInteger<>4 then
    LRect.Fill.Color := TAlphaColorRec.Orangered
     else
    LRect.Fill.Color := TAlphaColorRec.Green;
   self.FRectState := LRect;

   LLabel := TLabel.Create(LRect);
   LLabel.Parent := LRect;
   LLabel.Align := TAlignLayout.Center;
   LLabel.TextSettings.Font.Size := 12;
   LLabel.TextSettings.FontColor := TAlphaColorRec.Gray;
   LLabel.StyledSettings := [TStyledSetting.Family, TStyledSetting.Style, TStyledSetting.FontColor];
   if (self.Order.CarId<>'') and (self.Order.CarId<>'0') then
    LLabel.Text := self.Order.Team;
   LLabel.Width := LRect.Width - 10;
   LLabel.Margins.Left := 5;
   self.FLabelTeam := LLabel;}

 finally
 end;

end;

{ TSKItems }

procedure TSKItems.Add(AParent: TFmxObject; AItem: TSKItem; AWithCodeOnly: boolean = false);
var
 LDoAdd: boolean;
begin

  LDoAdd := true;
  if AWithCodeOnly then
   LDoAdd := AItem.SKUnit.Code.Trim <> '';

  if LDoAdd then
   begin
    self.FItems.Add(AItem.SKUnit.Id.ToInteger, AItem);
    AItem.MakeSKUnitBlock(AParent);
   end;

end;

procedure TSKItems.Clear;
begin
 self.FItems.Clear;
end;

constructor TSKItems.Create;
begin
 self.FItems := TDictionary<integer, TSKItem>.Create;
end;

{ TViewSKList }

procedure TViewSKList.AfterShow;
begin
  inherited;

  bmpImageDone := self.imgDone.Bitmap;

  if not self.DataLoaded then
   begin
    self.FSKItems := TSKItems.Create;
    self.RefreshData;
   end;

end;

procedure TViewSKList.Button1Click(Sender: TObject);
begin
  inherited;
  self.FSKItems.Items[0].ListBoxItem.Visible := false;
end;

procedure TViewSKList.ConfirmSKUnitForNewOrder;
begin
 self.SendMessage('confirm_skunit_for_new_order');
 self.CloseModal;
end;

procedure TViewSKList.ConfirmSKUnitForChat;
begin
  self.SendMessage('confirm_skunit_for_chat');
  self.CloseModal;
end;

procedure TViewSKList.edSKFilterTyping(Sender: TObject);
begin
  inherited;
  self.timFilterByName.Enabled := false;
  self.timFilterByName.Enabled := true;
end;

procedure TViewSKList.FilterByName;
var
 i:integer;
 LUpperText: string;
begin

 LUpperText := self.edSKFilter.Text.Trim.ToUpper;
 //TToast.Show(LUpperText+' '+self.FSKItems.Items.Count.ToString);

   try
    self.lbSKList.BeginUpdate;

     for i in self.FSKItems.Items.Keys do
       begin

        if not (LUpperText = '')  then
         begin
          if self.FSKItems.Items[i].SKUnit.Name.ToUpper.Contains(LUpperText) then
            self.FSKItems.Items[i].ListBoxItem.Height := 50
             else
            self.FSKItems.Items[i].ListBoxItem.Height := 0;
         end
          else
           self.FSKItems.Items[i].ListBoxItem.Height := 50;
       end;
   finally
    self.lbSKList.EndUpdate;
   end;

end;

procedure TViewSKList.lbSKListItemClick(const Sender: TCustomListBox;
  const Item: TListBoxItem);
begin
 inherited;

  if self.timDblClick.Tag = Item.Tag then
   begin
     self.timDblClick.Enabled := false;
     self.timDblClick.Tag := -1;

     case self.Caller of
       View.Base.SKLIST_CALLER_CHAT_RECPT: self.ConfirmSKUnitForChat;
       View.Base.SKLIST_CALLER_NEW_ORDER: self.ConfirmSKUnitForNewOrder;
     end;
   end;

  self.SelectSKUnit;

  self.timDblClick.Tag := Item.Tag;
  self.timDblClick.Enabled := true;
end;

procedure TViewSKList.RefreshData;
var
 i:integer;
 s:string;
 LParent:TListBox;
 LSKItem: TSKItem;
 LShowStates: set of byte;
 LSKUnit: TSKUnit;
begin
 try
   if not Assigned(ModelDataGlobal.SKList) then
     ModelDataGlobal.SKList := TSKList.Create;

   ModelDataGlobal.SKList.Lock;
   self.lbSKList.BeginUpdate;

   self.lbSKList.Content.DeleteChildren;

   self.FSKItems.Clear;

   try

     LParent:= self.lbSKList;

     for i:=0 to ModelDataGlobal.SKList.Items.Count - 1 do
      begin


         begin
          try
           LSKItem := TSKItem.Create;
           LSKItem.SKUnit.Assign(ModelDataGlobal.SKList.Items[i]);

           //LCarItem.Car.Lock;
            try
             self.FSKItems.Add(LParent, LSKItem, self.Caller = View.Base.SKLIST_CALLER_CHAT_RECPT);
            finally
              //LCarItem.Car.Unlock;
            end;
          except on E:Exception do
           TLogger.Save('i='+i.ToString+' '+e.Message);
          end;
         end;

      end;

   finally
     ModelDataGlobal.SKList.Unlock;
     self.lbSKList.EndUpdate;
   end;

  finally
   self.HideLoader;
  end;
end;

procedure TViewSKList.SelectSKUnit;
var
 LSKItem: TSKItem;
 b: boolean;
 LId: integer;
begin

 try

    //сначала снимаем выделение у предыдущего выделенного элемента
    if Assigned(ModelDataGlobal.CurrentSKUnit) then
     begin
      b := self.FSKItems.Items.TryGetValue(ModelDataGlobal.CurrentSKUnit.Id.ToInteger, LSKItem);
      if Assigned(LSKItem) then
       LSKItem.FRectWrapper.Fill.Color := TAlphaColorRec.Black
     end;

    //теперь выделяем текущий и загоняем в глобальное поле
    LId := -1;
    LId := self.lbSKList.Selected.Tag;

     b := self.FSKItems.Items.TryGetValue(LId, LSKItem);

     if b then
      begin
       ModelDataGlobal.CurrentSKUnit := LSKItem.SKUnit;
       LSKItem.FRectWrapper.Fill.Color := TAlphaColorRec.SaddleBrown;
      end;

 except on E:Exception do
   TLogger.Save('[TViewSKList.SelectSKUnit] err: '+e.Message);
 end;

end;

procedure TViewSKList.timDblClickTimer(Sender: TObject);
begin
  inherited;
  self.timDblClick.Enabled := false;
  self.timDblClick.Tag := -1;
end;

procedure TViewSKList.timFilterByNameTimer(Sender: TObject);
begin
  inherited;
  self.timFilterByName.Enabled := false;
  self.FilterByName;
end;

end.
