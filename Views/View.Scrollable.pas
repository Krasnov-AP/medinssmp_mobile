﻿unit View.Scrollable;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.TabControl,
  Helper.View.Input, FMX.ListBox, FMX.Edit, FMX.DateTimeCtrls, FMX.Memo.Types,
  FMX.ScrollBox, FMX.Memo, FMX.EditBox, FMX.NumberBox, System.Messaging, Core.Common;

type
  TViewScrollable = class(TViewBase)
    vsbScrollContent: TVertScrollBox;
    ltScrollableContent: TLayout;
    procedure vsbScrollContentTap(Sender: TObject; const Point: TPointF);
    procedure vsbScrollContentMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  strict protected
   FCurrentControl: TControl;
   FCurrentPoint: TPointF;
   FDeltaY: integer;
  strict private
   function GetControl(AParent:TLayout; ACoords: TPointF; AWithOffset: boolean = true): TLayout;
    { Private declarations }
  strict protected
   procedure ProcessMessages(const Sender: TObject; const M: TMessage); override;
  public
    procedure AfterShow; override;
    procedure BeforeClose; override;
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TViewScrollable.AfterShow;
begin
  inherited;
  //VKAutoShowMode := TVKAutoShowMode.Never; //сами будем показывать, а не полагаться на систему
  KeyboardHelper.CaptureClicks(self.ltScrollableContent);
  //self.ltScrollableContent.Height := self.vsbScrollContent.Height;
end;

procedure TViewScrollable.BeforeClose;
begin
  inherited;
  //VKAutoShowMode := TVKAutoShowMode.DefinedBySystem;
end;

function TViewScrollable.GetControl(AParent:TLayout; ACoords: TPointF; AWithOffset: boolean = true): TLayout;
var
 i:integer;
 LLayout: TLayout;
 LOffsetX, LOffsetY: single;
 LCoords: TPointF;
begin

 result := nil;

 if not Assigned(AParent) then exit;

 LOffsetX := 0;
 LOffsetY := 0;

 if AWithOffset then
  begin
   LOffsetX := self.vsbScrollContent.ViewportPosition.X;
   {$if defined(ANDROID) or defined(iOS)}
    LOffsetY := self.vsbScrollContent.ViewportPosition.Y - 50;
   {$else}
    LOffsetY := self.vsbScrollContent.ViewportPosition.Y;
   {$endif}
  end;

  ACoords.X := ACoords.X + LOffsetX;
  ACoords.Y := ACoords.Y + LOffsetY;

 for i := 0 to AParent.ChildrenCount - 1 do
   begin
     if (AParent.Children[i] is TLayout) then
      begin

      if (AParent.Children[i] is TLayout) then
       LLayout := AParent.Children[i] as TLayout
        else
         exit;

       if (ACoords.X >= LLayout.Position.X) and (ACoords.X <= (LLayout.Width + LLayout.Position.X) ) and
        (ACoords.Y >= LLayout.Position.Y) and (ACoords.Y <= (LLayout.Height + LLayout.Position.Y) ) then
         begin
          if AWithOffset then
            begin
             LCoords.X := ACoords.X - LLayout.Position.X;
             LCoords.Y := ACoords.Y - LLayout.Position.Y;
            end
             else
            begin
             LCoords := ACoords;
            end;

           result := self.GetControl(LLayout, LCoords, false);

           if result = nil then
            result := LLayout;

           exit;
         end;

      end;
   end;

end;

procedure TViewScrollable.ProcessMessages(const Sender: TObject;
  const M: TMessage);
var
 LMsg: string;
begin

  inherited;

  if not self.FNeedToProcessMessages then exit;

  LMsg := (M as TMessage<String>).Value;

  if Sender is THelperViewInput then
   begin
    if LMsg = 'keyboard_shown' then
     begin
      if Assigned(KeyboardHelper) then
        begin
//         self.FDeltaY := round(KeyboardHelper.BoundsRect.Height {+ 75} - self.FCurrentPoint.Y);
//         if self.FDeltaY < 0 then
//          self.vsbScrollContent.ScrollBy(0, self.FDeltaY);

         if self.FCurrentPoint.Y > KeyboardHelper.KeyboardBounds.Height + 75 then
          self.vsbScrollContent.Margins.Top := - (KeyboardHelper.KeyboardBounds.Height + 75);

         TToast.Show('Y='+self.FCurrentPoint.Y.ToString+' margin='+self.vsbScrollContent.Margins.Top.ToString);
        end;
     end
      else
    if LMsg = 'keyboard_hidden' then
     begin
//      if self.FDeltaY < 0 then
//       self.vsbScrollContent.ScrollBy(0, -self.FDeltaY);
       self.vsbScrollContent.Margins.Top := 0;
     end
   end;
end;


procedure TViewScrollable.vsbScrollContentMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
 var
 L: TLayout;
 p: TPointF;
begin
  {$if defined(MSWINDOWS) or defined(iOS)}

    try

       p.X := X;
       p.Y := Y;
       L := self.GetControl( self.ltScrollableContent, p);

       if Assigned(L.TagObject) then
        if L.TagObject <> nil then
         begin
          if (L.TagObject is TEdit) then
           KeyboardHelper.Show(L.TagObject as TEdit);
          if (L.TagObject is TMemo) then
           KeyboardHelper.Show(L.TagObject as TMemo);
         end;

    except on E:Exception do
    end;

 {$endif}
end;

procedure TViewScrollable.vsbScrollContentTap(Sender: TObject;
  const Point: TPointF);
var
 L: TLayout;
 p: TPointF;
 LEdit: TEdit;
 LMemo: TMemo;
begin
  exit;
  inherited;

  try

     p.X := Point.X;
     p.Y := Point.Y;
     self.FCurrentPoint.Y := Point.Y;
     self.FCurrentPoint.X := Point.X;
     L := self.GetControl( self.ltScrollableContent, p);

     if Assigned(L.TagObject) then
      if L.TagObject <> nil then
       begin

        if self.FCurrentControl <> nil then
         begin
           //TAppKeyboard.Hide;
           //self.FCurrentControl.CanFocus := false;
           //self.FCurrentControl.HitTest := false;
         end;

        if (L.TagObject is TEdit) then
         begin
          LEdit := L.TagObject as TEdit;
          self.FCurrentControl := LEdit;
          LEdit.CanFocus := true;
          //LEdit.HitTest := true;
          LEdit.SetFocus;
          //TAppKeyboard.Show(LEdit);
          //KeyboardHelper.Show(L.TagObject as TEdit);
         end;

        if (L.TagObject is TMemo) then
         begin
          LMemo := L.TagObject as TMemo;
          self.FCurrentControl := LMemo;
          LMemo.CanFocus := true;
          //LMemo.HitTest := true;
          LMemo.SetFocus;
          //TAppKeyboard.Show(LMemo);
          //KeyboardHelper.Show(L.TagObject as TMemo);
         end;
       end;

  except on E:Exception do
  end;

end;


end.
