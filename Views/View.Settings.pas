﻿unit View.Settings;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls, View.Base,
  FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.ListBox, Core.Common,
  Model.Global, FMX.Memo.Types, FMX.ScrollBox, FMX.Memo, Model.Cars;

type
  TViewSettings = class(TViewBase)
    Layout1: TLayout;
    Label1: TLabel;
    Layout7: TLayout;
    cbLang: TComboBox;
    imgLang: TImage;
    Layout2: TLayout;
    Label2: TLabel;
    lbUser: TLabel;
    Memo1: TMemo;
    Button1: TButton;
    timTest: TTimer;
    procedure cbLangChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure timTestTimer(Sender: TObject);
  private
    { Private declarations }
  public
   procedure AfterShow; override;
    { Public declarations }
  end;

implementation

{$R *.fmx}

procedure TViewSettings.AfterShow;
begin
  inherited;
  if ModelDataGlobal.CurrentUser.PersonalInfo <> nil then
   begin
    self.lbUser.Text := 'Пользователь: ' +
    ModelDataGlobal.CurrentUser.PersonalInfo.FirstName.Trim+' '+
    ModelDataGlobal.CurrentUser.PersonalInfo.MiddleName.Trim+' '+
    ModelDataGlobal.CurrentUser.PersonalInfo.LastName.Trim;
   end
    else
   self.lbUser.Text := 'Пользователь: нет данных';
end;

procedure TViewSettings.Button1Click(Sender: TObject);
var
 i: integer;
 s: string;
 Lcar: TCar;
begin
  inherited;
  self.Memo1.Lines.Clear;

   for i:=0 to ModelDataGlobal.Cars.Items.Count - 1 do
     begin

       Lcar := ModelDataGlobal.Cars.Items[i];

       s :=Lcar.Name+' Type='+LCar.PType+' Number='+LCar.Number+' '+LCar.Lat+' '+LCar.Lng;
       self.Memo1.Lines.Add(s);
     end;
end;

procedure TViewSettings.cbLangChange(Sender: TObject);
begin
  case self.cbLang.ItemIndex of
    0: //RU
     begin
       self.imgLang.Bitmap.LoadFromStream(imgFlagRU);
       Model.Global.ModelDataGlobal.AppSettings.Locale := 'RU';
       TLangManager.Locale := 'RU';
       self.Translate;
     end;
    1: //EN
     begin
       self.imgLang.Bitmap.LoadFromStream(imgFlagGB);
       Model.Global.ModelDataGlobal.AppSettings.Locale := 'EN';
       TLangManager.Locale := 'EN';
       self.Translate;
     end;
  end;
end;

procedure TViewSettings.timTestTimer(Sender: TObject);
begin
  exit;
  inherited;
  self.Button1.Text := 'test '+self.timTest.Tag.ToString;
  self.timTest.Tag := self.timTest.Tag + 1;
end;

end.
