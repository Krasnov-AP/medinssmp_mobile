﻿unit View.Survey;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  View.Base, FMX.Controls.Presentation, FMX.Objects, FMX.Layouts, FMX.Edit,
  FMX.EditBox, FMX.NumberBox, Model.Global;

type
  TViewSurvey = class(TViewBase)
    Button2: TButton;
    btnOK: TButton;
    Label1: TLabel;
    Label2: TLabel;
    edMins: TNumberBox;
    Label3: TLabel;
    procedure btnOKClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AfterShow; override;
  end;

implementation

{$R *.fmx}

{ TViewSurvey }

procedure TViewSurvey.AfterShow;
begin
  inherited;
  self.edMins.SetFocus;
end;

procedure TViewSurvey.btnOKClick(Sender: TObject);
begin
  inherited;
  ModelDataGlobal.SurveyValue := Round(self.edMins.Value);
  self.SendMessage('confirm_survey');
  self.HideFilter;
end;

procedure TViewSurvey.Button2Click(Sender: TObject);
begin
  inherited;
  self.HideFilter;
end;

end.
