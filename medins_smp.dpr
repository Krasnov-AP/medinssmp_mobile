﻿program medins_smp;

{$R *.dres}

uses
  System.StartUpCopy,
  FMX.Forms,
  FMX.Types,
  System.SysUtils,
  Form.Main in 'Forms\Form.Main.pas' {frmMain},
  View.Base in 'Views\View.Base.pas' {ViewBase: TFrame},
  Form.Modal in 'Forms\Form.Modal.pas' {FormModal},
  View.Auth in 'Views\View.Auth.pas' {ViewAuth: TFrame},
  Core.Common in 'Core\Core.Common.pas',
  Model.User in 'Models\Model.User.pas',
  Model.AnyData in 'Models\Model.AnyData.pas',
  Controller.AnyData in 'Controllers\Controller.AnyData.pas',
  Controller.Http in 'Controllers\Controller.Http.pas',
  Model.Global in 'Models\Model.Global.pas',
  Controller.Auth in 'Controllers\Controller.Auth.pas',
  Model.User.GeoData in 'Models\Model.User.GeoData.pas',
  IdWebSocketSimpleClient in '3dparty\IdWebSocketSimpleClient.pas',
  Model.SMPService in 'Models\Model.SMPService.pas',
  Controller.SMPService in 'Controllers\Controller.SMPService.pas',
  XSuperJSON in '3dparty\XSuperObject\XSuperJSON.pas',
  XSuperObject in '3dparty\XSuperObject\XSuperObject.pas',
  ElAES in '3dparty\ElAES.pas',
  Core.Crypt in 'Core\Core.Crypt.pas',
  Model.AppSettings in 'Models\Model.AppSettings.pas',
  data.main in 'DataModules\data.main.pas' {DataMain: TDataModule},
  Core.ErrorReporting in 'Core\Core.ErrorReporting.pas',
  Form.ShowMessage in 'Forms\Form.ShowMessage.pas' {frmMessage},
  Model.LangConsts in 'Models\Model.LangConsts.pas',
  View.Map in 'Views\View.Map.pas' {ViewMap: TFrame},
  Form.Base in 'Forms\Form.Base.pas' {FormBase},
  View.Settings in 'Views\View.Settings.pas' {Frame1: TFrame},
  Model.ViewParams in 'Models\Model.ViewParams.pas',
  Core.Funcs in 'Core\Core.Funcs.pas',
  View.Orders in 'Views\View.Orders.pas' {Frame2: TFrame},
  View.NewOrder in 'Views\View.NewOrder.pas' {ViewNewOrder: TFrame},
  Helper.View.Input in 'Helpers\Helper.View.Input.pas' {HelperViewInput: TFrame},
  Helper.Yandex.Adr.Autocomplete in 'Helpers\Helper.Yandex.Adr.Autocomplete.pas' {YandexAdrAutocomplete: TFrame},
  Model.Yandex.Geocode in 'Models\Model.Yandex.Geocode.pas',
  Controller.Yandex.Geocode in 'Controllers\Controller.Yandex.Geocode.pas',
  View.Scrollable in 'Views\View.Scrollable.pas' {ViewScrollable: TFrame},
  Model.PersonalInfo in 'Models\Model.PersonalInfo.pas',
  Controller.PersonalInfo in 'Controllers\Controller.PersonalInfo.pas',
  Controller.Orders in 'Controllers\Controller.Orders.pas',
  Model.Orders in 'Models\Model.Orders.pas',
  SG.ScriptGate in '3dparty\ScriptGate\SG.ScriptGate.pas',
  Controller.TCPClient in 'Controllers\Controller.TCPClient.pas',
  View.OrderDetail in 'Views\View.OrderDetail.pas' {ViewOrderDetail: TFrame},
  Core.WebConsole in 'Core\Core.WebConsole.pas',
  Model.Cars in 'Models\Model.Cars.pas',
  Controller.Cars in 'Controllers\Controller.Cars.pas',
  View.Cars in 'Views\View.Cars.pas' {ViewCars: TFrame},
  FMX.Consts in 'Core\FMX.Consts.pas',
  View.Filter.Cars in 'Views\Filters\View.Filter.Cars.pas' {ViewFilterCars: TFrame},
  View.Orders.Archive in 'Views\View.Orders.Archive.pas' {ViewOrdersArchive: TFrame},
  View.Filter.Archive in 'Views\Filters\View.Filter.Archive.pas' {ViewFilterOrdersArchive: TFrame},
  Model.Filter.Archive in 'Models\Filters\Model.Filter.Archive.pas',
  Controller.Orders.Archive in 'Controllers\Controller.Orders.Archive.pas',
  View.Order.Archive.Detail in 'Views\View.Order.Archive.Detail.pas' {ViewOrderArchiveDetail: TFrame},
  Form.Log in 'Forms\Form.Log.pas' {frmLog},
  View.Chat in 'Views\View.Chat.pas' {ViewChat: TFrame},
  Helper.Keyboard in 'Helpers\Helper.Keyboard.pas',
  View.Adaptive in 'Views\View.Adaptive.pas' {ViewAdaptive: TFrame},
  View.NewOrder2 in 'Views\View.NewOrder2.pas' {ViewNewOrder2: TFrame},
  Form.NewOrder in 'Forms\Form.NewOrder.pas' {FormNewOrder},
  Model.SkList in 'Models\Model.SkList.pas',
  Controller.SKList in 'Controllers\Controller.SKList.pas',
  View.SKList in 'Views\View.SKList.pas' {ViewSKList: TFrame},
  View.ChatRecpt in 'Views\View.ChatRecpt.pas' {ViewChatRecpt: TFrame},
  Model.Chat in 'Models\Model.Chat.pas',
  Controller.Chat in 'Controllers\Controller.Chat.pas',
  Helper.Autocomplete in 'Helpers\Helper.Autocomplete.pas' {Autocomplete: TFrame},
  Helper.SKAutocomplete in 'Helpers\Helper.SKAutocomplete.pas' {SKAutocomplete: TFrame},
  Model.Hospital in 'Models\Model.Hospital.pas',
  Controller.Hospital in 'Controllers\Controller.Hospital.pas',
  View.Hospitals in 'Views\View.Hospitals.pas' {ViewHospitals: TFrame},
  Helper.Hospitals.Autocomplete in 'Helpers\Helper.Hospitals.Autocomplete.pas' {HospitalsAutocomplete: TFrame},
  View.OrderLog in 'Views\View.OrderLog.pas' {ViewOrderLog: TFrame},
  View.Survey in 'Views\View.Survey.pas' {ViewSurvey: TFrame},
  Controller.Survey in 'Controllers\Controller.Survey.pas',
  DW.Android.Helpers in '3dparty\KastriFree\Core\DW.Android.Helpers.pas';

{$R *.res}

begin

  Application.Initialize;
  //VKAutoShowMode := TVKAutoShowMode.Never; //сами будем показывать, а не полагаться на систему
  TLogger.Save('[Application.CreateForm(TDataMain, DataMain)] before');
  //здесь грузим настройки и поддержку мультиязычности, поэтому первая
  Application.CreateForm(TDataMain, DataMain);
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmLog, frmLog);
  Application.CreateForm(TFormModal, FormModal);
  //Application.CreateForm(TFormNewOrder, FormNewOrder);
  Application.MainForm := frmMain;

  Application.Run;
end.






















































